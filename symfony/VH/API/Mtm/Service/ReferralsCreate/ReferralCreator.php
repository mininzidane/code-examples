<?php
/**
 * ReferralCreator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Api\Mtm\Service\ReferralsCreate;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use VirtualHealth\Episode\Service\EpisodeIdGenerator;
use VirtualHealth\OrmBundle\Entity\CareplanProblemReference;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\Episode;
use VirtualHealth\OrmBundle\Entity\EpisodeReferral;
use VirtualHealth\OrmBundle\Entity\User;
use VirtualHealth\OrmBundle\Repository\CareplanProblemReferenceRepository;
use VirtualHealth\OrmBundle\Repository\EpisodeReferralRepository;
use VirtualHealth\OrmBundle\Repository\EpisodeRepository;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class ReferralCreator
 *
 * @package VirtualHealth\Api\Mtm\Service\ReferralsCreate
 */
class ReferralCreator
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EpisodeRepository
     */
    private $episodeRepository;

    /**
     * @var CareplanProblemReferenceRepository
     */
    private $carePlanProblemReferenceRepository;

    /**
     * @var EpisodeReferralRepository
     */
    private $episodeReferralRepository;

    /**
     * @var EpisodeIdGenerator
     */
    private $episodeIdGenerator;

    /**
     * @var CommonUsersGetter
     */
    private $commonUsersGetter;

    /**
     * @var string|null
     */
    private $error;

    /**
     * @var int|null
     */
    private $errorCode;

    /**
     * @var array
     */
    private $data = [];

    /**
     * ReferralCreator constructor.
     *
     * @param Connection $connection
     * @param EpisodeRepository $episodeRepository
     * @param CareplanProblemReferenceRepository $carePlanProblemReferenceRepository
     * @param EpisodeReferralRepository $episodeReferralRepository
     * @param EpisodeIdGenerator $episodeIdGenerator
     * @param CommonUsersGetter $commonUsersGetter
     */
    public function __construct(
        Connection $connection,
        EpisodeRepository $episodeRepository,
        CareplanProblemReferenceRepository $carePlanProblemReferenceRepository,
        EpisodeReferralRepository $episodeReferralRepository,
        EpisodeIdGenerator $episodeIdGenerator,
        CommonUsersGetter $commonUsersGetter
    ) {
        $this->connection = $connection;
        $this->episodeRepository = $episodeRepository;
        $this->carePlanProblemReferenceRepository = $carePlanProblemReferenceRepository;
        $this->episodeReferralRepository = $episodeReferralRepository;
        $this->episodeIdGenerator = $episodeIdGenerator;
        $this->commonUsersGetter = $commonUsersGetter;
    }

    /**
     * @param Client $client
     * @param string $episodeType
     * @param string $referralSource
     * @param string $referralType
     * @param string|null $referralSubType
     * @param string|null $referralDescription
     *
     * @return bool
     *
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(
        Client $client,
        string $episodeType,
        string $referralSource,
        string $referralType,
        ?string $referralSubType = null,
        ?string $referralDescription = null
    ): bool {
        $this->error = null;
        $this->errorCode = null;
        /** @var Episode|null $episode */
        $episode = $this->episodeRepository->getLastEpisode((int) $client->getUserId(), null, null, EpisodeRepository::OPEN_STATE_OPENED);
        $this->connection->beginTransaction();

        try {
            if ($episode !== null) {
                /** @var CareplanProblemReference $episodeTypeReference */
                $episodeTypeReference = $episode->getEpisodeType();

                if ($episodeTypeReference->getValue() === $episodeType) {
                    $referralExists = false;
                    /** @var EpisodeReferral $referral */
                    // IF this open Episode does NOT already contain a matching Referral (same Source, Type and Subtype)
                    foreach ($episode->getReferrals() as $referral) {
                        $modelReferralSource = $referral->getSource() ? $referral->getSource()->getValue() : null;
                        $modelReferralType = $referral->getType() ? $referral->getType()->getValue() : null;
                        /** @var CareplanProblemReference $subType */
                        $subTypes = [];

                        foreach ($referral->getSubTypes() as $subType) {
                            $subTypes[] = $subType->getValue();
                        }

                        if ($referralSource === $modelReferralSource && $referralType === $modelReferralType && \in_array($referralSubType, $subTypes, true)) {
                            $referralExists = true;
                            break;
                        }
                    }

                    if ($referralExists && isset($referral)) {
                        $this->data = [
                            'referralId' => $referral->getId(),
                        ];
                    } else {
                        $this->createReferral($episode, $client->getUser(), $referralSource, $referralType, $referralSubType, $referralDescription);
                    }
                }
            } else {
                $episode = (new Episode())
                    ->setStatus($this->carePlanProblemReferenceRepository->findEpisodeStatusByName(Episode::STATUS_PENDING))
                    ->setEpisodeId($this->episodeIdGenerator->createEpisodeId(\date('Y-m-d H:i:s')))
                    ->setUser($this->commonUsersGetter->getSystemSuperAdmin())
                    ->setClient($client);
                $type = $this->carePlanProblemReferenceRepository->findEpisodeTypeByName($episodeType);

                if ($type !== null) {
                    $episode->setEpisodeType($type);
                }
                $this->episodeRepository->save($episode);
                $this->createReferral($episode, $client->getUser(), $referralSource, $referralType, $referralSubType, $referralDescription);
            }
            $this->connection->commit();
        } catch (\Throwable $e) {
            if ($this->connection->isTransactionActive()) {
                $this->connection->rollBack();
            }
            $this->error = $e->getMessage();
            $this->errorCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

            return false;
        }

        return true;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @return int|null
     */
    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param Episode $episode
     * @param User $user
     * @param string $referralSource
     * @param string $referralType
     * @param string|null $referralSubType
     * @param string|null $referralDescription
     *
     * @throws \Doctrine\ORM\ORMException
     */
    private function createReferral(
        Episode $episode,
        User $user,
        string $referralSource,
        string $referralType,
        ?string $referralSubType = null,
        ?string $referralDescription = null
    ): void {
        $episodeReferral = (new EpisodeReferral())
            ->setEpisode($episode)
            ->setUser($user)
            ->setDescription((string) $referralDescription);
        $source = $this->carePlanProblemReferenceRepository->findEpisodeReferralSourceByName($referralSource);

        if ($source !== null) {
            $episodeReferral->setSource($source);
        }
        $type = $this->carePlanProblemReferenceRepository->findEpisodeReferralTypeByName($referralType);

        if ($type !== null) {
            $episodeReferral->setType($type);
        }

        if ($referralSubType) {
            $episodeReferralSubType = $this->carePlanProblemReferenceRepository->findEpisodeReferralSubTypeByName($referralSubType);

            if ($episodeReferralSubType !== null) {
                $episodeReferral->addSubType($episodeReferralSubType);
            }
        }
        $this->episodeReferralRepository->save($episodeReferral);
        $this->data = [
            'referralId' => $episodeReferral->getId(),
        ];
    }
}
