<?php
/**
 * RequestValidator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Api\Mtm\Service\ReferralsCreate;

use VirtualHealth\OrmBundle\Entity\CareplanProblemReference;
use VirtualHealth\OrmBundle\Repository\CareplanProblemReferenceRepository;

/**
 * Class RequestValidator
 * @package VirtualHealth\Api\Mtm\Service\ReferralsCreate
 */
class RequestValidator
{
    public const TYPE_SUBSCRIBER = 'Subscriber';
    public const TYPE_SEQ_MEMBER = 'SeqMember';
    public const TYPE_MEDICAID = 'Medicaid';
    public const TYPE_MEDICARE = 'Medicare';

    /**
     * @var CareplanProblemReferenceRepository
     */
    private $carePlanProblemReferenceRepository;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * RequestValidator constructor.
     * @param CareplanProblemReferenceRepository $carePlanProblemReferenceRepository
     */
    public function __construct(CareplanProblemReferenceRepository $carePlanProblemReferenceRepository)
    {
        $this->carePlanProblemReferenceRepository = $carePlanProblemReferenceRepository;
    }

    /**
     * @param string|null $transactionId
     * @param string|null $memberId
     * @param string|null $memberIdType
     * @param string|null $episodeType
     * @param string|null $referralSource
     * @param string|null $referralType
     * @param string|null $referralSubType
     * @return bool
     */
    public function validate(
        ?string $transactionId,
        ?string $memberId,
        ?string $memberIdType,
        ?string $episodeType,
        ?string $referralSource,
        ?string $referralType,
        ?string $referralSubType = null
    ): bool
    {
        $this->errors = [];
        if (!$transactionId) {
            $this->addError('TransactionID');
        }
        if (!$memberId) {
            $this->addError('MemberID');
        }
        if (!\in_array($memberIdType, [
            self::TYPE_SUBSCRIBER,
            self::TYPE_SEQ_MEMBER,
            self::TYPE_MEDICAID,
            self::TYPE_MEDICARE,
        ], true)) {
            $this->addError('MemberIDType');
        }
        if (!$episodeType) {
            $this->addError('EpisodeType');
        }
        if (!$referralSource) {
            $this->addError('ReferralSource');
        }
        if (!$referralType) {
            $this->addError('ReferralType');
        }

        if ($episodeType && !$this->checkReferenceValueExists($episodeType, CareplanProblemReference::TYPE_EPISODE_TYPE)) {
            $this->addError('EpisodeType');
        }
        if ($referralSource && !$this->checkReferenceValueExists($referralSource, CareplanProblemReference::TYPE_EPISODE_REFERRAL_SOURCE)) {
            $this->addError('ReferralSource');
        }
        if ($referralType && !$this->checkReferenceValueExists($referralType, CareplanProblemReference::TYPE_EPISODE_REFERRAL_TYPE)) {
            $this->addError('ReferralType');
        }
        if ($referralSubType && !$this->checkReferenceValueExists($referralSubType, CareplanProblemReference::TYPE_EPISODE_REFERRAL_SUB_SET)) {
            $this->addError('ReferralSubtype');
        }

        return empty($this->errors);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string|null $value
     * @param string $referenceType
     * @return bool
     */
    private function checkReferenceValueExists(?string $value, string $referenceType): bool
    {
        if (!$value) {
            return false;
        }

        $entities = $this->carePlanProblemReferenceRepository->findByType($referenceType);
        foreach ($entities as $carePlanProblemReference) {
            if ($carePlanProblemReference->getValue() === $value) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $fieldName
     * @return void
     */
    private function addError(string $fieldName): void
    {
        $this->errors[] = "Invalid {$fieldName}";
    }
}