<?php
/**
 * ClientSearch class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Api\Mtm\Service;

use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Repository\ClientInsuranceCommercialRepository;
use VirtualHealth\OrmBundle\Repository\ClientInsuranceMedicaidFutureRepository;
use VirtualHealth\OrmBundle\Repository\ClientInsuranceMedicaidRepository;
use VirtualHealth\OrmBundle\Repository\ClientInsuranceMedicareFutureRepository;
use VirtualHealth\OrmBundle\Repository\ClientInsuranceMedicareRepository;

/**
 * Class ClientSearch
 * @package VirtualHealth\Api\Mtm\Service
 */
class ClientSearch
{
    public const TYPE_SUBSCRIBER = 'Subscriber';
    public const TYPE_SEQ_MEMBER = 'SeqMember';
    public const TYPE_MEDICAID = 'Medicaid';
    public const TYPE_MEDICARE = 'Medicare';

    /**
     * @var ClientInsuranceMedicaidRepository
     */
    private $clientInsuranceMedicaidRepository;

    /**
     * @var ClientInsuranceMedicareRepository
     */
    private $clientInsuranceMedicareRepository;

    /**
     * @var ClientInsuranceCommercialRepository
     */
    private $clientInsuranceCommercialRepository;

    /**
     * @var ClientInsuranceMedicaidFutureRepository
     */
    private $clientInsuranceMedicaidFutureRepository;

    /**
     * @var ClientInsuranceMedicareFutureRepository
     */
    private $clientInsuranceMedicareFutureRepository;

    /**
     * ClientSearch constructor.
     * @param ClientInsuranceMedicaidRepository $clientInsuranceMedicaidRepository
     * @param ClientInsuranceMedicareRepository $clientInsuranceMedicareRepository
     * @param ClientInsuranceCommercialRepository $clientInsuranceCommercialRepository
     * @param ClientInsuranceMedicaidFutureRepository $clientInsuranceMedicaidFutureRepository
     * @param ClientInsuranceMedicareFutureRepository $clientInsuranceMedicareFutureRepository
     */
    public function __construct(
        ClientInsuranceMedicaidRepository $clientInsuranceMedicaidRepository,
        ClientInsuranceMedicareRepository $clientInsuranceMedicareRepository,
        ClientInsuranceCommercialRepository $clientInsuranceCommercialRepository,
        ClientInsuranceMedicaidFutureRepository $clientInsuranceMedicaidFutureRepository,
        ClientInsuranceMedicareFutureRepository $clientInsuranceMedicareFutureRepository
    )
    {
        $this->clientInsuranceMedicaidRepository = $clientInsuranceMedicaidRepository;
        $this->clientInsuranceMedicareRepository = $clientInsuranceMedicareRepository;
        $this->clientInsuranceCommercialRepository = $clientInsuranceCommercialRepository;
        $this->clientInsuranceMedicaidFutureRepository = $clientInsuranceMedicaidFutureRepository;
        $this->clientInsuranceMedicareFutureRepository = $clientInsuranceMedicareFutureRepository;
    }

    /**
     * @param string $type
     * @param string $memberId
     * @return Client|null
     */
    public function findClient(string $type, string $memberId): ?Client
    {
        $client = null;
        switch ($type) {
            case self::TYPE_SUBSCRIBER:
                $insurance = $this->clientInsuranceMedicaidRepository->findOneBy([
                    'subscriberId' => $memberId
                ]);
                if ($insurance === null) {
                    $insurance = $this->clientInsuranceMedicareRepository->findOneBy([
                        'subscriberId' => $memberId,
                    ]);
                }
                if ($insurance === null) {
                    $insurance = $this->clientInsuranceCommercialRepository->findOneBy([
                        'subscriberId' => $memberId,
                    ]);
                }
                if ($insurance === null) {
                    $insurance = $this->clientInsuranceMedicaidFutureRepository->findOneBy([
                        'subscriberId' => $memberId,
                    ]);
                }
                if ($insurance === null) {
                    $insurance = $this->clientInsuranceMedicareFutureRepository->findOneBy([
                        'subscriberId' => $memberId,
                    ]);
                }
                if ($insurance === null) {
                    return null;
                }
                $client = $insurance->getClient();
                break;

            case self::TYPE_SEQ_MEMBER:
                $insurance = $this->clientInsuranceMedicaidRepository->findOneBy([
                    'medicaidSequentialMemberNumber' => $memberId
                ]);
                if ($insurance === null) {
                    $insurance = $this->clientInsuranceMedicareRepository->findOneBy([
                        'medicareSequentialMemberNumber' => $memberId,
                    ]);
                }
                if ($insurance === null) {
                    $insurance = $this->clientInsuranceMedicaidFutureRepository->findOneBy([
                        'medicaidSequentialMemberNumber' => $memberId,
                    ]);
                }
                if ($insurance === null) {
                    $insurance = $this->clientInsuranceMedicareFutureRepository->findOneBy([
                        'medicareSequentialMemberNumber' => $memberId,
                    ]);
                }
                if ($insurance === null) {
                    return null;
                }
                $client = $insurance->getClient();
                break;

            case self::TYPE_MEDICAID:
                $insurance = $this->clientInsuranceMedicaidRepository->findOneBy([
                    'number' => $memberId
                ]);
                if ($insurance === null) {
                    $insurance = $this->clientInsuranceMedicaidFutureRepository->findOneBy([
                        'number' => $memberId
                    ]);
                }
                if ($insurance === null) {
                    return null;
                }
                $client = $insurance->getClient();
                break;

            case self::TYPE_MEDICARE:
                $insurance = $this->clientInsuranceMedicareRepository->findOneBy([
                    'number' => $memberId
                ]);
                if ($insurance === null) {
                    $insurance = $this->clientInsuranceMedicareFutureRepository->findOneBy([
                        'number' => $memberId
                    ]);
                }
                if ($insurance === null) {
                    return null;
                }
                $client = $insurance->getClient();
                break;
            default:
                break;
        }

        return $client;
    }
}
