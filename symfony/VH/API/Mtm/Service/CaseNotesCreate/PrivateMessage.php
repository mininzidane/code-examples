<?php
/**
 * PrivateMessage class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Api\Mtm\Service\CaseNotesCreate;

use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Query\Client\GetPrimaryCareTeamMember;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class PrivateMessage
 * @package VirtualHealth\Api\Mtm\Service\CaseNotesCreate
 */
class PrivateMessage
{
    /**
     * @var GetPrimaryCareTeamMember
     */
    private $getPrimaryCareTeamMember;

    /**
     * @var CommonUsersGetter
     */
    private $commonUsersGetter;

    /**
     * PrivateMessage constructor.
     * @param GetPrimaryCareTeamMember $getPrimaryCareTeamMember
     * @param CommonUsersGetter $commonUsersGetter
     */
    public function __construct(
        GetPrimaryCareTeamMember $getPrimaryCareTeamMember,
        CommonUsersGetter $commonUsersGetter
    )
    {
        $this->getPrimaryCareTeamMember = $getPrimaryCareTeamMember;
        $this->commonUsersGetter = $commonUsersGetter;
    }

    /**
     * @param Client $client
     * @param string $sourceType
     * @throws \CHttpException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function sendMessage(Client $client, string $sourceType)
    {
        if ($primaryMember = $this->getPrimaryCareTeamMember->read($client->getUser()->getId())) {
            /** @var \PmModule $pm */
            $pm = mod('pm');
            $pm::sendMessage(
                $this->commonUsersGetter->getSystemSuperAdmin()->getId(),
                $primaryMember->getMemberId(),
                \t('Message', 'New External Case Note for Client {name}', ['{name}' => $client->getFullName()]),
                \t(
                    'Message',
                    'New {source} case note has been added for Client {name}',
                    ['{source}' => $sourceType, '{name}' => $client->getFullName()]
                )
            );
        }
    }
}
