<?php
/**
 * RequestValidator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Api\Mtm\Service\CaseNotesCreate;

/**
 * Class RequestValidator
 * @package VirtualHealth\Api\Mtm\Service\CaseNotesCreate
 */
class RequestValidator
{
    public const TYPE_SUBSCRIBER = 'Subscriber';
    public const TYPE_SEQ_MEMBER = 'SeqMember';
    public const TYPE_MEDICAID = 'Medicaid';
    public const TYPE_MEDICARE = 'Medicare';

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @param null|string $memberId
     * @param null|string $memberIdType
     * @param null|string $caseNoteSource
     * @param null|string $note
     * @param null|string $transactionId
     * @param null|string $notification
     * @return bool
     */
    public function validate(
        ?string $memberId,
        ?string $memberIdType,
        ?string $caseNoteSource,
        ?string $note,
        ?string $transactionId,
        ?string $notification = null
    ): bool
    {
        $this->errors = [];
        if (!$memberId) {
            $this->addError('MemberID');
        }

        if (!\in_array($memberIdType, [
            self::TYPE_SUBSCRIBER,
            self::TYPE_SEQ_MEMBER,
            self::TYPE_MEDICAID,
            self::TYPE_MEDICARE,
        ], true)) {
            $this->addError('MemberIDType');
        }

        if (!\in_array($caseNoteSource, \ClientMemo::TYPES, true)) {
            $this->addError('CaseNoteSource');
        }

        if (!$note) {
            $this->addError('Note');
        }

        if (!$transactionId) {
            $this->addError('TransactionID');
        }

        if ($notification && $notification != 1) {
            $this->addError('Notification');
        }

        return empty($this->errors);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string $fieldName
     * @return void
     */
    private function addError(string $fieldName): void
    {
        $this->errors[] = "Invalid {$fieldName}";
    }
}
