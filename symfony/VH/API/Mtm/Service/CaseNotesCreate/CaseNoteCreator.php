<?php
/**
 * CaseNoteCreator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Api\Mtm\Service\CaseNotesCreate;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\ClientMemo;
use VirtualHealth\User\CommonUsersGetter;
use VirtualHealth\OrmBundle\Repository\ClientMemoRepository;

/**
 * Class CaseNoteCreator
 * @package VirtualHealth\Api\Mtm\Service\CaseNotesCreate
 */
class CaseNoteCreator
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ClientMemoRepository
     */
    private $clientMemoRepository;

    /**
     * @var CommonUsersGetter
     */
    private $commonUsersGetter;

    /**
     * @var PrivateMessage
     */
    private $privateMessage;

    /**
     * @var string|null
     */
    private $error;

    /**
     * @var int|null
     */
    private $errorCode;

    /**
     * @var array
     */
    private $data = [];

    /**
     * CaseNoteCreator constructor.
     * @param Connection $connection
     * @param CommonUsersGetter $commonUsersGetter
     * @param ClientMemoRepository $clientMemoRepository
     * @param PrivateMessage $privateMessage
     */
    public function __construct(
        Connection $connection,
        CommonUsersGetter $commonUsersGetter,
        ClientMemoRepository $clientMemoRepository,
        PrivateMessage $privateMessage
    )
    {
        $this->connection = $connection;
        $this->commonUsersGetter = $commonUsersGetter;
        $this->clientMemoRepository = $clientMemoRepository;
        $this->privateMessage = $privateMessage;
    }

    /**
     * @param Client $client
     * @param string $memo
     * @param string $sourceType
     * @param null|string $notification
     * @return bool
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(
        Client $client,
        string $memo,
        string $sourceType,
        ?string $notification = null
    ): bool {
        $this->error = null;
        $this->errorCode = null;
        $this->connection->beginTransaction();

        try {
            if ($this->checkDuplicate($client, $sourceType, $memo)) {
                $this->error = 'Duplicate note.';
                $this->errorCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
                return false;
            }

            $caseNote = (new ClientMemo())
                ->setMemo($memo)
                ->setClient($client)
                ->setSourceType($sourceType)
                ->setOwner($this->commonUsersGetter->getSystemSuperAdmin());
            $this->clientMemoRepository->save($caseNote);

            $this->data = [
                'caseNoteId' => $caseNote->getId(),
            ];

            if ($notification) {
                $this->privateMessage->sendMessage($client, $sourceType);
            }

            $this->connection->commit();
        } catch (\Throwable $e) {
            if ($this->connection->isTransactionActive()) {
                $this->connection->rollBack();
            }
            $this->error = $e->getMessage();
            $this->errorCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

            return false;
        }

        return true;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @return int|null
     */
    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param Client $client
     * @param string $sourceType
     * @param string $memo
     * @return bool
     */
    private function checkDuplicate(Client $client, string $sourceType, string $memo): bool
    {
        /** @var \Doctrine\DBAL\Driver\Statement $duplicateCaseNote */
        $duplicateCaseNote = $this->connection->createQueryBuilder()
            ->select('m.id')
            ->from('client_memo m')
            ->where('m.client_id = :client')
            ->andWhere('m.source_type = :sourceType')
            ->andWhere('DATE_FORMAT(m.created_date, "%Y-%m-%d") = :todayDate')
            ->andWhere('m.memo = :memo')
            ->setParameters([
                ':client' => $client->getUser()->getId(),
                ':sourceType' => $sourceType,
                ':todayDate' => (new \DateTime())->format('Y-m-d'),
                ':memo' => $memo
            ])->execute();

        return (bool)$duplicateCaseNote->fetchColumn();
    }
}
