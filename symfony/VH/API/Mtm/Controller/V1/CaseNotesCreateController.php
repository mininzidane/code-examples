<?php
/**
 * CaseNotesCreateController class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Api\Mtm\Controller\V1;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use VirtualHealth\Api\Auth\UserToken\TokenCheck;
use VirtualHealth\Api\Mtm\Service\CaseNotesCreate\CaseNoteCreator;
use VirtualHealth\Api\Mtm\Service\CaseNotesCreate\RequestValidator;
use VirtualHealth\Api\Mtm\Service\ClientSearch;
use VirtualHealth\OrmBundle\Entity\CaseNotesApi;
use VirtualHealth\OrmBundle\Repository\CaseNotesApiRepository;
use Psr\Log\LoggerInterface;

/**
 * Class CaseNotesCreateController
 * @package VirtualHealth\Api\Mtm\Controller\V1
 * @Rest\Version("v1")
 * @Rest\Prefix("{version}")
 */
class CaseNotesCreateController extends BaseController
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var TokenCheck
     */
    private $tokenCheck;

    /**
     * @var RequestValidator
     */
    private $requestValidator;

    /**
     * @var CaseNoteCreator;
     */
    private $caseNoteCreator;

    /**
     * @var ClientSearch
     */
    private $clientSearch;

    /**
     * @var CaseNotesApiRepository
     */
    private $caseNotesApiRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CaseNotesCreateController constructor.
     * @param Connection $connection
     * @param TokenCheck $tokenCheck
     * @param RequestValidator $requestValidator
     * @param ClientSearch $clientSearch
     * @param CaseNoteCreator $caseNoteCreator
     * @param CaseNotesApiRepository $caseNotesApiRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        Connection $connection,
        TokenCheck $tokenCheck,
        RequestValidator $requestValidator,
        ClientSearch $clientSearch,
        CaseNoteCreator $caseNoteCreator,
        CaseNotesApiRepository $caseNotesApiRepository,
        LoggerInterface $logger
    ) {
        $this->connection = $connection;
        $this->tokenCheck = $tokenCheck;
        $this->requestValidator = $requestValidator;
        $this->clientSearch = $clientSearch;
        $this->caseNoteCreator = $caseNoteCreator;
        $this->caseNotesApiRepository = $caseNotesApiRepository;
        $this->logger = $logger;
    }

    /**
     * @Rest\Post("/casenotes/create", name="mtm_case_notes_create")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function postAction(Request $request): JsonResponse
    {
        $this->connection->beginTransaction();
        $result = $this->processRequest($request);

        $caseNoteApi = (new CaseNotesApi())
            ->setTransactionId($result['transactionID'] ?? '')
            ->setCallBody($result['rawCall'] ?? '');

        try {
            $this->caseNotesApiRepository->save($caseNoteApi);
            $this->connection->commit();
        } catch (\Throwable $e) {
            if ($this->connection->isTransactionActive()) {
                $this->connection->rollBack();
            }
            $this->logger->error($e->getMessage());
            return $this->error(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

        if (isset($result['error'])) {
            [$code, $text] = $result['error'];
            return $this->error($code, $text);
        } else {
            return $this->success($result['data']);
        }
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Doctrine\DBAL\ConnectionException
     */
    private function processRequest(Request $request): array
    {
        if (!$this->tokenCheck->check()) {
            return ['error' => [JsonResponse::HTTP_UNAUTHORIZED, (string) $this->tokenCheck->getErrorLabel()]];
        }

        try {
            $json = $request->getContent();
            $data = \GuzzleHttp\json_decode($json, true);
        } catch (\Throwable $e) {
            return ['error' => [JsonResponse::HTTP_BAD_REQUEST, 'Request is invalid']];
        }

        $memberId = $data['MemberID'] ?? null;
        $memberIDType = $data['MemberIDType'] ?? null;
        $source = $data['Source'] ?? null;
        $note = $data['Note'] ?? null;
        $notification = $data['Notification'] ?? null;
        $transactionId = $data['TransactionID'] ?? null;

        if (!$this->requestValidator->validate($memberId, $memberIDType, $source, $note, $transactionId, $notification)) {
            return ['error' => [JsonResponse::HTTP_BAD_REQUEST, \implode('; ', $this->requestValidator->getErrors())]];
        }

        $client = $this->clientSearch->findClient($memberIDType, $memberId);
        if ($client === null) {
            return ['error' => [JsonResponse::HTTP_NOT_FOUND, 'Client not found']];
        }

        if (!$this->caseNoteCreator->create($client, $note, $source, $notification)) {
            $this->logger->error((string) $this->caseNoteCreator->getError());
            return ['error' => [(int) $this->caseNoteCreator->getErrorCode(), (string) $this->caseNoteCreator->getError()]];
        }

        return [
            'transactionID' => $transactionId,
            'rawCall' => $json,
            'data' => $this->caseNoteCreator->getData()
        ];
    }
}
