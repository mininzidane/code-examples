<?php
/**
 * ReferralsCreateController class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Api\Mtm\Controller\V1;

use Doctrine\DBAL\Connection;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use VirtualHealth\Api\Auth\UserToken\TokenCheck;
use VirtualHealth\Api\Mtm\Service\ClientSearch;
use VirtualHealth\Api\Mtm\Service\ReferralsCreate\ReferralCreator;
use VirtualHealth\Api\Mtm\Service\ReferralsCreate\RequestValidator;
use VirtualHealth\OrmBundle\Entity\ReferralsApi;
use VirtualHealth\OrmBundle\Repository\ReferralsApiRepository;

/**
 * Class ReferralsCreateController
 * @package VirtualHealth\Api\Mtm\Controller\V1
 * @Rest\Version("v1")
 * @Rest\Prefix("{version}")
 */
class ReferralsCreateController extends BaseController
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var RequestValidator
     */
    private $requestValidator;

    /**
     * @var ClientSearch
     */
    private $clientSearch;

    /**
     * @var ReferralCreator
     */
    private $referralCreator;

    /**
     * @var ReferralsApiRepository
     */
    private $referralsApiRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TokenCheck
     */
    private $tokenCheck;

    /**
     * ReferralsCreateController constructor.
     * @param Connection $connection
     * @param RequestValidator $requestValidator
     * @param ClientSearch $clientSearch
     * @param ReferralCreator $referralCreator
     * @param ReferralsApiRepository $referralsApiRepository
     * @param LoggerInterface $logger
     * @param TokenCheck $tokenCheck
     */
    public function __construct(
        Connection $connection,
        RequestValidator $requestValidator,
        ClientSearch $clientSearch,
        ReferralCreator $referralCreator,
        ReferralsApiRepository $referralsApiRepository,
        LoggerInterface $logger,
        TokenCheck $tokenCheck
    )
    {
        $this->connection = $connection;
        $this->requestValidator = $requestValidator;
        $this->clientSearch = $clientSearch;
        $this->referralCreator = $referralCreator;
        $this->referralsApiRepository = $referralsApiRepository;
        $this->logger = $logger;
        $this->tokenCheck = $tokenCheck;
    }

    /**
     * @Rest\Post("/referrals/create", name="mtm_referrals_create")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postAction(Request $request): JsonResponse
    {
        $this->connection->beginTransaction();
        $result = $this->processRequest($request);

        $referralsApi = (new ReferralsApi())
            ->setTransactionId($result['transactionID'] ?? '')
            ->setCallBody($result['rawCall'] ?? '');

        try {
            $this->referralsApiRepository->save($referralsApi);
            $this->connection->commit();
        } catch (\Throwable $e) {
            if ($this->connection->isTransactionActive()) {
                $this->connection->rollBack();
            }
            $this->logger->error($e->getMessage());
            return $this->error(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

        if (isset($result['error'])) {
            [$code, $text] = $result['error'];
            return $this->error($code, $text);
        } else {
            return $this->success($result['data']);
        }
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Doctrine\DBAL\ConnectionException
     */
    private function processRequest(Request $request): array
    {
        if (!$this->tokenCheck->check()) {
            return ['error' => [JsonResponse::HTTP_UNAUTHORIZED, (string) $this->tokenCheck->getErrorLabel()]];
        }

        try {
            $json = $request->getContent();
            $data = \GuzzleHttp\json_decode($json, true);
        } catch (\Throwable $e) {
            return ['error' => [JsonResponse::HTTP_BAD_REQUEST, 'Request is invalid']];
        }

        $memberId = $data['MemberID'] ?? null;
        $memberIDType = $data['MemberIDType'] ?? null;
        $referralSource = $data['ReferralSource'] ?? null;
        $referralType = $data['ReferralType'] ?? null;
        $referralSubtype = $data['ReferralSubtype'] ?? null;
        $referralDescription = $data['ReferralDescription'] ?? null;
        $episodeType = $data['EpisodeType'] ?? null;
        $transactionId = $data['TransactionID'] ?? null;

        if (!$this->requestValidator->validate($transactionId, $memberId, $memberIDType, $episodeType, $referralSource, $referralType, $referralSubtype)) {
            return ['error' => [JsonResponse::HTTP_BAD_REQUEST, \implode('; ', $this->requestValidator->getErrors())]];
        }

        $client = $this->clientSearch->findClient($memberIDType, $memberId);
        if ($client === null) {
            return ['error' => [JsonResponse::HTTP_NOT_FOUND, 'Client not found']];
        }

        if (!$this->referralCreator->create($client, $episodeType, $referralSource, $referralType, $referralSubtype, $referralDescription)) {
            $this->logger->error((string) $this->referralCreator->getError());
            return ['error' => [(int) $this->referralCreator->getErrorCode(), (string) $this->referralCreator->getError()]];
        }

        return [
            'transactionID' => $transactionId,
            'rawCall' => $json,
            'data' => $this->referralCreator->getData()
        ];
    }
}
