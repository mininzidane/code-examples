<?php
/**
 * BaseController class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Api\Mtm\Controller\V1;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class BaseController
 * @package VirtualHealth\Api\Mtm\Controller\V1
 */
abstract class BaseController extends AbstractFOSRestController
{
    /**
     * @param int $code
     * @param string $message
     * @return JsonResponse
     */
    protected function error(int $code, string $message): JsonResponse
    {
        $arr = [
            'status' => 'error',
            'error' => [
                'code' => $code,
                'message' => $message
            ]
        ];

        return new JsonResponse($arr);
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    protected function success(array $data = []): JsonResponse
    {
        $arr = [
            'status' => 'success',
            'data' => $data,
        ];

        return new JsonResponse($arr);
    }
}