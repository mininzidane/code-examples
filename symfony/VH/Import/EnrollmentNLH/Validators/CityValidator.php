<?php
/**
 * CityValidator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Validators;

use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location\CityFinder;
use VirtualHealth\Import\Validators\BaseValidator;
use VirtualHealth\Import\Validators\ValidatorInterface;

/**
 * Class CityValidator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Validators
 */
class CityValidator extends BaseValidator implements ValidatorInterface
{

    /**
     * @var CityFinder
     */
    private $cityFinder;

    /**
     * @var array
     */
    private $regionCodes;

    /**
     * CityValidator constructor.
     * @param CityFinder $cityFinder
     * @param string $field
     * @param string|null $fieldLabel
     */
    public function __construct(CityFinder $cityFinder, string $field, string $fieldLabel = null)
    {
        $this->cityFinder = $cityFinder;
        parent::__construct($field, $fieldLabel);
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @return CityValidator
     */
    public function setFileRowCollection(FileRowCollection $fileRowCollection): CityValidator
    {
        $this->cityFinder->setFileRowCollection($fileRowCollection);

        $this->regionCodes = [];
        foreach ($fileRowCollection->getValues() as $fileRow) {
            $this->regionCodes[] = $fileRow->STATE;
        }
        return $this;
    }

    /**
     * @param FileRow $fileRow
     * @return bool
     */
    public function validate($fileRow): bool
    {
        return $this->validateValue($fileRow->{$this->field}, $fileRow->STATE);
    }

    /**
     * @param array $dataByRow
     * @return array
     */
    public function validateAll(array $dataByRow): array
    {
        $result = [];
        foreach ($dataByRow as $i => $value) {
            $regionCode = $this->regionCodes[$i] ?? null;
            if ($regionCode === null) {
                throw new \InvalidArgumentException("Region code not found for city: {$value} and index: {$i}");
            }
            $result[$i] = $this->validateValue($value, $regionCode);
        }

        return $result;
    }

    /**
     * @param string $cityTitle
     * @param string $regionCode
     * @return bool
     */
    protected function validateValue(string $cityTitle, string $regionCode): bool
    {
        return $this->cityFinder->findByTitleAndRegionCode($cityTitle, $regionCode) !== null;
    }
}
