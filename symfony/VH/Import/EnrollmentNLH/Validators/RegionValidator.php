<?php
/**
 * RegionValidator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Validators;

use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location\RegionFinder;
use VirtualHealth\Import\Validators\BaseValidator;
use VirtualHealth\Import\Validators\ValidatorInterface;

/**
 * Class RegionValidator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Validators
 */
class RegionValidator extends BaseValidator implements ValidatorInterface
{
    /**
     * @var RegionFinder
     */
    private $regionFinder;

    /**
     * RegionValidator constructor.
     * @param RegionFinder $regionFinder
     * @param string $field
     * @param string|null $fieldLabel
     */
    public function __construct(RegionFinder $regionFinder, string $field, string $fieldLabel = null)
    {
        $this->regionFinder = $regionFinder;
        parent::__construct($field, $fieldLabel);
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @return RegionValidator
     */
    public function setFileRowCollection(FileRowCollection $fileRowCollection): RegionValidator
    {
        $this->regionFinder->setFileRowCollection($fileRowCollection);
        return $this;
    }

    /**
     * @param FileRow $fileRow
     * @return bool
     */
    public function validate($fileRow): bool
    {
        return $this->validateValue($fileRow->{$this->field});
    }

    /**
     * @param array $dataByRow
     * @return array
     */
    public function validateAll(array $dataByRow): array
    {
        $result = [];
        foreach ($dataByRow as $i => $value) {
            $result[$i] = $this->validateValue($value);
        }

        return $result;
    }

    /**
     * @param string $code
     * @return bool
     */
    protected function validateValue(string $code): bool
    {
        return $this->regionFinder->findByCode($code) !== null;
    }
}
