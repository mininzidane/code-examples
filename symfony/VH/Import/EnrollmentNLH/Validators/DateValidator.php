<?php
/**
 * DateValidator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Validators;

use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use VirtualHealth\Import\Validators\BaseValidator;
use VirtualHealth\Import\Validators\ValidatorInterface;

/**
 * Class DateValidator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Validators
 */
class DateValidator extends BaseValidator implements ValidatorInterface
{
    /**
     * DateValidator constructor.
     * @param string $field
     * @param string|null $fieldLabel
     */
    public function __construct(string $field, string $fieldLabel = null)
    {
        parent::__construct($field, $fieldLabel);
    }

    /**
     * @param FileRow $fileRow
     * @return bool
     */
    public function validate($fileRow): bool
    {
        return $this->validateValue($fileRow->{$this->field});
    }

    /**
     * @param array $dataByRow
     * @return array
     */
    public function validateAll(array $dataByRow): array
    {
        $result = [];
        foreach ($dataByRow as $i => $value) {
            $result[$i] = $this->validateValue($value);
        }

        return $result;
    }

    /**
     * @param string $value
     * @return bool
     */
    private function validateValue(string $value): bool
    {
        $parts = \explode('-', $value);
        if (!isset($parts[0], $parts[1], $parts[2])) {
            return false;
        }
        if (!\ctype_digit($parts[0]) || !\ctype_digit($parts[1]) || !\ctype_digit($parts[2])) {
            return false;
        }

        [$month, $day, $year] = [$parts[1], $parts[2], $parts[0]];
        if ($year < 1000) {
            return false;
        }
        return \checkdate($month, $day, $year);
    }
}
