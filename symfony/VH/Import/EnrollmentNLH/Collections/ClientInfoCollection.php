<?php
/**
 * ClientInfoCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use VirtualHealth\OrmBundle\Entity\Client;

/**
 * Class ClientInfoCollection
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Collections
 */
class ClientInfoCollection extends ArrayCollection
{
    /**
     * @param Client $client
     * @param array $careProviderData
     */
    public function addCareProviderData(Client $client, array $careProviderData): void
    {
        $this->setArrayValue($client, 'careproviderData', $careProviderData);
    }

    /**
     * @param Client $client
     * @param bool $isNewClient
     */
    public function setIsNewClient(Client $client, bool $isNewClient = true): void
    {
        $this->setArrayValue($client, 'isNewClient', $isNewClient);
    }

    /**
     * @param Client $client
     * @param bool $needRecalculatePosition
     */
    public function setIsNeedRecalculatePosition(Client $client, bool $needRecalculatePosition = true): void
    {
        $this->setArrayValue($client, 'needRecalculatePosition', $needRecalculatePosition);
    }

    /**
     * @param Client $client
     * @param bool $needRecalculateRisk
     */
    public function setIsNeedRecalculateRisk(Client $client, bool $needRecalculateRisk = true): void
    {
        $this->setArrayValue($client, 'needRecalculateRisk', $needRecalculateRisk);
    }

    /**
     * @param Client $client
     * @param string $medicaidNumber
     */
    public function addMedicaid(Client $client, string $medicaidNumber): void
    {
        $this->setArrayValue($client, 'medicaid', $medicaidNumber);
    }

    /**
     * @param Client $client
     * @param string $enrollmentDate
     */
    public function addEnrollmentDate(Client $client, string $enrollmentDate): void
    {
        $this->setArrayValue($client, 'enrollment_date', $enrollmentDate);
    }

    /**
     * @param Client[] $clients Persisted list of clients after save
     * @return array
     */
    public function getAllForCaching(array $clients): array
    {
        $result = [];
        $data = $this->toArray();
        foreach ($clients as $client) {
            $result[$client->getUser()->getId()] = \json_encode($data[\spl_object_hash($client)] ?? []);
        }

        return $result;
    }

    /**
     * @param Client $client
     * @param string $key
     * @param mixed $value
     */
    private function setArrayValue(Client $client, string $key, $value): void
    {
        $hash = \spl_object_hash($client);
        $data = $this->get($hash) ?? [];
        $data[$key] = $value;
        $this->set($hash, $data);
    }
}
