<?php
/**
 * UserCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Collections;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class UserCollection
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Collections
 */
class UserCollection extends ArrayCollection
{

}
