<?php
/**
 * ClientStateDataCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use VirtualHealth\OrmBundle\Entity\ClientStateData;

/**
 * Class ClientStateDataCollection
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Collections
 *
 * @method add(ClientStateData $clientStateData)
 * @method ClientStateData[] getValues()
 */
class ClientStateDataCollection extends ArrayCollection
{

}
