<?php
/**
 * FileRowCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Collections;

use application\modules\dataWarehouse\parsers\enrollment\FileParserLog;
use application\modules\dataWarehouse\parsers\enrollment\FileParserLog as Log;
use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use Doctrine\Common\Collections\ArrayCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\RowDataManipulator;

/**
 * Class FileRowCollection
 * @package VirtualHealth\Import\Membership\EnrollmentNLH
 *
 * @method FileRow[] getValues()
 */
class FileRowCollection extends ArrayCollection
{
    /**
     * @var FileRow[]
     */
    private $existingClientFileRows;

    /**
     * @var FileRow[]
     */
    private $newClientFileRows;

    /**
     * @var FileParserLog
     */
    private $log;

    /**
     * @return FileRow[]
     */
    public function getExistingClientFileRows(): array
    {
        return $this->existingClientFileRows;
    }

    /**
     * @param array $existingClientFileRows
     * @return FileRowCollection
     */
    public function setExistingClientFileRows(array $existingClientFileRows): FileRowCollection
    {
        $this->existingClientFileRows = $existingClientFileRows;
        return $this;
    }

    /**
     * @return FileRow[]
     */
    public function getNewClientFileRows(): array
    {
        return $this->newClientFileRows;
    }

    /**
     * @param array $newClientFileRows
     * @return FileRowCollection
     */
    public function setNewClientFileRows(array $newClientFileRows): FileRowCollection
    {
        $this->newClientFileRows = $newClientFileRows;
        return $this;
    }

    /**
     * @param FileParserLog $log
     * @return FileRowCollection
     */
    public function setLog(FileParserLog $log): FileRowCollection
    {
        $this->log = $log;
        return $this;
    }

    /**
     * @param FileRow $row
     * @param \Throwable $e
     * @throws \CException
     */
    public function addError(FileRow $row, \Throwable $e): void
    {
        $this->log->inc(Log::SUMMARY_ERRORS);
        $this->log->addError(
            $row->getNum(),
            $row->HFS_MEMBER_NBR ?: RowDataManipulator::NA,
            $e->getCode() ?: Log::INFO_ITEM,
            $e->attributes ?? null,
            $e->details ?? null
        );
        $this->log->addError($row->getNum(), $row->HFS_MEMBER_NBR ?: RowDataManipulator::NA, Log::INFO_ITEM);
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function remove($key)
    {
        $fileRow = $this->get($key);
        $hash = \spl_object_hash($fileRow);
        unset($this->existingClientFileRows[$hash], $this->newClientFileRows[$hash]);

        return parent::remove($key);
    }
}
