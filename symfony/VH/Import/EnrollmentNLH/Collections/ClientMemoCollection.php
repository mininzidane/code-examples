<?php
/**
 * ClientMemoCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use VirtualHealth\OrmBundle\Entity\ClientMemo;

/**
 * Class ClientMemoCollection
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Collections
 *
 * @method add(ClientMemo $clientMemo)
 * @method ClientMemo[] getValues()
 */
class ClientMemoCollection extends ArrayCollection
{

}
