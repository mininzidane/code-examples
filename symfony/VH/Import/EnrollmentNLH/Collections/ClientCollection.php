<?php
/**
 * ClientCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Collections;

use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use VirtualHealth\OrmBundle\Entity\Client;

/**
 * Class ClientCollection
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Collections
 */
class ClientCollection
{
    /**
     * @var Client[]
     */
    private $newClientsByFileRowHash = [];

    /**
     * @var Client[]
     */
    private $existingClientsByFileRowHash = [];

    /**
     * @return Client[]
     */
    public function getNewClientsByFileRowHash(): array
    {
        return $this->newClientsByFileRowHash;
    }

    /**
     * @param Client[] $newClientsByFileRowHash
     * @return ClientCollection
     */
    public function setNewClientsByFileRowHash(array $newClientsByFileRowHash): ClientCollection
    {
        $this->newClientsByFileRowHash = $newClientsByFileRowHash;
        return $this;
    }

    /**
     * @return Client[]
     */
    public function getExistingClientsByFileRowHash(): array
    {
        return $this->existingClientsByFileRowHash;
    }

    /**
     * @param Client[] $existingClientsByFileRowHash
     * @return ClientCollection
     */
    public function setExistingClientsByFileRowHash(array $existingClientsByFileRowHash): ClientCollection
    {
        $this->existingClientsByFileRowHash = $existingClientsByFileRowHash;
        return $this;
    }

    /**
     * @return Client[]
     */
    public function getAll(): array
    {
        return \array_merge($this->existingClientsByFileRowHash, $this->newClientsByFileRowHash);
    }

    /**
     * @param FileRow $fileRow
     * @return Client
     */
    public function findNewClient(FileRow $fileRow): Client
    {
        return $this->newClientsByFileRowHash[\spl_object_hash($fileRow)];
    }

    /**
     * Medicaid number may repeat in the batch, in that case row count > found clients
     * @param FileRow $fileRow
     * @return Client|null
     */
    public function findExistingClient(FileRow $fileRow): ?Client
    {
        return $this->existingClientsByFileRowHash[\spl_object_hash($fileRow)] ?? null;
    }
}
