<?php
/**
 * ClientInsuranceMedicaidCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicaid;

/**
 * Class ClientInsuranceMedicaidCollection
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Collections
 *
 * @method add(ClientInsuranceMedicaid $clientInsuranceMedicaid)
 * @method ClientInsuranceMedicaid[] getValues()
 */
class ClientInsuranceMedicaidCollection extends ArrayCollection
{

}
