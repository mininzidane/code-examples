<?php
/**
 * FileParser class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH;

use application\modules\dataWarehouse\parsers\enrollment\FileParserLog as Log;
use Doctrine\ORM\EntityManager;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInfoCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInsuranceMedicaidCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInsuranceMedicaidManualChangedCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInternalCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientMemoCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientStateDataCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\UserCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Cache\Cache;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\ClientPreparation;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\DataPopulate;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\RowDataManipulator;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Saver;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\TaskCreationQueueCreator;

/**
 * Class FileParser
 * @package VirtualHealth\Import\Membership\EnrollmentNLH
 */
class FileParser
{
    public const PROCESSED_CLIENTS_KEY = 'enrollmentClientsInfo';

    /**
     * @var ClientPreparation
     */
    private $clientPreparation;

    /**
     * @var DataPopulate
     */
    private $dataPopulate;

    /**
     * @var Saver
     */
    private $saver;

    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var TaskCreationQueueCreator
     */
    private $taskCreationQueueCreator;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * FileParser constructor.
     * @param ClientPreparation $clientPreparation
     * @param DataPopulate $dataPopulate
     * @param Saver $saver
     * @param RowDataManipulator $rowDataManipulator
     * @param TaskCreationQueueCreator $taskCreationQueueCreator
     * @param EntityManager $entityManager
     * @param Cache $cache
     */
    public function __construct(
        ClientPreparation $clientPreparation,
        DataPopulate $dataPopulate,
        Saver $saver,
        RowDataManipulator $rowDataManipulator,
        TaskCreationQueueCreator $taskCreationQueueCreator,
        EntityManager $entityManager,
        Cache $cache
    )
    {
        $this->clientPreparation = $clientPreparation;
        $this->dataPopulate = $dataPopulate;
        $this->saver = $saver;
        $this->rowDataManipulator = $rowDataManipulator;
        $this->taskCreationQueueCreator = $taskCreationQueueCreator;
        $this->entityManager = $entityManager;
        $this->cache = $cache;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @throws FileRowException
     * @throws \CException
     * @throws \CHttpException
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Throwable
     * @throws \application\modules\dataWarehouse\parsers\enrollment\FileRowServiceException
     */
    public function parse(FileRowCollection $fileRowCollection): void
    {
        $userCollection = new UserCollection();
        $clientCollection = new ClientCollection();
        $clientInfoCollection = new ClientInfoCollection();
        $clientMemoCollection = new ClientMemoCollection();
        $clientStateDataCollection = new ClientStateDataCollection();
        $clientInsuranceMedicaidCollection = new ClientInsuranceMedicaidCollection();
        $clientInsuranceMedicaidManualChangedCollection = new ClientInsuranceMedicaidManualChangedCollection();
        $clientInternalCollection = new ClientInternalCollection();

        $this->clientPreparation->prepare($fileRowCollection, $clientCollection, $userCollection);
        $this->incrementParseLog($fileRowCollection);

        $this->dataPopulate
            ->setClientCollection($clientCollection)
            ->setClientInfoCollection($clientInfoCollection)
            ->setClientMemoCollection($clientMemoCollection)
            ->setClientStateDataCollection($clientStateDataCollection)
            ->setClientInsuranceMedicaidCollection($clientInsuranceMedicaidCollection)
            ->setClientInsuranceMedicaidManualChangedCollection($clientInsuranceMedicaidManualChangedCollection)
            ->setClientInternalCollection($clientInternalCollection)
            ->populate($fileRowCollection)
        ;
        $this->saver
            ->setUserCollection($userCollection)
            ->setClientCollection($clientCollection)
            ->setClientMemoCollection($clientMemoCollection)
            ->setClientStateDataCollection($clientStateDataCollection)
            ->setClientInsuranceMedicaidCollection($clientInsuranceMedicaidCollection)
            ->setClientInsuranceMedicaidManualChangedCollection($clientInsuranceMedicaidManualChangedCollection)
            ->setClientInternalCollection($clientInternalCollection)
            ->batchSave()
        ;
        $this->cacheClientData($clientInfoCollection, $clientCollection);
        $this->taskCreationQueueCreator->create($fileRowCollection, $clientCollection);
        $this->entityManager->clear();
    }

    /**
     * @param ClientInfoCollection $clientInfoCollection
     * @param ClientCollection $clientCollection
     * @throws FileRowException
     */
    private function cacheClientData(
        ClientInfoCollection $clientInfoCollection,
        ClientCollection $clientCollection
    ): void
    {
        foreach ($clientInfoCollection->getAllForCaching($clientCollection->getAll()) as $clientId => $json) {
            if (!$this->cache->setForClient(
                self::PROCESSED_CLIENTS_KEY,
                (int)$clientId,
                $json
            )) {
                throw $this->rowDataManipulator->exception(Log::ERROR_CLIENT_SAVE_CACHE);
            }
        }
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @throws \CException
     */
    private function incrementParseLog(
        FileRowCollection $fileRowCollection
    ): void
    {
        foreach ($fileRowCollection->getNewClientFileRows() as $fileRow) {
            $fileRow->getParser()->getLog()->inc(Log::SUMMARY_CLIENTS_CREATED);
        }

        foreach ($fileRowCollection->getExistingClientFileRows() as $fileRow) {
            $fileRow->getParser()->getLog()->inc(Log::SUMMARY_CLIENTS_CONFIRMED);
        }
    }
}
