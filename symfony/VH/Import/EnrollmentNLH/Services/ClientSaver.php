<?php
/**
 * ClientSaver class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use Doctrine\ORM\EntityManager;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\UserCollection;
use VirtualHealth\OrmBundle\Saver\User\UserSaver;

/**
 * Class ClientSaver
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientSaver
{
    /**
     * @var UserSaver
     */
    private $userSaver;

    /**
     * @var \VirtualHealth\OrmBundle\Saver\Client\ClientSaver
     */
    private $clientSaver;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ClientSaver constructor.
     * @param UserSaver $userSaver
     * @param \VirtualHealth\OrmBundle\Saver\Client\ClientSaver $clientSaver
     * @param EntityManager $entityManager
     */
    public function __construct(
        UserSaver $userSaver,
        \VirtualHealth\OrmBundle\Saver\Client\ClientSaver $clientSaver,
        EntityManager $entityManager
    )
    {
        $this->userSaver = $userSaver;
        $this->clientSaver = $clientSaver;
        $this->entityManager = $entityManager;
    }

    /**
     * @param UserCollection $userCollection
     * @param ClientCollection $clientCollection
     * @throws \CDbException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function save(
        UserCollection $userCollection,
        ClientCollection $clientCollection
    ): void
    {
        if (!empty($userCollection->getValues())) {
            $this->userSaver->saveAll($userCollection->getValues());
        }
        $this->clientSaver
            ->setUpdateByImportProcess(false)
            ->saveAll($clientCollection->getNewClientsByFileRowHash())
        ;
        $this->clientSaver
            ->setUpdateByImportProcess(true)
            ->saveAll($clientCollection->getExistingClientsByFileRowHash())
        ;
        foreach ($clientCollection->getNewClientsByFileRowHash() as $client) {
            $this->entityManager->getUnitOfWork()->registerManaged(
                $client,
                ['userId' => $client->getUser()->getId()],
                []
            );
        }
    }
}
