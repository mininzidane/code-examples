<?php
/**
 * WaiverGetter class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\OrmBundle\Entity\Waiver;
use VirtualHealth\OrmBundle\Repository\WaiverRepository;

/**
 * Class WaiverGetter
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class WaiverGetter
{
    private const WAIVER_AIDS = 'd407e09c-24f2-431a-a421-6b9d8f9c97f0';
    private const WAIVER_AGING = 'b795cdc5-b0e3-4255-be5d-4e256c92c557';
    private const WAIVER_DD_ADULT = 'd1cd2e58-cb49-447f-8efb-e8b2d25d8d79';
    private const WAIVER_PHISICALLY_DISABLED = '0ca98803-9c52-4c04-8778-dd9b6aa2a733';
    private const WAIVER_SUPPORTIVE_LIVING_FACILITY = 'ca02c126-1282-4de9-9cf0-08e2d51c2d6a';
    private const WAIVER_TBI = '5f934564-b842-463b-bbdb-c15d56e01655';
    private const WAIVER_DD_CHILDREN_IN_HOME_SUPPORT = 'cc0dc12b-dd82-4be2-9ec0-9008561d79b1';
    private const WAIVER_DD_CHILDREN_RESIDENTIAL = '3913f78f-3450-4b51-8a9e-de74261a7e88';
    private const WAIVER_MFTD = '359dcc6a-d7a3-4081-af32-9f0404fe0442';

    private const CODE_MAP = [
        'aids' => self::WAIVER_AIDS,
        'aging' => self::WAIVER_AGING,
        'dd adult' => self::WAIVER_DD_ADULT,
        'physically disabled' => self::WAIVER_PHISICALLY_DISABLED,
        'supportive living facility' => self::WAIVER_SUPPORTIVE_LIVING_FACILITY,
        'tbi' => self::WAIVER_TBI,
        'dd children in home support' => self::WAIVER_DD_CHILDREN_IN_HOME_SUPPORT,
        'dd children residential' => self::WAIVER_DD_CHILDREN_RESIDENTIAL,
        'mftd' => self::WAIVER_MFTD,
        'a0' => self::WAIVER_AGING, // 'aging',
        'b0' => self::WAIVER_TBI, // 'tbi',
        'c0' => self::WAIVER_AIDS, // 'aids',
        'd0' => self::WAIVER_DD_ADULT, // 'dd adult',
        'f0' => self::WAIVER_SUPPORTIVE_LIVING_FACILITY, // 'supportive living facility',
        'h0' => self::WAIVER_PHISICALLY_DISABLED, // 'physically disabled',
        'd1' => self::WAIVER_DD_CHILDREN_IN_HOME_SUPPORT, // 'dd children in home support',
        'd2' => self::WAIVER_DD_CHILDREN_RESIDENTIAL, // 'dd children residential',
        'g0' => self::WAIVER_MFTD, // 'mftd',
        'g1' => self::WAIVER_MFTD, // 'mftd',
        'g6' => self::WAIVER_MFTD, // 'mftd',
        'gh' => self::WAIVER_MFTD, // 'mftd',
        'gn' => self::WAIVER_MFTD, // 'mftd',
    ];

    /**
     * @var WaiverRepository
     */
    private $waiverRepository;

    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var FileRowCollection
     */
    private $fileRowCollection;

    /**
     * @var array
     */
    private $codeToWaiverId = [];

    /**
     * WaiverGetter constructor.
     * @param WaiverRepository $waiverRepository
     * @param RowDataManipulator $rowDataManipulator
     */
    public function __construct(
        WaiverRepository $waiverRepository,
        RowDataManipulator $rowDataManipulator
    )
    {
        $this->waiverRepository = $waiverRepository;
        $this->rowDataManipulator = $rowDataManipulator;
    }

    /**
     * @param string $waiverCode
     * @return string|null
     */
    public function getWaiver(string $waiverCode): ?string
    {
        return $this->codeToWaiverId[$waiverCode] ?? null;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @return WaiverGetter
     * @throws \CException
     */
    public function setFileRowCollection(FileRowCollection $fileRowCollection): WaiverGetter
    {
        $this->fileRowCollection = $fileRowCollection;
        $this->prepareMapping();

        return $this;
    }

    /**
     * @throws \CException
     */
    private function prepareMapping(): void
    {
        $waiverCodes = [];
        /** @var FileRow[] $codeToFileRow */
        $codeToFileRow = [];
        foreach ($this->fileRowCollection->getValues() as $fileRow) {
            $code = \strtolower($fileRow->WAIVER_CODE);
            if ($code === 'no waiver' || $code === '-') {
                $this->codeToWaiverId[$fileRow->WAIVER_CODE] = 0;
                continue;
            }

            if (!\array_key_exists($code, self::CODE_MAP)) {
                $this->rowDataManipulator->warningWrongFieldFormat($fileRow, 'WAIVER_CODE');
                $this->codeToWaiverId[$fileRow->WAIVER_CODE] = null;
                continue;
            }

            $waiverCodes[] = self::CODE_MAP[$code];
            $codeToFileRow[self::CODE_MAP[$code]] = $fileRow;
        }

        /** @var Waiver[] $waivers */
        $waivers = $this->waiverRepository->findBy(['systemId' => $waiverCodes]);
        $foundWaiverCodes = [];
        foreach ($waivers as $waiver) {
            $fileRow = $codeToFileRow[$waiver->getSystemId()];
            $foundWaiverCodes[] = $waiver->getSystemId();
            $this->codeToWaiverId[$fileRow->WAIVER_CODE] = $waiver->getId();
        }

        $notFoundWaiverCodes = \array_diff($waiverCodes, $foundWaiverCodes);
        foreach ($notFoundWaiverCodes as $notFoundWaiverCode) {
            $this->rowDataManipulator->warningWrongFieldFormat($codeToFileRow[$notFoundWaiverCode], 'WAIVER_CODE');
        }
    }
}
