<?php
/**
 * ClientDataPopulate class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use application\components\helpers\CustomerHelper;
use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInfoCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientMemoCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location\CityFinder;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location\RegionFinder;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\ClientMemo;
use VirtualHealth\OrmBundle\Entity\Country;
use VirtualHealth\OrmBundle\Entity\Roles;
use VirtualHealth\OrmBundle\Entity\User;
use VirtualHealth\OrmBundle\Query\CareTeam\FindAllMemberIdsByRoleName;
use VirtualHealth\OrmBundle\Query\Supervisor\GetSupervisorByCaremanagerUserIds;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class ClientDataPopulate
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientDataPopulate
{
    private const DEFAULT_ELIG_TERM_DT_MAX = '2099-12-31';

    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var CommonUsersGetter
     */
    private $commonUsersGetter;

    /**
     * @var ClientMemoCollection
     */
    private $clientMemoCollection;

    /**
     * @var ClientInfoCollection
     */
    private $clientInfoCollection;

    /**
     * @var \PmModule
     */
    private $pmModule;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var FindAllMemberIdsByRoleName
     */
    private $findAllMemberIdsByRoleName;

    /**
     * @var GetSupervisorByCaremanagerUserIds
     */
    private $getSupervisorByCaremanagerUserIds;

    /**
     * @var RegionFinder
     */
    private $regionFinder;

    /**
     * @var CityFinder
     */
    private $cityFinder;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var bool
     */
    private $useMedicaidDates;

    /**
     * @var string
     */
    private $dateFormat;

    /**
     * ClientDataPopulate constructor.
     * @param RowDataManipulator $rowDataManipulator
     * @param EntityManager $entityManager
     * @param CommonUsersGetter $commonUsersGetter
     * @param LoggerInterface $logger
     * @param FindAllMemberIdsByRoleName $findAllMemberIdsByRoleName
     * @param GetSupervisorByCaremanagerUserIds $getSupervisorByCaremanagerUserIds
     * @param RegionFinder $regionFinder
     * @param CityFinder $cityFinder
     * @param OutputInterface $output
     * @param bool $useMedicaidDates
     * @param string $date
     */
    public function __construct(
        RowDataManipulator $rowDataManipulator,
        EntityManager $entityManager,
        CommonUsersGetter $commonUsersGetter,
        LoggerInterface $logger,
        FindAllMemberIdsByRoleName $findAllMemberIdsByRoleName,
        GetSupervisorByCaremanagerUserIds $getSupervisorByCaremanagerUserIds,
        RegionFinder $regionFinder,
        CityFinder $cityFinder,
        OutputInterface $output,
        bool $useMedicaidDates,
        string $date
    )
    {
        $this->rowDataManipulator = $rowDataManipulator;
        $this->em = $entityManager;
        $this->commonUsersGetter = $commonUsersGetter;
        $this->pmModule = mod('pm');
        $this->logger = $logger;
        $this->findAllMemberIdsByRoleName = $findAllMemberIdsByRoleName;
        $this->getSupervisorByCaremanagerUserIds = $getSupervisorByCaremanagerUserIds;
        $this->regionFinder = $regionFinder;
        $this->cityFinder = $cityFinder;
        $this->output = $output;
        $this->useMedicaidDates = $useMedicaidDates;
        $this->dateFormat = $date;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientsCollection
     * @param ClientMemoCollection $clientMemoCollection
     * @param ClientInfoCollection $clientInfoCollection
     * @throws \CException
     * @throws \CHttpException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function batchPopulate(
        FileRowCollection $fileRowCollection,
        ClientCollection $clientsCollection,
        ClientMemoCollection $clientMemoCollection,
        ClientInfoCollection $clientInfoCollection
    ): void
    {
        $this->clientMemoCollection = $clientMemoCollection;
        $this->clientInfoCollection = $clientInfoCollection;

        $this->batchDisenrollmentNotification($fileRowCollection, $clientsCollection);

        foreach ($fileRowCollection->getExistingClientFileRows() as $fileRow) {
            $client = $clientsCollection->findExistingClient($fileRow);
            if ($client === null) {
                continue;
            }

            $this->populateCommonClient($client, $fileRow);
            $this->populateExistingClient($client, $fileRow);
        }

        foreach ($fileRowCollection->getNewClientFileRows() as $fileRow) {
            $client = $clientsCollection->findNewClient($fileRow);

            $this->populateCommonClient($client, $fileRow);
            $this->populateNewClient($client, $fileRow);
        }
    }

    /**
     * @param Client $client
     * @param FileRow $fileRow
     */
    private function populateExistingClient(Client $client, FileRow $fileRow): void
    {
        // recover user
        $client->getUser()->setDeletedUser(User::IS_NOT_SOFT_DELETED);

        if ((int)$client->getUser()->getStatus() !== User::STATUS_ACTIVE) {
            $client->getUser()->setStatus(User::STATUS_ACTIVE);

            $this->updateClientImportStatus($fileRow, $client, User::IMPORT_STATUS_RESTORED);
        }

        $oldValues = $client->getFirstName() . $client->getLastName();
        if ($value = CustomerHelper::fixUserName($fileRow->FIRST_NAME)) {
            $client->setFirstName($value);
        }
        if ($value = CustomerHelper::fixUserName($fileRow->LAST_NAME)) {
            $client->setLastName($value);
        }
        $newValues = $client->getFirstName() . $client->getLastName();
        if ($oldValues !== $newValues) {
            $this->clientInfoCollection->setIsNeedRecalculatePosition($client);
            $client->getUser()->setFirstName($client->getFirstName());
            $client->getUser()->setLastName($client->getLastName());
        }
    }

    /**
     * @param Client $client
     * @param FileRow $fileRow
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function populateCommonClient(Client $client, FileRow $fileRow): void
    {
        $oldGender = $client->getGender();
        $oldBirthday = $client->getBirthday();
        $client->setGender($this->rowDataManipulator->getFormattedGender($fileRow));
        $client->setBirthday($this->rowDataManipulator->getDate($fileRow, 'DOB'));

        $oldDrsValue = $client->getDrsCaseId();
        $newDrsValue = $fileRow->DRS_ID;
        if ($newDrsValue) {
            $client->setDrsCaseId($newDrsValue);
        }

        $isNeedRecalculateRisk = $client->getUser()->getId() !== null && ($oldGender !== $client->getGender() || $oldBirthday !== $client->getBirthday());
        $this->clientInfoCollection->setIsNeedRecalculateRisk($client, $isNeedRecalculateRisk);

        $this->generateMemoAboutDrsChange($client, $oldDrsValue, $newDrsValue);

        $this->clientInfoCollection->addCareProviderData($client, [
            'npi' => $fileRow->PRAC_NPI,
            'tin' => $fileRow->PCP_AFF_TIN,
        ]);
    }

    /**
     * @param Client $client
     * @param FileRow $fileRow
     * @throws \Doctrine\ORM\ORMException
     */
    private function populateNewClient(Client $client, FileRow $fileRow): void
    {
        $client->setAddress1(CustomerHelper::fixCapitalization($fileRow->ADDRESS1));
        if ($fileRow->ADDRESS2) {
            $client->setAddress2(CustomerHelper::fixCapitalization($fileRow->ADDRESS2));
        }
        $client->setCity($this->cityFinder->findByTitleAndRegionCode($fileRow->CITY, $fileRow->STATE));
        $client->setRegion($this->regionFinder->findByCode($fileRow->STATE));
        $client->setCountry(
            $this->em->getReference(Country::class, RegionFinder::DEFAULT_COUNTRY_ID)
        );

        if ($value = CustomerHelper::fixPostalCode($fileRow->ZIP)) {
            $client->setPostalCode($value);
        }

        $client->setPhone($this->rowDataManipulator->getFormattedPhone($fileRow->PHONE));

        if (!$this->useMedicaidDates) {
            if ($fileRow->CURR_ELIG_DT) {
                $client->setEffectiveDate($this->rowDataManipulator->getDate($fileRow, 'CURR_ELIG_DT'));
            }
            if ($fileRow->ELIG_TERM_DT) {
                $client->setTerminationDate($this->rowDataManipulator->getDate($fileRow, 'ELIG_TERM_DT'));
            }
        }

            $this->updateClientImportStatus($fileRow, $client, User::IMPORT_STATUS_NEW);

        $this->clientInfoCollection->setIsNewClient($client);
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     * @throws \CException
     * @throws \CHttpException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function batchDisenrollmentNotification(FileRowCollection $fileRowCollection, ClientCollection $clientCollection): void
    {
        $clientIds = [];
        foreach ($fileRowCollection->getExistingClientFileRows() as $fileRow) {
            $client = $clientCollection->findExistingClient($fileRow);
            if ($client === null) {
                continue;
            }

            $clientIds[] = $client->getUser()->getId();
        }

        $primaryCmUserIds = $this->findAllMemberIdsByRoleName->read($clientIds, Roles::NAME_PRIMARY, User::TYPE_CAREMANAGER);
        $supervisors = $this->getSupervisorByCaremanagerUserIds->read(\array_values($primaryCmUserIds));

        foreach ($fileRowCollection->getExistingClientFileRows() as $fileRow) {
            $client = $clientCollection->findExistingClient($fileRow);
            if ($client === null) {
                continue;
            }

            $primaryCmUserId = $primaryCmUserIds[$client->getUser()->getId()] ?? null;

            $disEnrollmentDate = $this->rowDataManipulator->getDate($fileRow, 'ELIG_TERM_DT');

            $terminationDate = $client->getTerminationDate() ? $client->getTerminationDate()->format($this->dateFormat) : null;
            if ($disEnrollmentDate !== null
                && $primaryCmUserId !== null
                && $disEnrollmentDate->format('Y-m-d') !== self::DEFAULT_ELIG_TERM_DT_MAX
                && $disEnrollmentDate->format($this->dateFormat) !== $terminationDate
            ) {
                $subject = t('PMessage', 'Disenrollment Notification');
                $text = t(
                    'PMessage',
                    'Disenrollment Notification: {clientFirstName} {clientLastName} scheduled to disenroll as of {terminationDate}',
                    [
                        '{clientFirstName}' => $client->getFirstName(),
                        '{clientLastName}' => $client->getLastName(),
                        '{terminationDate}' => $disEnrollmentDate->format($this->dateFormat),
                    ]
                );
                $senderId = $this->commonUsersGetter->getSystemSuperAdmin()->getId();

                $recipientUserIds = [$primaryCmUserId];

                foreach ($supervisors[$primaryCmUserId] ?? [] as $supervisor) {
                    $recipientUserIds[] = $supervisor['user_id'];
                }

                $this->output->writeln('Send disenrollment notification for client #' . $client->getUser()->getId());

                foreach ($recipientUserIds as $recipientUserId) {
                    $message = new \PersonalMessage();
                    $message->sender_id = $senderId;
                    $message->subject = $subject;
                    $message->text = $text;
                    $message->recipient_id = $recipientUserId;
                    if (!$this->pmModule->send($message)) {
                        $this->logger->error("Can not send personal message to user #{$recipientUserId} with text: '{$text}'");
                    }
                }
            }
        }
    }

    /**
     * @param FileRow $fileRow
     * @param Client $client
     * @param string $status
     */
    private function updateClientImportStatus(FileRow $fileRow, Client $client, string $status): void
    {
        /** Import status need to update only if
         * ((Termination_Date in file is null) OR (Current Date <=Termination_Date in file))
         * @see https://bug.virtualhealth.com/issue/NLH-1144
         */
        $clientTerminationDate = $this->rowDataManipulator->getDate($fileRow, 'ELIG_TERM_DT');

        if ($clientTerminationDate === null || Carbon::parse($clientTerminationDate->format('Y-m-d'))->isFuture()) {
            $client->getUser()->setImportStatus($status);
        }
    }

    /**
     * @param Client $client
     * @param string|null $oldDrsValue
     * @param string|null $newDrsValue
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function generateMemoAboutDrsChange(Client $client, ?string $oldDrsValue, ?string $newDrsValue): void
    {
        if ((!$oldDrsValue && !$newDrsValue) || ((string)$oldDrsValue !== '' && $newDrsValue !== '')) {
            return;
        }

        $memo = (new ClientMemo())
            ->setClient($client)
            ->setSourceType(ClientMemo::TYPE_PROFILE)
            ->setOwner($this->commonUsersGetter->getSystemSuperAdmin());
        if (!$oldDrsValue && $newDrsValue) {
            $params = [
                '{value}' => $newDrsValue,
                '{action}' => 'added',
            ];
        } else {
            $params = [
                '{value}' => $oldDrsValue,
                '{action}' => 'removed',
            ];
        }
        $memo->setMemo(
            t(
                'Notes',
                'DRS Case ID: {value} {action}',
                $params
            )
        );
        $this->clientMemoCollection->add($memo);
    }
}
