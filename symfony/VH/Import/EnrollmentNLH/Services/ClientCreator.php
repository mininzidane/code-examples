<?php
/**
 * ClientCreator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use application\components\helpers\CustomerHelper;
use VirtualHealth\Import\Base\Helpers\Generator;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\UserCollection;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\User;
use VirtualHealth\OrmBundle\Query\Client\GetLastIdMedrec;

/**
 * Class ClientCreator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientCreator
{
    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var Generator
     */
    private $generator;

    /**
     * @var GetLastIdMedrec
     */
    private $lastIdMedrecQuery;

    /**
     * @var string
     */
    private $lastIdMedrec;

    /**
     * ClientCreator constructor.
     * @param RowDataManipulator $rowDataManipulator
     * @param Generator $generator
     * @param GetLastIdMedrec $lastIdMedrecQuery
     */
    public function __construct(
        RowDataManipulator $rowDataManipulator,
        Generator $generator,
        GetLastIdMedrec $lastIdMedrecQuery
    )
    {
        $this->rowDataManipulator = $rowDataManipulator;
        $this->generator = $generator;
        $this->lastIdMedrecQuery = $lastIdMedrecQuery;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     * @param UserCollection $userCollection
     */
    public function batchCreate(
        FileRowCollection $fileRowCollection,
        ClientCollection $clientCollection,
        UserCollection $userCollection
    ): void
    {
        $clients = [];
        $emails = [];
        foreach ($fileRowCollection->getNewClientFileRows() as $fileRow) {
            try {
                $email = $this->generator->generateUserEmail(
                    \strtolower($fileRow->FIRST_NAME . '.' . $fileRow->LAST_NAME),
                    'm_',
                    '',
                    false,
                    $emails
                );
                $emails[] = $email;
                $lastName = CustomerHelper::fixUserName($fileRow->LAST_NAME);
                $firstName = CustomerHelper::fixUserName($fileRow->FIRST_NAME);

                $user = (new User())
                    ->setEmail($email)
                    ->setPassword($this->generator->generatePassword())
                    ->setType(User::TYPE_CLIENT)
                    ->setStatus(\BaseModel::YES)
                    ->setFirstName($firstName ?: null)
                    ->setLastName($lastName ?: null)
                ;
                $client = (new Client())
                    ->setUser($user)
                    ->setFirstName($user->getFirstName())
                    ->setLastName($user->getLastName())
                    ->setIdMedrec(\bcadd($this->getNewIdMedrec(), '1'))
                ;
                $clients[\spl_object_hash($fileRow)] = $client;
                $userCollection->add($user);

                // logs
                $fileRow->cliEcho(sprintf('Client %s created.', $this->rowDataManipulator->getClientFullname($fileRow)));

            } catch (\Exception $e) {
                cliEcho($e->getMessage());
                cliEcho($e->getTraceAsString());
            }
        }

        $clientCollection->setNewClientsByFileRowHash($clients);
    }

    /**
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getNewIdMedrec(): string
    {
        if ($this->lastIdMedrec === null) {
            return $this->lastIdMedrec = \bcadd($this->lastIdMedrecQuery->read(), \random_int(15, 45));
        }

        return $this->lastIdMedrec = \bcadd($this->lastIdMedrec, '1');
    }
}
