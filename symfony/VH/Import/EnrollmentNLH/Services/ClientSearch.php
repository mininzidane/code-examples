<?php
/**
 * ClientSearch class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\OrmBundle\Query\Client\FindClientsByMedicaidNumbers;

/**
 * Class ClientSearch
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientSearch
{
    /**
     * @var FindClientsByMedicaidNumbers
     */
    private $findClientsByMedicaidNumbers;

    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * ClientSearch constructor.
     * @param FindClientsByMedicaidNumbers $findClientsByMedicaidNumbers
     * @param RowDataManipulator $rowDataManipulator
     */
    public function __construct(
        FindClientsByMedicaidNumbers $findClientsByMedicaidNumbers,
        RowDataManipulator $rowDataManipulator
    )
    {
        $this->findClientsByMedicaidNumbers = $findClientsByMedicaidNumbers;
        $this->rowDataManipulator = $rowDataManipulator;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     */
    public function batchSearch(FileRowCollection $fileRowCollection, ClientCollection $clientCollection): void
    {
        $medicaids = [];
        foreach ($fileRowCollection->getValues() as $fileRow) {
            $medicaids[\spl_object_hash($fileRow)] = $this->rowDataManipulator->getMedicaid($fileRow);
        }

        $clients = $this->findClientsByMedicaidNumbers->read($medicaids);
        $clientsByFileRowHash = [];
        foreach ($clients as $medicaid => $client) {
            $clientsByFileRowHash[\array_search((string)$medicaid, $medicaids, true)] = $client;
        }
        $clientCollection->setExistingClientsByFileRowHash($clientsByFileRowHash);

        $medicaidsFound = \array_keys($clients);
        $clientFoundFileRows = [];
        $clientNotFoundFileRows = [];
        foreach ($fileRowCollection->getValues() as $fileRow) {
            $hash = \spl_object_hash($fileRow);
            if (\in_array($medicaids[$hash], $medicaidsFound, false)) {
                $clientFoundFileRows[$hash] = $fileRow;
            } else {
                $clientNotFoundFileRows[$hash] = $fileRow;
            }
        }
        $fileRowCollection->setExistingClientFileRows($clientFoundFileRows);
        $fileRowCollection->setNewClientFileRows($clientNotFoundFileRows);
    }
}
