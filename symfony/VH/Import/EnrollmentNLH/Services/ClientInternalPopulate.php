<?php
/**
 * ClientInternalPopulate class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInternalCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\OrmBundle\Entity\ClientInternal;

/**
 * Class ClientInternalPopulate
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientInternalPopulate
{
    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     * @param ClientInternalCollection $clientInternalCollection
     */
    public function batchPopulate(
        FileRowCollection $fileRowCollection,
        ClientCollection $clientCollection,
        ClientInternalCollection $clientInternalCollection
    ): void
    {
        foreach ($fileRowCollection->getNewClientFileRows() as $fileRow) {
            $client = $clientCollection->findNewClient($fileRow);

            $clientInternal = new ClientInternal();
            $clientInternal->setClient($client);
            $this->populateClientInternal($clientInternal, $fileRow);

            $clientInternalCollection->add($clientInternal);
        }

        foreach ($fileRowCollection->getExistingClientFileRows() as $fileRow) {
            $client = $clientCollection->findExistingClient($fileRow);
            if ($client === null) {
                continue;
            }

            if (!$clientInternal = $client->getClientInternal()) {
                $clientInternal = new ClientInternal();
                $clientInternal->setClient($client);
            }
            $this->populateClientInternal($clientInternal, $fileRow);

            $clientInternalCollection->add($clientInternal);
        }
    }

    /**
     * @param ClientInternal $clientInternal
     * @param FileRow $fileRow
     */
    private function populateClientInternal(
        ClientInternal $clientInternal,
        FileRow $fileRow
    ): void
    {
        $clientInternal->setTocFlag($fileRow->TOC_FLAG);
        $clientInternal->setBhFlag($fileRow->BH_FLAG);
    }
}
