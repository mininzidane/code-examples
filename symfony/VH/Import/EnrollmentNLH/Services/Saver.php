<?php
/**
 * Saver class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInsuranceMedicaidCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInsuranceMedicaidManualChangedCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInternalCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientMemoCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientStateDataCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\UserCollection;
use VirtualHealth\OrmBundle\Repository\ClientInternalRepository;
use VirtualHealth\OrmBundle\Saver\Client\ClientInsuranceMedicaidSaver;
use VirtualHealth\OrmBundle\Saver\Client\ClientMemoSaver;
use VirtualHealth\OrmBundle\Saver\Client\ClientStateDataSaver;

/**
 * Class Saver
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class Saver
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ClientSaver
     */
    private $clientSaver;

    /**
     * @var ClientMemoSaver
     */
    private $clientMemoSaver;

    /**
     * @var ClientStateDataSaver
     */
    private $clientStateDataSaver;

    /**
     * @var ClientInsuranceMedicaidSaver
     */
    private $clientInsuranceMedicaidSaver;

    /**
     * @var ClientInternalRepository
     */
    private $clientInternalRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var ClientCollection
     */
    private $clientCollection;

    /**
     * @var ClientMemoCollection
     */
    private $clientMemoCollection;

    /**
     * @var ClientStateDataCollection
     */
    private $clientStateDataCollection;

    /**
     * @var ClientInsuranceMedicaidCollection
     */
    private $clientInsuranceMedicaidCollection;

    /**
     * @var ClientInsuranceMedicaidManualChangedCollection
     */
    private $clientInsuranceMedicaidManualChangedCollection;

    /**
     * @var ClientInternalCollection
     */
    private $clientInternalCollection;

    /**
     * Saver constructor.
     * @param Connection $connection
     * @param ClientSaver $clientSaver
     * @param ClientMemoSaver $clientMemoSaver
     * @param ClientStateDataSaver $clientStateDataSaver
     * @param ClientInsuranceMedicaidSaver $clientInsuranceMedicaidSaver
     * @param ClientInternalRepository $clientInternalRepository
     * @param LoggerInterface $logger
     * @param EntityManager $entityManager
     */
    public function __construct(
        Connection $connection,
        ClientSaver $clientSaver,
        ClientMemoSaver $clientMemoSaver,
        ClientStateDataSaver $clientStateDataSaver,
        ClientInsuranceMedicaidSaver $clientInsuranceMedicaidSaver,
        ClientInternalRepository $clientInternalRepository,
        LoggerInterface $logger,
        EntityManager $entityManager
    )
    {
        $this->connection = $connection;
        $this->clientSaver = $clientSaver;
        $this->clientMemoSaver = $clientMemoSaver;
        $this->clientStateDataSaver = $clientStateDataSaver;
        $this->clientInsuranceMedicaidSaver = $clientInsuranceMedicaidSaver;
        $this->clientInternalRepository = $clientInternalRepository;
        $this->logger = $logger;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Throwable
     */
    public function batchSave(): void
    {
        $this->connection->beginTransaction();
        try {
            $this->clientSaver->save(
                $this->userCollection,
                $this->clientCollection
            );

            $this->clientMemoSaver->saveAll($this->clientMemoCollection->getValues());
            $this->clientStateDataSaver->saveAll($this->clientStateDataCollection->getValues());
            $this->clientInsuranceMedicaidSaver
                ->setOnClientInsuranceManualChangeEvent(false)
                ->saveAll($this->clientInsuranceMedicaidCollection->getValues())
            ;
            $this->clientInsuranceMedicaidSaver
                ->setOnClientInsuranceManualChangeEvent(true)
                ->saveAll($this->clientInsuranceMedicaidManualChangedCollection->getValues())
            ;
            $this->clientInternalRepository->save($this->clientInternalCollection->getValues());
            $this->connection->commit();

        } catch (\Throwable $e) {
            if ($this->connection->isTransactionActive()) {
                $this->connection->rollBack();
            }
            $this->logger->error("Error on saving enrollment import: {$e->getMessage()}");
            $this->entityManager->clear();

            throw $e;
        }
    }

    /**
     * @param UserCollection $userCollection
     * @return Saver
     */
    public function setUserCollection(UserCollection $userCollection): Saver
    {
        $this->userCollection = $userCollection;
        return $this;
    }

    /**
     * @param ClientCollection $clientCollection
     * @return Saver
     */
    public function setClientCollection(ClientCollection $clientCollection): Saver
    {
        $this->clientCollection = $clientCollection;
        return $this;
    }

    /**
     * @param ClientMemoCollection $clientMemoCollection
     * @return Saver
     */
    public function setClientMemoCollection(ClientMemoCollection $clientMemoCollection): Saver
    {
        $this->clientMemoCollection = $clientMemoCollection;
        return $this;
    }

    /**
     * @param ClientStateDataCollection $clientStateDataCollection
     * @return Saver
     */
    public function setClientStateDataCollection(ClientStateDataCollection $clientStateDataCollection): Saver
    {
        $this->clientStateDataCollection = $clientStateDataCollection;
        return $this;
    }

    /**
     * @param ClientInsuranceMedicaidCollection $clientInsuranceMedicaidCollection
     * @return Saver
     */
    public function setClientInsuranceMedicaidCollection(ClientInsuranceMedicaidCollection $clientInsuranceMedicaidCollection): Saver
    {
        $this->clientInsuranceMedicaidCollection = $clientInsuranceMedicaidCollection;
        return $this;
    }

    /**
     * @param ClientInsuranceMedicaidManualChangedCollection $clientInsuranceMedicaidManualChangedCollection
     * @return Saver
     */
    public function setClientInsuranceMedicaidManualChangedCollection(ClientInsuranceMedicaidManualChangedCollection $clientInsuranceMedicaidManualChangedCollection): Saver
    {
        $this->clientInsuranceMedicaidManualChangedCollection = $clientInsuranceMedicaidManualChangedCollection;
        return $this;
    }

    /**
     * @param ClientInternalCollection $clientInternalCollection
     * @return Saver
     */
    public function setClientInternalCollection(ClientInternalCollection $clientInternalCollection): Saver
    {
        $this->clientInternalCollection = $clientInternalCollection;
        return $this;
    }
}
