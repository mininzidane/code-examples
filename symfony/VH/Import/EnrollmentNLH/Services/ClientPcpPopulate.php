<?php
/**
 * ClientPcpPopulate class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use application\components\helpers\MemoPadHelper;
use application\modules\dataWarehouse\parsers\enrollment\FileParserLog as Log;
use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use Psr\Log\LoggerInterface;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientMemoCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Memo\CareTeamLinksDataGetter;
use VirtualHealth\OrmBundle\Entity\Careprovider;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\ClientMemo;
use VirtualHealth\OrmBundle\Entity\Roles;
use VirtualHealth\OrmBundle\Query\CareTeam\FindAllMemberIdsByRoleName;
use VirtualHealth\OrmBundle\Repository\CareproviderRepository;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class ClientPcpPopulate
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientPcpPopulate
{
    /**
     * @var CareproviderRepository
     */
    private $careProviderRepository;

    /**
     * @var Careprovider[]
     */
    private $careProvidersByFileRowHash;

    /**
     * @var FindAllMemberIdsByRoleName
     */
    private $findAllMemberIdsByRoleName;

    /**
     * @var \PmModule
     */
    private $pmModule;

    /**
     * @var CommonUsersGetter
     */
    private $commonUsersGetter;

    /**
     * @var array
     */
    private $careProvidersByClients;

    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CareTeamLinksDataGetter
     */
    private $careTeamLinksDataGetter;

    /**
     * ClientPcpPopulate constructor.
     * @param CareproviderRepository $careProviderRepository
     * @param FindAllMemberIdsByRoleName $findAllMemberIdsByRoleName
     * @param CommonUsersGetter $commonUsersGetter
     * @param RowDataManipulator $rowDataManipulator
     * @param LoggerInterface $logger
     * @param CareTeamLinksDataGetter $careTeamLinksDataGetter
     */
    public function __construct(
        CareproviderRepository $careProviderRepository,
        FindAllMemberIdsByRoleName $findAllMemberIdsByRoleName,
        CommonUsersGetter $commonUsersGetter,
        RowDataManipulator $rowDataManipulator,
        LoggerInterface $logger,
        CareTeamLinksDataGetter $careTeamLinksDataGetter
    )
    {
        $this->careProviderRepository = $careProviderRepository;
        $this->findAllMemberIdsByRoleName = $findAllMemberIdsByRoleName;
        $this->commonUsersGetter = $commonUsersGetter;
        $this->rowDataManipulator = $rowDataManipulator;
        $this->pmModule = mod('pm');
        $this->logger = $logger;
        $this->careTeamLinksDataGetter = $careTeamLinksDataGetter;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     * @param ClientMemoCollection $clientMemoCollection
     * @throws \CException
     * @throws \CHttpException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function batchPopulate(
        FileRowCollection $fileRowCollection,
        ClientCollection $clientCollection,
        ClientMemoCollection $clientMemoCollection
    ): void
    {
        $this->collectCareProviders($fileRowCollection);

        foreach ($fileRowCollection->getNewClientFileRows() as $fileRow) {
            $client = $clientCollection->findNewClient($fileRow);

            // if client is new, $client->getPcp() always returns null (if we did not set PCP earlier.
            // But in old code we have such condition `if ($this->row->PRAC_NPI && !$this->client->pcp) {`
            if ($fileRow->PRAC_NPI && !$client->getPcp()) {
                $careProvider = $this->careProvidersByFileRowHash[\spl_object_hash($fileRow)] ?? null;
                if ($careProvider !== null) {
                    $client->setPcp($careProvider);
                }
            }
        }

        $this->careProvidersByClients = [];
        $clientIds = [];
        $fileRows = [];
        $oldPcps = [];
        foreach ($fileRowCollection->getExistingClientFileRows() as $fileRow) {
            $client = $clientCollection->findExistingClient($fileRow);
            if ($client === null) {
                continue;
            }

            $careProvider = $this->careProvidersByFileRowHash[\spl_object_hash($fileRow)] ?? null;
            if ($careProvider === null) {
                continue;
            }

            $oldPcp = $client->getPcp();
            if ($oldPcp !== null && $oldPcp->getId() === $careProvider->getId()) {
                continue;
            }

            if ($oldPcp !== null) {
                $oldPcps[\spl_object_hash($fileRow)] = $oldPcp;
            }

            $client->setPcp($careProvider);
            $clientIds[] = $client->getUser()->getId();
            $fileRows[] = $fileRow;
        }

        $this->batchSendPmToPrimaryCareTeamMember($clientIds, $fileRows, $clientCollection);
        $this->batchCreateCommonCaseNotes($fileRows, $clientCollection, $clientMemoCollection, $oldPcps);
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @throws \CException
     */
    private function collectCareProviders(FileRowCollection $fileRowCollection): void
    {
        $npiByHash = [];
        foreach ($fileRowCollection->getValues() as $fileRow) {
            if ($fileRow->PRAC_NPI) {
                $npiByHash[\spl_object_hash($fileRow)] = $fileRow->PRAC_NPI;
            }
        }
        $data = $this->careProviderRepository->findBy([
            'npi' => $npiByHash,
        ]);
        // collect found careproviders
        $careProviderByNpi = [];
        foreach ($data as $careProvider) {
            $careProviderByNpi[$careProvider->getNpi()] = $careProvider;
        }
        // add non critical errors
        foreach ($fileRowCollection->getValues() as $fileRow) {
            $hash = \spl_object_hash($fileRow);
            $npi = $npiByHash[$hash] ?? null;
            if ($npi !== null && ($careProviderByNpi[$npi] ?? false)) {
                $this->careProvidersByFileRowHash[$hash] = $careProviderByNpi[$npi];
            } else {
                $this->rowDataManipulator->nonCriticalError(Log::WARNING_FIND_PROVIDER_BY_PRAC_NPI, $fileRow);
                $fileRow->getParser()->getLog()->addNotMatchedPracNpiClientNames($this->rowDataManipulator->getClientFullname($fileRow));
            }
        }
    }

    /**
     * @param int[] $clientIds
     * @param FileRow[] $fileRows
     * @param ClientCollection $clientCollection
     * @throws \CHttpException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function batchSendPmToPrimaryCareTeamMember(array $clientIds, array $fileRows, ClientCollection $clientCollection): void
    {
        $userIds = $this->findAllMemberIdsByRoleName->read($clientIds, Roles::NAME_PRIMARY);
        foreach ($fileRows as $fileRow) {
            /** @var Client $client */
            $client = $clientCollection->findExistingClient($fileRow);
            if ($client === null) {
                continue;
            }

            $userId = $userIds[$client->getUser()->getId()] ?? null;
            if ($userId === null) {
                continue;
            }

            $message = new \PersonalMessage();
            $message->sender_id = $this->commonUsersGetter->getSystemSuperAdmin()->getId();
            $message->recipient_id = $userId;
            $message->subject = t('PMessage', 'Member PCP Updated');
            $message->text = t(
                'PMessage',
                'The provider assignment was updated to {provider}'
                . ' for member {client} with RIN {medicaid} on {date}'
                . ' (available <a href="{url}">LINK</a>).',
                [
                    '{provider}' => $client->getPcp()->getFullName(),
                    '{client}' => $this->rowDataManipulator->getClientFullname($fileRow),
                    '{medicaid}' => $this->rowDataManipulator->getMedicaid($fileRow),
                    '{date}' => (new \DateTime())->format('m/d/Y'),
                    '{url}' => '/clients/clientUpdate/' . $userId,
                ]
            );
            if (!$this->pmModule->send($message)) {
                $this->logger->error("Can not send personal message to user #{$userId} with text: '{$message->text}'");
            }
        }
    }

    /**
     * @param FileRow[] $fileRows
     * @param ClientCollection $clientCollection
     * @param ClientMemoCollection $clientMemoCollection
     * @param Careprovider[] $oldPcps
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function batchCreateCommonCaseNotes(
        array $fileRows,
        ClientCollection $clientCollection,
        ClientMemoCollection $clientMemoCollection,
        array $oldPcps
    ): void
    {
        foreach ($fileRows as $fileRow) {
            /** @var Client $client */
            $client = $clientCollection->findExistingClient($fileRow);
            if ($client === null) {
                continue;
            }

            $oldPcp = $oldPcps[\spl_object_hash($fileRow)] ?? null;
            if ($oldPcp === null) {
                continue;
            }

            $clientMemo = (new ClientMemo())
                ->setClient($client)
                ->setSourceType(ClientMemo::TYPE_CARE_TEAM)
                ->setMemo(
                    t('General', 'PCP Updated from {old} to {new} on {date}', [
                        '{old}' => $oldPcp->getFullName(),
                        '{new}' => $client->getPcp()->getFullName(),
                        '{date}' => (new \DateTime())->format('m/d/Y g:i a'),
                    ])
                )
                ->setLinksData($this->careTeamLinksDataGetter->get($client->getUser()->getId()))
                ->setOwner($this->commonUsersGetter->getSystemSuperAdmin())
            ;
            $clientMemoCollection->add($clientMemo);
        }
    }
}
