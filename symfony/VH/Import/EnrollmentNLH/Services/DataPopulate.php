<?php
/**
 * DataPopulate class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInfoCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInsuranceMedicaidCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInsuranceMedicaidManualChangedCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInternalCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientMemoCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientStateDataCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;

/**
 * Class DataPopulate
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class DataPopulate
{
    /**
     * @var ClientDataPopulate
     */
    private $clientDataPopulate;

    /**
     * @var ClientStateDataPopulate
     */
    private $clientsStateDataPopulate;

    /**
     * @var ClientInsuranceMedicaidPopulate
     */
    private $clientInsuranceMedicaidPopulate;

    /**
     * @var ClientPcpPopulate
     */
    private $clientPcpPopulate;

    /**
     * @var ClientInternalPopulate
     */
    private $clientInternalPopulate;

    /**
     * @var ClientCollection
     */
    private $clientCollection;

    /**
     * @var ClientMemoCollection
     */
    private $clientMemoCollection;

    /**
     * @var ClientInfoCollection
     */
    private $clientInfoCollection;

    /**
     * @var ClientStateDataCollection
     */
    private $clientStateDataCollection;

    /**
     * @var ClientInsuranceMedicaidCollection
     */
    private $clientInsuranceMedicaidCollection;

    /**
     * @var ClientInsuranceMedicaidManualChangedCollection
     */
    private $clientInsuranceMedicaidManualChangedCollection;

    /**
     * @var ClientInternalCollection
     */
    private $clientInternalCollection;

    /**
     * DataPopulate constructor.
     * @param ClientDataPopulate $clientDataPopulate
     * @param ClientStateDataPopulate $clientsStateDataPopulate
     * @param ClientInsuranceMedicaidPopulate $clientInsuranceMedicaidPopulate
     * @param ClientPcpPopulate $clientPcpPopulate
     * @param ClientInternalPopulate $clientInternalPopulate
     */
    public function __construct(
        ClientDataPopulate $clientDataPopulate,
        ClientStateDataPopulate $clientsStateDataPopulate,
        ClientInsuranceMedicaidPopulate $clientInsuranceMedicaidPopulate,
        ClientPcpPopulate $clientPcpPopulate,
        ClientInternalPopulate $clientInternalPopulate
    )
    {
        $this->clientDataPopulate = $clientDataPopulate;
        $this->clientsStateDataPopulate = $clientsStateDataPopulate;
        $this->clientInsuranceMedicaidPopulate = $clientInsuranceMedicaidPopulate;
        $this->clientPcpPopulate = $clientPcpPopulate;
        $this->clientInternalPopulate = $clientInternalPopulate;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @throws \CException
     * @throws \CHttpException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \application\modules\dataWarehouse\parsers\enrollment\FileRowServiceException
     */
    public function populate(FileRowCollection $fileRowCollection): void
    {
        $this->clientDataPopulate->batchPopulate(
            $fileRowCollection,
            $this->clientCollection,
            $this->clientMemoCollection,
            $this->clientInfoCollection
        );
        $this->clientsStateDataPopulate->batchPopulate(
            $fileRowCollection,
            $this->clientCollection,
            $this->clientStateDataCollection
        );
        $this->clientInsuranceMedicaidPopulate->batchPopulate(
            $fileRowCollection,
            $this->clientCollection,
            $this->clientInsuranceMedicaidCollection,
            $this->clientInsuranceMedicaidManualChangedCollection,
            $this->clientInfoCollection
        );
        $this->clientPcpPopulate->batchPopulate(
            $fileRowCollection,
            $this->clientCollection,
            $this->clientMemoCollection
        );
        $this->clientInternalPopulate->batchPopulate(
            $fileRowCollection,
            $this->clientCollection,
            $this->clientInternalCollection
        );
    }

    /**
     * @param ClientCollection $clientCollection
     * @return DataPopulate
     */
    public function setClientCollection(ClientCollection $clientCollection): DataPopulate
    {
        $this->clientCollection = $clientCollection;
        return $this;
    }

    /**
     * @param ClientMemoCollection $clientMemoCollection
     * @return DataPopulate
     */
    public function setClientMemoCollection(ClientMemoCollection $clientMemoCollection): DataPopulate
    {
        $this->clientMemoCollection = $clientMemoCollection;
        return $this;
    }

    /**
     * @param ClientInfoCollection $clientInfoCollection
     * @return DataPopulate
     */
    public function setClientInfoCollection(ClientInfoCollection $clientInfoCollection): DataPopulate
    {
        $this->clientInfoCollection = $clientInfoCollection;
        return $this;
    }

    /**
     * @param ClientStateDataCollection $clientStateDataCollection
     * @return DataPopulate
     */
    public function setClientStateDataCollection(ClientStateDataCollection $clientStateDataCollection): DataPopulate
    {
        $this->clientStateDataCollection = $clientStateDataCollection;
        return $this;
    }

    /**
     * @param ClientInsuranceMedicaidCollection $clientInsuranceMedicaidCollection
     * @return DataPopulate
     */
    public function setClientInsuranceMedicaidCollection(ClientInsuranceMedicaidCollection $clientInsuranceMedicaidCollection): DataPopulate
    {
        $this->clientInsuranceMedicaidCollection = $clientInsuranceMedicaidCollection;
        return $this;
    }

    /**
     * @param ClientInsuranceMedicaidManualChangedCollection $clientInsuranceMedicaidManualChangedCollection
     * @return DataPopulate
     */
    public function setClientInsuranceMedicaidManualChangedCollection(ClientInsuranceMedicaidManualChangedCollection $clientInsuranceMedicaidManualChangedCollection): DataPopulate
    {
        $this->clientInsuranceMedicaidManualChangedCollection = $clientInsuranceMedicaidManualChangedCollection;
        return $this;
    }

    /**
     * @param ClientInternalCollection $clientInternalCollection
     * @return DataPopulate
     */
    public function setClientInternalCollection(ClientInternalCollection $clientInternalCollection): DataPopulate
    {
        $this->clientInternalCollection = $clientInternalCollection;
        return $this;
    }
}
