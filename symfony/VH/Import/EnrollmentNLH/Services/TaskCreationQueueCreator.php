<?php
/**
 * TaskCreationQueueCreator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use Carbon\Carbon;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Query\EnrollmentTasksQueueBatchInsert;

/**
 * Class TaskCreationQueueCreator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class TaskCreationQueueCreator
{
    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var EnrollmentTasksQueueBatchInsert
     */
    private $enrollmentTasksQueueBatchInsert;

    /**
     * TaskCreationQueueCreator constructor.
     * @param RowDataManipulator $rowDataManipulator
     * @param EnrollmentTasksQueueBatchInsert $enrollmentTasksQueueBatchInsert
     */
    public function __construct(
        RowDataManipulator $rowDataManipulator,
        EnrollmentTasksQueueBatchInsert $enrollmentTasksQueueBatchInsert
    )
    {
        $this->rowDataManipulator = $rowDataManipulator;
        $this->enrollmentTasksQueueBatchInsert = $enrollmentTasksQueueBatchInsert;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     * @throws \CException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function create(FileRowCollection $fileRowCollection, ClientCollection $clientCollection): void
    {
        $dataToInsert = [];
        // This a newly created enrolled user. As of NLH-842 tasks should be created after assignment and termination processes
        foreach ($fileRowCollection->getNewClientFileRows() as $fileRow) {
            $client = $clientCollection->findNewClient($fileRow);

            $clientTerminationDate = $this->rowDataManipulator->getDate($fileRow, 'ELIG_TERM_DT');
            if ($clientTerminationDate === null || Carbon::parse($clientTerminationDate->format('Y-m-d'))->isFuture()) {
                $dataToInsert[] = [
                    $client->getUser()->getId(),
                    $fileRow->LTC_FLAG,
                ];
            }
        }
        $this->enrollmentTasksQueueBatchInsert->write($dataToInsert);
    }
}
