<?php
/**
 * RegionFinder class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location;

use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\RowDataManipulator;
use VirtualHealth\OrmBundle\Entity\Country;
use VirtualHealth\OrmBundle\Entity\Region;
use VirtualHealth\OrmBundle\Repository\RegionRepository;

/**
 * Class RegionFinder
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location
 */
class RegionFinder
{
    /**
     * @var int Country ID.
     */
    public const DEFAULT_COUNTRY_ID = 254;

    /**
     * @var Region[]
     */
    private $allRegions;

    /**
     * @var Region[]
     */
    private $regionByCode = [];

    /**
     * @var FileRowCollection
     */
    private $fileRowCollection;

    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var RegionRepository
     */
    private $regionRepository;

    /**
     * @var string
     */
    private $defaultState;

    /**
     * RegionFinder constructor.
     * @param RowDataManipulator $rowDataManipulator
     * @param RegionRepository $regionRepository
     * @param string $defaultState
     */
    public function __construct(
        RowDataManipulator $rowDataManipulator,
        RegionRepository $regionRepository,
        string $defaultState
    )
    {
        $this->rowDataManipulator = $rowDataManipulator;
        $this->regionRepository = $regionRepository;
        $this->defaultState = $defaultState;
    }

    /**
     * @param string $code
     * @return Region|null
     */
    public function findByCode(string $code): ?Region
    {
        return $this->regionByCode[$code] ?? null;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @return RegionFinder
     */
    public function setFileRowCollection(FileRowCollection $fileRowCollection): RegionFinder
    {
        $this->fileRowCollection = $fileRowCollection;
        $this->prepareMapping();
        return $this;
    }

    private function prepareMapping(): void
    {
        foreach ($this->fileRowCollection->getValues() as $fileRow) {
            $code = $fileRow->STATE;

            if (!\array_key_exists($code, $this->regionByCode)) {
                $this->regionByCode[$code] = $this->findRegion($code);
            }

            if ($this->regionByCode[$code] === null) {
                $this->regionByCode[$code] = $this->findRegion($this->defaultState);

                $this->rowDataManipulator->warningUnknownValue($fileRow, 'STATE');
            }
        }
    }

    /**
     * @param string $code
     * @return Region|null
     * @throws \Doctrine\ORM\ORMException
     */
    private function findRegion(string $code): ?Region
    {
        if ($this->allRegions === null) {
            $this->allRegions = $this->regionRepository->findBy([
                'country' => $this->regionRepository->getEntityReference(Country::class, self::DEFAULT_COUNTRY_ID)
            ]);
        }

        if (empty($this->allRegions)) {
            return null;
        }

        foreach ($this->allRegions as $region) {
            if (!\strcasecmp($region->getCode(), $code)) {
                return $region;
            }
        }

        return null;
    }
}
