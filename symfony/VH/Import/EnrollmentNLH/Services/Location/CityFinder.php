<?php
/**
 * CityFinder class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location;

use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\RowDataManipulator;
use VirtualHealth\OrmBundle\Entity\City;
use VirtualHealth\OrmBundle\Repository\CityRepository;

/**
 * Class CityFinder
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location
 */
class CityFinder
{
    /**
     * @var string Value for unknown city.
     */
    private const DEFAULT_CITY = 'Unknown';

    /**
     * @var array additional city mapping https://bug.virtualhealth.com/issue/NLH-583
     */
    private static $additionalCityMapping = [
        'CHICAGO HTS' => 'Chicago Heights',
        'CNTRY CLB HLS' => 'Country Club Hills',
        'EVERGREEN PK' => 'Evergreen Park',
        'N RIVERSIDE' => 'North Riverside',
        'PROSPECT HTS' => 'Prospect Heights',
        'ROLLING MDWS' => 'Rolling Meadows',
        'S CHICAGO HEI' => 'South Chicago Heights',
        'S CHICAGO HEIGHTS' => 'South Chicago Heights',
        'ST CHARLES' => 'Saint Charles',
        'WILLOW SPGS' => 'Willow Springs',
    ];

    /**
     * Unknown city id
     * @var City
     */
    private $foundedUnknownCity;

    /**
     * @var RegionFinder
     */
    private $regionFinder;

    /**
     * @var array [regionCode => City[], ...]
     */
    private $allCitiesByRegionCode = [];

    /**
     * @var array [title => id]
     */
    private $cityTitleToId = [];

    /**
     * @var FileRowCollection
     */
    private $fileRowCollection;

    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var CityRepository
     */
    private $cityRepository;

    /**
     * CityFinder constructor.
     * @param RegionFinder $regionFinder
     * @param RowDataManipulator $rowDataManipulator
     * @param CityRepository $cityRepository
     */
    public function __construct(
        RegionFinder $regionFinder,
        RowDataManipulator $rowDataManipulator,
        CityRepository $cityRepository
    )
    {
        $this->regionFinder = $regionFinder;
        $this->rowDataManipulator = $rowDataManipulator;
        $this->cityRepository = $cityRepository;
    }

    /**
     * @param string $cityTitle
     * @param string $regionCode
     * @return City|null
     */
    public function findByTitleAndRegionCode(string $cityTitle, string $regionCode): ?City
    {
        return $this->cityTitleToId[$regionCode][self::$additionalCityMapping[$cityTitle] ?? $cityTitle] ?? null;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @return CityFinder
     */
    public function setFileRowCollection(FileRowCollection $fileRowCollection): CityFinder
    {
        $this->fileRowCollection = $fileRowCollection;
        $this->prepareMapping();
        return $this;
    }

    private function prepareMapping(): void
    {
        foreach ($this->fileRowCollection->getValues() as $fileRow) {
            $cityTitle = $fileRow->CITY;
            $regionCode = $fileRow->STATE;

            // $this->cityTitleToId[$regionCode][$cityTitle] may be null after previous iteration, so do not make extra search
            $citiesForRegion = $this->cityTitleToId[$regionCode] ?? [];
            if (!\array_key_exists($cityTitle, $citiesForRegion)) {
                $upperCaseCity = \mb_strtoupper($cityTitle);
                if (\array_key_exists($upperCaseCity, self::$additionalCityMapping)) {
                    $cityTitle = self::$additionalCityMapping[$upperCaseCity];
                }

                $this->cityTitleToId[$regionCode][$cityTitle] = $this->findCity($cityTitle, $regionCode);
            }

            if ($this->cityTitleToId[$regionCode][$cityTitle] === null) {
                if ($this->foundedUnknownCity === null) {
                    $this->foundedUnknownCity = $this->findCity(self::DEFAULT_CITY, $regionCode);
                }

                $this->cityTitleToId[$regionCode][$cityTitle] = $this->foundedUnknownCity;
                $this->rowDataManipulator->warningUnknownValue($fileRow, 'CITY');
            }
        }
    }

    /**
     * @param string $cityTitle
     * @param string $regionCode
     * @return City|null
     */
    private function findCity(string $cityTitle, string $regionCode): ?City
    {
        if (!\array_key_exists($regionCode, $this->allCitiesByRegionCode)) {
            $this->allCitiesByRegionCode[$regionCode] = $this->cityRepository->findBy(['region' => $this->regionFinder->findByCode($regionCode)]);
        }

        if (empty($this->allCitiesByRegionCode[$regionCode] ?? [])) {
            return null;
        }

        /** @var City $city */
        foreach ($this->allCitiesByRegionCode[$regionCode] as $city) {
            if (!\strcasecmp($city->getTitle(), $cityTitle)) {
                return $city;
            }
        }

        return null;
    }

    /**
     * @return City|null
     */
    public function getFoundedUnknownCity(): ?City
    {
        return $this->foundedUnknownCity;
    }
}
