<?php
/**
 * ClientStateDataPopulate class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use Doctrine\ORM\EntityManager;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientStateDataCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location\CityFinder;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location\RegionFinder;
use VirtualHealth\OrmBundle\Entity\City;
use VirtualHealth\OrmBundle\Entity\ClientStateData;
use VirtualHealth\OrmBundle\Entity\Region;

/**
 * Class ClientStateDataPopulate
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientStateDataPopulate
{
    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var CityFinder
     */
    private $cityFinder;

    /**
     * @var RegionFinder
     */
    private $regionFinder;

    /**
     * ClientStateDataPopulate constructor.
     * @param RowDataManipulator $rowDataManipulator
     * @param EntityManager $entityManager
     * @param CityFinder $cityFinder
     * @param RegionFinder $regionFinder
     */
    public function __construct(
        RowDataManipulator $rowDataManipulator,
        EntityManager $entityManager,
        CityFinder $cityFinder,
        RegionFinder $regionFinder
    )
    {
        $this->rowDataManipulator = $rowDataManipulator;
        $this->entityManager = $entityManager;
        $this->cityFinder = $cityFinder;
        $this->regionFinder = $regionFinder;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     * @param ClientStateDataCollection $clientStateDataCollection
     * @throws \Doctrine\ORM\ORMException
     */
    public function batchPopulate(
        FileRowCollection $fileRowCollection,
        ClientCollection $clientCollection,
        ClientStateDataCollection $clientStateDataCollection
    ): void
    {
        foreach ($fileRowCollection->getNewClientFileRows() as $fileRow) {
            $client = $clientCollection->findNewClient($fileRow);

            $clientStateData = new ClientStateData();
            $clientStateData->setClient($client);
            $this->populateClientStateData($clientStateData, $fileRow);
            $clientStateDataCollection->add($clientStateData);
        }

        foreach ($fileRowCollection->getExistingClientFileRows() as $fileRow) {
            $client = $clientCollection->findExistingClient($fileRow);
            if ($client === null) {
                continue;
            }

            if (!$clientStateData = $client->getClientStateData()) {
                $clientStateData = new ClientStateData();
                $clientStateData->setClient($client);
            }
            $this->populateClientStateData($clientStateData, $fileRow);
            $clientStateDataCollection->add($clientStateData);
        }
    }

    /**
     * @param ClientStateData $clientStateData
     * @param FileRow $fileRow
     * @throws \Doctrine\ORM\ORMException
     */
    private function populateClientStateData(ClientStateData $clientStateData, FileRow $fileRow): void
    {
        if ($value = $fileRow->FIRST_NAME) {
            $clientStateData->setFirstName($value);
        }

        if ($value = $fileRow->LAST_NAME) {
            $clientStateData->setLastName($value);
        }

        if ($value = $this->rowDataManipulator->getDate($fileRow, 'DOB')) {
            $clientStateData->setBirthDate($value->format('m/d/Y'));
        }

        if ($value = $fileRow->ADDRESS1) {
            $clientStateData->setAddress1($value);
        }

        if ($value = $fileRow->HFS_MEMBER_NBR) {
            $clientStateData->setMedicaidNumber($value);
        }

        if ($value = $this->rowDataManipulator->getFormattedGender($fileRow)) {
            $clientStateData->setGender($value);
        }

        if ($value = $fileRow->ADDRESS2) {
            $clientStateData->setAddress2($value);
        }

        if ($value = $this->cityFinder->findByTitleAndRegionCode($fileRow->CITY, $fileRow->STATE)) {
            $clientStateData->setCity($value);

            $foundedUnknownCityId = $this->cityFinder->getFoundedUnknownCity() !== null ? $this->cityFinder->getFoundedUnknownCity()->getId() : null;
            if ($clientStateData->getCity()->getId() === $foundedUnknownCityId) {
                $clientStateData->setUnknownCity(\mb_convert_case($fileRow->CITY, MB_CASE_TITLE, 'UTF-8'));
            }
        }

        if ($value = $this->regionFinder->findByCode($fileRow->STATE)) {
            $clientStateData->setRegion($value);
        }

        if ($value = $fileRow->ZIP) {
            $clientStateData->setPostalCode($value);
        }

        if ($value = $this->rowDataManipulator->getFormattedPhone($fileRow->PHONE)) {
            $clientStateData->setPrimaryPhone($value);
        }

        if ($value = $fileRow->PRAC_NPI) {
            $clientStateData->setProviderNpi($value);
        }
    }
}
