<?php
/**
 * RiskCleaner class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses;

use Doctrine\DBAL\Connection;

/**
 * Class RiskCleaner
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses
 */
class RiskCleaner
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * RiskCleaner constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $riskNamesByClientId
     * @throws \Doctrine\DBAL\DBALException
     */
    public function deleteEntities(array $riskNamesByClientId): void
    {
        $sqlDeletes = [];
        foreach ($riskNamesByClientId as $clientId => $riskNames) {
            $sqlDelete = <<<SQL
DELETE FROM `risk` WHERE `client_id` = {$clientId} AND `name` IN ('%s');
SQL;
            if (!empty($riskNames)) {
                $sqlDeletes[] = \sprintf($sqlDelete, \implode('\', \'', $riskNames));
            }
        }
        if (!empty($sqlDeletes)) {
            $this->connection->exec(\implode('', $sqlDeletes));
        }
    }
}
