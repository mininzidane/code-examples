<?php
/**
 * CareProviderIdsGetter class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses;

use Doctrine\DBAL\Connection;
use VirtualHealth\OrmBundle\Entity\Careprovider;

/**
 * Class CareProviderIdsGetter
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses
 */
class CareProviderIdsGetter
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * CareProviderIdsGetter constructor.
     * @param Connection $connection
     */
    public function __construct(
        Connection $connection
    )
    {
        $this->connection = $connection;
    }

    /**
     * @param array $clientDataByClientId CP data indexed by client id
     * @return array cp ids indexed by client id based on cp data
     */
    public function getByClientId(array $clientDataByClientId): array
    {
        if (empty($clientDataByClientId)) {
            return [];
        }

        $qb = $this->connection->createQueryBuilder();
        $qb
            ->select('cp.id', 'cp.npi', 'cp.tax_id')
            ->from('careprovider', 'cp')
        ;
        $orConditions = [];
        $clientIdsByNpi = [];
        $clientIdsByTin = [];
        $result = [];
        foreach ($clientDataByClientId as $clientId => $clientData) {
            $cpData = $clientData['careproviderData'] ?? false;

            if (!$cpData) {
                continue;
            }

            if (!$cpData['npi'] && !$cpData['tin']) {
                continue;
            }

            if ($cpData['npi']) {
                $orConditions[] = $qb->expr()->andX(
                    $qb->expr()->eq('cp.npi', $qb->expr()->literal($cpData['npi'])),
                    $qb->expr()->eq('cp.profile_type', Careprovider::PROFILE_INDIVIDUAL)
                );
                $clientIdsByNpi[$cpData['npi']][] = $clientId;
            }
            if ($cpData['tin']) {
                $orConditions[] = $qb->expr()->andX(
                    $qb->expr()->eq('cp.tax_id', $qb->expr()->literal($cpData['tin'])),
                    $qb->expr()->eq('cp.profile_type', Careprovider::PROFILE_GROUP)
                );
                $clientIdsByTin[$cpData['tin']][] = $clientId;
            }
        }
        $qb->where(
            $qb->expr()->orX(...$orConditions)
        );
        $data = $qb->execute()->fetchAll();
        foreach ($data as $row) {
            if (\array_key_exists($row['npi'], $clientIdsByNpi)) {
                foreach ($clientIdsByNpi[$row['npi']] as $clientId) {
                    $result[$clientId][] = $row['id'];
                }
            }
            if (\array_key_exists($row['tax_id'], $clientIdsByTin)) {
                foreach ($clientIdsByTin[$row['tax_id']] as $clientId) {
                    $result[$clientId][] = $row['id'];
                }
            }
        }

        return $result;
    }
}
