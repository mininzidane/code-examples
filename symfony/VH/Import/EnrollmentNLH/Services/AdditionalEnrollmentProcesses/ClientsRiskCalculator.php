<?php
/**
 * ClientsRiskCalculator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses;

use application\components\risk\AssignedRiskCalculator;
use application\components\risk\BaseRiskCalculator;
use application\components\risk\DiagnosisRiskCalculator;
use application\components\risk\MedicareRiskCalculator;
use application\components\risk\ScreeningRiskCalculator;
use application\components\risk\ScreeningTskRiskCalculator;

/**
 * Class ClientsRiskCalculator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientsRiskCalculator
{
    /**
     * @var RiskCreator
     */
    private $riskCreator;

    /**
     * @var RiskCleaner
     */
    private $riskCleaner;

    /**
     * @var \Client[]
     */
    private $clients;

    private const RISK_TYPES = [
        RISK_TYPE_MEDICARE => MedicareRiskCalculator::class,
        RISK_TYPE_DIAGNOSIS => DiagnosisRiskCalculator::class,
        RISK_TYPE_SCREENING => ScreeningRiskCalculator::class,
        RISK_TYPE_SCREENING_TSK => ScreeningTskRiskCalculator::class,
        RISK_TYPE_ASSIGNED => AssignedRiskCalculator::class,
    ];

    /**
     * ClientsRiskCalculator constructor.
     * @param RiskCreator $riskCreator
     * @param RiskCleaner $riskCleaner
     */
    public function __construct(
        RiskCreator $riskCreator,
        RiskCleaner $riskCleaner
    )
    {
        $this->riskCreator = $riskCreator;
        $this->riskCleaner = $riskCleaner;
    }

    /**
     * @param \Client[] $clients
     * @throws \Doctrine\DBAL\DBALException
     */
    public function calculate(array $clients): void
    {
        if (empty($clients)) {
            return;
        }

        $this->clients = $clients;
        $allowedCalculatorClassNames = [];
        foreach (self::RISK_TYPES as $riskType => $calculatorClassName) {
            if (\Risk::isTypeEnabled($riskType)) {
                $allowedCalculatorClassNames[] = $calculatorClassName;
            }
        }
        $this->bulkSaveRisk($allowedCalculatorClassNames);
    }

    /**
     * @param array $calculatorClassNameList
     * @throws \Doctrine\DBAL\DBALException
     */
    private function bulkSaveRisk(array $calculatorClassNameList): void
    {
        $risksToDelete = [];
        $risksToCreate = [];
        foreach ($this->clients as $client) {
            foreach ($calculatorClassNameList as $calculatorClassName) {
                /* @var BaseRiskCalculator $calculator */
                $calculator = new $calculatorClassName($client);

                // For some clients we can forbid manual recalculation, depending on some conditions
                // @see AssignedRiskCalculator
                if (!$calculator->allowManualRecalculation()) {
                    continue;
                }

                // Delete old risks
                $risksToDelete[$client->user_id][] = $calculator->getRiskName();

                // Check the possibility of calculating the value of risk.
                // If the risk should not be calculated for the current client,
                // then we normally finish the work of the method.
                if (!$calculator->validate()) {
                    continue;
                }
                // Get the calculated risk value and its description.
                // And save everything in the database to assign to the user.
                $risksToCreate[$client->user_id][] = [
                    $calculator->getRiskName(),
                    $calculator->getRiskWeight(),
                    $calculator->getRiskDetails(),
                ];
            }
        }

        $this->riskCleaner->deleteEntities($risksToDelete);
        $this->riskCreator->save($risksToCreate);
    }
}
