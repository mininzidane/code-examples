<?php
/**
 * ClientPcpAssociateCreator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses;

use Doctrine\DBAL\Connection;

/**
 * Class ClientPcpAssociateCreator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses
 */
class ClientPcpAssociateCreator
{
    private const BATCH_INSERT_SIZE = 100;

    /**
     * @var CareProviderIdsGetter
     */
    private $careProviderIdsGetter;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * ClientPcpAssociateCreator constructor.
     * @param CareProviderIdsGetter $careProviderIdsGetter
     * @param Connection $connection
     */
    public function __construct(
        CareProviderIdsGetter $careProviderIdsGetter,
        Connection $connection
    )
    {
        $this->careProviderIdsGetter = $careProviderIdsGetter;
        $this->connection = $connection;
    }

    /**
     * @param array $clients
     * @param array $clientDataByUserId
     * @throws \Doctrine\DBAL\DBALException
     */
    public function associate(array $clients, array $clientDataByUserId): void
    {
        if (empty($clients)) {
            return;
        }

        $careProviderIdsByClientId = $this->careProviderIdsGetter->getByClientId($clientDataByUserId);

        $sqlInsert = 'INSERT IGNORE INTO `client_careprovider` (`client_id`, `careprovider_id`) VALUES %s;';
        $sqlInsertValues = [];
        foreach ($clients as $client) {
            if ($client->pcp) {
                $sqlInsertValues[] = "({$client->user_id}, {$client->pcp})";
            }
            foreach ($careProviderIdsByClientId[$client->user_id] ?? [] as $careProviderId) {
                if ($careProviderId === $client->pcp) {
                    continue;
                }

                $sqlInsertValues[] = "({$client->user_id}, {$careProviderId})";
            }
        }

        // batch output inserting to prevent lock wait time out
        $sqlInsertsBatched = [];
        foreach (\array_chunk($sqlInsertValues, self::BATCH_INSERT_SIZE) as $sqlInsertValuesInBatch) {
            $sqlInsertsBatched[] = \sprintf($sqlInsert, \implode(', ', $sqlInsertValuesInBatch));
        }

        $this->connection->exec(\implode('', $sqlInsertsBatched));
    }
}
