<?php
/**
 * InvalidPhoneTaskCreator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses;

use application\components\risk\ScreeningRiskCalculator;
use application\modules\dataWarehouse\models\AdditionalEnrollmentProcesses;
use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\RowDataManipulator;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\Department;
use VirtualHealth\OrmBundle\Entity\Risk;
use VirtualHealth\OrmBundle\Entity\Roles;
use VirtualHealth\OrmBundle\Entity\Task;
use VirtualHealth\OrmBundle\Entity\TaskRelation;
use VirtualHealth\OrmBundle\Entity\TaskTemplates;
use VirtualHealth\OrmBundle\Entity\User;
use VirtualHealth\OrmBundle\Query\CareTeam\FindAllMemberIdsByRoleName;
use VirtualHealth\OrmBundle\Repository\DepartmentRepository;
use VirtualHealth\OrmBundle\Repository\RiskRepository;
use VirtualHealth\OrmBundle\Repository\TaskTemplatesRepository;
use VirtualHealth\OrmBundle\Saver\Task\TaskSaver;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class InvalidPhoneTaskCreator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses
 */
class InvalidPhoneTaskCreator
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TaskSaver
     */
    private $taskSaver;

    /**
     * @var CommonUsersGetter
     */
    private $commonUsersGetter;

    /**
     * @var TaskTemplatesRepository
     */
    private $taskTemplatesRepository;

    /**
     * @var FindAllMemberIdsByRoleName
     */
    private $findAllMemberIdsByRoleName;

    /**
     * @var DepartmentRepository
     */
    private $departmentRepository;

    /**
     * @var RiskRepository
     */
    private $riskRepository;

    /**
     * @var string
     */
    private $dateFormat;

    /**
     * InvalidPhoneTaskCreator constructor.
     * @param EntityManager $em
     * @param TaskSaver $taskSaver
     * @param CommonUsersGetter $commonUsersGetter
     * @param TaskTemplatesRepository $taskTemplatesRepository
     * @param FindAllMemberIdsByRoleName $findAllMemberIdsByRoleName
     * @param DepartmentRepository $departmentRepository
     * @param RiskRepository $riskRepository
     * @param string $date
     */
    public function __construct(
        EntityManager $em,
        TaskSaver $taskSaver,
        CommonUsersGetter $commonUsersGetter,
        TaskTemplatesRepository $taskTemplatesRepository,
        FindAllMemberIdsByRoleName $findAllMemberIdsByRoleName,
        DepartmentRepository $departmentRepository,
        RiskRepository $riskRepository,
        string $date
    )
    {
        $this->em = $em;
        $this->taskSaver = $taskSaver;
        $this->commonUsersGetter = $commonUsersGetter;
        $this->taskTemplatesRepository = $taskTemplatesRepository;
        $this->findAllMemberIdsByRoleName = $findAllMemberIdsByRoleName;
        $this->departmentRepository = $departmentRepository;
        $this->riskRepository = $riskRepository;
        $this->dateFormat = $date;
    }

    /**
     * @param \Client[] $clients
     * @param array $clientDataByUserId
     * @throws \CException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(array $clients, array $clientDataByUserId): void
    {
        if (empty($clients)) {
            return;
        }

        $tasks = [];
        $clientIds = \array_map(function (\Client $client) {
            return $client->user_id;
        }, $clients);
        $cmIdsByClientId = $this->findAllMemberIdsByRoleName->read($clientIds, Roles::NAME_COMMUNITY_SPECIALIST_MANAGER, User::TYPE_CAREMANAGER);
        $riskCountsByClientId = $this->riskRepository->countsByClients(
            $clientIds,
            [ScreeningRiskCalculator::HIGH, ScreeningRiskCalculator::MODERATE],
            [Risk\Assigned::class, Risk\DiagnosisImported::class]
        );

        $department = $this->departmentRepository->findOneByName(Department::NAME_COMMUNITY_SPECIALIST);

        $systemCareManagerUser = $this->commonUsersGetter->getSystemCareManagerUser();

        foreach ($clients as $client) {
            $clientData = $clientDataByUserId[$client->user_id];
            if ($client->phone !== RowDataManipulator::DEFAULT_PHONE || !isset($clientData['isNewClient'])) {
                continue;
            }

            $dueDate = !empty($clientData['enrollment_date'])
                ? Carbon::createFromFormat($this->dateFormat, $clientData['enrollment_date'])
                : new Carbon();
            $dueDate->addDays(25);
            $riskCount = $riskCountsByClientId[$client->user_id]['count'] ?? 0;
            /** @var Client $clientReference */
            $clientReference = $this->em->getReference(Client::class, $client->user_id);

            $task = (new Task())
                ->setClient($clientReference)
                ->setUser($systemCareManagerUser)
                ->setDuedate($dueDate)
                ->setType(
                    $this
                        ->taskTemplatesRepository
                        ->getActiveTaskTypeByName(TaskTemplates::LOCKED_TYPE_MEMBER_ENGAGEMENT)
                )
                ->setPriority($riskCount > 0 ? Task::PRIORITY_HIGH : Task::PRIORITY_MEDIUM)
                ->setDescription(
                    AdditionalEnrollmentProcesses::getTaskDescriptionPhoneInvalid(
                        $client,
                        !empty($clientData['medicaid']) ? $clientData['medicaid'] : $client->medicalRecordId
                    )
                )
            ;
            $cmIds = $cmIdsByClientId[$client->user_id] ?? [];
            if ($cmIds) {
                foreach ($cmIds as $cmId) {
                    /** @var User $careManagerUserReference */
                    $careManagerUserReference = $this->taskTemplatesRepository->getEntityReference(User::class, $cmId);
                    $taskRelation = (new TaskRelation())->setUser($careManagerUserReference);
                    $task->setRelation($taskRelation);
                }
            } elseif ($department) {
                $taskRelation = (new TaskRelation())->setDepartment($department);
                $task->setRelation($taskRelation);
            }

            $tasks[] = $task;
        }

        $this->taskSaver->saveAll($tasks);
    }
}
