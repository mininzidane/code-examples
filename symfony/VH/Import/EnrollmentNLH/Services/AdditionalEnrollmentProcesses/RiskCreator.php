<?php
/**
 * RiskSaver class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses;

use Doctrine\DBAL\Connection;

/**
 * Class RiskSaver
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services\AdditionalEnrollmentProcesses
 */
class RiskCreator
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * RiskCreator constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $riskAttributesByClientId A list of risk attributes indexed by client id
     * @return void
     * @throws \Doctrine\DBAL\DBALException
     */
    public function save(array $riskAttributesByClientId): void
    {
        $sqlInsert = 'INSERT IGNORE INTO `risk` (`client_id`, `name`, `weight`, `weight_decimal`, `details`) VALUES %s;';
        $sqlInsertValues = [];
        foreach ($riskAttributesByClientId as $clientId => $riskAttributes) {
            foreach ($riskAttributes as $riskAttribute) {
                [$name, $weight, $details] = $riskAttribute;
                if ($details !== null) {
                    $details = \json_encode($details);
                }
                $sqlInsertValues[] = "({$clientId}, '{$name}', {$weight}, {$weight}, '{$details}')";
            }
        }

        if (!empty($sqlInsertValues)) {
            $this->connection->exec(\sprintf($sqlInsert, \implode(', ', $sqlInsertValues)));
        }
    }
}
