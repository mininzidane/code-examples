<?php
/**
 * ClientPreparation class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\UserCollection;

/**
 * Class ClientPreparation
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientPreparation
{
    /**
     * @var ClientSearch
     */
    private $clientSearch;

    /**
     * @var ClientCreator
     */
    private $clientCreator;

    /**
     * @var BatchValidator
     */
    private $batchValidator;

    /**
     * ClientPreparation constructor.
     * @param ClientSearch $clientSearch
     * @param ClientCreator $clientCreator
     * @param BatchValidator $batchValidator
     */
    public function __construct(
        ClientSearch $clientSearch,
        ClientCreator $clientCreator,
        BatchValidator $batchValidator
    )
    {
        $this->clientSearch = $clientSearch;
        $this->clientCreator = $clientCreator;
        $this->batchValidator = $batchValidator;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     * @param UserCollection $userCollection
     * @throws \CException
     */
    public function prepare(
        FileRowCollection $fileRowCollection,
        ClientCollection $clientCollection,
        UserCollection $userCollection
    ): void
    {
        $this->clientSearch->batchSearch($fileRowCollection, $clientCollection);
        $this->batchValidator->validateBatch($fileRowCollection, $clientCollection);
        $this->clientCreator->batchCreate($fileRowCollection, $clientCollection, $userCollection);
    }
}
