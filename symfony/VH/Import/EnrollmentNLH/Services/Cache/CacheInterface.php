<?php
/**
 * CacheInterface class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\Cache;

/**
 * Class CacheInterface
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services\Cache
 */
interface CacheInterface
{
    /**
     * @param string $key
     * @return array
     */
    public function getForAllClients(string $key): array;

    /**
     * @param string $key
     * @param int $clientId
     * @param mixed $value
     * @return mixed
     */
    public function setForClient(string $key, int $clientId, $value);

    /**
     * @param string $key
     * @param int $clientId
     * @return int
     */
    public function deleteForClient(string $key, int $clientId): int;
}
