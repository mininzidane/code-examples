<?php
/**
 * PRedisCache class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services\Cache;

use Predis\Client;

/**
 * Class PRedisCache
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services\Cache
 */
class Cache implements CacheInterface
{
    /**
     * @var Client
     */
    private $cache;

    /**
     * PRedisCache constructor.
     * @param Client $cache
     */
    public function __construct(Client $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @inheritDoc
     */
    public function getForAllClients(string $key): array
    {
        return $this->cache->hgetall($key);
    }

    /**
     * @inheritDoc
     */
    public function setForClient(string $key, int $clientId, $value)
    {
        return $this->cache->hmset($key, [$clientId => $value]);
    }

    /**
     * @inheritDoc
     */
    public function deleteForClient(string $key, int $clientId): int
    {
        return $this->cache->hdel($key, $clientId);
    }
}
