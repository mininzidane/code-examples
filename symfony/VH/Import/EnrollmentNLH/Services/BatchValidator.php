<?php
/**
 * BatchValidator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use application\modules\dataWarehouse\parsers\enrollment\FileParserLog as Log;
use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location\CityFinder;
use VirtualHealth\Import\Membership\EnrollmentNLH\Services\Location\RegionFinder;
use VirtualHealth\Import\Membership\EnrollmentNLH\Validators\CityValidator;
use VirtualHealth\Import\Membership\EnrollmentNLH\Validators\DateValidator;
use VirtualHealth\Import\Membership\EnrollmentNLH\Validators\RegionValidator;
use VirtualHealth\Import\Validators\ClientUniquenessValidator;
use VirtualHealth\Import\Validators\LengthValidator;
use VirtualHealth\Import\Validators\ValidatorInterface;
use VirtualHealth\OrmBundle\Repository\ClientRepository;

/**
 * Class BatchValidator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class BatchValidator
{
    /**
     * @var ValidatorInterface[]
     */
    private $validators;

    /**
     * @var array
     */
    private $validatorLogMapping;

    /**
     * @var array
     */
    private $warningWrongFieldFormatFlags;

    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var ClientUniquenessValidator
     */
    private $clientUniquenessValidator;

    /**
     * @var RegionValidator
     */
    private $regionValidator;

    /**
     * @var CityValidator
     */
    private $cityValidator;

    /**
     * @var WaiverGetter
     */
    private $waiverGetter;

    /**
     * BatchValidator constructor.
     * @param ClientRepository $clientRepository
     * @param RowDataManipulator $rowDataManipulator
     * @param RegionFinder $regionFinder
     * @param CityFinder $cityFinder
     * @param WaiverGetter $waiverGetter
     * @param bool $useMedicaidNumberLength
     * @param int $medicaidNumberLength
     * @param int $medicaidNumberMinLength
     */
    public function __construct(
        ClientRepository $clientRepository,
        RowDataManipulator $rowDataManipulator,
        RegionFinder $regionFinder,
        CityFinder $cityFinder,
        WaiverGetter $waiverGetter,
        bool $useMedicaidNumberLength,
        int $medicaidNumberLength,
        int $medicaidNumberMinLength
    )
    {
        $this->rowDataManipulator = $rowDataManipulator;
        $this->waiverGetter = $waiverGetter;

        if ($useMedicaidNumberLength) {
            $lengthValidatorParams = [
                $medicaidNumberLength,
                $medicaidNumberMinLength,
            ];
        } else {
            $lengthValidatorParams = [50];
        }
        $this->clientUniquenessValidator = (
            new ClientUniquenessValidator(
                'DRS_ID',
                $clientRepository,
                ClientUniquenessValidator::ATTRIBUTE_DRS_CASE_ID,
                'DRS Case ID'
            )
        )
            ->setSeverityCritical()
        ;
        $this->regionValidator = (new RegionValidator($regionFinder, 'STATE'))->setSeverityCritical();
        $this->cityValidator = (new CityValidator($cityFinder, 'CITY'))->setSeverityCritical();

        $this->validators = [
            0 => $this->clientUniquenessValidator,
            1 => new LengthValidator(...\array_merge(['HFS_MEMBER_NBR', 'Member Number'], $lengthValidatorParams)),
            2 => (new DateValidator('DOB'))->setSeverityCritical(),
            3 => $this->regionValidator,
            4 => $this->cityValidator,
            5 => new DateValidator('ELIG_TERM_DT'),
            6 => new DateValidator('CURR_ELIG_DT'),
            7 => new DateValidator('RENEWAL_DT'),
        ];
        // logs indexed by validators
        $this->validatorLogMapping = [
            0 => Log::ERROR_CLIENT_SAVE,
            1 => Log::ERROR_CLIENT_INSURANCE_MEDICAID_SAVE,
            2 => Log::ERROR_CLIENT_DOB,
            3 => Log::ERROR_CLIENT_STATE,
            4 => Log::ERROR_CLIENT_CITY,
        ];
        // warnings indexed by validators
        $this->warningWrongFieldFormatFlags = [
            5 => true,
            6 => true,
            7 => true,
        ];
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     * @throws \CException
     */
    public function validateBatch(FileRowCollection $fileRowCollection, ClientCollection $clientCollection): void
    {
        $this->clientUniquenessValidator->setClients($clientCollection->getExistingClientsByFileRowHash());
        $this->regionValidator->setFileRowCollection($fileRowCollection);
        $this->cityValidator->setFileRowCollection($fileRowCollection);
        // this only need to set warnings for file rows before they will be deleted by critical-error validators
        $this->waiverGetter->setFileRowCollection($fileRowCollection);

        $rowIndexesToBeDeleted = [];
        foreach ($this->validators as $k => $validator) {
            $dataRowsForCurrentColumn = [];
            /** @var FileRow $fileRow */
            foreach ($fileRowCollection as $i => $fileRow) {
                $dataRowsForCurrentColumn[$i] = $fileRow->{$validator->getField()};
            }
            $errorResult = $validator->validateAll($dataRowsForCurrentColumn);

            $logCode = $this->validatorLogMapping[$k] ?? null;
            $warningFlag = $this->warningWrongFieldFormatFlags[$k] ?? false;
            foreach ($errorResult as $i => $errorMessage) {
                if ($errorMessage === true) {
                    continue;
                }

                $fileRow = $fileRowCollection[$i];
                if ($validator->getSeverity() === ValidatorInterface::SEVERITY_NON_CRITICAL) {
                    if ($logCode !== null) {
                        $this->rowDataManipulator->nonCriticalError($logCode, $fileRowCollection[$i], [], $errorMessage);
                    }
                    // add wrong format warnings
                    if ($warningFlag) {
                        $this->rowDataManipulator->warningWrongFieldFormat($fileRow, $validator->getField());
                    }
                    $fileRow->{$validator->getField()} = null; // set null for invalid value
                } else {
                    // remove file row from collection for critical errors
                    $rowIndexesToBeDeleted[$i] = 1;

                    $fileRow->getParser()->getLog()->inc(Log::SUMMARY_ERRORS);
                    $fileRow->getParser()->getLog()->addError(
                        $fileRow->getNum(),
                        $fileRow->HFS_MEMBER_NBR ?: RowDataManipulator::NA,
                        $logCode,
                        [
                            '{FIELD_NAME}' => $fileRow->{$validator->getField()},
                            '{CLIENT_NAME}' => $this->rowDataManipulator->getClientFullname($fileRow),
                        ],
                        $errorMessage
                    );
                    $fileRow->getParser()->getLog()->addError(
                        $fileRow->getNum(),
                        $fileRow->HFS_MEMBER_NBR ?: RowDataManipulator::NA,
                        Log::INFO_ITEM
                    );
                }
            }
        }

        foreach (\array_keys($rowIndexesToBeDeleted) as $key) {
            // remove found on prev step clients from collection
            $hash = \spl_object_hash($fileRowCollection->get($key));
            $existingClientsByFileRowHash = $clientCollection->getExistingClientsByFileRowHash();
            unset($existingClientsByFileRowHash[$hash]);
            $clientCollection->setExistingClientsByFileRowHash($existingClientsByFileRowHash);
            $fileRowCollection->remove($key);
        }
    }
}
