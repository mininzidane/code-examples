<?php
/**
 * RowDataManipulator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use application\modules\dataWarehouse\parsers\enrollment\FileParserLog as Log;
use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\FileRowException;
use VirtualHealth\OrmBundle\Entity\Client;

/**
 * Class RowDataManipulator
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class RowDataManipulator
{
    public const NA = 'N/A';

    /**
     * @var string Value for phone.
     */
    public const DEFAULT_PHONE = '000-000-0000';

    /**
     * Returns formatted medicaid ID.
     *
     * @param FileRow $row
     * @return string
     */
    public function getMedicaid(FileRow $row): string
    {
        return \ClientInsuranceMedicaid::formatNumber($row->HFS_MEMBER_NBR);
    }

    /**
     * @param FileRow $row
     * @return string
     */
    public function getClientFullName(FileRow $row): string
    {
        return $row->LAST_NAME . ', ' . $row->FIRST_NAME;
    }

    /**
     * @param FileRow $fileRow
     * @return string
     */
    public function getFormattedGender(FileRow $fileRow): string
    {
        switch ($fileRow->GENDER) {
            case 'M':
                return Client::GENDER_MALE;
            case 'F':
                return Client::GENDER_FEMALE;
            default:
                return Client::GENDER_NONE;
        }
    }

    /**
     * @param FileRow $fileRow
     * @param string $field
     * @return \DateTime|null
     */
    public function getDate(FileRow $fileRow, string $field): ?\DateTime
    {
        $value = $fileRow->{$field};
        $date = \DateTime::createFromFormat('Y-m-d', $value);
        return $date ?: null;
    }

    /**
     * A convenient method to throw the FileRowException.
     *
     * @param int $code
     * @param FileRow $fileRow
     * @param array|string $attributes
     * @return FileRowException
     */
    public function exception(int $code, FileRow $fileRow = null, $attributes = []): FileRowException
    {
        $e = new FileRowException(sprintf('Exception in "%s" with code "%d"', __CLASS__, $code), $code);
        $clientFullName = $fileRow === null ? [$this->getClientFullname($fileRow)] : [];
        $e->attributes = $attributes === [] ? $clientFullName : (array)$attributes;

        return $e;
    }

    /**
     * A convenient method to log non-critical error.
     *
     * @param int $code
     * @param FileRow $fileRow
     * @param array $attributes
     * @param string|null $details
     * @throws \CException
     */
    public function nonCriticalError(int $code, FileRow $fileRow, $attributes = [], ?string $details = null): void
    {
        $fileRow->getParser()->getLog()->inc(Log::SUMMARY_ERRORS);

        $attributes = $attributes === [] ? [$this->getClientFullname($fileRow)] : (array)$attributes;
        $fileRow->getParser()->getLog()->addError(
            $fileRow->getNum(),
            $fileRow->HFS_MEMBER_NBR ?: self::NA,
            $code,
            $attributes,
            $details
        );
    }

    /**
     * A convenient method to log an error without exception.
     *
     * @param FileRow $fileRow
     * @param string $field Field name.
     * @throws \CException
     */
    public function warningUnknownValue(FileRow $fileRow, string $field): void
    {
        $fileRow->getParser()->getLog()->inc(Log::SUMMARY_ERRORS);
        $fileRow->getParser()->getLog()->addError(
            $fileRow->getNum(),
            $fileRow->HFS_MEMBER_NBR ?: self::NA,
            Log::WARNING_UNKNOWN_VALUE,
            [
                '{FIELD}' => $field,
                '{VALUE}' => $fileRow->$field,
                '{CLIENT_NAME}' => $this->getClientFullname($fileRow),
            ]
        );
    }

    /**
     * A convenient method to log an error without exception.
     *
     * @param FileRow $fileRow
     * @param string $field Field name.
     * @throws \CException
     */
    public function warningWrongFieldFormat(FileRow $fileRow, string $field): void
    {
        $fileRow->getParser()->getLog()->inc(Log::SUMMARY_ERRORS);
        $fileRow->getParser()->getLog()->addError(
            $fileRow->getNum(),
            $fileRow->HFS_MEMBER_NBR ?: self::NA,
            Log::WARNING_WRONG_FIELD_FORMAT,
            [
                '{FIELD_NAME}' => $field,
                '{CLIENT_NAME}' => $this->getClientFullname($fileRow),
            ]
        );
    }

    /**
     * @param string $phone
     * @return string formatted or default phone
     */
    public function getFormattedPhone(string $phone): string
    {
        $maxLength = 10;
        //if phone is empty
        if (!$phone) {
            return self::DEFAULT_PHONE;
        }

        $phone = \preg_replace('/(\D)/is', '', $phone);

        if (\strlen($phone) > $maxLength) {
            $phone = substr($phone, 0, $maxLength);
        }

        $count = 0;
        $phone = \preg_replace('/^(\d{3})(\d{3})(\d{1,4})$/', '$1-$2-$3', $phone, -1, $count);

        return $count > 0 ? $phone : self::DEFAULT_PHONE;
    }
}
