<?php
/**
 * ClientInsuranceMedicaidPopulate class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Services;

use application\modules\dataWarehouse\parsers\enrollment\FileRow;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInfoCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInsuranceMedicaidCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\ClientInsuranceMedicaidManualChangedCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Collections\FileRowCollection;
use VirtualHealth\Import\Membership\EnrollmentNLH\Query\FindClientInsuranceMedicaidManualChangeRecordsByClientIds;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicaid;
use VirtualHealth\OrmBundle\Entity\Plan;
use VirtualHealth\OrmBundle\Query\Plan\FindAllByNamesAndTypes;

/**
 * Class ClientInsuranceMedicaidPopulate
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Services
 */
class ClientInsuranceMedicaidPopulate
{
    /**
     * @var RowDataManipulator
     */
    private $rowDataManipulator;

    /**
     * @var FindAllByNamesAndTypes
     */
    private $findAllByNamesAndTypes;

    /**
     * @var Plan[]
     */
    private $plans;

    /**
     * @var string[]
     */
    private $planDescriptions;

    /**
     * @var FindClientInsuranceMedicaidManualChangeRecordsByClientIds
     */
    private $findClientInsuranceMedicaidManualChangeRecordsByClientIds;

    /**
     * @var int[]
     */
    private $wasNonWaiverFieldChangedManuallyForClientIds;

    /**
     * @var ClientInfoCollection
     */
    private $clientInfoCollection;

    /**
     * @var array
     */
    private $clientInsuranceManualChangedModels;

    /**
     * @var WaiverGetter
     */
    private $waiverGetter;

    /**
     * @var bool
     */
    private $useMedicaidDates;

    /**
     * @var string
     */
    private $dateFormat;

    /**
     * ClientInsuranceMedicaidPopulate constructor.
     * @param RowDataManipulator $rowDataManipulator
     * @param FindAllByNamesAndTypes $findAllByNamesAndTypes
     * @param FindClientInsuranceMedicaidManualChangeRecordsByClientIds $findClientInsuranceMedicaidManualChangeRecordsByClientIds
     * @param WaiverGetter $waiverGetter
     * @param bool $useMedicaidDates
     * @param string $date
     */
    public function __construct(
        RowDataManipulator $rowDataManipulator,
        FindAllByNamesAndTypes $findAllByNamesAndTypes,
        FindClientInsuranceMedicaidManualChangeRecordsByClientIds $findClientInsuranceMedicaidManualChangeRecordsByClientIds,
        WaiverGetter $waiverGetter,
        bool $useMedicaidDates,
        string $date
    )
    {
        $this->rowDataManipulator = $rowDataManipulator;
        $this->findAllByNamesAndTypes = $findAllByNamesAndTypes;
        $this->findClientInsuranceMedicaidManualChangeRecordsByClientIds = $findClientInsuranceMedicaidManualChangeRecordsByClientIds;
        $this->waiverGetter = $waiverGetter;
        $this->useMedicaidDates = $useMedicaidDates;
        $this->dateFormat = $date;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     * @param ClientCollection $clientCollection
     * @param ClientInsuranceMedicaidCollection $clientInsuranceMedicaidCollection
     * @param ClientInsuranceMedicaidManualChangedCollection $clientInsuranceMedicaidManualChangedCollection
     * @param ClientInfoCollection $clientInfoCollection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function batchPopulate(
        FileRowCollection $fileRowCollection,
        ClientCollection $clientCollection,
        ClientInsuranceMedicaidCollection $clientInsuranceMedicaidCollection,
        ClientInsuranceMedicaidManualChangedCollection $clientInsuranceMedicaidManualChangedCollection,
        ClientInfoCollection $clientInfoCollection
    ): void
    {
        $this->collectPlans($fileRowCollection);
        $this->collectMedicaidManualChanges($clientCollection);
        $this->clientInfoCollection = $clientInfoCollection;
        $this->clientInsuranceManualChangedModels = [];

        foreach ($fileRowCollection->getNewClientFileRows() as $fileRow) {
            $client = $clientCollection->findNewClient($fileRow);

            $clientInsuranceMedicaid = new ClientInsuranceMedicaid();
            $clientInsuranceMedicaid->setClient($client);
            $this->populateClientInsuranceMedicaid($clientInsuranceMedicaid, $fileRow);
            $clientInsuranceMedicaidCollection->add($clientInsuranceMedicaid);
        }

        foreach ($fileRowCollection->getExistingClientFileRows() as $fileRow) {
            $client = $clientCollection->findExistingClient($fileRow);
            if ($client === null) {
                continue;
            }

            if (!$clientInsuranceMedicaid = $client->getInsuranceMedicaid()) {
                $clientInsuranceMedicaid = new ClientInsuranceMedicaid();
                $clientInsuranceMedicaid->setClient($client);
            }
            $this->populateClientInsuranceMedicaid($clientInsuranceMedicaid, $fileRow);
            if (\in_array(\spl_object_hash($clientInsuranceMedicaid), $this->clientInsuranceManualChangedModels, true)) {
                $clientInsuranceMedicaidManualChangedCollection->add($clientInsuranceMedicaid);
            } else {
                $clientInsuranceMedicaidCollection->add($clientInsuranceMedicaid);
            }
        }
    }

    /**
     * @param ClientInsuranceMedicaid $clientInsuranceMedicaid
     * @param FileRow $fileRow
     */
    private function populateClientInsuranceMedicaid(
        ClientInsuranceMedicaid $clientInsuranceMedicaid,
        FileRow $fileRow
    ): void
    {
        $clientInsuranceMedicaid->setNumber($this->rowDataManipulator->getMedicaid($fileRow));

        if ($fileRow->PRODUCT) {
            $clientInsuranceMedicaid->setPlanDescription(
                $this->planDescriptions[\spl_object_hash($fileRow)]
            );

            //only for NLH and NLHENCARE instances, they do not use plan_id
            //so find one assigned from our side by name
            $plan = $this->plans[$clientInsuranceMedicaid->getPlanDescription()] ?? null;
            if (null !== $plan) {
                $clientInsuranceMedicaid->setPlanId($plan->getPlanId());
            }
        }

        if ($fileRow->WAIVER_CODE) {
            $this->populateWaiver(
                $fileRow,
                $clientInsuranceMedicaid,
                $this->waiverGetter->getWaiver($fileRow->WAIVER_CODE)
            );
        }

        if ($this->useMedicaidDates) {
            if ($fileRow->CURR_ELIG_DT) {
                $clientInsuranceMedicaid->setEnrollmentDate($this->rowDataManipulator->getDate($fileRow, 'CURR_ELIG_DT'));
            }
            if ($fileRow->ELIG_TERM_DT) {
                $clientInsuranceMedicaid->setTerminationDate($this->rowDataManipulator->getDate($fileRow, 'ELIG_TERM_DT'));
            }
        }

        if (!empty($fileRow->RENEWAL_DT)) {
            $clientInsuranceMedicaid->setRenewalDate($this->rowDataManipulator->getDate($fileRow, 'RENEWAL_DT'));
        }

        $this->clientInfoCollection->addMedicaid($clientInsuranceMedicaid->getClient(), $clientInsuranceMedicaid->getNumber());
        if ($clientInsuranceMedicaid->getEnrollmentDate() !== null) {
            $this->clientInfoCollection->addEnrollmentDate(
                $clientInsuranceMedicaid->getClient(),
                $clientInsuranceMedicaid->getEnrollmentDate()->format($this->dateFormat)
            );
        }
    }

    /**
     * @param FileRowCollection $fileRowCollection
     */
    private function collectPlans(FileRowCollection $fileRowCollection): void
    {
        $types = [];
        $this->planDescriptions = [];
        foreach ($fileRowCollection->getValues() as $fileRow) {
            $this->planDescriptions[\spl_object_hash($fileRow)] = (string)$fileRow->PRODUCT === 'LTS'
                ? ClientInsuranceMedicaid::PLAN_DESCRIPTION_MLTSS
                : $fileRow->PRODUCT;
            $types[] = Plan::TYPE_MEDICAID;
        }

        $this->plans = [];
        $foundPlans = $this->findAllByNamesAndTypes->read(\array_values($this->planDescriptions), $types);
        foreach ($foundPlans as $plan) {
            $this->plans[$plan->getName()] = $plan;
        }
    }

    /**
     * @param ClientCollection $clientCollection
     * @throws \Doctrine\DBAL\DBALException
     */
    private function collectMedicaidManualChanges(ClientCollection $clientCollection): void
    {
        $clientIds = [];
        foreach ($clientCollection->getExistingClientsByFileRowHash() as $client) {
            $clientIds[] = $client->getUser()->getId();
        }
        $this->wasNonWaiverFieldChangedManuallyForClientIds = $this
            ->findClientInsuranceMedicaidManualChangeRecordsByClientIds
            ->read('non_waiver', $clientIds);
    }

    /**
     * @param FileRow $fileRow
     * @param ClientInsuranceMedicaid $clientInsuranceMedicaid
     * @param string|null $waiver
     */
    private function populateWaiver(
        FileRow $fileRow,
        ClientInsuranceMedicaid $clientInsuranceMedicaid,
        ?string $waiver
    ): void
    {
        if ($fileRow->WAIVER_CODE === 'LTC' && !$clientInsuranceMedicaid->getNonWaiver()) {
            if ($clientInsuranceMedicaid->getClient()->getUser()->getId() === null || !\in_array(
                $clientInsuranceMedicaid->getClient()->getUser()->getId(),
                $this->wasNonWaiverFieldChangedManuallyForClientIds,
                false
            )) {
                $clientInsuranceMedicaid->setNonWaiver(1); // 'Nursing Home (NH)'
                $this->clientInsuranceManualChangedModels[] = \spl_object_hash($clientInsuranceMedicaid);
            }
        } elseif (!$clientInsuranceMedicaid->getWaiver()) {
            // can not set new value, left old
            if ($waiver === null) {
                return;
            }

            $clientInsuranceMedicaid->setWaiver($waiver);
        }

        if ($fileRow->WAIVER_CODE !== 'LTC' && $fileRow->LTC_FLAG === 'Y'
            && ($clientInsuranceMedicaid->getClient()->getUser()->getId() === null || !\in_array(
                $clientInsuranceMedicaid->getClient()->getUser()->getId(),
                $this->wasNonWaiverFieldChangedManuallyForClientIds,
                false
                )
            )
        ) {
            $clientInsuranceMedicaid->setNonWaiver(1); // 'Nursing Home (NH)'
        }
    }
}
