<?php
/**
 * EnrollmentTasksQueueBatchInsert class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Query;

use Doctrine\DBAL\Connection;

/**
 * Class EnrollmentTasksQueueBatchInsert
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Query
 */
class EnrollmentTasksQueueBatchInsert
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * EnrollmentTasksQueueBatchInsert constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $userIdsLtcFlags
     * @throws \Doctrine\DBAL\DBALException
     */
    public function write(array $userIdsLtcFlags): void
    {
        if (empty($userIdsLtcFlags)) {
            return;
        }

        $sqlInsert = <<<SQL
INSERT INTO `enrollment_tasks_queue` (`user_id`, `ltc_flag`) VALUES %s;
SQL;
        $values = [];
        foreach ($userIdsLtcFlags as $userIdLtcFlag) {
            $values[] = "({$userIdLtcFlag[0]}, '{$userIdLtcFlag[1]}')";
        }
        $this->connection->exec(\sprintf($sqlInsert, \implode(', ', $values)));
    }
}
