<?php
/**
 * FindClientInsuranceMedicaidManualChangeRecordsByClientIds class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH\Query;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;

/**
 * Class FindClientInsuranceMedicaidManualChangeRecordsByClientIds
 * @package VirtualHealth\Import\Membership\EnrollmentNLH\Query
 */
class FindClientInsuranceMedicaidManualChangeRecordsByClientIds
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * FindClientInsuranceMedicaidManualChangeRecordsByClientIds constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $field
     * @param int[] $clientIds
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function read(string $field, array $clientIds): array
    {
        if (empty($clientIds)) {
            return [];
        }

        $sql = <<<SQL
SELECT `user_id` FROM `client_insurance_medicaid_manual_change` WHERE `user_id` IN (?) AND `field` = ?
SQL;
        $statement = $this->connection->executeQuery(
            $sql,
            [
                $clientIds,
                $field,
            ],
            [
                Connection::PARAM_INT_ARRAY,
                ParameterType::STRING,
            ]
        );
        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }
}
