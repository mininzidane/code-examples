<?php
/**
 * FileRowException class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\EnrollmentNLH;

use application\modules\dataWarehouse\parsers\enrollment\FileRowServiceException;

/**
 * Class FileRowException
 * @package VirtualHealth\Import\Membership\EnrollmentNLH
 */
class FileRowException extends FileRowServiceException
{

}
