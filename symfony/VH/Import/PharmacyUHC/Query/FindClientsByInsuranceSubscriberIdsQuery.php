<?php
/**
 * FindClientsByInsuranceSubscriberIdsQuery class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC\Query;

use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Repository\ClientRepository;

/**
 * Class FindClientsByInsuranceSubscriberIdsQuery
 * @package VirtualHealth\Import\Claims\PharmacyUHC\Query
 */
class FindClientsByInsuranceSubscriberIdsQuery
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * FindClientsByInsuranceSubscriberIdsQuery constructor.
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param array $subscriberIds
     * @return Client[]
     */
    public function read(array $subscriberIds): array
    {
        if (empty($subscriberIds)) {
            return [];
        }

        $qb = $this->clientRepository->createQueryBuilder('c');
        $orConditions = [];
        foreach ($subscriberIds as $subscriberId) {
            $orConditions[] = $qb->expr()->orX(
                $qb->expr()->eq('medicaid.subscriberId', $qb->expr()->literal($subscriberId)),
                $qb->expr()->eq('medicaidF.subscriberId', $qb->expr()->literal($subscriberId)),
                $qb->expr()->eq('medicare.subscriberId', $qb->expr()->literal($subscriberId)),
                $qb->expr()->eq('medicareF.subscriberId', $qb->expr()->literal($subscriberId))
            );
        }
        $qb
            ->leftJoin('c.insuranceMedicaid', 'medicaid')
            ->leftJoin('c.insuranceMedicaidFuture', 'medicaidF')
            ->leftJoin('c.insuranceMedicare', 'medicare')
            ->leftJoin('c.insuranceMedicareFuture', 'medicareF')
            ->addSelect('medicaid')
            ->addSelect('medicaidF')
            ->addSelect('medicare')
            ->addSelect('medicareF')
            ->where(
                $qb->expr()->orX(...$orConditions)
            )
        ;
        $clients = $qb->getQuery()->getResult();

        $clientsIndexed = [];
        /** @var Client $client */
        foreach ($clients as $client) {
            $insurance = $client->getInsuranceMedicaid()
                ?? $client->getInsuranceMedicare()
                ?? $client->getInsuranceMedicaidFuture()
                ?? $client->getInsuranceMedicareFuture()
            ;
            if ($insurance !== null) {
                $clientsIndexed[$insurance->getSubscriberId()] = $client;
            }
        }

        return $clientsIndexed;
    }
}
