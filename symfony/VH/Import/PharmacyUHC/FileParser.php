<?php
/**
 * FileParser class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC;

use VirtualHealth\Import\Base\Readers\CsvFileReader;
use VirtualHealth\Import\Claims\FileParserBase;
use VirtualHealth\Import\Claims\Log\ParseLog;
use VirtualHealth\Import\Claims\Log\ParseLogDetail;
use VirtualHealth\Import\Claims\Log\ParseLogError;
use VirtualHealth\Import\Claims\Log\ParseLogErrorCsvWriter;
use VirtualHealth\Import\Claims\Log\ParseLogSummary;
use VirtualHealth\Import\Claims\PharmacyUHC\Collections\FileRowCollection;
use VirtualHealth\Import\Claims\PharmacyUHC\Collections\PharmacyClaimCollection;
use VirtualHealth\Import\Claims\PharmacyUHC\Parser\DataPreparer;
use VirtualHealth\Import\Claims\PharmacyUHC\Parser\Saver;
use VirtualHealth\OrmBundle\Entity\ClaimsFile;
use VirtualHealth\OrmBundle\Repository\ClaimsFileRepository;
use VirtualHealth\Utils\Unzip\Unzipper;

/**
 * Class FileParser
 * @package VirtualHealth\Import\Claims\PharmacyUHC
 */
class FileParser extends FileParserBase
{
    private const BATCH_SIZE = 100;

    /**
     * @var DataPreparer
     */
    private $dataPreparer;

    /**
     * @var FileRowParser
     */
    private $fileRowParser;

    /**
     * @var Saver
     */
    private $saver;

    /**
     * @var ClaimsFileRepository
     */
    private $fileRepository;

    /**
     * FileParser constructor.
     * @param CsvFileReader $fileReader
     * @param DataPreparer $dataPreparer
     * @param FileRowParser $fileRowParser
     * @param Saver $saver
     * @param ParseLogErrorCsvWriter $parseLogWriter
     * @param ClaimsFileRepository $fileRepository
     * @param Unzipper $unzipper
     */
    public function __construct(
        CsvFileReader $fileReader,
        DataPreparer $dataPreparer,
        FileRowParser $fileRowParser,
        Saver $saver,
        ParseLogErrorCsvWriter $parseLogWriter,
        ClaimsFileRepository $fileRepository,
        Unzipper $unzipper
    )
    {
        $this->fileReader = $fileReader;
        $this->dataPreparer = $dataPreparer;
        $this->fileRowParser = $fileRowParser;
        $this->saver = $saver;
        $this->parseLogWriter = $parseLogWriter;
        $this->fileRepository = $fileRepository;
        $this->unzipper = $unzipper;
    }

    /**
     * @param ClaimsFile $file
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Throwable
     * @throws \VirtualHealth\Import\Base\Exceptions\FatalFileReaderException
     */
    public function parse(ClaimsFile $file): void
    {
        $rowsWithError = $file->getRowsWithErrors() ?? 0;
        $rowsWithoutError = $file->getRowsWithoutErrors() ?? 0;
        $filePath = $file->getFullPathName();

        $this->parseLog = new ParseLog(new ParseLogSummary($file->getFilenameOriginal(), $rowsWithError + $rowsWithoutError, $rowsWithError));
        $initResult = $this->initFileReader($filePath);

        if ($initResult !== true) {
            return;
        }

        //Use first row of file as header
        $headRowData = $this->fileReader->getRow();
        if ($headRowData !== false) {
            $headRowData = \array_flip($headRowData);
        } else {
            return;
        }
        $headerParseResult = $this->parseHeader(FileRow::getHeaderFieldsToFileRowProps(), $headRowData);

        if ($headerParseResult !== true) {
            $this->storeParseLog($filePath);
            return;
        }

        //Restore read position
        if ($file->getFileOffset()) {
            $this->fileReader->setFilePosition($file->getFileOffset());
        }

        //Restore rows count
        if ($file->getLastProcessedLine()) {
            $this->fileReader->setRowNumber((int)$file->getLastProcessedLine());
        }

        while (!empty($dataRows = $this->fileReader->getRows(self::BATCH_SIZE))) {
            $file = $this->fileRepository->find($file->getId()); // Reload file from repository due to clearing entity manager

            // skip empty lines
            $dataRows = \array_filter($dataRows, function ($dataRow) {
                return !empty(\array_filter($dataRow));
            });

            // prepare data: use only columns which presented in header
            // output: [1: [columnName1 => value1, columnName2 => value2, ...], 2: [...]]
            $fileRowCollection = new FileRowCollection();
            foreach ($dataRows as $dataRow) {
                $fileRowCollection->add(FileRow::fromArray($dataRow, $headRowData));
            }

            $this->fileRowParser->clearStorage();
            $this->dataPreparer->fill($fileRowCollection);
            $pharmacyClaimCollection = new PharmacyClaimCollection();

            /** @var FileRow $fileRow */
            foreach ($fileRowCollection as $i => $fileRow) {
                $this->parseLog->getSummary()->increaseTotal();
                $parseLogDetail = new ParseLogDetail((int)($file->getLastProcessedLine() + $i + 1));

                if (\count($dataRows[$i]) === \count($headRowData)) {
                    $this->fileRowParser->parse(
                        $fileRow,
                        $parseLogDetail,
                        $this->parseLog,
                        $this->dataPreparer->getMedicationCollection(),
                        $pharmacyClaimCollection
                    );
                } else {
                    $parseLogDetail->addError(ParseLogError::critical('Header column count is not the same as row cell count'));
                    $this->parseLog->addDetail($parseLogDetail);
                    $this->parseLog->getSummary()->increaseErrors();
                }
            }

            $this->saver
                ->setFileReader($this->fileReader)
                ->setParseLog($this->parseLog)
                ->save($file, $pharmacyClaimCollection)
            ;
            $this->storeParseLog($filePath);
        }
    }
}
