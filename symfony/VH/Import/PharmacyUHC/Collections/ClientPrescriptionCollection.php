<?php
/**
 * ClientPrescriptionCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use VirtualHealth\OrmBundle\Entity\ClientPrescription;

/**
 * Class ClientPrescriptionCollection
 * @package VirtualHealth\Import\Claims\PharmacyUHC\Collections
 * @method ClientPrescription[] getValues()
 */
class ClientPrescriptionCollection extends ArrayCollection
{

}
