<?php
/**
 * FileRowCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use VirtualHealth\Import\Claims\PharmacyUHC\FileRow;

/**
 * Class FileRowCollection
 * @package VirtualHealth\Import\Claims\PharmacyUHC\Collections
 * @method FileRow[] getValues()
 */
class FileRowCollection extends ArrayCollection
{

}
