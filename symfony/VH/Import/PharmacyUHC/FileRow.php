<?php
/**
 * FileRow class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC;

use VirtualHealth\Import\Base\Rows\BaseDTO;

/**
 * Class FileRow
 * @package VirtualHealth\Import\Claims\PharmacyUHC
 */
class FileRow extends BaseDTO
{
    /**
     * @const array COLUMN_INDEXES_TO_FILE_ROW_PROPS non-zero-based index of column to property
     */
    private const HEADER_FIELDS_TO_FILE_ROW_PROPS = [
        'Claim_ID' => 'claimId',
        'Member_ID' => 'memberId',
        'Prescribing_Provider_ID' => 'prescribingProviderId',
        'Dispensing_Provider_ID' => 'dispensingProviderId',
        'Fill_Date' => 'fillDate',
        'End_Date' => 'endDate',
        'Prescription_Date' => 'prescriptionDate',
        'Drug_Code' => 'drugCode',
        'Drug_Class' => 'drugClass',
        'Drug_Name' => 'drugName',
        'Dispensed_Quantity' => 'dispensedQuantity',
        'Days_Supply' => 'daysSupply',
        'Frequency' => 'frequency',
        'Drug_Strength' => 'drugStrength',
        'Copay_Amount' => 'copayAmount',
    ];

    public $claimId;
    public $memberId;
    public $prescribingProviderId;
    public $dispensingProviderId;
    public $fillDate;
    public $endDate;
    public $prescriptionDate;
    public $drugCode;
    public $drugClass;
    public $drugName;
    public $dispensedQuantity;
    public $daysSupply;
    public $frequency;
    public $drugStrength;
    public $copayAmount;

    /**
     * @inheritDoc
     */
    public static function getHeaderFieldsToFileRowProps(): array
    {
        return self::HEADER_FIELDS_TO_FILE_ROW_PROPS;
    }
}
