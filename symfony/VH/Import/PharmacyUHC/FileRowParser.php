<?php
/**
 * FileRowParser class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC;

use VirtualHealth\Import\Claims\Log\ParseLog;
use VirtualHealth\Import\Claims\Log\ParseLogDetail;
use VirtualHealth\Import\Claims\Log\ParseLogError;
use VirtualHealth\Import\Claims\PharmacyUHC\Collections\ClientPrescriptionCollection;
use VirtualHealth\Import\Claims\PharmacyUHC\Collections\PharmacyClaimCollection;
use VirtualHealth\Import\Claims\PharmacyUHC\Parser\ClientPrescriptionCreator;
use VirtualHealth\Import\Claims\PharmacyUHC\Parser\ClientPrescriptionUpdater;
use VirtualHealth\Import\Claims\PharmacyUHC\Parser\DataPreparer;
use VirtualHealth\Import\Claims\PharmacyUHC\Parser\FileRowValidator;
use VirtualHealth\Import\Claims\PharmacyUHC\Parser\PharmacyClaimCreator;

/**
 * Class FileRowParser
 * @package VirtualHealth\Import\Claims\PharmacyUHC
 */
class FileRowParser
{
    public const DATE_FORMAT = 'm/d/Y';

    public const FREQUENCY_MAPPING = [
        'PC' => 'After food, after meals (PC)',
        'PM' => 'Afternoon, evening (PM)',
        'PRN' => 'As needed (PRN)',
        'HS' => 'At bedtime (HS)',
        'AC' => 'Before food, before meals (AC)',
        'Q' => 'Each, every (Q)',
        'QD' => 'Every day (QD)',
        'QH' => 'Every hour (QH)',
        'QOD' => 'Every other day (QOD)',
        'QID' => 'Four times a day (QID)',
        'H' => 'Hour, at the hour of (H)',
        'STAT' => 'Immediately (STAT)',
        'AM' => 'Morning (AM)',
        'TID' => 'Three times a day (TID)',
        'BID' => 'Twice a day (BID)',
    ];

    /**
     * @var DataPreparer
     */
    private $dataPreparer;

    /**
     * @var PharmacyClaimCreator
     */
    private $pharmacyClaimCreator;

    /**
     * @var ClientPrescriptionUpdater
     */
    private $clientPrescriptionUpdater;

    /**
     * @var ClientPrescriptionCreator
     */
    private $clientPrescriptionCreator;

    /**
     * @var FileRowValidator
     */
    private $fileRowValidator;

    /**
     * FileRowParser constructor.
     * @param DataPreparer $dataPreparer
     * @param PharmacyClaimCreator $pharmacyClaimCreator
     * @param ClientPrescriptionUpdater $clientPrescriptionUpdater
     * @param ClientPrescriptionCreator $clientPrescriptionCreator
     * @param FileRowValidator $fileRowValidator
     */
    public function __construct(
        DataPreparer $dataPreparer,
        PharmacyClaimCreator $pharmacyClaimCreator,
        ClientPrescriptionUpdater $clientPrescriptionUpdater,
        ClientPrescriptionCreator $clientPrescriptionCreator,
        FileRowValidator $fileRowValidator
    )
    {
        $this->dataPreparer = $dataPreparer;
        $this->pharmacyClaimCreator = $pharmacyClaimCreator;
        $this->clientPrescriptionUpdater = $clientPrescriptionUpdater;
        $this->clientPrescriptionCreator = $clientPrescriptionCreator;
        $this->fileRowValidator = $fileRowValidator;
    }

    /**
     * @param FileRow $fileRow
     * @param ParseLogDetail $parseLogDetail
     * @param ParseLog $parseLog
     * @param ClientPrescriptionCollection $clientPrescriptionCollection
     * @param PharmacyClaimCollection $pharmacyClaimCollection
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function parse(
        FileRow $fileRow,
        ParseLogDetail $parseLogDetail,
        ParseLog $parseLog,
        ClientPrescriptionCollection $clientPrescriptionCollection,
        PharmacyClaimCollection $pharmacyClaimCollection
    ): void
    {
        $this->fileRowValidator
            ->validate($fileRow, $parseLogDetail, $parseLog)
        ;
        if ($this->fileRowValidator->isHasCriticalError()) {
            return;
        }
        $client = $this->dataPreparer->getClientBySubscriberId($fileRow->memberId);
        if ($client === null) {
            $parseLogDetail->addError(ParseLogError::critical("Client not found by member id: {$fileRow->memberId}"));
            $parseLog->addDetail($parseLogDetail);
            $parseLog->getSummary()->increaseErrors();
            return;
        }

        $pharmacyClaimCollection->add($this->pharmacyClaimCreator->create($client, $fileRow));

        $medicationKey = $client->getIdMedrec()
            . \VirtualHealth\Import\Claims\Pharmacy\FileRowParser::ID_MED_REC_NDC_SEPARATOR
            . $fileRow->drugCode;
        $medication = $this->dataPreparer->getMedicationCollection()->get($medicationKey);
        if ($medication !== null) {
            $this->clientPrescriptionUpdater->update($fileRow, $client, $medication);
        } else {
            $clientPrescriptionCollection->set($medicationKey, $this->clientPrescriptionCreator->create($fileRow, $client));
        }
    }

    public function clearStorage(): void
    {
        $this->clientPrescriptionUpdater->clearStorage();
        $this->clientPrescriptionCreator->clearStorage();
    }
}
