<?php
/**
 * DataPreparer class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC\Parser;

use VirtualHealth\Import\Claims\Pharmacy\Query\FindClientPrescriptionQuery;
use VirtualHealth\Import\Claims\PharmacyUHC\Collections\ClientPrescriptionCollection;
use VirtualHealth\Import\Claims\PharmacyUHC\Collections\FileRowCollection;
use VirtualHealth\Import\Claims\PharmacyUHC\Query\FindClientsByInsuranceSubscriberIdsQuery;
use VirtualHealth\OrmBundle\Entity\CareproviderPfin;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Repository\CareproviderPfinRepository;

/**
 * Class DataPreparer
 * @package VirtualHealth\Import\Claims\PharmacyUHC\Parser
 */
class DataPreparer
{
    /**
     * @var FindClientsByInsuranceSubscriberIdsQuery
     */
    private $findClientsByInsuranceSubscriberIdsQuery;

    /**
     * @var Client[] indexed by subscriber id
     */
    private $clientsBySubscriberId;

    /**
     * @var CareproviderPfinRepository
     */
    private $careProviderPfinRepository;

    /**
     * @var CareproviderPfin[]
     */
    private $careProviderPfinsByPfinId;

    /**
     * @var FindClientPrescriptionQuery
     */
    private $findClientPrescriptionQuery;

    /**
     * @var ClientPrescriptionCollection
     */
    private $medicationCollection;

    /**
     * DataPreparer constructor.
     * @param FindClientsByInsuranceSubscriberIdsQuery $findClientsByInsuranceSubscriberIdsQuery
     * @param CareproviderPfinRepository $careProviderPfinRepository
     * @param FindClientPrescriptionQuery $findClientPrescriptionQuery
     */
    public function __construct(
        FindClientsByInsuranceSubscriberIdsQuery $findClientsByInsuranceSubscriberIdsQuery,
        CareproviderPfinRepository $careProviderPfinRepository,
        FindClientPrescriptionQuery $findClientPrescriptionQuery
    )
    {
        $this->findClientsByInsuranceSubscriberIdsQuery = $findClientsByInsuranceSubscriberIdsQuery;
        $this->careProviderPfinRepository = $careProviderPfinRepository;
        $this->findClientPrescriptionQuery = $findClientPrescriptionQuery;
    }

    /**
     * @param FileRowCollection $fileRowCollection
     */
    public function fill(FileRowCollection $fileRowCollection): void
    {
        $memberIds = [];
        $careProviderPfinIds = [];
        $ndcList = [];
        foreach ($fileRowCollection->getValues() as $fileRow) {
            $memberIds[] = $fileRow->memberId;
            $careProviderPfinIds[] = $fileRow->prescribingProviderId;
            $ndcList[] = $fileRow->drugCode;
        }
        $this->clientsBySubscriberId = $this->findClientsByInsuranceSubscriberIdsQuery->read($memberIds);

        $this->careProviderPfinsByPfinId = $this
            ->careProviderPfinRepository
            ->findByPfinAndCareProviderType($careProviderPfinIds)
        ;
        $idMedRecsAndNDCs = [];
        foreach ($memberIds as $i => $memberId) {
            $ndc = $ndcList[$i]; // must be because member id and ndc are both required
            $client = $this->clientsBySubscriberId[$memberId] ?? null;
            if ($client === null) {
                continue;
            }

            $idMedRecsAndNDCs[] = [
                $client->getIdMedrec(),
                $ndc,
            ];
        }
        $this->medicationCollection = new ClientPrescriptionCollection(
            $this->findClientPrescriptionQuery->read($idMedRecsAndNDCs, true)
        );
    }

    /**
     * @param string $subscriberId
     * @return Client|null
     */
    public function getClientBySubscriberId(string $subscriberId): ?Client
    {
        return $this->clientsBySubscriberId[$subscriberId] ?? null;
    }

    /**
     * @param string $pfinId
     * @return CareproviderPfin|null
     */
    public function getCareProviderPfinByPfinId(string $pfinId): ?CareproviderPfin
    {
        return $this->careProviderPfinsByPfinId[$pfinId] ?? null;
    }

    /**
     * @return ClientPrescriptionCollection
     */
    public function getMedicationCollection(): ClientPrescriptionCollection
    {
        return $this->medicationCollection;
    }
}
