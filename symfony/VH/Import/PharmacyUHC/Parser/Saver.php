<?php
/**
 * Saver class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC\Parser;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use VirtualHealth\Dict\ORM\Entity\RxcuiNdcMap;
use VirtualHealth\Dict\ORM\Repository\RxcuiNdcRepository;
use VirtualHealth\Import\Base\Readers\CsvFileReader;
use VirtualHealth\Import\Claims\Log\ParseLog;
use VirtualHealth\Import\Claims\Log\ParseLogError;
use VirtualHealth\Import\Claims\Pharmacy\Query\AssociateClientsWithCareProvidersQuery;
use VirtualHealth\Import\Claims\Pharmacy\Query\CreateClientPrescriptionPharmacyClaimRecords;
use VirtualHealth\Import\Claims\PharmacyUHC\Collections\PharmacyClaimCollection;
use VirtualHealth\OrmBundle\Entity\ClaimsFile;
use VirtualHealth\OrmBundle\Entity\ParseLog as ParseLogEntity;
use VirtualHealth\OrmBundle\Entity\User;
use VirtualHealth\OrmBundle\Repository\ClaimsFileRepository;
use VirtualHealth\OrmBundle\Repository\ClientPharmacyClaimRepository;
use VirtualHealth\OrmBundle\Saver\Client\ClientPrescriptionSaver;
use VirtualHealth\OrmBundle\Saver\ParseLogSaver;

/**
 * Class Saver
 * @package VirtualHealth\Import\Claims\PharmacyUHC\Parser
 */
class Saver
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ClientPrescriptionSaver
     */
    private $clientPrescriptionSaver;

    /**
     * @var ParseLog
     */
    private $parseLog;

    /**
     * @var CsvFileReader
     */
    private $fileReader;

    /**
     * @var ClientPharmacyClaimRepository
     */
    private $clientPharmacyClaimRepository;

    /**
     * @var RxcuiNdcRepository
     */
    private $rxcuiNdcRepository;

    /**
     * @var DataPreparer
     */
    private $dataPreparer;

    /**
     * @var ClientPrescriptionCreator
     */
    private $clientPrescriptionCreator;

    /**
     * @var ClientPrescriptionUpdater
     */
    private $clientPrescriptionUpdater;

    /**
     * @var CreateClientPrescriptionPharmacyClaimRecords
     */
    private $createClientPrescriptionPharmacyClaimRecords;

    /**
     * @var AssociateClientsWithCareProvidersQuery
     */
    private $associateClientsWithCareProvidersQuery;

    /**
     * @var ClaimsFileRepository
     */
    private $fileRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ParseLogSaver
     */
    private $parseLogSaver;

    /**
     * Saver constructor.
     * @param Connection $connection
     * @param ClientPrescriptionSaver $clientPrescriptionSaver
     * @param ClientPharmacyClaimRepository $clientPharmacyClaimRepository
     * @param DataPreparer $dataPreparer
     * @param RxcuiNdcRepository $rxcuiNdcRepository
     * @param ClientPrescriptionCreator $clientPrescriptionCreator
     * @param ClientPrescriptionUpdater $clientPrescriptionUpdater
     * @param CreateClientPrescriptionPharmacyClaimRecords $createClientPrescriptionPharmacyClaimRecords
     * @param AssociateClientsWithCareProvidersQuery $associateClientsWithCareProvidersQuery
     * @param ClaimsFileRepository $fileRepository
     * @param LoggerInterface $logger
     * @param EntityManager $entityManager
     * @param ParseLogSaver $parseLogSaver
     */
    public function __construct(
        Connection $connection,
        ClientPrescriptionSaver $clientPrescriptionSaver,
        ClientPharmacyClaimRepository $clientPharmacyClaimRepository,
        DataPreparer $dataPreparer,
        RxcuiNdcRepository $rxcuiNdcRepository,
        ClientPrescriptionCreator $clientPrescriptionCreator,
        ClientPrescriptionUpdater $clientPrescriptionUpdater,
        CreateClientPrescriptionPharmacyClaimRecords $createClientPrescriptionPharmacyClaimRecords,
        AssociateClientsWithCareProvidersQuery $associateClientsWithCareProvidersQuery,
        ClaimsFileRepository $fileRepository,
        LoggerInterface $logger,
        EntityManager $entityManager,
        ParseLogSaver $parseLogSaver
    )
    {
        $this->connection = $connection;
        $this->clientPrescriptionSaver = $clientPrescriptionSaver;
        $this->clientPharmacyClaimRepository = $clientPharmacyClaimRepository;
        $this->rxcuiNdcRepository = $rxcuiNdcRepository;
        $this->dataPreparer = $dataPreparer;
        $this->clientPrescriptionCreator = $clientPrescriptionCreator;
        $this->clientPrescriptionUpdater = $clientPrescriptionUpdater;
        $this->createClientPrescriptionPharmacyClaimRecords = $createClientPrescriptionPharmacyClaimRecords;
        $this->associateClientsWithCareProvidersQuery = $associateClientsWithCareProvidersQuery;
        $this->fileRepository = $fileRepository;
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->parseLogSaver = $parseLogSaver;
    }

    /**
     * @param ClaimsFile $file
     * @param PharmacyClaimCollection $pharmacyClaimCollection
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Throwable
     */
    public function save(
        ClaimsFile $file,
        PharmacyClaimCollection $pharmacyClaimCollection
    ): void
    {
        $this->connection->beginTransaction();

        try {
            $this->clientPharmacyClaimRepository->save($pharmacyClaimCollection->getValues());

            $ndcList = \array_merge(
                $this->clientPrescriptionCreator->getRouteSearchingNDCs(),
                $this->clientPrescriptionUpdater->getRouteSearchingNDCs()
            );

            $rxcuiNdcMaps = $this->rxcuiNdcRepository->findByNdcList($ndcList);

            $clientPrescriptionCollection = $this->dataPreparer->getMedicationCollection();
            foreach ($clientPrescriptionCollection->getValues() as $clientPrescription) {
                $objectHash = \spl_object_hash($clientPrescription);
                if (\array_key_exists($objectHash, $ndcList)
                    && \array_key_exists($ndcList[$objectHash], $rxcuiNdcMaps)) {
                    /** @var RxcuiNdcMap $rxcuiNdcMap */
                    $rxcuiNdcMap = $rxcuiNdcMaps[$ndcList[$objectHash]];
                    $productName = $rxcuiNdcMap->getTitle();
                    $clientPrescription->setName($productName);
                }
            }
            $this->clientPrescriptionSaver->saveAll($clientPrescriptionCollection->getValues());
            $insertValues = [];
            foreach ($pharmacyClaimCollection->getValues() as $clientPharmacyClaim) {
                $medicationKey = $clientPharmacyClaim->getClient()->getIdMedrec()
                    . \VirtualHealth\Import\Claims\Pharmacy\FileRowParser::ID_MED_REC_NDC_SEPARATOR
                    . $clientPharmacyClaim->getNdcCode();
                $insertValues[] = [
                    $clientPrescriptionCollection[$medicationKey]->getId(),
                    $clientPharmacyClaim->getId(),
                ];
            }
            $this->createClientPrescriptionPharmacyClaimRecords->write($insertValues);

            // write associations between client and care provider
            $clientToPfinIds = [];
            foreach ($this->clientPrescriptionUpdater->getClientIdsToCareProviderPfins() as $clientId => $pfinIds) {
                $clientToPfinIds[$clientId] = $pfinIds;
            }
            foreach ($this->clientPrescriptionCreator->getClientIdsToCareProviderPfins() as $clientId => $pfinIds) {
                $clientToPfinIds[$clientId] = \array_merge($clientToPfinIds[$clientId] ?? [], $pfinIds);
            }
            $this->associateClientsWithCareProvidersQuery->write($clientToPfinIds);

            $file->setRowsWithoutErrors($this->parseLog->getSummary()->getTotalRecords() - $this->parseLog->getSummary()->getRecordsWithErrors());
            $file->setRowsWithErrors($this->parseLog->getSummary()->getRecordsWithErrors());
            $file->setFileOffset($this->fileReader->getFilePosition());
            $file->setLastProcessedLine($this->fileReader->getRowNumber());
            $this->fileRepository->save($file);
            $this->saveParseLogToDb($file);
            $this->connection->commit();

        } catch (\Throwable $e) {
            if ($this->connection->isTransactionActive()) {
                $this->connection->rollBack();
            }
            $this->logger->error($e->getMessage());
            $this->entityManager->clear();

            throw $e;
        }

        $this->entityManager->clear();
    }

    /**
     * @param ParseLog $parseLog
     * @return \VirtualHealth\Import\Claims\PharmacyUHC\Parser\Saver
     */
    public function setParseLog(ParseLog $parseLog): Saver
    {
        $this->parseLog = $parseLog;
        return $this;
    }

    /**
     * @param CsvFileReader $fileReader
     * @return Saver
     */
    public function setFileReader(CsvFileReader $fileReader): Saver
    {
        $this->fileReader = $fileReader;
        return $this;
    }

    /**
     * @param ClaimsFile $file
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Mapping\MappingException
     * @throws \Doctrine\ORM\ORMException
     */
    private function saveParseLogToDb(ClaimsFile $file): void
    {
        foreach ($this->parseLog->getDetails() as $parseLogDetail) {
            foreach ($parseLogDetail->getErrors() as $parseLogError) {
                $parseLogEntity = new ParseLogEntity();
                /** @var User $owner */
                $owner = $this->fileRepository->getEntityReference(User::class, $file->getOwnerId());
                $parseLogEntity
                    ->setType(ParseLogEntity::TYPE_CLAIMS)
                    ->setFileId($file->getId())
                    ->setFileName($file->getFilenameOriginal())
                    ->setRecord('')
                    ->setErrorId($parseLogDetail->getRowNumber())
                    ->setError($parseLogError->getMessage())
                    ->setSeverity(
                        $parseLogError->getSeverity() === ParseLogError::CRITICAL
                        ? ParseLogEntity::SEVERITY_CRITICAL
                        : ParseLogEntity::SEVERITY_NON_CRITICAL
                    )
                    ->setCreatedBy($owner->getFullName(false, false, ', ', true))
                ;
                $this->parseLogSaver->append($parseLogEntity);
            }
        }
        $this->parseLogSaver->flush();
    }
}
