<?php
/**
 * FileRowValidator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC\Parser;

use VirtualHealth\Import\Claims\Validator\BaseFileRowValidator;
use VirtualHealth\Import\Validators\PatternValidator;
use VirtualHealth\Import\Validators\RangeValidator;
use VirtualHealth\Import\Validators\RequiredValidator;

/**
 * Class FileRowValidator
 * @package VirtualHealth\Import\Claims\PharmacyUHC\Parser
 */
class FileRowValidator extends BaseFileRowValidator
{
    /**
     * FileRowValidator constructor.
     */
    public function __construct()
    {
        $datePattern = '/^[0-1]\d\/[0-3]\d\/20\d{2}$/';
        $this->validators = [
            (new RequiredValidator('memberId'))->setSeverityCritical(),
            (new RequiredValidator('drugCode'))->setSeverityCritical(),
            (new RequiredValidator('drugClass'))->setSeverityCritical(),
            (new RangeValidator('drugClass', null, ['N']))->setSeverityCritical(),
            new PatternValidator('fillDate', null, $datePattern),
            new PatternValidator('endDate', null, $datePattern),
            new PatternValidator('prescriptionDate', null, $datePattern),
        ];
    }
}
