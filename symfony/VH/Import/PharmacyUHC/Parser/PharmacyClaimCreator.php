<?php
/**
 * PharmacyClaimCreator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC\Parser;

use VirtualHealth\Import\Claims\PharmacyUHC\FileRow;
use VirtualHealth\Import\Claims\PharmacyUHC\FileRowParser;
use VirtualHealth\OrmBundle\Entity\Careprovider;
use VirtualHealth\OrmBundle\Entity\CareproviderNpi;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\ClientPharmacyClaim;

/**
 * Class PharmacyClaimCreator
 * @package VirtualHealth\Import\Claims\PharmacyUHC\Parser
 */
class PharmacyClaimCreator
{
    /**
     * @var DataPreparer
     */
    private $dataPreparer;

    /**
     * PharmacyClaimCreator constructor.
     * @param DataPreparer $dataPreparer
     */
    public function __construct(DataPreparer $dataPreparer)
    {
        $this->dataPreparer = $dataPreparer;
    }

    /**
     * @param Client $client
     * @param FileRow $fileRow
     * @return ClientPharmacyClaim
     * @throws \Exception
     */
    public function create(Client $client, FileRow $fileRow): ClientPharmacyClaim
    {
        $clientPharmacyClaim = new ClientPharmacyClaim();
        $clientPharmacyClaim
            ->setClient($client)
            ->setExternalId($fileRow->claimId)
            ->setPharmacyNumber($fileRow->dispensingProviderId)
            ->setQuantity((float)$fileRow->dispensedQuantity)
            ->setDaysSupply((int)$fileRow->daysSupply)
            ->setNdcCode($fileRow->drugCode)
            ->setMedicationName($fileRow->drugName)
            ->setAmountBilled((float)$fileRow->copayAmount)
            ->setReceivedDate(new \DateTime())
            ->setRefills(0)
        ;
        $careproviderPfin = $this->dataPreparer->getCareProviderPfinByPfinId($fileRow->prescribingProviderId);
        if ($careproviderPfin !== null) {
            /** @var Careprovider $careprovider */
            $careprovider = $careproviderPfin->getCareprovider();
            /** @var CareproviderNpi $npi */
            $npi = $careprovider->getNpis()->first();
            if ($npi !== false) {
                $clientPharmacyClaim->setPrescribingProviderNpi($npi->getNpi());
            }
        }
        if ($fileRow->fillDate !== null
            && ($date = \DateTime::createFromFormat(FileRowParser::DATE_FORMAT, $fileRow->fillDate)) !== false) {
            $clientPharmacyClaim->setDateFilled($date);
        }

        return $clientPharmacyClaim;
    }
}
