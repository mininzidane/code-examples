<?php
/**
 * ClientPrescriptionCreator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC\Parser;

use VirtualHealth\Import\Claims\PharmacyUHC\FileRow;
use VirtualHealth\Import\Claims\PharmacyUHC\FileRowParser;
use VirtualHealth\OrmBundle\Entity\Careprovider;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\ClientPrescription;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class ClientPrescriptionCreator
 * @package VirtualHealth\Import\Claims\PharmacyUHC\Parser
 */
class ClientPrescriptionCreator
{
    /**
     * @var CommonUsersGetter
     */
    private $commonUser;

    /**
     * @var DataPreparer
     */
    private $dataPreparer;

    /**
     * @var array
     */
    private $routeSearchingNDCs = [];

    /**
     * @var array
     */
    private $clientIdsToCareProviderPfins = [];

    /**
     * ClientPrescriptionCreator constructor.
     * @param CommonUsersGetter $commonUser
     * @param DataPreparer $dataPreparer
     */
    public function __construct(
        CommonUsersGetter $commonUser,
        DataPreparer $dataPreparer
    )
    {
        $this->commonUser = $commonUser;
        $this->dataPreparer = $dataPreparer;
    }

    /**
     * @param FileRow $fileRow
     * @param Client $client
     * @return ClientPrescription
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function create(FileRow $fileRow, Client $client): ClientPrescription
    {
        $clientPrescription = new ClientPrescription();
        $clientPrescription
            ->setNdc($fileRow->drugCode)
            ->setClient($client)
            ->setReconciled(false)
            ->setAdherenceStatus(ClientPrescription::ADHERENCE_STATUS_NA)
            ->setOwner($this->commonUser->getSystemSuperAdmin())
            ->setType(ClientPrescription::TYPE_PRESCRIPTION)
            ->setServiceLocationId(0)
            ->setCreatedDate(new \DateTime())
            ->setQuantity($fileRow->dispensedQuantity)
            ->setDaysSupply($fileRow->daysSupply)
            ->setFrequency(FileRowParser::FREQUENCY_MAPPING[$fileRow->frequency] ?? null)
            ->setDosage($fileRow->drugStrength)
            ->setNotes("Co-Pay Amount: {$fileRow->copayAmount}")
        ;
        $careproviderPfin = $this->dataPreparer->getCareProviderPfinByPfinId($fileRow->prescribingProviderId);
        if ($careproviderPfin !== null) {
            /** @var Careprovider $careprovider */
            $careprovider = $careproviderPfin->getCareprovider();
            $clientPrescription->setCareprovider($careprovider);
            $npi = $careprovider->getNpis()->first();
            if ($npi !== false) {
                $clientPrescription->setCareproviderNpiId($npi->getId());
            }
            $this->clientIdsToCareProviderPfins[$client->getUser()->getId()][] = $fileRow->prescribingProviderId;
        }
        if ($fileRow->fillDate) {
            $clientPrescription->setStartedDate(new \DateTime($fileRow->fillDate));
        }
        if ($fileRow->endDate) {
            $clientPrescription->setEndedDate(new \DateTime($fileRow->endDate));
        }
        if ($fileRow->prescriptionDate) {
            $clientPrescription->setPrescriptionDate(new \DateTime($fileRow->prescriptionDate));
        }
        if ($fileRow->drugName && \strtolower($fileRow->drugName) !== 'unknown') {
            $clientPrescription->setName($fileRow->drugName);
        }
        $objectHash = \spl_object_hash($clientPrescription);
        $this->routeSearchingNDCs[$objectHash] = $fileRow->drugCode;

        return $clientPrescription;
    }

    /**
     * @return array
     */
    public function getRouteSearchingNDCs(): array
    {
        return $this->routeSearchingNDCs;
    }

    /**
     * @return array
     */
    public function getClientIdsToCareProviderPfins(): array
    {
        return $this->clientIdsToCareProviderPfins;
    }

    public function clearStorage(): void
    {
        $this->routeSearchingNDCs = [];
        $this->clientIdsToCareProviderPfins = [];
    }
}
