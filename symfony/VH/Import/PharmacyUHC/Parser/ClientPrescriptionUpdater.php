<?php
/**
 * ClientPrescriptionUpdated class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Claims\PharmacyUHC\Parser;

use VirtualHealth\Import\Claims\PharmacyUHC\FileRow;
use VirtualHealth\Import\Claims\PharmacyUHC\FileRowParser;
use VirtualHealth\OrmBundle\Entity\Careprovider;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\ClientPrescription;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class ClientPrescriptionUpdated
 * @package VirtualHealth\Import\Claims\PharmacyUHC\Parser
 */
class ClientPrescriptionUpdater
{
    /**
     * @var CommonUsersGetter
     */
    private $commonUser;

    /**
     * @var DataPreparer
     */
    private $dataPreparer;

    /**
     * @var array
     */
    private $routeSearchingNDCs = [];

    /**
     * @var array
     */
    private $clientIdsToCareProviderPfins = [];

    /**
     * ClientPrescriptionUpdater constructor.
     * @param CommonUsersGetter $commonUser
     * @param DataPreparer $dataPreparer
     */
    public function __construct(
        CommonUsersGetter $commonUser,
        DataPreparer $dataPreparer
    )
    {
        $this->commonUser = $commonUser;
        $this->dataPreparer = $dataPreparer;
    }

    /**
     * @param FileRow $fileRow
     * @param Client $client
     * @param ClientPrescription $clientPrescription
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function update(FileRow $fileRow, Client $client, ClientPrescription $clientPrescription): void
    {
        $clientPrescription
            ->setOwner($this->commonUser->getSystemSuperAdmin())
            ->setUpdatedDate(new \DateTime())
        ;
        $careproviderPfin = $this->dataPreparer->getCareProviderPfinByPfinId($fileRow->prescribingProviderId);
        if ($careproviderPfin !== null) {
            /** @var Careprovider $careprovider */
            $careprovider = $careproviderPfin->getCareprovider();
            $clientPrescription->setCareprovider($careprovider);
            $npi = $careprovider->getNpis()->first();
            if ($npi !== false) {
                $clientPrescription->setCareproviderNpiId($npi->getId());
            }
            $this->clientIdsToCareProviderPfins[$client->getUser()->getId()][] = $fileRow->prescribingProviderId;
        }
        if ($fileRow->fillDate !== null) {
            $clientPrescription->setStartedDate(new \DateTime($fileRow->fillDate));
        }
        if ($fileRow->endDate !== null) {
            $clientPrescription->setEndedDate(new \DateTime($fileRow->endDate));
        }
        if ($fileRow->prescriptionDate !== null) {
            $clientPrescription->setPrescriptionDate(new \DateTime($fileRow->prescriptionDate));
        }
        if ($fileRow->drugName !== null && \strtolower($fileRow->drugName) !== 'unknown') {
            $clientPrescription->setName($fileRow->drugName);
        }
        if ($fileRow->dispensedQuantity !== null) {
            $clientPrescription->setQuantity($fileRow->dispensedQuantity);
        }
        $clientPrescription->setDaysSupply((int)$fileRow->daysSupply);
        if (FileRowParser::FREQUENCY_MAPPING[$fileRow->frequency] ?? false) {
            $clientPrescription->setFrequency(FileRowParser::FREQUENCY_MAPPING[$fileRow->frequency]);
        }
        $clientPrescription->setDosage($fileRow->drugStrength);
        if ($fileRow->copayAmount !== null) {
            $clientPrescription->setNotes("Co-Pay Amount: {$fileRow->copayAmount}");
        }
        $objectHash = \spl_object_hash($clientPrescription);
        $this->routeSearchingNDCs[$objectHash] = $fileRow->drugCode;
    }

    /**
     * @return array
     */
    public function getRouteSearchingNDCs(): array
    {
        return $this->routeSearchingNDCs;
    }

    /**
     * @return array
     */
    public function getClientIdsToCareProviderPfins(): array
    {
        return $this->clientIdsToCareProviderPfins;
    }

    public function clearStorage(): void
    {
        $this->routeSearchingNDCs = [];
        $this->clientIdsToCareProviderPfins = [];
    }
}
