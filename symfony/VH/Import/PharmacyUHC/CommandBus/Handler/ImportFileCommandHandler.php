<?php
/**
 * ImportFileCommandHandler class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Claims\PharmacyUHC\CommandBus\Handler;

use Psr\Log\LoggerInterface;
use VirtualHealth\Import\Claims\PharmacyUHC\CommandBus\ImportFileCommand;
use VirtualHealth\Import\Claims\PharmacyUHC\FileParser;
use VirtualHealth\Infrastructure\WebNotifier;
use VirtualHealth\OrmBundle\Entity\ClaimsFile;
use VirtualHealth\OrmBundle\Repository\ClaimsFileRepository;

/**
 * Class ImportFileCommandHandler
 * @package VirtualHealth\Import\Claims\PharmacyUHC\CommandBus\Handler
 */
class ImportFileCommandHandler
{
    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var WebNotifier $notifier
     */
    private $notifier;

    /**
     * @var ClaimsFileRepository $importFileRepository
     */
    private $importFileRepository;

    /**
     * @var FileParser $parser
     */
    private $parser;

    /**
     * ImportFileCommandHandler constructor.
     * @param LoggerInterface $logger
     * @param WebNotifier $notifier
     * @param ClaimsFileRepository $claimsFileRepository
     * @param FileParser $pharmacyParser
     */
    public function __construct(
        LoggerInterface $logger,
        WebNotifier $notifier,
        ClaimsFileRepository $claimsFileRepository,
        FileParser $pharmacyParser
    ) {
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->importFileRepository = $claimsFileRepository;
        $this->parser = $pharmacyParser;
    }

    /**
     * @param ImportFileCommand $command
     * @throws \Throwable
     */
    public function handle(ImportFileCommand $command): void
    {
        /** @var ClaimsFile|null $importFile */
        $importFile = makeAttempts(function () use ($command) {
            return $this->importFileRepository->findOneBy(['id' => $command->getModelId()]);
        });

        if (null === $importFile) {
            $this->logger->error(
                t('AlertMessage', '{item} #{id} could not be found', [
                    '{item}' => 'Pharmacy Claims Import File',
                    '{id}' => $command->getModelId()
                ])
            );
            return;
        }

        try {
            $importFile->setStartedCount($importFile->getStartedCount() + 1);
            $this->importFileRepository->save($importFile);

            $this->parser->parse($importFile);

            // parse method calls \Doctrine\ORM\EntityManager::clear
            // so we need to get file again
            $importFile = $this->importFileRepository->find($command->getModelId());
            $importFile->setInProgress(false);
            $this->importFileRepository->save($importFile);

            $this->notifier->inProgressSuccessMessage(
                $command->getInitiatorId(),
                t('AlertMessage', 'BACKGROUND PROCESS COMPLETED'),
                t(
                    'AlertMessage',
                    '{subject} processing has finished.',
                    [
                        '{subject}' => 'Providers Profiles Import #' . $command->getModelId(),
                    ]
                )
            );

        } catch (\Throwable $e) {
            $importFile->setIsFailed(true);
            $this->importFileRepository->save($importFile);

            $this->notifier->inProgressErrorMessage(
                $command->getInitiatorId(),
                t('AlertMessage', 'BACKGROUND PROCESS INCOMPLETED'),
                t(
                    'AlertMessage',
                    '{subject} cannot be parsed.',
                    [
                        '{subject}' => 'Pharmacy Claims Import #' . $command->getModelId(),
                    ]
                )
            );
            throw $e;
        }
    }
}
