<?php
/**
 * ImportFileCommand class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Claims\PharmacyUHC\CommandBus;

use VirtualHealth\CommandBusBundle\Middleware\BaseCommand;

/**
 * Class ImportFileCommand
 * @package VirtualHealth\Import\Claims\PharmacyUHC\CommandBus
 */
class ImportFileCommand extends BaseCommand
{
    /**
     * @var int
     */
    private $modelId;

    /**
     * @var int
     */
    private $initiatorId;

    /**
     * Initiator user Id
     *
     * @return int
     */
    public function getInitiatorId(): int
    {
        return $this->initiatorId;
    }

    /**
     * File model Id
     *
     * @return int
     */
    public function getModelId(): int
    {
        return $this->modelId;
    }

    /**
     * ImportFileCommand constructor.
     *
     * @param int $modelId
     * @param int $initiatorId
     */
    public function __construct(int $modelId, int $initiatorId)
    {
        $this->modelId = $modelId;
        $this->initiatorId = $initiatorId;
    }
}
