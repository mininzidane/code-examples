<?php
/**
 * File class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834;

use VirtualHealth\OrmBundle\Entity\MembershipFile;

/**
 * Class Edi834File
 * @package VirtualHealth\Import\Membership\Edi834
 */
class File implements \Iterator
{
    const START_RECORD_ELEMENT = 'INS';

    /**
     * @var resource
     */
    private $fileHandler;

    /**
     * @var int
     */
    private $key;

    /**
     * @var int
     */
    private $filePosition;

    /**
     * @var string
     */
    private $currentRecord;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @param MembershipFile $membershipFile
     * @return string
     */
    public function buildFilePath(MembershipFile $membershipFile): string
    {
        return UPLOAD_DIR . DS
            . 'membership' . DS
            . 'members' . DS
            . $membershipFile->getId() . DS
            . $membershipFile->getFilename();
    }

    /**
     * Open file for reading
     * @param string $fileName
     * @param int $filePosition
     */
    public function openFile($fileName, $filePosition = 0)
    {
        $this->filePath = $fileName;
        $this->fileHandler = fopen($fileName, 'r');
        $this->setFilePosition($filePosition);
        $filePosition == 0 ? $this->findFirstRecord() : $this->readRecord();
    }

    /**
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * Close file
     */
    public function closeFile()
    {
        if (is_resource($this->fileHandler)) {
            fclose($this->fileHandler);
        }
    }

    /**
     * @inheritdoc
     */
    public function __destruct()
    {
        $this->closeFile();
    }

    /**
     * @inheritdoc
     */
    public function current()
    {
        return $this->currentRecord;
    }

    /**
     * @inheritdoc
     */
    public function next()
    {
        $this->readRecord();
    }

    /**
     * @inheritdoc
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * @inheritdoc
     */
    public function valid()
    {
        return !empty($this->currentRecord);
    }

    /**
     * @inheritdoc
     */
    public function rewind()
    {
        rewind($this->fileHandler);
        $this->findFirstRecord();
    }

    /**
     * Get file position
     * @return int
     */
    public function getFilePosition()
    {
        return $this->filePosition;
    }

    /**
     * Set file position
     * @param int $position
     */
    private function setFilePosition($position)
    {
        $this->filePosition = $position;
        fseek($this->fileHandler, $position);
    }

    /**
     * Read a record
     */
    private function readRecord()
    {
        $record = '';
        $eor = false;
        $this->key = $this->filePosition;
        while (!feof($this->fileHandler) && !$eor) {
            $record .= fread($this->fileHandler, 1024);
            if (false !== ($pos = strpos($record, '~' . self::START_RECORD_ELEMENT . '*'))) {
                $record = substr($record, 0, $pos + 1);
                $eor = true;
            }
        }
        $this->setFilePosition($this->filePosition + strlen($record));
        $this->currentRecord = $record;
    }

    /**
     * Find the first record
     */
    private function findFirstRecord()
    {
        $this->readRecord();
        if (0 !== strpos($this->current(), self::START_RECORD_ELEMENT . '*')) {
            $this->readRecord();
        }
    }
}
