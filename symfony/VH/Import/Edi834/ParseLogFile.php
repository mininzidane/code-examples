<?php
/**
 * ParseLogFile class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834;

/**
 * Class ParseLogFile
 * @package VirtualHealth\Import\Membership
 * @deprecated  please remove all dependencies
 */
class ParseLogFile
{
    /**
     * @var ParseLog
     */
    private $parseLog;

    /**
     * @var string
     */
    private $filePath;

    /**
     * ParseLogFile constructor.
     * @param string $filePath
     * @param int|null $partNumber
     */
    public function __construct(string $filePath, ?int $partNumber = null)
    {
        $this->filePath = $filePath;
        $log = [];
        $fileName = $this->buildFileName($partNumber);
        if (file_exists($fileName)) {
            try {
                $log = json_decode(file_get_contents($fileName), true, 512, JSON_OBJECT_AS_ARRAY);
            } catch (\Exception $e) {}
        }
        $this->parseLog = new ParseLog($log);
    }

    /**
     * @param int|null $partNumber
     * @return bool
     */
    public function save(?int $partNumber = null)
    {
        try {
            file_put_contents(
                $this->buildFileName($partNumber),
                json_encode($this->parseLog->getLog())
            );
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param int|null $partNumber
     * @return bool
     */
    public function delete(?int $partNumber = null): bool
    {
        try {
            return unlink($this->buildFileName($partNumber));
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return ParseLog
     */
    public function getParseLog()
    {
        return $this->parseLog;
    }

    /**
     * @param ParseLog $parseLog
     */
    public function setParseLog($parseLog)
    {
        $this->parseLog = $parseLog;
    }

    /**
     * @param int|null $partNumber
     * @return string
     */
    private function buildFileName(?int $partNumber)
    {
        return $this->filePath . '-log' . ($partNumber !== null ? '-' . $partNumber : '') . '.json';
    }
}
