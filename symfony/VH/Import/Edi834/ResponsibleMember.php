<?php
/**
 * ResponsibleMember class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834;

/**
 * Class ResponsibleMember
 * @package VirtualHealth\Import\Membership\Edi834
 */
class ResponsibleMember
{
    /**
     * @var string
     */
    private $identifierCode;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $primaryPhone;

    /**
     * @var string
     */
    private $alternatePhone;

    /**
     * @var string
     */
    private $addressLine1;

    /**
     * @var string
     */
    private $addressLine2;

    /**
     * @var string
     */
    private $cityTitle;

    /**
     * @var string
     */
    private $regionTitle;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * ResponsibleMember constructor.
     * @param string $identifierCode
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct(string $identifierCode, string $firstName, string $lastName)
    {
        $this->identifierCode = $identifierCode;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return bool
     */
    public function isGuardian(): bool
    {
        return in_array($this->identifierCode, ['E1', 'QD']);
    }

    /**
     * @return bool
     */
    public function isPoa(): bool
    {
        return $this->identifierCode == 'J6';
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getPrimaryPhone(): ?string
    {
        return $this->primaryPhone;
    }

    /**
     * @param string|null $primaryPhone
     * @return $this
     */
    public function setPrimaryPhone(?string $primaryPhone)
    {
        $this->primaryPhone = $primaryPhone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlternatePhone(): ?string
    {
        return $this->alternatePhone;
    }

    /**
     * @param string|null $alternatePhone
     * @return $this
     */
    public function setAlternatePhone(?string $alternatePhone)
    {
        $this->alternatePhone = $alternatePhone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddressLine1(): ?string
    {
        return $this->addressLine1;
    }

    /**
     * @param string|null $addressLine1
     * @return $this
     */
    public function setAddressLine1(?string $addressLine1)
    {
        $this->addressLine1 = $addressLine1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddressLine2(): ?string
    {
        return $this->addressLine2;
    }

    /**
     * @param string|null $addressLine2
     * @return $this
     */
    public function setAddressLine2(?string $addressLine2)
    {
        $this->addressLine2 = $addressLine2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCityTitle(): ?string
    {
        return $this->cityTitle;
    }

    /**
     * @param string|null $cityTitle
     * @return $this
     */
    public function setCityTitle(?string $cityTitle)
    {
        $this->cityTitle = $cityTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegionTitle(): ?string
    {
        return $this->regionTitle;
    }

    /**
     * @param string|null $regionTitle
     * @return $this
     */
    public function setRegionTitle(?string $regionTitle)
    {
        $this->regionTitle = $regionTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * @param string|null $postalCode
     * @return $this
     */
    public function setPostalCode(?string $postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }
}
