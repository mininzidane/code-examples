<?php

/**
 * MembershipEdi834ImportManagementPlugin class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019. Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Membership\Edi834\Management;

use Doctrine\ORM\NonUniqueResultException;
use SimpleBus\Message\Bus\MessageBus;
use VirtualHealth\CommandBusBundle\Middleware\BaseCommand;
use VirtualHealth\Import\Membership\Edi834\CommandBus\ImportFileCommand;
use VirtualHealth\Import\Membership\Management\BaseMembershipImportManagementPlugin;
use VirtualHealth\OrmBundle\Entity\MembershipFile;
use VirtualHealth\OrmBundle\Repository\MembershipFileRepository;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class MembershipEdi834ImportManagementPlugin
 * @package VirtualHealth\Import\Membership\Edi834\Management
 */
class MembershipEdi834ImportManagementPlugin extends BaseMembershipImportManagementPlugin
{
    /**
     * @var CommonUsersGetter
     */
    private $commonUserGetter;

    /**
     * MembershipEdi834ImportManagementPlugin constructor.
     * @param MembershipFileRepository $repository
     * @param CommonUsersGetter $commonUsersGetter
     * @param MessageBus $messageBus
     */
    public function __construct(
        MembershipFileRepository $repository,
        CommonUsersGetter $commonUsersGetter,
        MessageBus $messageBus
    )
    {
        parent::__construct($repository, $commonUsersGetter, $messageBus);

        $this->commonUserGetter = $commonUsersGetter;
    }

    /**
     * @return string
     */
    public function getImportName(): string
    {
        return 'Membership_EDI834';
    }

    /**
     * @return string
     */
    protected function getType(): string
    {
        return MembershipFile::TYPE_834;
    }

    /**
     * @param MembershipFile $file
     * @return BaseCommand
     * @throws NonUniqueResultException
     */
    protected function createCommand(MembershipFile $file): BaseCommand
    {
        return new ImportFileCommand(
            $file->getId(),
            $this->commonUserGetter->getSystemSuperAdmin()->getId(),
            'member-grid'
        );
    }
}
