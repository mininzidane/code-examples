<?php
/**
 * ParseLog class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834;

/**
 * Class ParseLog
 * @package VirtualHealth\Import\Membership\Edi834
 * @deprecated   please remove all dependencies
 */
class ParseLog
{
    /**
     * @var array
     */
    private $criticalErrors = [];

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var int
     */
    private $totalErrors = 0;

    /**
     * @var int
     */
    private $total = 0;

    /**
     * @var int
     */
    private $totalCreated = 0;

    /**
     * @var int
     */
    private $totalUpdated = 0;

    /**
     * ParseLog constructor.
     * @param array $log
     */
    public function __construct($log)
    {
        $this->total = isset($log['total']) ? (int) $log['total'] : 0;
        $this->totalCreated = isset($log['totalCreated']) ? (int) $log['totalCreated'] : 0;
        $this->totalUpdated = isset($log['totalUpdated']) ? (int) $log['totalUpdated'] : 0;
        $this->totalErrors = isset($log['totalErrors']) ? (int) $log['totalErrors'] : 0;
        $this->criticalErrors = isset($log['criticalErrors']) ? $log['criticalErrors'] : [];
        $this->errors = isset($log['errors']) ? $log['errors'] : [];
    }

    /**
     * @param int $num
     * @param string $subscriberId
     * @param string $message
     * @param bool $isCritical
     */
    public function addError($num, $subscriberId, $message, $isCritical = false)
    {
        $fieldName = $isCritical ? 'criticalErrors' : 'errors';
        if (!isset($this->{$fieldName}[$num])) {
            $this->{$fieldName}[$num] = [];
        }
        $this->totalErrors++;
        $this->{$fieldName}[$num][] = [
            'subscriberId' => $subscriberId,
            'message' => $message,
        ];
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return $this->totalErrors > 0;
    }

    public function incrementTotal()
    {
        $this->total++;
    }

    /**
     * @param int $count
     */
    public function incrementTotalCreated($count = 1)
    {
        $this->totalCreated += $count;
    }

    /**
     * @param int $count
     */
    public function incrementTotalUpdated($count = 1)
    {
        $this->totalUpdated += $count;
    }

    /**
     * @param int $count
     */
    public function decrementTotalCreated($count = 1)
    {
        $this->totalCreated -= $count;
    }

    /**
     * @param int $count
     */
    public function decrementTotalUpdated($count = 1)
    {
        $this->totalUpdated -= $count;
    }

    /**
     * @return array
     */
    public function getLog()
    {
        return [
            'total' => $this->total,
            'totalCreated' => $this->totalCreated,
            'totalUpdated' => $this->totalUpdated,
            'totalErrors' => $this->totalErrors,
            'criticalErrors' => $this->criticalErrors,
            'errors' => $this->errors,
        ];
    }

    /**
     * @return array
     */
    public function getCriticalErrors()
    {
        return $this->criticalErrors;
    }


    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getTotalErrors()
    {
        return $this->totalErrors;
    }

    /**
     * @return int
     */
    public function getTotalCreated()
    {
        return $this->totalCreated;
    }

    /**
     * @return int
     */
    public function getTotalUpdated()
    {
        return $this->totalUpdated;
    }
}
