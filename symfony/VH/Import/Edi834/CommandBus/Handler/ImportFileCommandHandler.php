<?php
/**
 * ImportFileCommandHandler class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\CommandBus\Handler;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Output\OutputInterface;
use VirtualHealth\Import\Membership\Edi834\CommandBus\ImportFileCommand;
use VirtualHealth\Import\Membership\Edi834\File;
use VirtualHealth\Import\Membership\Edi834\Monitoring\MembershipImportMonitor;
use VirtualHealth\Import\Membership\Edi834\ParseLog\ParseLogPartsJsonCsvConsolidator;
use VirtualHealth\Infrastructure\WebNotifier;
use VirtualHealth\OrmBundle\Entity\MembershipFile;
use VirtualHealth\OrmBundle\Query\Client\GetLastIdMedrec;
use VirtualHealth\OrmBundle\Repository\MembershipFileRepository;
use VirtualHealth\Utils\UnzipService;
use VirtualHealth\Import\Membership\Edi834\Services\ImportFileService;

/**
 * Class ImportFileCommandHandler
 * @package VirtualHealth\Import\Membership\Edi834\CommandBus\Handler
 */
final class ImportFileCommandHandler
{
    const BATCH_SIZE = 100;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var WebNotifier
     */
    private $notifier;

    /**
     * @var MembershipFileRepository
     */
    private $importFileRepository;

    /**
     * @var UnzipService
     */
    private $unzipService;

    /**
     * @var GetLastIdMedrec
     */
    private $lastIdMedrecQuery;
    /**
     * @var MembershipImportMonitor
     */
    private $monitor;

    /**
     * @var ImportFileService
     */
    private $importFileService;

    /**
     * ImportFileCommandHandler constructor.
     * @param EntityManager $entityManager
     * @param OutputInterface $output
     * @param WebNotifier $notifier
     * @param MembershipFileRepository $importFileRepository
     * @param UnzipService $unzipService
     * @param GetLastIdMedrec $lastIdMedrecQuery
     * @param ImportFileService $importFileService
     * @param MembershipImportMonitor $monitor
     */
    public function __construct(
        EntityManager $entityManager,
        OutputInterface $output,
        WebNotifier $notifier,
        MembershipFileRepository $importFileRepository,
        UnzipService $unzipService,
        GetLastIdMedrec $lastIdMedrecQuery,
        ImportFileService $importFileService,
        MembershipImportMonitor $monitor
    ) {
        $this->entityManager = $entityManager;
        $this->connection = $this->entityManager->getConnection();
        $this->output = $output;
        $this->notifier = $notifier;
        $this->importFileRepository = $importFileRepository;
        $this->unzipService = $unzipService;
        $this->lastIdMedrecQuery = $lastIdMedrecQuery;
        $this->monitor = $monitor;
        $this->importFileService = $importFileService;
    }

    /**
     * @param ImportFileCommand $command
     * @throws \Doctrine\DBAL\DBALException
     * @throws \VirtualHealth\Utils\Unzip\UnzipperException
     * @throws \Exception
     */
    public function handle(ImportFileCommand $command)
    {
        /** @var MembershipFile $model */
        $model = makeAttempts(function () use ($command) {
            return $this->importFileRepository->find($command->getModelId());
        });
        if (null === $model) {
            $message = t('AlertMessage', '{item} could not be found.', ['{item}' => 'Edi 834 File']);
            $this->notifier->inProgressErrorMessage(
                $command->getInitiatorId(),
                'BACKGROUND PROCESS COULD NOT BE COMPLETED',
                $message,
                $command->getGridId()
            );
            $this->consoleLog($command->getModelId(), $message);
            return;
        }

        $this->updateFileCounters($model);

        $this->monitor->fileProcessingStarted($model); // monitoring

        $file = new File();
        $filePath = $file->buildFilePath($model);
        if ($this->unzipService->checkFileType($filePath)) {
            $filePath = $this->unzipService->unzip($filePath);
        }
        $file->openFile($filePath, $model->getFileOffset());

        $parseLogConsolidator = new ParseLogPartsJsonCsvConsolidator($file->getFilePath());

        $lastIdMedrec = $this->getIdMedrec();
        while ($file->valid()) {
            try {
                $this->connection->beginTransaction();
                $model = $this->importFileRepository->find($command->getModelId());
                $lastIdMedrec = $this->importFileService->processBatch(
                    $file,
                    $model,
                    self::BATCH_SIZE,
                    $lastIdMedrec,
                    $parseLogConsolidator
                );
                $this->importFileRepository->save($model);
                $this->connection->commit();
                $this->entityManager->clear();

                $this->monitor->fileBatchProcessed($model); // monitoring
            } catch (\Exception $e) {
                if ($this->connection->isConnected() && $this->connection->isTransactionActive()) {
                    $this->connection->rollBack();
                }
                throw $e;
            }
        }

        $parseLogConsolidator->save();

        $this->monitor->fileProcessingFinished($model); // monitoring
    }

    /**
     * @return bool|string
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getIdMedrec()
    {
        $lastIdMedrec = \bcadd($this->lastIdMedrecQuery->read(), \random_int(15, 45));
        return $lastIdMedrec;
    }

    /**
     * @param int $modelId
     * @param string $message
     */
    private function consoleLog(int $modelId, string $message): void
    {
        $this->output->writeln('---------- ' . $message . ' ----------');
        $this->output->writeln('ID: ' . $modelId);
    }

    /**
     * @param MembershipFile $file
     * @throws \Exception
     */
    private function updateFileCounters(MembershipFile $file): void
    {
        if (!$file->getFirstStartDate()) {
            $file->setFirstStartDate();
        }
        $file->incrementStartsCount();

        $this->importFileRepository->save($file);
    }
}
