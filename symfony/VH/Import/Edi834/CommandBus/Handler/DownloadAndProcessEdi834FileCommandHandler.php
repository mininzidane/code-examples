<?php

/**
 * DownloadAndProcessEdi834FileCommandHandler class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Membership\Edi834\CommandBus\Handler;

use application\modules\membership\models\MembershipFile;
use Doctrine\DBAL\Driver\Connection;
use League\Flysystem\AdapterInterface;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use SimpleBus\Message\Bus\MessageBus;
use VirtualHealth\CommandBusBundle\Middleware\BaseCommand;
use VirtualHealth\Import\Membership\Edi834\CommandBus\DownloadAndProcessEdi834FileCommand;
use VirtualHealth\Import\Membership\Edi834\CommandBus\ImportFileCommand;
use VirtualHealth\Import\Membership\Edi834\Services\AutomationRemoteFilesFinder;
use VirtualHealth\OrmBundle\Repository\LobRepository;
use VirtualHealth\OrmBundle\Repository\MembershipFileRepository;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class DownloadAndProcessEdi834FileCommandHandler
 * @package VirtualHealth\Import\Membership\Edi834\CommandBus\Handler
 */
class DownloadAndProcessEdi834FileCommandHandler extends BaseCommand
{
    /**
     * @var CommonUsersGetter
     */
    private $usersGetter;

    /**
     * @var MembershipFileRepository
     */
    private $membershipFileRepository;

    /**
     * @var LobRepository
     */
    private $lobRepository;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var FilesystemInterface
     */
    private $remoteFilesystem;

    /**
     * @var FilesystemInterface
     */
    private $localFilesystem;

    /**
     * @var MessageBus
     */
    private $commandBus;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var AutomationRemoteFilesFinder
     */
    private $remoteFilesFinder;

    /**
     * DownloadAndProcessEdi834FileCommandHandler constructor.
     * @param CommonUsersGetter $usersGetter
     * @param MembershipFileRepository $membershipFileRepository
     * @param LobRepository $lobRepository
     * @param Connection $connection
     * @param FilesystemInterface $remoteFilesystem
     * @param FilesystemInterface $localFilesystem
     * @param MessageBus $commandBus
     * @param LoggerInterface $logger
     */
    public function __construct(
        CommonUsersGetter $usersGetter,
        MembershipFileRepository $membershipFileRepository,
        LobRepository $lobRepository,
        Connection $connection,
        FilesystemInterface $remoteFilesystem,
        FilesystemInterface $localFilesystem,
        AutomationRemoteFilesFinder $remoteFilesFinder,
        MessageBus $commandBus,
        LoggerInterface $logger
    )
    {
        $this->usersGetter = $usersGetter;
        $this->membershipFileRepository = $membershipFileRepository;
        $this->lobRepository = $lobRepository;
        $this->connection = $connection;
        $this->remoteFilesystem = $remoteFilesystem;
        $this->localFilesystem = $localFilesystem;
        $this->commandBus = $commandBus;
        $this->logger = $logger;
        $this->remoteFilesFinder = $remoteFilesFinder;
    }

    /**
     * @param DownloadAndProcessEdi834FileCommand $command
     * @throws \Exception
     */
    public function handle(DownloadAndProcessEdi834FileCommand $command): void
    {
        $startTime = new \DateTime();
        $this->logger->info('---- EDI834 files import started ' . $startTime->format('Y-m-d H:i:s') . ' -----');

        $author = $this->usersGetter->getSystemSuperAdmin();

        $remotePaths = $this->remoteFilesFinder->getRemoteFilesPathsForDate($command->getMode(), $command->getFileDate(), $author->getId());
        $filesCount = 0;
        $paths = [];

        try {
            foreach ($remotePaths as $remotePath) {
                $this->logger->info($remotePath);

                $filesCount++;

                $localPath = tempnam(sys_get_temp_dir(), 'Import');
                $this->downloadFile($remotePath, $localPath);
                $paths[$remotePath] = $localPath;
            }

            $files = [];

            $this->connection->beginTransaction();

            foreach ($paths as $remotePath => &$path) {
                $file = $this->createFileRecordInDB($remotePath, $author->getId());

                $localPath = $file->getLocalRelativePath();
                $this->moveFile($path, $localPath);
                $path = $localPath;

                $files[] = $file;
            }

            $this->connection->commit();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            // Skip all possible exceptions to ensure the deletion of files on the disk.
            try {
                $this->connection->rollBack();
            } catch (\Exception $e) {
            }

            foreach ($paths as $path) {
                $this->removeFile($path);
            }

            throw $e;
        }

        foreach ($files as $file) {
            $this->runImportCommand($file);
        }

        $this->logger->info($filesCount . ' total files found.');

        $endTime = new \DateTime();
        $this->logger->info('---- EDI834 files import finished ' . $endTime->format('Y-m-d H:i:s') . ' -----');
    }

    /**
     * @param string $remotePath
     * @param string $localPath
     * @throws \Exception
     */
    private function downloadFile(string $remotePath, string $localPath): void
    {
        $stream = $this->remoteFilesystem->readStream($remotePath);
        if (!$stream) {
            throw new \Exception('Failed to read remote file ' . $remotePath . '.');
        }

        if (!$this->localFilesystem->putStream($localPath, $stream)) {
            throw new \Exception('Failed to put data to ' . $localPath . '.');
        }
    }

    /**
     * @param string $remotePath
     * @param int $ownerId
     * @return MembershipFile
     */
    public function createFileRecordInDB(string $remotePath, int $ownerId): MembershipFile
    {
        $filename = basename($remotePath);

        //TODO: use entity instead of AR
        $file = new MembershipFile();
        $file->disableBehavior('fileBehavior');
        $file->type = MembershipFile::TYPE_834;
        $file->in_progress = 1;
        $file->filename = $filename;
        $file->filename_original = $filename;

        if ($ownerId > 0) {
            $file->author_id = $ownerId;
        }

        $file->save(false);
        $file->enableBehavior('fileBehavior');

        return $file;
    }

    /**
     * @param string $srcPath
     * @param string $destPath
     * @throws \Exception
     */
    public function moveFile(string $srcPath, string $destPath): void
    {
        $localDir = dirname($srcPath);

        if (!$this->localFilesystem->createDir($localDir)) {
            throw new \Exception('Failed to create directory ' . $localDir . '.');
        }
        $this->logger->info('Created local directory ' . $localDir . '.');

        if ($this->localFilesystem->has($destPath)) {
            throw new \Exception('Failed to rename ' . $srcPath . ' to ' . $destPath . '. File exists.');
        }

        if (!$this->localFilesystem->rename($srcPath, $destPath)) {
            throw new \Exception('Failed to rename ' . $srcPath . ' to ' . $destPath . '.');
        }

        if (!$this->localFilesystem->setVisibility($destPath, AdapterInterface::VISIBILITY_PRIVATE)) {
            throw new \Exception('Failed to change visibility for ' . $destPath . '.');
        }

        $this->logger->info('Renamed ' . $srcPath . ' to ' . $destPath . '.');
    }

    /**
     * @param string $path
     */
    public function removeFile(string $path): void
    {
        // Skip all possible exceptions to ensure the deletion of other files.
        try {
            $this->localFilesystem->delete($path);
            $this->logger->info('Removed ' . $path . '.');
        } catch (\Exception $e) {
        }
    }

    /**
     * @param MembershipFile $file
     */
    private function runImportCommand(MembershipFile $file): void
    {
        $command = new ImportFileCommand($file->id, $file->author_id, 'member-grid');
        $command->markAsBackgroundJob();
        $this->commandBus->handle($command);
    }
}
