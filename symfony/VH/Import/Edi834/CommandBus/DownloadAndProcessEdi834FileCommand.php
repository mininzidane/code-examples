<?php

/**
 * DownloadAndProcessEdi834FileCommand class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Membership\Edi834\CommandBus;

use VirtualHealth\CommandBusBundle\Middleware\BaseCommand;

/**
 * Class DownloadAndProcessEdi834FileCommand
 * @package VirtualHealth\Import\Membership\Edi834\CommandBus
 */
class DownloadAndProcessEdi834FileCommand extends BaseCommand
{
    public const MODE_CHANGE = 'change';

    public const MODE_AUDIT = 'audit';

    /**
     * @var string
     */
    private $mode;

    /**
     * @var \DateTime
     */
    private $fileDate;

    /**
     * DownloadAndProcessEdi834FileCommand constructor.
     * @param string $mode
     * @param \DateTime $fileDate
     */
    public function __construct(string $mode, \DateTime $fileDate)
    {
        $this->mode = $mode;
        $this->fileDate = $fileDate;
    }

    /**
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * @return \DateTime
     */
    public function getFileDate(): \DateTime
    {
        return $this->fileDate;
    }
}
