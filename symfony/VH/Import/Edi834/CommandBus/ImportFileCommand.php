<?php
/**
 * ImportFileCommand class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\CommandBus;

use VirtualHealth\CommandBusBundle\Middleware\BaseCommand;

/**
 * Class ImportFileCommand
 * @package VirtualHealth\Import\Membership\Edi834\CommandBus
 */
final class ImportFileCommand extends BaseCommand
{
    /**
     * @Constraints\NotBlank()
     * @Constraints\Type("integer")
     * @Constraints\GreaterThan(0)
     * @var integer
     */
    private $modelId;

    /**
     * @Constraints\NotBlank()
     * @Constraints\Type("integer")
     * @Constraints\GreaterThan(0)
     * @var integer
     */
    private $initiatorId;

    /**
     * @Constraints\NotBlank()
     * @Constraints\Type("string")
     * @var string
     */
    private $gridId;

    /**
     * @return integer
     */
    public function getInitiatorId(): int
    {
        return $this->initiatorId;
    }

    /**
     * ImportFileCommand constructor.
     * @param int $modelId
     * @param int $initiatorId
     * @param string $gridId
     */
    public function __construct($modelId, $initiatorId, $gridId)
    {
        $this->modelId = $modelId;
        $this->initiatorId = $initiatorId;
        $this->gridId = $gridId;
    }

    /**
     * @return integer
     */
    public function getModelId(): int
    {
        return $this->modelId;
    }

    /**
     * @return string
     */
    public function getGridId(): string
    {
        return $this->gridId;
    }
}
