<?php

/**
 * UploadEdi834FileCommand
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright © 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Membership\Edi834\ConsoleCommand;

use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VirtualHealth\Import\Membership\Edi834\CommandBus\DownloadAndProcessEdi834FileCommand;

/**
 * Class UploadEdi834FileCommand
 * @package VirtualHealth\Import\Membership\Edi834\ConsoleCommand
 */
class UploadEdi834FileCommand extends Command
{
    private const MODE_ARGUMENT_NAME = 'mode';

    private const DATE_OPTION_NAME = 'date';

    private const DEFAULT_CHANGE_DATE = 'yesterday';

    private const DEFAULT_AUDIT_DATE = 'previous sunday';

    /**
     * @var MessageBus
     */
    private $commandBus;

    /**
     * UploadEdi834FileCommand constructor.
     * @param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    protected function configure(): void
    {
        $this
            ->setName('vh:edi834:import')
            ->setDescription('Runs import of EDI834 files')
            ->addArgument(self::MODE_ARGUMENT_NAME, InputArgument::REQUIRED, '\'change\' - run Change Automation OR \'audit\' - run Audit Automation')
            ->addOption(self::DATE_OPTION_NAME, null, InputOption::VALUE_REQUIRED, 'Date of file');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $mode = $input->getArgument(self::MODE_ARGUMENT_NAME);
        $date = $input->getOption(self::DATE_OPTION_NAME);

        switch ($mode) {
            case DownloadAndProcessEdi834FileCommand::MODE_CHANGE:
                $date = $date ?? self::DEFAULT_CHANGE_DATE;
                break;
            case DownloadAndProcessEdi834FileCommand::MODE_AUDIT:
                $date = $date ?? self::DEFAULT_AUDIT_DATE;
                break;
            default:
                throw new InvalidArgumentException('Required attribute `mode` can be only - `change` OR `audit`');
                break;
        }

        $command = new DownloadAndProcessEdi834FileCommand($mode, new \DateTime($date));
        $command->markAsBackgroundJob();
        $this->commandBus->handle($command);

        $output->writeln('Download and process command was sent to the bus.');
    }
}
