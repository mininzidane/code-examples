<?php
/**
 * ParseLog class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;

use VirtualHealth\Import\ParseLog\ParseLogInterface;

/**
 * Wrapper-class for VirtualHealth\Import\ParseLog\ParseLog
 *
 * Class AdapterParseLog
 *
 * @package VirtualHealth\Import\Membership\Edi834
 */
class AdapterParseLog
{
    /**
     * @var ParseLogEdi834
     */
    private $parseLogEdi834;

    /**
     * ParseLog constructor.
     *
     * @param ParseLogInterface $parseLogEdi834
     */
    public function __construct(ParseLogInterface $parseLogEdi834 = null)
    {
        $this->parseLogEdi834 = $parseLogEdi834 ?: new ParseLogEdi834();
    }

    /**
     * @return ParseLogEdi834
     */
    public function getParseLogEdi834(): ParseLogEdi834
    {
        return $this->parseLogEdi834;
    }

    /**
     * @param int $num
     * @param string $subscriberId
     * @param string $message
     * @param bool $isCritical
     */
    public function addError($num, $subscriberId, $message, $isCritical = false): void
    {
        $this->parseLogEdi834->getSummary()->incrementTotalErrors();
        $this->parseLogEdi834->addErrorText($num, $message, $isCritical, ['subscriberId' => $subscriberId]);
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->parseLogEdi834->totalAddedErrors() > 0;
    }

    public function incrementTotal(): void
    {
        $this->parseLogEdi834->getSummary()->incrementTotal();
    }

    /**
     * @param int $count
     */
    public function incrementTotalCreated($count = 1): void
    {
        $this->parseLogEdi834->getSummary()->incrementTotalCreated($count);
    }

    /**
     * @param int $count
     */
    public function incrementTotalUpdated($count = 1): void
    {
        $this->parseLogEdi834->getSummary()->incrementTotalUpdated($count);
    }

    /**
     * @param int $count
     */
    public function decrementTotalCreated($count = 1): void
    {
        $this->parseLogEdi834->getSummary()->incrementTotalCreated($count);
    }

    /**
     * @param int $count
     */
    public function decrementTotalUpdated($count = 1): void
    {
        $this->parseLogEdi834->getSummary()->decrementTotalUpdate($count);
    }

    /**
     * @return array
     */
    public function getLog(): array
    {
        return [
            'total' => $this->parseLogEdi834->getSummary()->getTotalRows(),
            'totalCreated' => $this->parseLogEdi834->getSummary()->getTotalCreated(),
            'totalUpdated' => $this->parseLogEdi834->getSummary()->getTotalUpdated(),
            'totalErrors' => $this->parseLogEdi834->totalAddedErrors(),
            'criticalErrors' => $this->getCriticalErrors(),
            'errors' => $this->getErrors(),
        ];
    }

    /**
     * @param bool $critical
     *
     * @return array
     */
    private function fetchErrorsArray($critical = true): array
    {
        $criticalErrors = [];

        foreach ($this->parseLogEdi834->getRowsErrors() as $rowErrors) {
            /** @var ParseLogErrorEdi834[] $rowErrorsError */
            $rowErrorsError = $rowErrors->getErrors();

            foreach ($rowErrorsError as $rowError) {
                if ($rowError->isCritical() === $critical) {
                    $criticalErrors[$rowErrors->getRowNumber()][] =
                        [
                            'subscriberId' => $rowError->getSubscriberId(),
                            'message' => $rowError->getMessage(),
                        ];
                }
            }
        }

        return $criticalErrors;
    }

    /**
     * @return array
     */
    public function getCriticalErrors(): array
    {
        return $this->fetchErrorsArray(true);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->fetchErrorsArray(false);
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->parseLogEdi834->getSummary()->getTotalRows();
    }

    /**
     * @return int
     */
    public function getTotalErrors(): int
    {
        return $this->parseLogEdi834->getSummary()->getTotalErrors();
    }

    /**
     * @return int
     */
    public function getTotalCreated(): int
    {
        return $this->parseLogEdi834->getSummary()->getTotalCreated();
    }

    /**
     * @return int
     */
    public function getTotalUpdated(): int
    {
        return $this->parseLogEdi834->getSummary()->getTotalUpdated();
    }
}
