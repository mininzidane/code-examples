<?php
/**
 * ParseLogFile class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;

use VirtualHealth\Import\ParseLog\ParseLogInterface;
use VirtualHealth\Import\ParseLog\StorageData\StorageDataInterface;
use VirtualHealth\Import\ParseLog\Writer\WriterInterface;

/**
 * Class WriterJson
 *
 * @package VirtualHealth\Import\Membership\Edi834
 *
 * @deprecated keep only as example
 */
class WriterJsonFile implements WriterInterface
{
    /**
     * @param ParseLogInterface $parseLog
     * @param StorageDataInterface $storageData
     */
    public function store(ParseLogInterface $parseLog, StorageDataInterface $storageData): void
    {
        if (!$storageData instanceof StorageDataJsonFileDTO) {
            throw new \RuntimeException(\sprintf("Can't use StorageData object of class %s", \get_class($storageData)));
        }

        $parseLogWrapper = new AdapterParseLog($parseLog);
        $fileNameJson = LogFilePathBuilder::buildFileNameJson($storageData->getFilePath(), $storageData->getPartNum());

        if (!file_exists($fileNameJson)) {
            file_put_contents(
                $fileNameJson,
                json_encode($parseLogWrapper->getLog())
            );

            $parseLog->setStorageData($storageData);
        }
    }

    /**
     * @param ParseLogInterface $parseLog
     * @param StorageDataInterface $storageData
     */
    public function storeAndClearErrors(ParseLogInterface $parseLog, StorageDataInterface $storageData): void
    {
        $this->store($parseLog, $storageData);
        $parseLog->clearErrors();
    }
}
