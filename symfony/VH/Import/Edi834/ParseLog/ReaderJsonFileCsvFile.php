<?php
/**
 * ParseLogFile class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;

use VirtualHealth\Import\ParseLog\ParseLogInterface;
use VirtualHealth\Import\ParseLog\Reader\ReaderJsonCsvFile;
use VirtualHealth\Import\ParseLog\StorageData\StorageDataInterface;
use VirtualHealth\Import\ParseLog\ParseLog;

/**
 * Class ReaderJsonFileCsvFile
 * @package VirtualHealth\Import\Membership\Edi834
 */
class ReaderJsonFileCsvFile extends ReaderJsonCsvFile
{
    /**
     * @param StorageDataInterface $storageData
     * @param ParseLogInterface|null $emptyParseLog
     * @return ParseLog
     */
    public function restore(StorageDataInterface $storageData, ParseLogInterface $emptyParseLog = null): ParseLogInterface
    {
        if (!$storageData instanceof StorageDataPartJsonFileCsvFileDto) {
            throw new \RuntimeException(\sprintf("Can't use StorageData object of class %s", \get_class($storageData)));
        }

        $this->parseLog = new ParseLogEdi834();
        $this->storageData = $storageData;

        $fileNameJson = LogFilePathBuilder::buildFileNameJson($storageData->getFilePath(), $storageData->getPartNum());
        $fileNameCsv = LogFilePathBuilder::buildFileNameCsv($storageData->getFilePath(), $storageData->getPartNum());
        $jsonData = json_decode(file_get_contents($fileNameJson), true, 512, JSON_OBJECT_AS_ARRAY);

        if (file_exists($fileNameJson) && file_exists($fileNameCsv)) {

            //read json summary
            $this->parseLog->getSummary()->restoreData($jsonData);

            //read csv if has now limits
            if ($this->parseLog->getSummary()->getTotalErrors() <= $storageData->getMaxReadRowsCount()) {
                $this->readCsv($fileNameCsv);
            }

            $this->parseLog->setStorageData($storageData);
            return $this->parseLog;
        }

        throw new \RuntimeException('Can\'t read file csv file: ' . $this->storageData->getCsvFilePath());
    }
}
