<?php
/**
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;


use VirtualHealth\Import\ParseLog\AbstractParseLogError;
use VirtualHealth\Import\ParseLog\ParseLogErrorInterface;

/**
 * Class ParseLogErrorEdi834
 * @package VirtualHealth\Import\Membership\Edi834\ParseLog
 */
class ParseLogErrorEdi834 extends AbstractParseLogError
{
    /**
     * @var integer
     */
    private $subscriberId;

    /**
     * @return string|null
     */
    public function getSubscriberId(): ?string
    {
        return $this->subscriberId;
    }

    /**
     * @param string|null $subscriberId
     * @return ParseLogErrorEdi834
     */
    public function setSubscriberId(?string $subscriberId): ParseLogErrorEdi834
    {
        $this->subscriberId = $subscriberId;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function storeData(): array
    {
        return [
            'Record' => $this->getNum(),
            'Subscriber Id' => $this->getSubscriberId(),
            'Message' => $this->getMessage(),
            'Severity' => $this->isCritical() ? 'Critical' : 'Non critical'
        ];
    }

    /**
     * @inheritdoc
     */
    public function restoreData($data): void
    {
        $this->setNum($data[0]);
        $this->setSubscriberId($data[1]);
        $this->setMessage($data[2]);
        $this->setCritical($data[3] === 'Critical');
    }
}