<?php
/**
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;


use VirtualHealth\Import\ParseLog\ParseLog;
use VirtualHealth\Import\ParseLog\ParseLogErrorInterface;

class ParseLogEdi834 extends ParseLog
{
    /**
     * @inheritdoc
     */
    public function createNewError(): ParseLogErrorInterface
    {
        return new ParseLogErrorEdi834();
    }
}