<?php
/**
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;


use VirtualHealth\Import\ParseLog\StorageData\StorageDataJsonCsvFileDto;

/**
 * Class StorageDataPartJsonFileCsvFileDTO (for new WriterJsonFileCsvFile/ReaderJsonFileCsvFile)
 *
 * @package VirtualHealth\Import\Membership\Edi834\ParseLog
 */
class StorageDataPartJsonFileCsvFileDto extends StorageDataJsonCsvFileDto
{
    /**
     * @var integer|null
     */
    private $partNum;

    /**
     * StorageDataJsonCvs constructor.
     * @param $filePath
     * @param $partNum
     * @param $maxReadRowsCount
     */
    public function __construct($filePath, $partNum, $maxReadRowsCount = null)
    {
        parent::__construct($filePath, self::CSV_DELIMITER, true, $maxReadRowsCount);
        $this->partNum = $partNum;
    }

    /**
     * @return int
     */
    public function getPartNum(): ?int
    {
        return $this->partNum;
    }

    /**
     * @param int $partNum
     */
    public function setPartNum(int $partNum): void
    {
        $this->partNum = $partNum;
    }
}