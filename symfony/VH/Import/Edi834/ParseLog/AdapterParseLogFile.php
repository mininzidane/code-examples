<?php
/**
 * ParseLogFile class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;

/**
 * Class ParseLogFileAdapter
 * @package VirtualHealth\Import\Membership\Edi834
 */
class AdapterParseLogFile
{
    public const MAX_CSV_ROWS_LIMIT = 1000;

    /**
     * @var AdapterParseLog
     */
    private $parseLog;

    /**
     * @var string
     */
    private $filePath;

    /**
     * ParseLogFile constructor.
     * @param string $filePath
     * @param int|null $partNumber
     */

    public function __construct(string $filePath, ?int $partNumber = null)
    {
        $this->filePath = $filePath;

        $parseLogEdi834 = null;
        if (file_exists(LogFilePathBuilder::buildFileNameCsv($this->filePath, $partNumber))) {

            /* new reader (for CSV supporting) */
            $parseLogReaderJsonCsv = new ReaderJsonFileCsvFile();
            $parseLogEdi834 = $parseLogReaderJsonCsv->restore(
                new StorageDataPartJsonFileCsvFileDto($this->filePath, $partNumber, self::MAX_CSV_ROWS_LIMIT)
            );
        } elseif (file_exists(LogFilePathBuilder::buildFileNameJson($this->filePath, $partNumber))) {
            /* json reader */
            $parseLogReaderJson = new ReaderJsonFile();
            $parseLogEdi834 = $parseLogReaderJson->restore(
                new StorageDataJsonFileDTO($this->filePath, $partNumber)
            );
        }

        $this->parseLog = new AdapterParseLog($parseLogEdi834);
    }

    /**
     * @param int|null $partNumber
     * @return bool
     */
    public function save(?int $partNumber = null): ?bool
    {
        try {
            /* new writer (for CSV supporting) */
            $parseLogWriter = new WriterJsonFileCsvFile();
            $parseLogWriter->storeAndClearErrors(
                $this->parseLog->getParseLogEdi834(),
                new StorageDataPartJsonFileCsvFileDto($this->filePath, $partNumber)
            );
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return AdapterParseLog
     */
    public function getParseLog(): AdapterParseLog
    {
        return $this->parseLog;
    }

    /**
     * @param AdapterParseLog $parseLog
     */
    public function setParseLog($parseLog): void
    {
        $this->parseLog = $parseLog;
    }
}
