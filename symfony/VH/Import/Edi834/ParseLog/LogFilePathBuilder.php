<?php
/**
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;


class LogFilePathBuilder
{
    /**
     * @param $filePath
     * @param $partNum
     * @return string
     */
    public static function buildFileNameCsv($filePath, $partNum = null): string
    {
        return $filePath . '-log' . ($partNum !== null ? '-' . $partNum : '') . '.csv';
    }

    /**
     * @param $filePath
     * @param $partNum
     * @return string
     */
    public static function buildFileNameJson($filePath, $partNum = null): string
    {
        return $filePath . '-log' . ($partNum !== null ? '-' . $partNum : '') . '.json';
    }
}