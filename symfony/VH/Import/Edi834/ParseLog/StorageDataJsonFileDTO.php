<?php
/**
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;

use VirtualHealth\Import\ParseLog\StorageData\StorageDataInterface;


/**
 * Class StorageDataJsonFile (for old WriterJsonFile/ReaderJsonFile)
 *
 * @package VirtualHealth\Import\Membership\Edi834\ParseLog
 */
class StorageDataJsonFileDTO implements StorageDataInterface
{
    /**
     * @var static
     */
    private $filePath;

    /**
     * @var integer
     */
    private $partNum;

    /**
     * StorageDataJsonFile constructor.
     * @param $filePath
     * @param $partNum
     */
    public function __construct($filePath, $partNum)
    {
        $this->filePath = $filePath;
        $this->partNum = $partNum;
    }

    /**
     * @return static
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @return string|null
     */
    public function getPartNum(): ?string
    {
        return $this->partNum;
    }
}