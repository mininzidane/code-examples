<?php
/**
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;

/**
 * Class ParseLogPartsJsonCsvConsolidator
 * @package VirtualHealth\Import\Membership\Edi834\ParseLog
 */
class ParseLogPartsJsonCsvConsolidator
{
    /**
     * @var string
     */
    private $filePath;

    /**
     * @var string
     */
    private $filePathLogJson;

    /**
     * @var string
     */
    private $filePathLogCsv;

    /**
     * @var array
     */
    private $summary = [
        'total' => 0,
        'totalCreated' => 0,
        'totalUpdated' => 0,
        'totalErrors' => 0,
    ];

    /**
     * @var array
     */
    private $csvErrorsFiles = [];

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
        $this->filePathLogJson = LogFilePathBuilder::buildFileNameJson($filePath);
        $this->filePathLogCsv = LogFilePathBuilder::buildFileNameCsv($filePath);
    }

    /**
     * @param $partNumber
     */
    public function addPart($partNumber): void
    {
        $fileNameJsonPart = LogFilePathBuilder::buildFileNameJson($this->filePath, $partNumber);
        if (file_exists($fileNameJsonPart)) {
            $jsonData = json_decode(file_get_contents($fileNameJsonPart), true, 512, JSON_OBJECT_AS_ARRAY);
            $this->summary['total'] += $jsonData['total'];
            $this->summary['totalCreated'] += $jsonData['totalCreated'];
            $this->summary['totalUpdated'] += $jsonData['totalUpdated'];
            $this->summary['totalErrors'] += $jsonData['totalErrors'];
            unlink($fileNameJsonPart);
        }

        $fileNameCsvPart = LogFilePathBuilder::buildFileNameCsv($this->filePath, $partNumber);
        $this->csvErrorsFiles[] = $fileNameCsvPart;

    }

    /**
     * @return void
     */
    public function save(): void
    {
        file_put_contents(
            $this->filePathLogJson,
            json_encode($this->summary)
        );

        $fh = fopen($this->filePathLogCsv, 'wb');
        foreach ($this->csvErrorsFiles as $fileNum => $csvErrorsFile) {
            if (!file_exists($csvErrorsFile)) {
                continue;
            }
            $fhPart = fopen($csvErrorsFile, 'rb');
            $rowNumOfPart = 0;
            while (\is_resource($fhPart) && !feof($fhPart)) {
                if ($fileNum > 0 && $rowNumOfPart === 0) {
                    $rowNumOfPart++;
                    fgets($fhPart);
                    continue; //skip headers
                }
                $rowNumOfPart++;
                fwrite($fh, fgets($fhPart));
            }
            fclose($fhPart);
            unlink($csvErrorsFile);
        }
        fclose($fh);
    }
}