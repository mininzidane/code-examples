<?php
/**
 * ParseLogFile class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;

use VirtualHealth\Import\ParseLog\ParseLogInterface;
use VirtualHealth\Import\ParseLog\SimpleParseLogSummary;
use VirtualHealth\Import\ParseLog\StorageData\StorageDataInterface;
use VirtualHealth\Import\ParseLog\Reader\ReaderInterface;

/**
 * Class ParseLogWriter
 * @package VirtualHealth\Import\Membership\Edi834
 */
class ReaderJsonFile implements ReaderInterface
{
    /**
     * @param $log
     * @return ParseLogEdi834
     */
    public static function createByLogArray($log): ParseLogEdi834
    {
        $parseLog = new ParseLogEdi834();
        /** @var SimpleParseLogSummary $parseLogSummary */
        $parseLogSummary = $parseLog->getSummary();
        $parseLogSummary
            ->setTotalRows($log['total'])
            ->setTotalCreated($log['totalCreated'])
            ->setTotalCreated($log['totalUpdated'])
            ->setTotalUpdated($log['totalErrors']);

        foreach ($log['criticalErrors'] as $num => $criticalErrors) {
            foreach ($criticalErrors as $criticalError) {
                $parseLog->addErrorText(
                    $num,
                    $criticalError['message'],
                    true,
                    ['subscriberId' => $criticalError['subscriberId']]
                );
            }
        }

        foreach ($log['errors'] as $num => $errors) {
            foreach ($errors as $error) {
                $parseLog->addErrorText(
                    $num,
                    $error['message'],
                    false,
                    ['subscriberId' => $error['subscriberId']]
                );
            }
        }
        return $parseLog;
    }

    /**
     * @param StorageDataInterface $storageData
     * @param ParseLogInterface|null $emptyParseLog
     * @return ParseLogInterface
     */
    public function restore(StorageDataInterface $storageData, ParseLogInterface $emptyParseLog = null): ParseLogInterface
    {
        if (!$storageData instanceof StorageDataJsonFileDTO) {
            throw new \RuntimeException(\sprintf("Can't use StorageData object of class %s", \get_class($storageData)));
        }

        $fileNameJson = LogFilePathBuilder::buildFileNameJson($storageData->getFilePath(), $storageData->getPartNum());
        if (file_exists($fileNameJson)) {
            $jsonData = json_decode(file_get_contents($fileNameJson), true, 512, JSON_OBJECT_AS_ARRAY);
            $parseLog = self::createByLogArray($jsonData);
            $parseLog->setStorageData($storageData);
            return $parseLog;
        }

        return new ParseLogEdi834();
    }
}
