<?php
/**
 * ParseLogFile class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\ParseLog;

use VirtualHealth\Import\ParseLog\ParseLogInterface;
use VirtualHealth\Import\ParseLog\StorageData\StorageDataInterface;
use VirtualHealth\Import\ParseLog\Writer\WriterJsonCsvFile;

/**
 * Class ParseLogWriterJsonCsv
 * @package VirtualHealth\Import\Membership\Edi834\ParseLog
 */
class WriterJsonFileCsvFile extends WriterJsonCsvFile
{
    /**
     * @var StorageDataPartJsonFileCsvFileDto
     */
    protected $storageData;

    /**
     * @return string
     */
    protected function createFileNameCsv(): string
    {
        return LogFilePathBuilder::buildFileNameCsv($this->storageData->getFilePath(),
            $this->storageData->getPartNum());
    }

    /**
     * @return string
     */
    protected function createFileNameJson(): string
    {
        return LogFilePathBuilder::buildFileNameJson($this->storageData->getFilePath(),
            $this->storageData->getPartNum());
    }

    /**
     * @param ParseLogInterface $parseLog
     * @param StorageDataInterface $storageData
     * @return void
     */
    public function store(ParseLogInterface $parseLog, StorageDataInterface $storageData): void
    {
        if (!$storageData instanceof StorageDataPartJsonFileCsvFileDto) {
            throw new \RuntimeException(\sprintf("Can't use StorageData object of class %s", \get_class($storageData)));
        }

        $this->parseLog = $parseLog;

        $this->storageData = $storageData;

        $fileNameJson = $this->createFileNameJson();
        $fileNameCsv = $this->createFileNameCsv();

        if (!file_exists($fileNameJson) && !file_exists($fileNameCsv)) {

            // save json
            $json = $this->generateJson();
            file_put_contents($fileNameJson, $json);

            // save csv
            $this->writeErrorsIntoCsvFile($fileNameCsv);

            $parseLog->setStorageData($storageData);
        } else {
            throw new \RuntimeException('Log files exist');
        }
    }
}
