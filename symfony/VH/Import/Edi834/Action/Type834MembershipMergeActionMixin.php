<?php
/**
 * MembershipMergeAction class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Action;

use application\modules\membership\models\MembershipFileMergeLog;
use VirtualHealth\Import\Membership\Edi834\EventListener\PotentialMemberFoundEventListener;
use VirtualHealth\Import\Membership\Edi834\Services\ImportClientService;
use VirtualHealth\Import\Membership\Edi834\Record;
use VirtualHealth\OrmBundle\Entity\Client;

class Type834MembershipMergeActionMixin
{
    /**
     * @var \Client
     */
    public $oldClient;
    /**
     * @var MembershipFileMergeLog
     */
    public $model;
    /**
     * @var Record
     */
    public $record;

    public $resolveTaskTemporaryMemberDescription = PotentialMemberFoundEventListener::TEMPORARY_MEMBER_TASK_DESCRIPTION;

    private $importClientHelper;

    public function __construct(ImportClientService $importClientHelper)
    {
        $this->importClientHelper = $importClientHelper;
    }

    public function getMergeCaseNoteText()
    {
        return t('Model', 'Temporary Member {name} was merged with {newName} ({subscriberId}).', [
            '{name}' => $this->oldClient->getFullName(),
            '{newName}' => $this->model->client->getFullName(),
            '{subscriberId}' => $this->record->getSubscriberId(),
        ]);
    }

    /**
     * @param Client $newClient
     * @return string
     */
    public function getCreateCaseNoteText($newClient)
    {
        return t('Model', 'New member {newName} ({subscriberId}) created. Member was flagged as a potential merge with {name}.', [
            '{newName}' => $newClient->getFullName(),
            '{subscriberId}' => $newClient->getUserId(),
            '{name}' => $this->oldClient->getFullName(),
        ]);
    }

    public function prepareRowService()
    {
        $data = json_decode($this->model->data, true);

        if (isset($data['original'])) {
            $this->oldClient = clone $this->model->client;
            $this->record = unserialize($data['original']);
        } else {
            throw new \CHttpException(500, 'Member data is empty.');
        }
    }

    /**
     * @return Client|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function createClient()
    {
        return $this->importClientHelper->createClient($this->record);
    }

    /**
     * @return Client|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function mergeClient()
    {
        $this->importClientHelper->mergeClient($this->record, $this->model->client_id);
    }
}