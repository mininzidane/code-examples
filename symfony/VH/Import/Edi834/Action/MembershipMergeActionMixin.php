<?php
/**
 * MembershipMergeAction class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Action;

use application\modules\membership\actions\MembershipMergeAction;
use application\modules\membership\components\helpers\InsuranceHelper;
use application\modules\membership\models\MembershipFileMergeLog;
use application\modules\membership\parsers\member\FileParser;
use application\modules\membership\parsers\member\FileParserLog;
use application\modules\membership\parsers\member\FileRow;

class MembershipMergeActionMixin
{
    /**
     * @var \Client
     */
    public $oldClient;
    /**
     * @var MembershipFileMergeLog
     */
    public $model;
    /**
     * @var FileRow
     */
    public $row;

    public $resolveTaskTemporaryMemberDescription = 'Resolve Potential Temp Member Merge.';

    public function getMergeCaseNoteText()
    {
        return t('Model', 'Member {name} ({number}) was merged with {newName} ({subscriberId}) from the enrollment file.', [
            '{name}' => $this->oldClient->getFullName(),
            '{number}' => $this->oldClient->id_medrec,
            '{newName}' => $this->model->client->getFullName(),
            '{subscriberId}' => $this->row->memberId,
        ]);
    }

    /**
     * @param \Client $newClient
     * @return string
     */
    public function getCreateCaseNoteText($newClient)
    {
        return t('Model', 'New member {newName} ({subscriberId}) created from the enrollment file. This member was flagged as a potential merge with {name} ({number})', [
            '{newName}' => $newClient->getFullName(),
            '{subscriberId}' => $this->row->memberId,
            '{name}' => $this->oldClient->getFullName(),
            '{number}' => $this->oldClient->id_medrec,
        ]);
    }

    public function prepareRowService()
    {
        $data = json_decode($this->model->data, true);

        if (isset($data['original'])) {
            $this->oldClient = clone $this->model->client;

            $log = new FileParserLog($this->model->file);
            $parser = new FileParser($this->model->file, $log);
            $this->row = new FileRow($parser, $data['original'], 1);
        } else {
            throw new \CHttpException(500, 'Member data is empty.');
        }
    }

    /**
     * @param MembershipMergeAction $action
     * @throws \CException
     * @throws \application\modules\membership\parsers\member\FileRowServiceException
     * @return \Client
     */
    public function createClient($action)
    {
        $insuranceHelper = InsuranceHelper::newInstance(
            $action->getController()->getContainer()
        );
        $this->row->service->setInsuranceHelper($insuranceHelper);
        $this->row->service->createClient();
        return $this->row->service->getClient();
    }

    /**
     * @param MembershipMergeAction $action
     * @throws \CException
     * @throws \application\modules\membership\parsers\member\FileRowServiceException
     */
    public function mergeClient($action)
    {
        $insuranceHelper = InsuranceHelper::newInstance(
            $action->getController()->getContainer()
        );
        $this->row->service->setClient($this->model->client);
        $this->row->service->isExistingTemporaryClient = false;
        $this->row->service->setInsuranceHelper($insuranceHelper);
        $this->row->service->handleClient();
    }
}