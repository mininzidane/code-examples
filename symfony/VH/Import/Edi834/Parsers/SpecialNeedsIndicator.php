<?php
/**
 * SpecialNeedsIndicator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use application\components\actions\admin\interfaces\medi\ParseLogAction;
use VirtualHealth\Import\Membership\Edi834\Record;
use VirtualHealth\OrmBundle\Entity\SpecialNeeds;

/**
 * Class SpecialNeedsIndicator
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class SpecialNeedsIndicator implements Element
{
    /**
     * @inheritDoc
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'REF' && $elements[1] == 'XX1' && isset($elements[2])) {
            $needs = explode('#', $elements[2]);
            if (count($needs) > 1 || !empty($needs[0])) {
                $record->appendSpecialNeeds($needs);
            }
        } else {
            $today = new \DateTime('midnight'); // Y-m-d 00:00:00
            $plan = $record->getCurrentPlan();
            if (
                $plan->getEnrollmentDate() <= $today &&
                (
                    $plan->getTerminationDate() === null ||
                    $plan->getTerminationDate() >= $today
                )
            ) {
                if ($elements[0] == 'REF' && $elements[1] == 'CE' && isset($elements[2]) && 'BN' == $elements[2]) {
                    $record->appendSpecialNeeds([SpecialNeeds::NAME_FL_BNET]);
                }
                if ($elements[0] == 'REF' && $elements[1] == 'M7' && isset($elements[2]) && 'NE-STATE-WARD' == $elements[2]) {
                    $record->appendSpecialNeeds([SpecialNeeds::NAME_STATE_WARD]);
                }
            }
        }
    }
}
