<?php
/**
 * SSN class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

class SSN implements Element
{
    public function parse(Record $record, $elements)
    {
        if (isset($elements[0], $elements[8], $elements[9]) && $elements[0] === 'NM1' && $elements[8] === '34') {
            $record->setSSN($elements[9]);
        }
    }
}
