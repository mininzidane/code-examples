<?php
/**
 * Plan class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class Plan
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class Plan implements Element
{
    private const PLAN_ID = 'PLAN_ID';
    private const PLAN_ELIGIBILITY_DATE = 'PLAN_ELIGIBILITY_DATE';
    private const PLAN_TERMINATION_DATE = 'PLAN_TERMINATION_DATE';

    /**
     * @param Record $record
     * @param string[] $elements
     * @throws \Exception
     */
    public function parse(Record $record, $elements)
    {
        $this->processPlanID($record, $elements);
        $this->processPlanDate($record, $elements);
    }

    private function processPlanID(Record $record, array $elements)
    {
        $planId = null;

        if (isset($elements[0], $elements[1], $elements[2]) && $elements[0] === 'REF' && $elements[1] === 'PID') {
            $planId = $elements[2];

            $record->checkForNewPlan(self::PLAN_ID);
            $record->addPlanAttribute(self::PLAN_ID, $planId);

            if ($planId) {
                $record->setPlanId($planId);
            }
        }

    }

    private function processPlanDate(Record $record, array $elements)
    {
        $date = null;

        if ($elements[0] === 'DTP' && $elements[2] === 'D8') {
            $condition = preg_match('#^([\d]{4})([\d]{2})([\d]{2})$#', $elements[3], $matches) == 1
                && \checkdate($matches[2], $matches[3], $matches[1]);

            if ($condition) {
                $date = new \DateTime($matches[1] . '-' . $matches[2] . '-' . $matches[3]);
            } else {
                $date = null;
            }

            if ($elements[1] === '348') {
                $this->addEligibilityDate($date, $record);
            } else {
                $this->addTerminationDate($date, $record);
            }
        }
    }

    private function addEligibilityDate(?\DateTime $date, $record)
    {
        $record->checkForNewPlan(self::PLAN_ELIGIBILITY_DATE);
        $record->setPlanEnrollmentDate($date);
        $record->addPlanAttribute(self::PLAN_ELIGIBILITY_DATE, $date);
    }

    private function addTerminationDate(?\DateTime $date, $record)
    {
        $record->checkForNewPlan(self::PLAN_TERMINATION_DATE);
        $record->setPlanTerminationDate($date);
        $record->addPlanAttribute(self::PLAN_TERMINATION_DATE, $date);
    }
}

