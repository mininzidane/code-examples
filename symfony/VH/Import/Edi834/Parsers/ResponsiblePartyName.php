<?php
/**
 * ResponsiblePartyName class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class ResponsiblePartyName
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class ResponsiblePartyName implements Element
{
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'NM1' && in_array($elements[1], ['E1', 'QD', 'J6'])) {
            $record->addResponsibleMember(
                $elements[1],
                !empty($elements[4]) ? preg_replace('#[^a-zA-Z0-9\.,\-\${\ \#\(]#', '', $elements[4]) : '',
                !empty($elements[3]) ? preg_replace('#[^a-zA-Z0-9\.,\-\${\ \#\(]#', '', $elements[3]) : ''
            );
        }
    }
}
