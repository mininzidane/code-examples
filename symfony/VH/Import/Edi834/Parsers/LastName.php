<?php
/**
 * Class LastName
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

class LastName implements Element
{
    public function parse(Record $record, $elements)
    {
        if (isset($elements[0], $elements[3]) && $elements[0] === 'NM1') {
            $record->setLastName($elements[3]);
        }
    }

}
