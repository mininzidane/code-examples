<?php
/**
 * TnChoiceWaiver class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class TnChoiceWaiver
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class TnChoiceWaiver implements Element
{
    /**
     * @inheritDoc
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'REF' && $elements[1] == 'E8' && isset($elements[2])) {
            $waiverParts = explode(':', $elements[2], 2);
            if (count($waiverParts) === 2 && $waiverParts[1] !== '') {
                $record->setTnChoiceWaiver($waiverParts[1]);
            }
        }
    }
}
