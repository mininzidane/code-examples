<?php
/**
 * ReportingCategoryRange class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\InsurancePlan;
use VirtualHealth\Import\Membership\Edi834\Maps\SettingOfCareMap;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class ReportingCategoryRange
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class ReportingCategoryRange implements Element
{
    /**
     * @var SettingOfCareMap
     */
    private $settingOfCareMap;

    /**
     * @var \DateTime
     */
    private $todayDate;

    /**
     * ReportingCategoryRange constructor.
     * @param SettingOfCareMap $settingOfCareMap
     * @throws \Exception
     */
    public function __construct(SettingOfCareMap $settingOfCareMap)
    {
        $this->settingOfCareMap = $settingOfCareMap;
        $this->todayDate = new \DateTime('today');
    }

    /**
     * @param Record $record
     * @param string[] $elements
     * @throws \Exception
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] === 'DTP' && isset($elements[3]) && $elements[1] === '007') {
            $enrollmentDate = null;
            $terminationDate = null;

            if (
                $elements[2] === 'RD8'
                && preg_match(
                    '#^([\d]{4})([\d]{2})([\d]{2})\-([\d]{4})([\d]{2})([\d]{2})$#',
                    $elements[3],
                    $matches
                ) == 1
            ) {
                if (\checkdate($matches[2], $matches[3], $matches[1])) {
                    $enrollmentDate = new \DateTime($matches[1] . '-' . $matches[2] . '-' . $matches[3]);
                }
                if (\checkdate($matches[5], $matches[6], $matches[4])) {
                    $terminationDate = new \DateTime($matches[4] . '-' . $matches[5] . '-' . $matches[6]);
                }
            } elseif (
                $elements[2] === 'D8'
                && preg_match('#^([\d]{4})([\d]{2})([\d]{2})$#', $elements[3], $matches) == 1
                && \checkdate($matches[2], $matches[3], $matches[1])
            ) {
                $enrollmentDate = new \DateTime($matches[1] . '-' . $matches[2] . '-' . $matches[3]);
            }

            if ($enrollmentDate !== null) {
                foreach ($record->getPlans() as $plan) {
                    switch ($record->getCurrentReportingCategoryName()) {
                        case 'WAIVER CODE':
                            if (
                                $plan->getEnrollmentDate() == $enrollmentDate &&
                                (
                                    $terminationDate === null ||
                                    $plan->getTerminationDate() == $terminationDate
                                )
                            ) {
                                $plan->setWaiver($record->getMedicaidWaiver());
                            }

                            break;
                        case 'RATE CODE':
                            if (
                                $plan->getEnrollmentDate() == $enrollmentDate &&
                                (
                                    $terminationDate === null ||
                                    $plan->getTerminationDate() == $terminationDate
                                )
                            ) {
                                $plan->setRateCode($record->getRateCode());
                            }

                            break;
                        case 'INCARCERATED':
                            if (
                                !$record->isValidIncarcerated() &&
                                in_array($plan->getPeriod(), [InsurancePlan::PERIOD_FUTURE, InsurancePlan::PERIOD_PRESENT]) &&
                                $plan->getEnrollmentDate() == $enrollmentDate &&
                                (
                                    $terminationDate === null ||
                                    $plan->getTerminationDate() == $terminationDate
                                )
                            ) {
                                $record->setValidIncarcerated(true);
                            }

                            break;
                    }
                }
                if ($record->getCurrentReportingCategoryName() === 'SETTING OF CARE') {
                    $settingOfCareId = $this->settingOfCareMap->getIdByName($record->getSettingOfCare());
                    if ($settingOfCareId &&
                        $this->todayDate >= $enrollmentDate &&
                        (
                            null === $terminationDate ||
                            $this->todayDate <= $terminationDate
                        )
                    ) {
                        $record->setSettingOfCareId($settingOfCareId);
                    }

                }
            }
        }
    }
}
