<?php
/**
 * Address class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RegionsMap;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class Address
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class Address implements Element
{
    private const INDEX_SEGMENT = 0;

    private const SEGMENT_ADDRESS = 'N3';
    private const INDEX_ADDRESS_LINE1 = 1;
    private const INDEX_ADDRESS_LINE2 = 2;

    private const SEGMENT_CITY = 'N4';
    private const INDEX_CITY = 1;
    private const INDEX_STATE = 2;
    private const INDEX_POSTAL_CODE = 3;
    private const INDEX_COUNTY_QUALIFIER = 5;
    private const INDEX_COUNTY = 6;
    private const COUNTY_QUALIFIER = 'CY';

    /**
     * @var RegionsMap
     */
    private $regionsMap;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /**
     * @var bool $attemptAddressAnyway
     */
    private $attemptAddressAnyway;

    /**
     * Address constructor.
     *
     * @param RegionsMap $regionsMap
     * @param CitiesMap $citiesMap
     * @param bool $attemptAddressAnyway
     */
    public function __construct(RegionsMap $regionsMap, CitiesMap $citiesMap, bool $attemptAddressAnyway)
    {
        $this->regionsMap = $regionsMap;
        $this->citiesMap = $citiesMap;
        $this->attemptAddressAnyway = $attemptAddressAnyway;
    }

    public function parse(Record $record, $elements)
    {
        if ($elements[self::INDEX_SEGMENT] == self::SEGMENT_ADDRESS && isset($elements[self::INDEX_ADDRESS_LINE1])) {
            $record->setHomeAddress1($elements[self::INDEX_ADDRESS_LINE1]);
            if (isset($elements[self::INDEX_ADDRESS_LINE2])) {
                $record->setHomeAddress2($elements[self::INDEX_ADDRESS_LINE2]);
            }
        }
        if ($elements[self::INDEX_SEGMENT] == self::SEGMENT_CITY) {
            $cityTitle = $elements[self::INDEX_CITY] ?? '';
            $stateCode = $elements[self::INDEX_STATE] ?? '';
            $postalCode = $elements[self::INDEX_POSTAL_CODE] ?? '';

            $stateTitle = $this->regionsMap->getStateTitleByCode($stateCode);

            if ($stateTitle !== null && $this->citiesMap->isCorrectCity($cityTitle, $stateTitle)) {
                $record->setHomeState($stateTitle);
                $record->setHomeCity($cityTitle);
            } elseif ($this->attemptAddressAnyway && !empty($postalCode)) {
                $this->tryToFillCorrectStateAndCity($record, $postalCode, $stateTitle);
            } else {
                $record->setHomeState($stateTitle ?? 'Unknown');
                $city = $this->citiesMap->getCity('Unknown', $record->getHomeState());
                if ($city !== null) {
                    $record->setHomeCity($city->getTitle());
                }
            }

            if (!empty($postalCode)) {
                $record->setHomeZipCode($postalCode);
            }
            if (
                isset($elements[self::INDEX_COUNTY])
                && $elements[self::INDEX_COUNTY_QUALIFIER] == self::COUNTY_QUALIFIER
            ) {
                $record->setHomeCounty($elements[self::INDEX_COUNTY]);
            }
        }
    }

    /**
     * @param Record $record
     * @param string $postalCode
     * @param string|null $stateTitle
     */
    private function tryToFillCorrectStateAndCity(Record $record, string $postalCode, ?string $stateTitle): void
    {
        if ($stateTitle !== null) {
            $cityIds = $this->citiesMap->getCityIdsByZipCodeAndStateTitle($postalCode, $stateTitle);
        } else {
            $cityIds = $this->citiesMap->getCityIdsByZipCode($postalCode);
        }
        if (count($cityIds) === 1) {
            $cityId = reset($cityIds);
            $city = $this->citiesMap->getCityById($cityId);
        } else {
            $city = $this->citiesMap->getCity('Unknown', $stateTitle ?? 'Unknown');
        }
        if ($city !== null) {
            $record->setHomeState($city->getRegion()->getTitle());
            $record->setHomeCity($city->getTitle());
        }
    }
}
