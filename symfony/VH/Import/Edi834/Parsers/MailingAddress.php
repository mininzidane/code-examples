<?php
/**
 * MailingAddress class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RegionsMap;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class MailingAddress
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class MailingAddress implements Element
{
    private const N401_CITY = 1;
    private const N402_STATE = 2;
    private const N403_POSTAL_CODE = 3;

    /**
     * @var RegionsMap
     */
    private $regionsMap;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /**
     * @var bool $attemptAddressAnyway
     */
    private $attemptAddressAnyway;

    /**
     * MailingAddress constructor.
     *
     * @param RegionsMap $regionsMap
     * @param CitiesMap $citiesMap
     * @param bool $attemptAddressAnyway
     */
    public function __construct(RegionsMap $regionsMap, CitiesMap $citiesMap, bool $attemptAddressAnyway)
    {
        $this->regionsMap = $regionsMap;
        $this->citiesMap = $citiesMap;
        $this->attemptAddressAnyway = $attemptAddressAnyway;
    }

    /**
     * @inheritdoc
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'N3') {
            $record->setMailingAddress1($elements[1]);
            if (isset($elements[2])) {
                $record->setMailingAddress2($elements[2]);
            }
        }
        if ($elements[0] == 'N4') {
            $cityTitle = $elements[self::N401_CITY] ?? '';
            $stateCode = $elements[self::N402_STATE] ?? '';
            $postalCode = $elements[self::N403_POSTAL_CODE] ?? '';

            $stateTitle = $this->regionsMap->getStateTitleByCode($stateCode);

            if ($stateTitle !== null && $this->citiesMap->isCorrectCity($cityTitle, $stateTitle)) {
                $record->setMailingState($stateTitle);
                $record->setMailingCity($cityTitle);
            } elseif ($this->attemptAddressAnyway && !empty($postalCode)) {
                $this->tryToFillCorrectStateAndCity($record, $postalCode, $stateTitle);
            } else {
                if (null !== $stateTitle) {
                    $record->setMailingState($stateTitle);
                }
            }

            if (!empty($postalCode)) {
                $record->setMailingPostalCode($postalCode);
            }
        }
    }

    /**
     * @param Record $record
     * @param string $postalCode
     * @param string|null $stateTitle
     */
    private function tryToFillCorrectStateAndCity(Record $record, string $postalCode, ?string $stateTitle): void
    {
        if ($stateTitle !== null) {
            $cityIds = $this->citiesMap->getCityIdsByZipCodeAndStateTitle($postalCode, $stateTitle);
        } else {
            $cityIds = $this->citiesMap->getCityIdsByZipCode($postalCode);
        }
        if (count($cityIds) === 1) {
            $cityId = reset($cityIds);
            $city = $this->citiesMap->getCityById($cityId);
        } else {
            $city = $this->citiesMap->getCity('Unknown', $stateTitle ?? 'Unknown');
        }
        if ($city !== null) {
            $record->setMailingState($city->getRegion()->getTitle());
            $record->setMailingCity($city->getTitle());
        }
    }
}
