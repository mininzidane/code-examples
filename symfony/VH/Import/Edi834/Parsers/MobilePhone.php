<?php
/**
 * MobilePhone class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

class MobilePhone implements Element
{
    public function parse(Record $record, $elements)
    {
        if (isset($elements[0], $elements[5], $elements[8]) && $elements[0] === 'PER' && $elements[5] === 'CP') {
            $record->setMobilePhone($elements[8]);
        }
    }
}
