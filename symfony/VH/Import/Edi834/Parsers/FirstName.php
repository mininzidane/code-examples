<?php
/**
 * Class FirstName
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

class FirstName implements Element
{
    public function parse(Record $record, $elements)
    {
        if (isset($elements[0], $elements[4]) && $elements[0] === 'NM1') {
            $record->setFirstName($elements[4]);
        }
    }
}
