<?php
/**
 * CurrentIndex class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class CurrentIndex
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class CurrentIndex implements Element
{
    /**
     * @inheritdoc
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'LX') {
            $record->setCurrentIndex($elements[1]);
        }
    }
}
