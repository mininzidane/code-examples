<?php
/**
 * ResponsiblePartyPhone class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class ResponsiblePartyPhone
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class ResponsiblePartyPhone implements Element
{
    /**
     * @inheritDoc
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'PER' && isset($elements[3], $elements[4])) {
            if ($elements[3] === 'HP') {
                $record->setResponsiblePartyPrimaryPhone($elements[4]);
            } else {
                $record->setResponsiblePartyAlternatePhone($elements[4]);
            }
        }
    }
}
