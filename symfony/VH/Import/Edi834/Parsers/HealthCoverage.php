<?php
/**
 * HealthCoverage class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class HealthCoverage
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class HealthCoverage implements Element
{
    public function parse(Record $record, $elements)
    {
        /* "Plan Name:
- Plan Name field will be populated with value from ""PLAN DESCRIPTION"" column  for corresponding value in ""PLAN_CODE"" column
Service Area:
- Service Area field will be populated with value from ""PRODUCT_GEOGRAPHY"" column for corresponding value in ""PLAN_CODE"" column
Product Type:
- Product Type field will be populated with value from ""PRODUCT_CATEGORY"" column for corresponding value in ""PLAN_CODE"" column
Market:
-  Market  field will be populated with value from ""STATE"" column for corresponding value in ""PLAN_CODE"" column
LOB:
-  LOB field will be populated with value from ""LINE_OF_BUSINESS"" column for corresponding value in ""PLAN_CODE"" column
See ""Medicaid Crosswalk"" and ""Medicare Crosswalk"" tabs for columns referenced above

Logic:
IF Plan Code == ZBH-BH, then BH Plan Name field should be populated
ELSE IF Plan Code in ""HD04"" segment is listed in ""Medicaid Crosswalk"" tab, then Health Plan/Medicaid tab fields should be populated
ELSE IF Plan Code in ""HD04"" segment is listed in ""Medicare Crosswalk"" tab, then Health Plan/Medicare tab fields should be populated"
         */
        if ($elements[0] == 'HD' && isset($elements[4])) {
            $record->addPlan();
            $record->setPlanId($elements[4]);
        }
    }
}
