<?php
/**
 * CommunicationNumber class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class CommunicationNumber
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class CommunicationNumber implements Element
{
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'PER' && isset($elements[3], $elements[4])) {
            switch ($elements[3]) {
                case 'HP':
                    $record->setHomePhone($elements[4]);
                    break;
                default:
                    $record->setMobilePhone($elements[4]);
            }
        }
    }
}
