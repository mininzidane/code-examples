<?php
/**
 * HealthCoverageDate class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class HealthCoverageDate
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class HealthCoverageDate implements Element
{
    /**
     * @param Record $record
     * @param string[] $elements
     * @throws \Exception
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] === 'DTP' && isset($elements[3]) && $elements[2] === 'D8') {
            if (
                preg_match('#^([\d]{4})([\d]{2})([\d]{2})$#', $elements[3], $matches) == 1
                && \checkdate($matches[2], $matches[3], $matches[1])
            ) {
                $date = new \DateTime($matches[1] . '-' . $matches[2] . '-' . $matches[3]);
            } else {
                $date = null;
            }

            switch ($elements[1]) {
                case '348':
                    $record->setPlanEnrollmentDate($date);
                    break;
                case '349':
                    $record->setPlanTerminationDate($date);
                    break;
                default:
                    break;
            }
        }
    }
}
