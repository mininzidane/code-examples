<?php
/**
 * Class MemberNumber
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

class MemberNumber implements Element
{
    public function parse(Record $record, $elements)
    {
        if (isset($elements[0], $elements[1], $elements[2]) && $elements[0] === 'REF' && $elements[1] === '4A') {
            $record->setMemberNumber($elements[2]);
        }
    }

}
