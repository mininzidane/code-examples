<?php
/**
 * SequentialNumber class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class SequentialNumber
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class SequentialNumber implements Element
{
    /**
     * @inheritDoc
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'REF' && $elements[1] == 'ZZ' && isset($elements[2])) {
            $record->setSequentialMemberNumber($elements[2]);
        }
    }
}
