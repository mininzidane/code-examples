<?php
/**
 * SubscriberName class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class SubscriberName
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class SubscriberName implements Element
{
    /**
     * @param string $value
     * @return string
     */
    private function stripUnneededChars(string $value): string
    {
        return preg_replace('#[^a-zA-Z_0-9\- \'\`\,]#', '', $value);
    }

    /**
     * @param Record $record
     * @param string[] $elements
     */
    public function parse(Record $record, $elements): void
    {
        if ($elements[0] === 'NM1' && $elements[1] === 'IL') {
            if (isset($elements[3])) {
                $record->setSubscriberLastName(
                    $this->stripUnneededChars($elements[3])
                );
            }
            if (isset($elements[4])) {
                $record->setSubscriberFirstName(
                    $this->stripUnneededChars($elements[4])
                );
            }
            if (isset($elements[5])) {
                $record->setSubscriberMiddleName(
                    $this->stripUnneededChars($elements[5])
                );
            }
            if (isset($elements[7])) {
                $record->setSubscriberSuffix($elements[7]);
            }
        }
    }
}
