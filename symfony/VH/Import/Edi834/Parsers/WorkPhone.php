<?php
/**
 * WorkPhone class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

class WorkPhone implements Element
{
    public function parse(Record $record, $elements)
    {
        if (isset($elements[0], $elements[5], $elements[6]) && $elements[0] === 'PER' && $elements[5] === 'WP') {
            $record->setWorkPhone($elements[6]);
        }
    }
}
