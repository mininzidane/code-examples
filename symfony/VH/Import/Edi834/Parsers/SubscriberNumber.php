<?php
/**
 * SubscriberNumber class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class SubscriberNumber
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class SubscriberNumber implements Element
{
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'REF' && isset($elements[1], $elements[2])) {
            switch ($elements[1]) {
                case '23':
                    $record->setMedicaidSubscriberNumber($elements[2]);
                    break;
                case 'F6':
                    $record->setMedicareSubscriberNumber($elements[2]);
                    break;
                case '0F':
                    $record->setSubscriberId($elements[2]);
                    break;
            }
        }
    }
}
