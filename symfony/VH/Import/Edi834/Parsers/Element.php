<?php
/**
 * Element class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Interface Element
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
interface Element
{
    /**
     * @param Record $record
     * @param string[] $elements
     */
    public function parse(Record $record, $elements);
}
