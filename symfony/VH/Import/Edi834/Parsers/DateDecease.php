<?php
/**
 * DateDecrease class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class DateDecease
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class DateDecease implements Element
{
    /**
     * @param Record $record
     * @param string[] $elements
     * @throws \Exception
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'INS' && isset($elements[11], $elements[12]) && $elements[11] == 'D8') {
            if (
                preg_match('#^([\d]{4})([\d]{2})([\d]{2})$#', $elements[12], $matches) == 1
                && \checkdate($matches[2], $matches[3], $matches[1])
            ) {
                $record->setDeathDate(
                    new \DateTime($matches[1] . '-' . $matches[2] . '-' . $matches[3])
                );
            }
        }
    }
}
