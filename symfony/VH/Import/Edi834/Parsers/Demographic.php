<?php
/**
 * Demographics class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class Demographic
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class Demographic implements Element
{
    /**
     * @param Record $record
     * @param string[] $elements
     * @throws \Exception
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'DMG') {
            if ($elements[1] == 'D8') {
                if (
                    preg_match('#^([\d]{4})([\d]{2})([\d]{2})$#', $elements[2], $matches) == 1
                    && \checkdate($matches[2], $matches[3], $matches[1])
                ) {
                    $record->setDateOfBirth(
                        new \DateTime($matches[1] . '-' . $matches[2] . '-' . $matches[3])
                    );
                }
            }
            if (isset($elements[3])) {
                switch ($elements[3]) {
                    case 'M':
                        $record->setSex('male');
                        break;
                    case 'F':
                        $record->setSex('female');
                        break;
                    case 'U':
                        $record->setSex('none');
                        break;
                }
            }
            if (isset($elements[5])) {
                switch ($elements[5]) {
                    case 'A':
                    case 'D':
                        $record->setRace('asian');
                        break;
                    case 'B':
                        $record->setRace('black_or_african_american');
                        break;
                    case 'C':
                        $record->setRace('white');
                        break;
                    case 'E':
                        $record->setRace('other');
                        break;
                    case 'H':
                        $record->setEthnicity('hispanic_latino');
                        break;
                    case 'I':
                    case 'F':
                        $record->setRace('hawaiian_or_pacific');
                        break;
                    case 'J':
                        $record->setRace('american_indian');
                        break;
                    case 'N':
                        $record->setRace('black_or_african_american');
                        $record->setEthnicity('not_hispanic_latino');
                        break;
                    case 'O':
                        $record->setRace('white');
                        $record->setEthnicity('not_hispanic_latino');
                        break;
                    case 'P':
                        $record->setRace('hawaiian_or_pacific');
                        break;
                    case '7':
                        $record->setRace('declined');
                        break;
                    case '8':
                    case 'Z':
                        $record->setRace('other');
                        break;
                }
            }
        }
    }
}
