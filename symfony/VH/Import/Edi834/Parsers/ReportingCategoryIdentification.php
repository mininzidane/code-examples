<?php
/**
 * ReportingCategoryIdentification class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class ReportingCategoryIdentification
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class ReportingCategoryIdentification implements Element
{
    /**
     * @var string[]
     */
    private $genderIdentityMap = [
        'IF' => \Customer::GENDER_IDENTITY_FEMALE,
        'IM' => \Customer::GENDER_IDENTITY_MALE,
        'NB' => \Customer::GENDER_IDENTITY_GENDERQUEER,
        'MTF' => \Customer::GENDER_IDENTITY_TRANSGENDER_WOMAN,
        'FTM' => \Customer::GENDER_IDENTITY_TRANSGENDER_MAN,

    ];

    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'REF' && $elements[1] == 'ZZ') {
            $value = $elements[2] ?? '';

            switch ($record->getCurrentReportingCategoryName()) {
                case 'ALTERNATE MEMBER ID':
                    $record->setAlternateMemberNumber($value);
                    break;
                case 'WAIVER CODE':
                    $record->setMedicaidWaiver($value);
                    break;
                case 'MCO TO':
                    $record->setToc($value);
                    break;
                case 'FRG':
                    $record->setFrg($value);
                    break;
                case 'RATE CODE':
                    $record->setRateCode($value);
                    break;
                case 'IDENTIFIED GENDER':
                    if (isset($this->genderIdentityMap[$value])) {
                        $record->setGenderIdentity($this->genderIdentityMap[$value]);
                    }
                    break;
                case 'SETTING OF CARE':
                    $record->setSettingOfCare($value);
                    break;
                case 'SPECIAL NEEDS NY CHILD':
                    $record->setSpecialNeedsNYChildCode($value);
                    break;
            }
        }
    }
}
