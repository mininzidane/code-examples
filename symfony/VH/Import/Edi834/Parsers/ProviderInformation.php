<?php
/**
 * ProviderInformation class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class ProviderInformation
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class ProviderInformation implements Element
{
    /**
     * @inheritDoc
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'NM1' && $elements[1] == 'P3' && isset($elements[8], $elements[9])) {
            switch ($elements[8]) {
                case 'XX':
                    $record->setProviderNpi($elements[9]);
                    break;
                case 'SV':
                    $record->setProviderId($elements[9]);
                    break;
            }
        }
    }
}
