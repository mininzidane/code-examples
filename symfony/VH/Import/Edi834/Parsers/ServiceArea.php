<?php
/**
 * Class ServiceArea
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

class ServiceArea implements Element
{
    public function parse(Record $record, $elements)
    {
        if (isset($elements[0], $elements[1], $elements[2]) && $elements[0] === 'REF' && $elements[1] === 'CE') {
            $serviceArea = $elements[2];
            $record->setServiceArea($this->map[$serviceArea] ?? null);
        }
    }

    private $map = [
        'N100' =>'NC R1 Avery 006',
        'N101' =>'NC R1 Buncombe 011',
        'N102' =>'NC R1 Burke 012',
        'N103' =>'NC R1 Caldwell 014',
        'N104' =>'NC R1 Cherokee 020',
        'N105' =>'NC R1 Clay 022',
        'N106' =>'NC R1 Graham 038',
        'N107' =>'NC R1 Haywood 044',
        'N108' =>'NC R1 Henderson 045',
        'N109' =>'NC R1 Jackson 050',
        'N110' =>'NC R1 Macon 056',
        'N111' =>'NC R1 Madison 057',
        'N112' =>'NC R1 McDowell 059',
        'N113' =>'NC R1 Mitchell 061',
        'N114' =>'NC R1/Polk 075',
        'N115' =>'NC R1 Rutherford 081',
        'N116' =>'NC R1 Swain 087',
        'N117' =>'NC R1 Transylvania 088',
        'N118' =>'NC R1 Yancey 100',
        'N200' =>'NC R2 Alleghany 003',
        'N201' =>'NC R2 Ashe 005',
        'N202' =>'NC R2 Davidson 029',
        'N203' =>'NC R2 Davie 030',
        'N204' =>'NC R2 Forsyth 034',
        'N205' =>'NC R2 Guilford 041',
        'N206' =>'NC R2 Randolph 076',
        'N207' =>'NC R2 Rockingham 079',
        'N208' =>'NC R2 Stokes 085',
        'N209' =>'NC R2 Surry 086',
        'N210' =>'NC R2 Watauga 095',
        'N211' =>'NC R2 Wilkes 097',
        'N212' =>'NC R2 Yadkin 099',
        'N300' =>'NC R3 Alexander 002',
        'N301' =>'NC R3 Anson 004',
        'N302' =>'NC R3 Cabarrus 013',
        'N303' =>'NC R3 Catawba 018',
        'N304' =>'NC R3 Cleveland 023',
        'N305' =>'NC R3 Gaston 036',
        'N306' =>'NC R3 Iredell 049',
        'N307' =>'NC R3 Lincoln 055',
        'N308' =>'NC R3 Mecklenburg 060',
        'N309' =>'NC R3 Rowan 080',
        'N310' =>'NC R3 Stanly 084',
        'N311' =>'NC R3 Union 090',
        'N400' =>'NC R4 Alamance 001',
        'N401' =>'NC R4 Caswell 017',
        'N402' =>'NC R4 Chatham 019',
        'N403' =>'NC R4 Durham 032',
        'N404' =>'NC R4 Franklin 035',
        'N405' =>'NC R4 Granville 039',
        'N406' =>'NC R4 Johnston 051',
        'N407' =>'NC R4 Nash 064',
        'N408' =>'NC R4 Orange 068',
        'N409' =>'NC R4 Person 073',
        'N410' =>'NC R4 Vance 091',
        'N411' =>'NC R4 Wake 092',
        'N412' =>'NC R4 Warren 093',
        'N413' =>'NC R4Wilson 098',
        'N500' =>'NC R5 Bladen 009',
        'N501' =>'NC R5 Brunswick 010',
        'N502' =>'NC R5 Columbus 024',
        'N503' =>'NC R5 Cumberland 026',
        'N504' =>'NC R5 Harnett 043',
        'N505' =>'NC R5 Hoke 047',
        'N506' =>'NC R5 Lee 053',
        'N507' =>'NC R5 Montgomery 062',
        'N508' =>'NC R5 Moore 063',
        'N509' =>'NC R5 New Hanover 065',
        'N510' =>'NC R5 Pender 071',
        'N511' =>'NC R5 Richmond 077',
        'N512' =>'NC R5 Robeson 078',
        'N513' =>'NC R5 Sampson 082',
        'N514' =>'NC R5 Scotland 083',
        'N600' =>'NC R6 Beaufort 007',
        'N601' =>'NC R6 Bertie 008',
        'N602' =>'NC R6 Camden 015',
        'N603' =>'NC R6 Carteret 016',
        'N604' =>'NC R6 Chowan 021',
        'N605' =>'NC R6 Craven 025',
        'N606' =>'NC R6 Currituck 027',
        'N607' =>'NC R6 Dare 028',
        'N608' =>'NC R6 Duplin 031',
        'N609' =>'NC R6 Edgecombe 033',
        'N610' =>'NC R6 Gates 037',
        'N611' =>'NC R6 Greene 040',
        'N612' =>'NC R6 Halifax 042',
        'N613' =>'NC R6 Hertford 046',
        'N614' =>'NC R6 Hyde 048',
        'N615' =>'NC R6 Jones 052',
        'N616' =>'NC R6 Lenoir 054',
        'N617' =>'NC R6 Martin 058',
        'N618' =>'NC R6 Northampton 066',
        'N619' =>'NC R6 Onslow 067',
        'N620' =>'NC R6 Pamlico 069',
        'N621' =>'NC R6 Pasquotank 070',
        'N622' =>'NC R6 Perquimans 072',
        'N623' =>'NC R6 Pitt 074',
        'N624' =>'NC R6 Tyrrell 089',
        'N625' =>'NC R6 Washington 094',
        'N626' =>'NC R6 Wayne 096',
        'NC97' => 'NC Out of State',
        'NC98' => 'NC Default Class',
    ];
}
