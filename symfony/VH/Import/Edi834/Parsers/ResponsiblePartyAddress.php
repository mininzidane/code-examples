<?php
/**
 * ResponsiblePartyAddress class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Maps\RegionsMap;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class ResponsiblePartyAddress
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class ResponsiblePartyAddress implements Element
{
    /**
     * @var RegionsMap
     */
    private $regionsMap;

    /**
     * ResponsiblePartyAddress constructor.
     * @param RegionsMap $regionsMap
     */
    public function __construct(RegionsMap $regionsMap)
    {
        $this->regionsMap = $regionsMap;
    }

    /**
     * @inheritdoc
     */
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'N3' && isset($elements[1])) {
            $record->setResponsiblePartyAddressLine1($elements[1]);
            if (!empty($elements[2])) {
                $record->setResponsiblePartyAddressLine2($elements[2]);
            }
        }
        if ($elements[0] == 'N4' && isset($elements[1])) {
            $record->setResponsiblePartyCity($elements[1]);
            if (!empty($elements[2])) {
                $stateCode = $this->regionsMap->getStateTitleByCode($elements[2]);
                if ($stateCode !== null) {
                    $record->setResponsiblePartyState($stateCode);
                }
            }
            if (!empty($elements[3])) {
                $record->setResponsiblePartyPostalCode($elements[3]);
            }
        }
    }
}
