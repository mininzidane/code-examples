<?php
/**
 * ReportingCategoryName class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Parsers;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class ReportingCategoryName
 * @package VirtualHealth\Import\Membership\Edi834\Parsers
 */
class ReportingCategoryName implements Element
{
    public function parse(Record $record, $elements)
    {
        if ($elements[0] == 'N1' && $elements[1] == '75' && isset($elements[2])) {
            $record->setCurrentReportingCategoryName(strtoupper($elements[2]));
        }
    }
}
