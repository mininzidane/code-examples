<?php
/**
 * Record class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834;

use VirtualHealth\Import\Membership\Edi834\Exception\PlanNotFoundException;
use VirtualHealth\OrmBundle\Entity\SpecialNeeds;

/**
 * Class Record
 *
 * @package VirtualHealth\Import\Membership\Edi834
 */
class Record
{
    /**
     * @var int
     */
    private $currentIndex;

    /**
     * @var int
     */
    private $processedLine;

    /**
     * @var string
     */
    private $subscriberId;

    /**
     * @var \DateTime
     */
    private $deathDate;

    private $medicaidRenewalDate;

    /**
     * @var string
     */
    private $medicaidSubscriberNumber;

    /**
     * @var string
     */
    private $medicareSubscriberNumber;

    /**
     * @var string
     */
    private $subscriberFirstName;

    private $lastName;

    private $firstName;

    private $middleName;

    private $SSN;

    private $planAttributes = [];

    public function checkForNewPlan($attrId)
    {
        if (count($this->plans) === 0 || $this->planAttrIdExists($attrId)) {
            $this->addPlan();
        }
    }

    public function addPlanAttribute($attrId, $value)
    {
        if ($this->planAttrIdExists($attrId) === false) {
            $this->planAttributes[$this->planIndex][$attrId] = $value;
        }
    }

    private function planAttrIdExists($attrId)
    {
        return isset($this->planAttributes[$this->planIndex]) 
            && isset($this->planAttributes[$this->planIndex][$attrId]);
    }

    /**
     * @var string
     */
    private $subscriberLastName;

    /**
     * @var string
     */
    private $subscriberMiddleName;

    /**
     * @var string
     */
    private $subscriberSuffix;

    /**
     * @var string
     */
    private $homePhone;

    /**
     * @var string
     */
    private $workPhone;

    /**
     * @var string
     */
    private $mobilePhone;

    /**
     * @var string
     */
    private $familyId;

    /**
     * @var string
     */
    private $homeAddress1;

    /**
     * @var string
     */
    private $homeAddress2;

    /**
     * @var string
     */
    private $homeCity = 'Unknown';

    /**
     * @var string
     */
    private $homeState = 'Unknown';

    /**
     * @var string
     */
    private $homeZipCode;

    /**
     * @var string
     */
    private $homeCounty;

    /**
     * @var \DateTime
     */
    private $dateOfBirth;

    /**
     * @var string
     */
    private $sex;

    /**
     * @var string
     */
    private $ethnicity;

    /**
     * @var string
     */
    private $race;

    /**
     * @var string
     */
    private $language;

    /**
     * @var string
     */
    private $mailingAddress1;

    /**
     * @var string
     */
    private $mailingAddress2;

    /**
     * @var string
     */
    private $mailingCity;

    /**
     * @var string
     */
    private $mailingState;

    /**
     * @var string
     */
    private $mailingPostalCode;

    /**
     * @var string
     */
    private $currentReportingCategoryName;

    private $memberNumber;

    /**
     * @var string
     */
    private $alternateMemberNumber;

    /**
     * @var string
     */
    private $sequentialMemberNumber;

    /**
     * @var string
     */
    private $medicaidWaiver;

    /**
     * @var string
     */
    private $toc;

    /**
     * @var string
     */
    private $providerNpi;

    /**
     * @var int
     */
    private $providerId;

    /**
     * @var int
     */
    private $planIndex = -1;

    /**
     * @var InsurancePlan[]
     */
    private $plans = [];

    /**
     * @var bool
     */
    private $hasBhPlan = false;

    /**
     * @var string
     */
    private $frg;

    /**
     * @var string
     */
    private $rateCode;

    /**
     * @var string
     */
    private $genderIdentity;

    /**
     * @var bool
     */
    private $validIncarcerated = false;

    /**
     * @var int
     */
    private $responsibleMemberIndex = -1;

    /**
     * @var ResponsibleMember[]
     */
    private $responsibleMembers = [];

    /**
     * @var string
     */
    private $responsibleEntityIdentifierCode;

    /**
     * @var string
     */
    private $responsiblePartyFirstName;

    private $serviceArea;

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param mixed $middleName
     */
    public function setMiddleName($middleName): void
    {
        $this->middleName = $middleName;
    }

    /**
     * @return mixed
     */
    public function getSSN()
    {
        return $this->SSN;
    }

    /**
     * @param mixed $SSN
     */
    public function setSSN($SSN): void
    {
        $this->SSN = $SSN;
    }

    /**
     * @var string
     */
    private $responsiblePartyLastName;

    /**
     * @var string
     */
    private $settingOfCare;

    /**
     * @var int
     */
    private $settingOfCareId;

    /**
     * @var string
     */
    private $specialNeedsNYChildCode = '';

    /**
     * @var bool
     */
    private $flBnetSpecialNeed = false;

    /**
     * @var bool
     */
    private $stateWardSpecialNeed = false;

    /**
     * @param int $currentIndex
     *
     * @return Record
     */
    public function setCurrentIndex($currentIndex)
    {
        $this->currentIndex = $currentIndex;

        return $this;
    }

    /**
     * @return int
     */
    public function getProcessedLine()
    {
        return $this->processedLine;
    }

    /**
     * @param int $processedLine
     *
     * @return Record
     */
    public function setProcessedLine($processedLine)
    {
        $this->processedLine = $processedLine;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriberId()
    {
        return $this->subscriberId;
    }

    /**
     * @param string $subscriberId
     *
     * @return Record
     */
    public function setSubscriberId($subscriberId)
    {
        $this->subscriberId = $subscriberId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeathDate()
    {
        return $this->deathDate;
    }

    /**
     * @param \DateTime $deathDate
     *
     * @return Record
     */
    public function setDeathDate($deathDate)
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMedicaidRenewalDate()
    {
        return $this->medicaidRenewalDate;
    }

    /**
     * @param mixed $medicaidRenewalDate
     */
    public function setMedicaidRenewalDate($medicaidRenewalDate): void
    {
        $this->medicaidRenewalDate = $medicaidRenewalDate;
    }

    /**
     * @return string
     */
    public function getMedicaidSubscriberNumber()
    {
        return $this->medicaidSubscriberNumber;
    }

    /**
     * @param string $medicaidSubscriberNumber
     *
     * @return Record
     */
    public function setMedicaidSubscriberNumber($medicaidSubscriberNumber)
    {
        $this->medicaidSubscriberNumber = $medicaidSubscriberNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getMedicareSubscriberNumber()
    {
        return $this->medicareSubscriberNumber;
    }

    /**
     * @param string $medicareSubscriberNumber
     *
     * @return Record
     */
    public function setMedicareSubscriberNumber($medicareSubscriberNumber)
    {
        $this->medicareSubscriberNumber = $medicareSubscriberNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriberFirstName()
    {
        return $this->subscriberFirstName;
    }

    /**
     * @param string $subscriberFirstName
     *
     * @return Record
     */
    public function setSubscriberFirstName($subscriberFirstName)
    {
        $this->subscriberFirstName = $subscriberFirstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriberLastName()
    {
        return $this->subscriberLastName;
    }

    /**
     * @param string $subscriberLastName
     *
     * @return Record
     */
    public function setSubscriberLastName($subscriberLastName)
    {
        $this->subscriberLastName = $subscriberLastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriberMiddleName()
    {
        return $this->subscriberMiddleName;
    }

    /**
     * @param string $subscriberMiddleName
     *
     * @return Record
     */
    public function setSubscriberMiddleName($subscriberMiddleName)
    {
        $this->subscriberMiddleName = $subscriberMiddleName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriberSuffix()
    {
        return $this->subscriberSuffix;
    }

    /**
     * @param string $subscriberSuffix
     *
     * @return Record
     */
    public function setSubscriberSuffix($subscriberSuffix)
    {
        $this->subscriberSuffix = $subscriberSuffix;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkPhone()
    {
        return $this->workPhone;
    }

    /**
     * @param mixed $workPhone
     */
    public function setWorkPhone($workPhone): void
    {
        $this->workPhone = $workPhone;
    }

    /**
     * @return string
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }

    /**
     * @param string $homePhone
     *
     * @return Record
     */
    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @param string $mobilePhone
     *
     * @return Record
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getFamilyId()
    {
        return $this->familyId;
    }

    /**
     * @param string $familyId
     *
     * @return Record
     */
    public function setFamilyId($familyId)
    {
        $this->familyId = $familyId;

        return $this;
    }

    /**
     * @return string
     */
    public function getHomeAddress1()
    {
        return $this->homeAddress1;
    }

    /**
     * @param string $homeAddress1
     *
     * @return Record
     */
    public function setHomeAddress1($homeAddress1)
    {
        $this->homeAddress1 = $homeAddress1;

        return $this;
    }

    /**
     * @return string
     */
    public function getHomeAddress2()
    {
        return $this->homeAddress2;
    }

    /**
     * @param string $homeAddress2
     *
     * @return Record
     */
    public function setHomeAddress2($homeAddress2)
    {
        $this->homeAddress2 = $homeAddress2;

        return $this;
    }

    /**
     * @return string
     */
    public function getHomeCity()
    {
        return $this->homeCity;
    }

    /**
     * @param string $homeCity
     *
     * @return Record
     */
    public function setHomeCity($homeCity)
    {
        $this->homeCity = $homeCity;

        return $this;
    }

    /**
     * @return string
     */
    public function getHomeState()
    {
        return $this->homeState;
    }

    /**
     * @param string $homeState
     *
     * @return Record
     */
    public function setHomeState($homeState)
    {
        $this->homeState = $homeState;

        return $this;
    }

    /**
     * @return string
     */
    public function getHomeZipCode()
    {
        return $this->homeZipCode;
    }

    /**
     * @param string $homeZipCode
     *
     * @return Record
     */
    public function setHomeZipCode($homeZipCode)
    {
        $this->homeZipCode = $homeZipCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getHomeCounty()
    {
        return $this->homeCounty;
    }

    /**
     * @param string $homeCounty
     *
     * @return Record
     */
    public function setHomeCounty($homeCounty)
    {
        $this->homeCounty = $homeCounty;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param string $dateOfBirth
     *
     * @return Record
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     *
     * @return Record
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * @return string
     */
    public function getEthnicity()
    {
        return $this->ethnicity;
    }

    /**
     * @param string $ethnicity
     *
     * @return Record
     */
    public function setEthnicity($ethnicity)
    {
        $this->ethnicity = $ethnicity;

        return $this;
    }

    /**
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @param string $race
     *
     * @return Record
     */
    public function setRace($race)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     *
     * @return Record
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailingAddress1()
    {
        return $this->mailingAddress1;
    }

    /**
     * @param string $mailingAddress1
     *
     * @return Record
     */
    public function setMailingAddress1($mailingAddress1)
    {
        $this->mailingAddress1 = $mailingAddress1;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailingAddress2()
    {
        return $this->mailingAddress2;
    }

    /**
     * @param string $mailingAddress2
     *
     * @return Record
     */
    public function setMailingAddress2($mailingAddress2)
    {
        $this->mailingAddress2 = $mailingAddress2;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailingCity()
    {
        return $this->mailingCity;
    }

    /**
     * @param string $mailingCity
     *
     * @return Record
     */
    public function setMailingCity($mailingCity)
    {
        $this->mailingCity = $mailingCity;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailingState()
    {
        return $this->mailingState;
    }

    /**
     * @param string $mailingState
     *
     * @return Record
     */
    public function setMailingState($mailingState)
    {
        $this->mailingState = $mailingState;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailingPostalCode()
    {
        return $this->mailingPostalCode;
    }

    /**
     * @param string $mailingPostalCode
     *
     * @return Record
     */
    public function setMailingPostalCode($mailingPostalCode)
    {
        $this->mailingPostalCode = $mailingPostalCode;

        return $this;
    }

    /**
     * @param string $responsibleEntityIdentifierCode
     *
     * @return Record
     */
    public function setResponsibleEntityIdentifierCode($responsibleEntityIdentifierCode)
    {
        $this->responsibleEntityIdentifierCode = $responsibleEntityIdentifierCode;

        return $this;
    }

    /**
     * @param string $responsiblePartyFirstName
     *
     * @return Record
     */
    public function setResponsiblePartyFirstName($responsiblePartyFirstName)
    {
        $this->responsiblePartyFirstName = $responsiblePartyFirstName;

        return $this;
    }

    /**
     * @param string $responsiblePartyLastName
     *
     * @return Record
     */
    public function setResponsiblePartyLastName($responsiblePartyLastName)
    {
        $this->responsiblePartyLastName = $responsiblePartyLastName;

        return $this;
    }

    /**
     * @param string $responsiblePartyPrimaryPhone
     *
     * @return Record
     */
    public function setResponsiblePartyPrimaryPhone($responsiblePartyPrimaryPhone)
    {
        $this->responsibleMembers[$this->responsibleMemberIndex]->setPrimaryPhone($responsiblePartyPrimaryPhone);

        return $this;
    }

    /**
     * @param string $responsiblePartyAlternatePhone
     *
     * @return Record
     */
    public function setResponsiblePartyAlternatePhone($responsiblePartyAlternatePhone)
    {
        $this->responsibleMembers[$this->responsibleMemberIndex]->setAlternatePhone($responsiblePartyAlternatePhone);

        return $this;
    }

    /**
     * @param string $responsiblePartyAddressLine1
     *
     * @return Record
     */
    public function setResponsiblePartyAddressLine1($responsiblePartyAddressLine1)
    {
        $this->responsibleMembers[$this->responsibleMemberIndex]->setAddressLine1($responsiblePartyAddressLine1);

        return $this;
    }

    /**
     * @param string $responsiblePartyAddressLine2
     *
     * @return Record
     */
    public function setResponsiblePartyAddressLine2($responsiblePartyAddressLine2)
    {
        $this->responsibleMembers[$this->responsibleMemberIndex]->setAddressLine2($responsiblePartyAddressLine2);

        return $this;
    }

    /**
     * @param string $responsiblePartyCity
     *
     * @return Record
     */
    public function setResponsiblePartyCity($responsiblePartyCity)
    {
        $this->responsibleMembers[$this->responsibleMemberIndex]->setCityTitle($responsiblePartyCity);

        return $this;
    }

    /**
     * @param string $responsiblePartyState
     *
     * @return Record
     */
    public function setResponsiblePartyState($responsiblePartyState)
    {
        $this->responsibleMembers[$this->responsibleMemberIndex]->setRegionTitle($responsiblePartyState);

        return $this;
    }

    /**
     * @param string $responsiblePartyPostalCode
     *
     * @return Record
     */
    public function setResponsiblePartyPostalCode($responsiblePartyPostalCode)
    {
        $this->responsibleMembers[$this->responsibleMemberIndex]->setPostalCode($responsiblePartyPostalCode);

        return $this;
    }

    /**
     * @param string $planId
     *
     * @return Record
     */
    public function setPlanId($planId)
    {
        if (isset($this->plans[$this->planIndex])) {
            $this->plans[$this->planIndex]->setId($planId);
        }

        if ('ZBH-BH' === $planId) {
            $this->hasBhPlan = true;
        }

        return $this;
    }

    /**
     * @param \DateTime $planEnrollmentDate
     *
     * @return Record
     */
    public function setPlanEnrollmentDate($planEnrollmentDate)
    {
        if (isset($this->plans[$this->planIndex])) {
            $this->plans[$this->planIndex]->setEnrollmentDate($planEnrollmentDate);
        }

        return $this;
    }

    /**
     * @param \DateTime $planTerminationDate
     *
     * @return Record
     */
    public function setPlanTerminationDate($planTerminationDate)
    {
        if (isset($this->plans[$this->planIndex])) {
            $this->plans[$this->planIndex]->setTerminationDate($planTerminationDate);
        }

        return $this;
    }

    /**
     * @param string $spanId
     *
     * @return Record
     */
    public function setSpanId($spanId)
    {
        $this->plans[$this->planIndex]->setSpanId($spanId);

        return $this;
    }

    /**
     * @param string[] $specialNeeds
     *
     * @return Record
     */
    public function setSpecialNeeds($specialNeeds)
    {
        $this->flBnetSpecialNeed = $this->flBnetSpecialNeed
            ?: in_array(SpecialNeeds::NAME_FL_BNET, $specialNeeds);
        $this->stateWardSpecialNeed = $this->stateWardSpecialNeed
            ?: in_array(SpecialNeeds::NAME_STATE_WARD, $specialNeeds);

        $this->plans[$this->planIndex]->setSpecialNeeds(array_unique($specialNeeds));

        return $this;
    }

    /**
     * @return bool
     */
    public function isFlBnetSpecialNeed(): bool
    {
        return $this->flBnetSpecialNeed;
    }

    /**
     * @return bool
     */
    public function isStateWardSpecialNeed(): bool
    {
        return $this->stateWardSpecialNeed;
    }

    /**
     * @param string[] $specialNeeds
     *
     * @return Record
     *
     * @throws \RuntimeException
     */
    public function appendSpecialNeeds(array $specialNeeds): self
    {
        return $this->setSpecialNeeds(array_merge($this->getCurrentPlan()->getSpecialNeeds(), $specialNeeds));
    }

    /**
     * @return string
     */
    public function getCurrentReportingCategoryName()
    {
        return $this->currentReportingCategoryName;
    }

    /**
     * @param string $currentReportingCategoryName
     *
     * @return Record
     */
    public function setCurrentReportingCategoryName($currentReportingCategoryName)
    {
        $this->currentReportingCategoryName = $currentReportingCategoryName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMemberNumber()
    {
        return $this->memberNumber;
    }

    /**
     * @param mixed $memberNumber
     */
    public function setMemberNumber($memberNumber): void
    {
        $this->memberNumber = $memberNumber;
    }

    /**
     * @return string
     */
    public function getAlternateMemberNumber()
    {
        return $this->alternateMemberNumber;
    }

    /**
     * @param string $alternateMemberNumber
     *
     * @return Record
     */
    public function setAlternateMemberNumber($alternateMemberNumber)
    {
        $this->alternateMemberNumber = $alternateMemberNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getSequentialMemberNumber()
    {
        return $this->sequentialMemberNumber;
    }

    /**
     * @param string $sequentialMemberNumber
     *
     * @return Record
     */
    public function setSequentialMemberNumber($sequentialMemberNumber)
    {
        $this->sequentialMemberNumber = $sequentialMemberNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getMedicaidWaiver()
    {
        return $this->medicaidWaiver;
    }

    /**
     * @param string $medicaidWaiver
     *
     * @return Record
     */
    public function setMedicaidWaiver($medicaidWaiver)
    {
        $this->medicaidWaiver = $medicaidWaiver;

        return $this;
    }

    /**
     * @return string
     */
    public function getProviderNpi()
    {
        return $this->providerNpi;
    }

    /**
     * @param string $providerNpi
     *
     * @return Record
     */
    public function setProviderNpi($providerNpi)
    {
        $this->providerNpi = $providerNpi;

        return $this;
    }

    /**
     * @return int
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    public function getPlanIndex()
    {
        return $this->planIndex;
    }

    /**
     * @param int $providerId
     *
     * @return Record
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;

        return $this;
    }

    /**
     * @return string
     */
    public function getToc()
    {
        return $this->toc;
    }

    /**
     * @param string $toc
     *
     * @return Record
     */
    public function setToc($toc)
    {
        $this->toc = $toc;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasMedicaid()
    {
        return !empty($this->medicaidSubscriberNumber);
    }

    /**
     * @return bool
     */
    public function hasMedicare()
    {
        return !empty($this->medicareSubscriberNumber);
    }

    /**
     * @return bool
     */
    public function hasProviderId()
    {
        return !empty($this->providerId);
    }

    /**
     * @return bool
     */
    public function hasProviderNpi()
    {
        return !empty($this->providerNpi);
    }

    /**
     * Add a new plan
     */
    public function addPlan()
    {
        $this->planIndex++;
        $this->plans[$this->planIndex] = new InsurancePlan();
    }

    /**
     * @return InsurancePlan[]
     */
    public function getPlans(): array
    {
        return $this->plans;
    }

    /**
     * @return bool
     */
    public function isBh()
    {
        return $this->hasBhPlan;
    }

    /**
     * @return string
     */
    public function getFrg(): ?string
    {
        return $this->frg;
    }

    /**
     * @param string $frg
     *
     * @return Record
     */
    public function setFrg(string $frg): Record
    {
        $this->frg = $frg;

        return $this;
    }

    /**
     * @return string
     */
    public function getRateCode(): ?string
    {
        return $this->rateCode;
    }

    /**
     * @param string $rateCode
     *
     * @return Record
     */
    public function setRateCode(string $rateCode): Record
    {
        $this->rateCode = $rateCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getGenderIdentity(): ?string
    {
        return $this->genderIdentity;
    }

    /**
     * @param string $genderIdentity
     *
     * @return Record
     */
    public function setGenderIdentity(string $genderIdentity): Record
    {
        $this->genderIdentity = $genderIdentity;

        return $this;
    }

    /**
     * @return bool
     */
    public function isValidIncarcerated(): bool
    {
        return $this->validIncarcerated;
    }

    /**
     * @param bool $validIncarcerated
     *
     * @return Record
     */
    public function setValidIncarcerated(bool $validIncarcerated): Record
    {
        $this->validIncarcerated = $validIncarcerated;

        return $this;
    }

    /**
     * @param string $identifierCode
     * @param string $firstName
     * @param string $lastName
     */
    public function addResponsibleMember(string $identifierCode, string $firstName, string $lastName): void
    {
        $this->responsibleMemberIndex++;
        $this->responsibleMembers[$this->responsibleMemberIndex] = new ResponsibleMember(
            $identifierCode,
            $firstName,
            $lastName
        );
    }

    /**
     * @return ResponsibleMember[]
     */
    public function getResponsibleMembers(): array
    {
        return $this->responsibleMembers;
    }

    /**
     * @param string $settingOfCare
     * @return Record
     */
    public function setSettingOfCare(string $settingOfCare): self
    {
        $this->settingOfCare = $settingOfCare;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSettingOfCare():?string
    {
        return $this->settingOfCare;
    }

    /**
     * @param int $settingOfCareId
     * @return Record
     */
    public function setSettingOfCareId(int $settingOfCareId): self
    {
        $this->settingOfCareId = $settingOfCareId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSettingOfCareId(): ?int
    {
        return  $this->settingOfCareId;
    }

    /**
     * @param string $waiver
     * @return Record
     * @throws \RuntimeException
     */
    public function setTnChoiceWaiver(string $waiver): self
    {
        $this->getCurrentPlan()->setWaiver($waiver);

        return $this;
    }

    /**
     * @return InsurancePlan
     */
    public function getCurrentPlan(): InsurancePlan
    {
        $currentPlan = $this->plans[$this->planIndex] ?? null;
        if (null === $currentPlan) {
            throw new PlanNotFoundException("Record do not contain current plan");
        }

        return $currentPlan;
    }

    /**
     * @return mixed
     */
    public function getServiceArea()
    {
        return $this->serviceArea;
    }

    /**
     * @param mixed $serviceArea
     */
    public function setServiceArea($serviceArea): void
    {
        $this->serviceArea = $serviceArea;
    }

    /**
     * @return string
     */
    public function getSpecialNeedsNYChildCode(): string
    {
        return $this->specialNeedsNYChildCode;
    }

    /**
     * @param string $specialNeedsNYChildCode
     * @return Record
     */
    public function setSpecialNeedsNYChildCode(string $specialNeedsNYChildCode): Record
    {
        $this->specialNeedsNYChildCode = $specialNeedsNYChildCode;
        return $this;
    }
}
