<?php
/**
 * ClientsCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use Doctrine\DBAL\Connection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use VirtualHealth\Import\Membership\Edi834\Maps\CareprovidersMap;
use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\CountiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\LanguagesMap;
use application\modules\membership\parsers\member\handlers\PotentialMember;
use VirtualHealth\Import\Membership\Edi834\Event\PotentialMemberFoundEvent;
use VirtualHealth\Import\Membership\Edi834\Maps\TocCodesMap;
use VirtualHealth\Import\Membership\Edi834\Services\ClientDataService;
use VirtualHealth\Import\Membership\Edi834\Record;
use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\Country;
use VirtualHealth\OrmBundle\Entity\User;
use VirtualHealth\OrmBundle\Query\Client\GetLastIdMedrec;
use VirtualHealth\OrmBundle\Repository\ClientRepository;
use VirtualHealth\OrmBundle\Repository\CountryRepository;
use VirtualHealth\OrmBundle\Saver\Client\ClientSaver;

/**
 * Class ClientsCollection
 * @package VirtualHealth\Import\Membership\Edi834
 */
class ClientsCollection
{
    /**
     * @var ClientSaver
     */
    private $clientSaver;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var CareprovidersMap
     */
    private $careprovidersMap;

    /**
     * @var CountiesMap
     */
    private $countiesMap;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /**
     * @var LanguagesMap
     */
    private $languagesMap;

    /**
     * @var TocCodesMap
     */
    private $tocCodesMap;

    /**
     * @var ClientRepository
     */
    private $repository;

    /**
     * @var string
     */
    private $dateFormat = 'm/d/Y';

    /**
     * @var string
     */
    private $lastIdMedrec;

    /**
     * @var Client[]
     */
    private $list = [];

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var int
     */
    private $membershipFileId;

    /**
     * @var ClientDataService
     */
    private $clientData;

    /**
     * @var GetLastIdMedrec
     */
    private $lastIdMedrecQuery;

    /**
     * @var Country
     */
    private $usaCountry;

    /**
     * @var array list of record keys which are potential members
     */
    private $potentialMergeList = [];

    /**
     * ClientsCollection constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param ClientRepository $clientRepository
     * @param ClientSaver $clientSaver
     * @param Connection $connection
     * @param CareprovidersMap $careprovidersMap
     * @param CountiesMap $countiesMap
     * @param CitiesMap $citiesMap
     * @param LanguagesMap $languagesMap
     * @param TocCodesMap $tocCodesMap
     * @param ClientDataService $clientData
     * @param GetLastIdMedrec $lastIdMedrecQuery
     * @param CountryRepository $countryRepository
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        ClientRepository $clientRepository,
        ClientSaver $clientSaver,
        Connection $connection,
        CareprovidersMap $careprovidersMap,
        CountiesMap $countiesMap,
        CitiesMap $citiesMap,
        LanguagesMap $languagesMap,
        TocCodesMap $tocCodesMap,
        ClientDataService $clientData,
        GetLastIdMedrec $lastIdMedrecQuery,
        CountryRepository $countryRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->repository = $clientRepository;
        $this->clientSaver = $clientSaver
            ->setAddSystemCMRelation(true)
            ->setOnProfileChangedEvent(false)
            ->setOnClientSaveEvent(false)
            ->setOnNewClientCreateEvent(false);
        $this->connection = $connection;
        $this->careprovidersMap = $careprovidersMap;
        $this->countiesMap = $countiesMap;
        $this->citiesMap = $citiesMap;
        $this->languagesMap = $languagesMap;
        $this->tocCodesMap = $tocCodesMap;
        $this->clientData = $clientData;
        $this->lastIdMedrecQuery = $lastIdMedrecQuery;
        $this->usaCountry = $countryRepository->find(Country::USA_ID);
    }

    /**
     * @param string $dateFormat
     * @return ClientsCollection
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = $dateFormat;
        return $this;
    }

    /**
     * @param string|null $lastIdMedrec
     * @return ClientsCollection
     */
    public function setLastIdMedrec(?string $lastIdMedrec): ClientsCollection
    {
        $this->lastIdMedrec = $lastIdMedrec;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastIdMedrec(): ?string
    {
        return $this->lastIdMedrec;
    }

    /**
     * @param int $initiatorId
     * @return ClientsCollection
     */
    public function setInitiatorId(int $initiatorId): ClientsCollection
    {
        $this->clientSaver->setInitiatorId($initiatorId);
        return $this;
    }

    /**
     * @param RecordsCollection $recordsCollection
     * @param UsersCollection $usersCollection
     * @param RecordsMap $map
     */
    public function fill(RecordsCollection $recordsCollection, UsersCollection $usersCollection, RecordsMap $map)
    {
        $this->list = [];
        $this->potentialMergeList = [];
        $this->loadModels($map, $recordsCollection);
        $this->updateCollection($recordsCollection, $usersCollection, $map);
    }

    /**
     * Save a collection
     * @throws \CDbException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function save()
    {
        $this->clientSaver->saveAll($this->list);
        $this->clientSaver->saveCareTeamPCP($this->list);
    }

    public function setMembershipFileId($fileId)
    {
        $this->membershipFileId = $fileId;
        return $this;
    }

    /**
     * @return Client[]
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Check client is in the potential merge list
     * @param $recordKey
     * @return bool
     */
    public function isPotentialMemberByRecordKey($recordKey)
    {
        return in_array($recordKey, $this->potentialMergeList);
    }

    /**
     * @param RecordsMap $map
     * @param RecordsCollection $recordsCollection
     */
    private function loadModels(RecordsMap $map, RecordsCollection $recordsCollection)
    {
        $models = $this->repository->findBy(['userId' => $map->getClientIdsMap()]);
        $modelsMap = [];
        foreach ($models as $model) {
            $modelsMap[$model->getUserId()] = $model;
        }
        foreach ($map->getClientIdsMap() as $recordKey => $clientId) {
            if (!isset($modelsMap[$clientId])) {
                continue;
            }
            if ($map->isPotentialMergeType($recordKey)) {
                $this->potentialMergeList[] = $recordKey;
                $this->initPotentialMerge($recordsCollection->getList()[$recordKey], $modelsMap[$clientId]);
            } else {
                $this->list[$recordKey] = $modelsMap[$clientId];
            }
        }
    }

    /**
     * @param Record $record
     * @param Client $client
     */
    private function initPotentialMerge(Record $record, Client $client)
    {
        $data = [];
        $clientAR = \Client::model()->findByPk($client->getUserId());
        $potentialClientData = PotentialMember::getClientData($clientAR);

        $existClientData = $this->clientData->getData($record);

        foreach ($existClientData as $modelName => $modelData) {
            foreach ($modelData as $field => $newValue) {
                $oldValue = isset($potentialClientData[$modelName][$field])
                    ? $potentialClientData[$modelName][$field] : null;
                $data[] = [
                    'modelName' => $modelName,
                    'fieldName' => $field,
                    'temporary' => $oldValue,
                    'merged' => $newValue,
                ];
            }
        }

        $data['original'] = serialize($record);
        $this->eventDispatcher->dispatch(
            PotentialMemberFoundEvent::NAME,
            new PotentialMemberFoundEvent($client, $data, $this->membershipFileId)
        );
    }

    /**
     * @param RecordsCollection $recordsCollection
     * @param UsersCollection $usersCollection
     * @param RecordsMap $map
     */
    private function updateCollection(
        RecordsCollection $recordsCollection,
        UsersCollection $usersCollection,
        RecordsMap $map
    ) {
        foreach ($recordsCollection->getList() as $key => $record) {
            if ($this->isPotentialMemberByRecordKey($key)) {
                continue;
            }

            if (!isset($this->list[$key])) {
                $this->list[$key] = $this->createClient($record, $usersCollection->getList()[$key]);
            }
            $this->updateMainInformation($this->list[$key], $record);
            $criteria = $map->getCriteriaTypeByRecordKey($key);
            if ($criteria == RecordsMap::CRITERIA_TYPE_TEMPORARY_CLIENT) {
                $this->updateTemporaryClient($this->list[$key], $record);
            }

            if ($criteria == RecordsMap::CRITERIA_TYPE_MERGE_CLIENT) {
                // When we merge an imported client with existing temporary client, we should also replace Member Number
                $this->updateTemporaryClient($this->list[$key], $record);
                $this->updateClient($this->list[$key], $record, $this->list[$key]->getUser(), false);
            }
        }
    }

    /**
     * @param Record $record
     * @param User $user
     * @return Client
     * @throws \Doctrine\DBAL\DBALException
     */
    private function createClient(Record $record, User $user)
    {
        return $this->updateClient(new Client(), $record, $user);
    }

    /**
     * List of setter functions that must called on non-null values only
     * @return array
     */
    private function getOverWritableOnNotNullOnlySetters()
    {
        return [
            'setFirstName',
            'setLastName',
            'setPhone',
            'setMobile',
            'setAddress1',
            'setAddress2',
            'setCity',
            'setRegion',
            'setPostalCode',
            'setCounty',
            'setBirthday',
        ];
    }

    /**
     * Get array list of setters and their arguments filtered due to null values or creating/updating of model
     * @param bool $isNewRecord
     * @param Record $record
     * @param User $user
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getUpdateSetters($isNewRecord, Record $record, User $user)
    {
        $setterFunctionToArgumentMap = [
            'setUser' => $user,
            'setUserId' => $user->getId(),
            'setFirstName' => $record->getSubscriberFirstName(),
            'setLastName' => $record->getSubscriberLastName(),
            'setMiddleName' => $record->getSubscriberMiddleName(),
            'setPhone' => !empty($record->getHomePhone()) ? $this->formatPhone($record->getHomePhone()) : null,
            'setMobile' => !empty($record->getMobilePhone()) ? $this->formatPhone($record->getMobilePhone()) : null,
            'setSuffix' => $record->getSubscriberSuffix(),
            'setAddress1' => $record->getHomeAddress1(),
            'setAddress2' => $record->getHomeAddress2(),
            'setPostalCode' => $record->getHomeZipCode(),
            'setBirthday' => $record->getDateOfBirth()->format($this->dateFormat),
            'setGender' => $record->getSex(),
            'setCareTransitionDestination' => $this->tocCodesMap->getNameByCode($record->getToc()),
            'setCountry' => $this->usaCountry,
        ];

        if (null !== ($language = $this->languagesMap->getByCode($record->getLanguage()))) {
            $setterFunctionToArgumentMap['setLanguage'] = $language->getId();
        }

        if ($isNewRecord) {
            $setterFunctionToArgumentMap['setIdMedrec'] = $this->generateIdMedrec();
            $setterFunctionToArgumentMap['setIsNumeric'] = true;
        }

        if (null !== ($city = $this->citiesMap->getCity($record->getHomeCity(), $record->getHomeState()))) {
            $setterFunctionToArgumentMap['setCity'] = $city;
            if ($city->getCountry() !== null) {
                $setterFunctionToArgumentMap['setCountry'] = $city->getCountry();
            }
            $setterFunctionToArgumentMap['setRegion'] = $city->getRegion();
            if (null !== ($county = $this->countiesMap->getCounty($city->getRegionId(), $record->getHomeCounty()))) {
                $setterFunctionToArgumentMap['setCounty'] = $county;
            }
        }

        if (!$isNewRecord) {
            $overWritableSetters = $this->getOverWritableOnNotNullOnlySetters();
            foreach ($setterFunctionToArgumentMap as $setter => $argument) {
                if (in_array($setter, $overWritableSetters) && empty($argument)) {
                    unset($setterFunctionToArgumentMap[$setter]);
                }
            }
        }

        return $setterFunctionToArgumentMap;
    }

    /**
     * @param Client $client
     * @param Record $record
     * @param User $user
     * @param bool $isNewRecord
     * @return Client
     * @throws \Doctrine\DBAL\DBALException
     */
    private function updateClient(Client $client, Record $record, User $user, $isNewRecord = true)
    {
        $setterFunctionToArgumentMap = $this->getUpdateSetters($isNewRecord, $record, $user);
        foreach ($setterFunctionToArgumentMap as $setter => $argument) {
            call_user_func([$client, $setter], $argument);
        }
        return $client;
    }

    /**
     * @param Client $client
     * @param Record $record
     */
    private function updateMainInformation(Client $client, Record $record)
    {
        if ($record->getDeathDate() !== null) {
            $client->setDateDeceased($record->getDeathDate());
        }
        if (!empty($record->getFamilyId())) {
            $client->setFamilyId($record->getFamilyId());
        }
        if ($record->getDateOfBirth() !== null) {
            $client->setBirthday($record->getDateOfBirth()->format($this->dateFormat));
        }
        if (!empty($record->getEthnicity())) {
            $client->setEthnicity($record->getEthnicity());
        }
        if (null !== ($provider = $this->careprovidersMap->getByRecord($record))) {
            $client->setPcp($provider);
        }
        if (!empty($record->getGenderIdentity())) {
            $client->setGenderIdentity($record->getGenderIdentity());
        }
        // if empty make blank (null)
        $client->setSettingOfCareId($record->getSettingOfCareId());
    }

    /**
     * @param Client $client
     * @param Record $record
     */
    private function updateTemporaryClient(Client $client, Record $record)
    {
        $client->setIdMedrec(preg_replace('|^T|', '1', $client->getIdMedrec()));
    }

    /**
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    private function generateIdMedrec()
    {
        if ($this->lastIdMedrec === null) {
            $this->lastIdMedrec = \bcadd($this->lastIdMedrecQuery->read(), \random_int(15, 45));
        }
        $this->lastIdMedrec = \bcadd($this->lastIdMedrec, 1);
        return $this->lastIdMedrec;
    }

    /**
     * @param $phone
     * @return string
     */
    private function formatPhone($phone)
    {
        $phone = preg_replace('#[^\d]+#', '', $phone);
        $length = strlen($phone);
        return $length > 6 && $length < 11
            ? substr($phone, 0, 3) . '-' . substr($phone, 3, 3) . '-' . substr($phone, 6)
            : '000-000-0000';
    }
}
