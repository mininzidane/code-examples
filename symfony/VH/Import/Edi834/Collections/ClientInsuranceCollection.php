<?php
/**
 * ClientInsuranceCollection class file
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use Doctrine\DBAL\DBALException;
use VirtualHealth\Import\Membership\Edi834\InsurancePlan;
use VirtualHealth\Import\Membership\Edi834\Maps\CareprovidersMap;
use VirtualHealth\Import\Membership\Edi834\Maps\PlansMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RegionsMap;
use VirtualHealth\Import\Membership\Edi834\Maps\ServiceAreasMap;
use VirtualHealth\Import\Membership\Edi834\Maps\WaiversMap;
use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;
use VirtualHealth\Import\Membership\Edi834\Services\ClientInsurancePersister;
use VirtualHealth\Import\Membership\Edi834\Validators\BaseValidator;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicaid;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicaidFuture;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicare;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicareFuture;
use VirtualHealth\OrmBundle\Entity\EnrollmentHistory;
use VirtualHealth\OrmBundle\Repository\ClientInsuranceMedicaidFutureRepository;
use VirtualHealth\OrmBundle\Repository\ClientInsuranceMedicaidRepository;
use VirtualHealth\OrmBundle\Repository\ClientInsuranceMedicareFutureRepository;
use VirtualHealth\OrmBundle\Repository\ClientInsuranceMedicareRepository;
use VirtualHealth\OrmBundle\Repository\EnrollmentHistoryRepository;

/**
 * Class ClientInsuranceCollection
 * @package VirtualHealth\Import\Membership\Edi834\Collections;
 */
class ClientInsuranceCollection
{

    /**
     * @var ClientInsuranceMedicare[][]|ClientInsuranceMedicaid[][]
     */
    private $presentList = [];

    /**
     * @var ClientInsuranceMedicareFuture[]|ClientInsuranceMedicaidFuture[]
     */
    private $futureList = [];

    /**
     * @var EnrollmentHistory[]
     */
    private $pastList = [];

    /**
     * @var ClientInsuranceMedicaidRepository
     */
    private $medicaidRepository;

    /**
     * @var ClientInsuranceMedicaidFutureRepository
     */
    private $medicaidFutureRepository;

    /**
     * @var ClientInsuranceMedicareRepository
     */
    private $medicareRepository;

    /**
     * @var ClientInsuranceMedicareFutureRepository
     */
    private $medicareFutureRepository;

    /**
     * @var ClientInsurancePersister
     */
    private $clientInsurancePersister;

    /**
     * @var EnrollmentHistoryRepository
     */
    private $enrollmentHistoryRepository;

    /**
     * @var WaiversMap
     */
    private $waiversMap;

    /**
     * @var CareprovidersMap
     */
    private $careprovidersMap;

    /**
     * @var PlansMap
     */
    private $plansMap;

    /**
     * @var RegionsMap
     */
    private $regionsMap;

    /**
     * @var ServiceAreasMap
     */
    private $serviceAreasMap;

    /**
     * simple structure, that requires for:
     * IF Client does NOT have anything recorded in their Present (Medicaid or Medicare) health plan table THEN
     * IF Client has at least one entry in their Past (Medicaid or Medicare) health plan table THEN
     * Populate copy of the health plan with the latest Termination Date from the Past table into the Present table (Medicaid or Medicare, accordingly)
     * This is a copy, meaning the information in the Past table should also be retained
     * NOTE: The reason we are doing this is to ensure consistency with the disenrollment process which retains a copy of the Client's last health plan in the Present table
     *
     * @var array
     */
    private $pastListForDisenrollment = [];

    /**
     * @var array
     */
    private $presentListForDisenrollment = [];

    /**
     * ClientInsuranceCollection constructor.
     * @param EnrollmentHistoryRepository $enrollmentHistoryRepository
     * @param ClientInsuranceMedicaidRepository $medicaidRepository
     * @param ClientInsuranceMedicaidFutureRepository $medicaidFutureRepository
     * @param ClientInsuranceMedicareRepository $medicareRepository
     * @param ClientInsuranceMedicareFutureRepository $medicareFutureRepository
     * @param ClientInsurancePersister $clientInsurancePersister
     * @param CareprovidersMap $careprovidersMap
     * @param PlansMap $plansMap
     * @param WaiversMap $waiversMap
     * @param RegionsMap $regionsMap
     * @param ServiceAreasMap $serviceAreasMap
     */
    public function __construct(
        EnrollmentHistoryRepository $enrollmentHistoryRepository,
        ClientInsuranceMedicaidRepository $medicaidRepository,
        ClientInsuranceMedicaidFutureRepository $medicaidFutureRepository,
        ClientInsuranceMedicareRepository $medicareRepository,
        ClientInsuranceMedicareFutureRepository $medicareFutureRepository,
        ClientInsurancePersister $clientInsurancePersister,
        CareprovidersMap $careprovidersMap,
        PlansMap $plansMap,
        WaiversMap $waiversMap,
        RegionsMap $regionsMap,
        ServiceAreasMap $serviceAreasMap
    ) {
        $this->enrollmentHistoryRepository = $enrollmentHistoryRepository;
        $this->medicaidRepository = $medicaidRepository;
        $this->medicaidFutureRepository = $medicaidFutureRepository;
        $this->medicareRepository = $medicareRepository;
        $this->medicareFutureRepository = $medicareFutureRepository;
        $this->clientInsurancePersister = $clientInsurancePersister;

        $this->careprovidersMap = $careprovidersMap;
        $this->plansMap = $plansMap;
        $this->waiversMap = $waiversMap;
        $this->regionsMap = $regionsMap;
        $this->serviceAreasMap = $serviceAreasMap;
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $recordsMap
     * @param AdapterParseLog $log
     *
     * @return $this
     *
     * @throws \Exception
     * @throws \CDbException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function handle(
        ClientsCollection $clientsCollection,
        RecordsCollection $recordsCollection,
        RecordsMap $recordsMap,
        AdapterParseLog $log
    ): ClientInsuranceCollection {
        $this->presentList = [];
        $this->futureList = [];
        $this->pastList = [];

        $this->pastListForDisenrollment = [];
        $this->presentListForDisenrollment = [];

        $this
            ->loadModels($recordsMap)
            ->process($clientsCollection, $recordsCollection, $recordsMap, $log)
            ->updatePresentPlansForDisenrollment();
        $this->clientInsurancePersister->save();

        return $this;
    }

    /**
     * @param int $clientIds
     * @return array
     */
    public function getActivePlanIds(int $clientIds): array
    {
        $planIds = [];
        foreach ($this->presentList[$clientIds] ?? [] as $spans) {
            foreach ($spans as $plan) {
                $planIds[] = $plan->getPlanId();
            }
        }
        foreach ($this->futureList[$clientIds] ?? [] as $spans) {
            foreach ($spans as $plan) {
                $planIds[] = $plan->getPlanId();
            }
        }
        return array_unique($planIds);
    }

    /**
     * Insert plans into present table from enrollment history
     *
     * @return void
     *
     * @throws \CDbException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    private function updatePresentPlansForDisenrollment()
    {
        $sortFunction = function (EnrollmentHistory $a, EnrollmentHistory $b) {
            return $a->getTerminationDate() <=> $b->getTerminationDate();
        };

        foreach ($this->pastListForDisenrollment as $clientId => $pastPlans) {
            if (!empty($this->presentListForDisenrollment[$clientId])
                || !$pastPlans
            ) {
                continue;
            }

            /** @var EnrollmentHistory[] $pastPlans */
            usort($pastPlans, $sortFunction);
            $latestPastPlan = array_pop($pastPlans);

            $this->clientInsurancePersister->upsert(
                $this->planToPresent($latestPastPlan, $latestPastPlan->getPlanType())
            );
        }
    }

    /**
     * Load client insurance plans
     *
     * @param RecordsMap $recordsMap
     *
     * @return ClientInsuranceCollection
     */
    private function loadModels(RecordsMap $recordsMap): ClientInsuranceCollection
    {
        $clientIds = $recordsMap->getClientIdsMap();
        //load past
        $pastPlans = $this->enrollmentHistoryRepository->findBy(
            ['client' => $clientIds],
            ['terminationDate' => 'DESC']
        );
        foreach ($pastPlans as $plan) {
            $clientId = $plan->getClient()->getUserId();
            $spanId = $plan->getSpanId();

            $this->pastList[$clientId][$spanId][$plan->getId()] = $plan;
            $this->pastListForDisenrollment[$clientId][$plan->getId()] = $plan;
        }

        //load present
        $presentPlans = [];
        $presentPlans[] = $this->medicareRepository->findBy(['client' => $clientIds]);
        $presentPlans[] = $this->medicaidRepository->findBy(['client' => $clientIds]);
        $presentPlans = array_merge(...$presentPlans);
        $this->presentList = $this->organizePlanList($presentPlans);
        //create structure for disenrollment
        foreach ($presentPlans as $presentPlan) {
            $this->presentListForDisenrollment[$presentPlan->getClient()->getUserId()][] = $presentPlan;
        }

        //load future
        $futurePlans = [];
        $futurePlans[] = $this->medicareFutureRepository->findBy(['client' => $clientIds]);
        $futurePlans[] = $this->medicaidFutureRepository->findBy(['client' => $clientIds]);
        $futurePlans = array_merge(...$futurePlans);
        $this->futureList = $this->organizePlanList($futurePlans);

        return $this;
    }

    /**
     * Organize plans structure
     *
     * @param ClientInsuranceMedicare[]|ClientInsuranceMedicaid[]|ClientInsuranceMedicaidFuture[]|ClientInsuranceMedicareFuture $plans
     *
     * @return array
     */
    private function organizePlanList($plans)
    {
        $result = [];
        foreach ($plans as $plan) {
            $clientId = $plan->getClient()->getUserId();
            $spanId = $plan->getSpanId();

            $planType = $this->isMedicarePlan($plan)
                ? EnrollmentHistory::PLAN_TYPE_MEDICARE
                : EnrollmentHistory::PLAN_TYPE_MEDICAID;

            $result[$clientId][$spanId][$planType] = $plan;
        }

        return $result;
    }

    /**
     * Process client insurance plans
     *
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $recordsMap
     * @param AdapterParseLog $log
     *
     * @return ClientInsuranceCollection
     *
     * @throws \Exception
     */
    private function process(
        ClientsCollection $clientsCollection,
        RecordsCollection $recordsCollection,
        RecordsMap $recordsMap,
        AdapterParseLog $log
    ): ClientInsuranceCollection {
        foreach ($recordsCollection->getList() as $key => $record) {
            /** @var Record $record */
            if ($clientsCollection->isPotentialMemberByRecordKey($key)
                || (!$record->hasMedicaid() && !$record->hasMedicare())
            ) {
                continue;
            }

            $client = $clientsCollection->getList()[$key];
            $clientId = $client->getUserId();

            $planType = EnrollmentHistory::PLAN_TYPE_MEDICAID;
            $anotherPlanType = EnrollmentHistory::PLAN_TYPE_MEDICARE;
            if ($record->hasMedicare()) {
                $planType = EnrollmentHistory::PLAN_TYPE_MEDICARE;
                $anotherPlanType = EnrollmentHistory::PLAN_TYPE_MEDICAID;
            }

            $isClientForMerge = $recordsMap->isClientMerge($key);
            $isBh = ($planType === EnrollmentHistory::PLAN_TYPE_MEDICAID
                && $recordsMap->getCriteriaTypeByRecordKey($key) == RecordsMap::CRITERIA_TYPE_SUBSCRIBER_NUMBER_ADVANCED);
            $isTemporaryClient = $recordsMap->getCriteriaTypeByRecordKey($key) == RecordsMap::CRITERIA_TYPE_TEMPORARY_CLIENT;

            foreach ($record->getPlans() as $plan) {
                $spanId = $plan->getSpanId();

                $pastPlans = $this->pastList[$clientId][$spanId] ?? [];
                $presentPlans = $this->presentList[$clientId][$spanId] ?? [];
                $futurePlans = $this->futureList[$clientId][$spanId] ?? [];

                $updatedModel = null;
                switch ($plan->getPeriod()) {
                    case InsurancePlan::PERIOD_PAST:
                        /** @var EnrollmentHistory $updatedModel */
                        $updatedModel = array_shift($pastPlans);

                        if (!$updatedModel) {
                            $updatedModel = (new EnrollmentHistory())
                                ->setSpanId($spanId);
                        } else {
                            foreach ($pastPlans as $pastPlan) {
                                /** @var EnrollmentHistory $pastPlan */
                                $this->markPlanForRemove($pastPlan);
                            }
                        }

                        $updatedModel->setPlanType($planType);

                        $this->updatePlan(
                            $updatedModel,
                            $record,
                            $plan,
                            $client,
                            $isClientForMerge,
                            $isTemporaryClient,
                            $isBh,
                            $log
                        );

                        if ($presentPlans) {
                            if (count($this->presentList[$clientId]) > 1) {
                                foreach ($presentPlans as $planToDelete) {
                                    $this->markPlanForRemove($planToDelete);
                                }
                            } else {
                                $presentPlan = $this->processPresentPlan(
                                    $presentPlans,
                                    $clientId,
                                    $spanId,
                                    $planType,
                                    $anotherPlanType
                                );
                                $this->updatePlan(
                                    $presentPlan,
                                    $record,
                                    $plan,
                                    $client,
                                    $isClientForMerge,
                                    $isTemporaryClient,
                                    $isBh,
                                    $log
                                );
                            }

                            //delete another plan
                            if (isset($presentPlans[$anotherPlanType])) {
                                $this->markPlanForRemove($presentPlans[$anotherPlanType]);
                            }
                        }

                        //delete future
                        $this->removeFuture($clientId, $spanId);
                        break;
                    case InsurancePlan::PERIOD_PRESENT:
                        //scenario 1
                        if ($presentPlans) {
                            $updatedModel = $this->processPresentPlan(
                                $presentPlans,
                                $clientId,
                                $spanId,
                                $planType,
                                $anotherPlanType
                            );

                            //delete another plan
                            if (isset($presentPlans[$anotherPlanType])) {
                                $this->markPlanForRemove($presentPlans[$anotherPlanType]);
                            }
                        } elseif (isset($this->presentList[$clientId])) {
                            //remove all present plans with same type and another span_id
                            foreach ($this->presentList[$clientId] as $presentPlans) {
                                if (isset($presentPlans[$planType])) {
                                    $this->markPlanForRemove($presentPlans[$planType]);
                                }
                            }
                        }

                        //scenario 2
                        if (!$updatedModel && $pastPlans) {
                            $updatedModel = $this->planToPresent(
                                reset($pastPlans),
                                $planType
                            );
                        }

                        //scenario 3
                        if (!$updatedModel && $futurePlans) {
                            $futurePlan = $futurePlans[$planType] ?? null;
                            if (!$futurePlan) {
                                $futurePlan = $futurePlans[$anotherPlanType];
                            }

                            $updatedModel = $this->planToPresent($futurePlan, $planType);
                        }

                        //create new plan
                        if (!$updatedModel) {
                            if ($planType === EnrollmentHistory::PLAN_TYPE_MEDICAID) {
                                $updatedModel = new ClientInsuranceMedicaid();
                            } else {
                                $updatedModel = new ClientInsuranceMedicare();
                            }
                            $updatedModel->setSpanId($spanId);
                        }

                        $this->updatePlan(
                            $updatedModel,
                            $record,
                            $plan,
                            $client,
                            $isClientForMerge,
                            $isTemporaryClient,
                            $isBh,
                            $log
                        );

                        $this->removePast($clientId, $spanId);
                        $this->removeFuture($clientId, $spanId);
                        break;
                    case InsurancePlan::PERIOD_FUTURE:
                        $updatedModel = null;
                        //scenario 1
                        if ($futurePlans) {
                            $updatedModel = $futurePlans[$planType] ?? null;
                            if (!$updatedModel) {
                                $futurePlan = $futurePlans[$anotherPlanType];
                                $updatedModel = $this->planToFuture($futurePlan, $planType);
                                // removing other plans with same type to prevent client_id duplication
                                foreach ($this->futureList[$clientId] as $futureSpanId => $anotherPlans) {
                                    if ($futureSpanId === $spanId) {
                                        continue;
                                    }
                                    if (isset($anotherPlans[$planType])) {
                                        $this->markPlanForRemove($anotherPlans[$planType]);
                                    }
                                }
                            }

                            //delete another plan
                            if (isset($futurePlans[$anotherPlanType])) {
                                $this->markPlanForRemove($futurePlans[$anotherPlanType]);
                            }
                        } elseif (isset($this->futureList[$clientId])) {
                            //remove all future plans with same type and another span_id
                            foreach ($this->futureList[$clientId] as $futurePlans) {
                                if (isset($futurePlans[$planType])) {
                                    $this->markPlanForRemove($futurePlans[$planType]);
                                }
                            }
                        }

                        //scenario 2
                        if (!$updatedModel && $presentPlans) {
                            $presentPlan = $presentPlans[$planType] ?? $presentPlans[$anotherPlanType];
                            $updatedModel = $this->planToFuture($presentPlan, $planType);
                        }

                        //scenario 3
                        if (!$updatedModel && $pastPlans) {
                            $updatedModel = $this->planToFuture(
                                reset($pastPlans),
                                $planType
                            );
                        }

                        //create new plan
                        if (!$updatedModel) {
                            if ($planType === EnrollmentHistory::PLAN_TYPE_MEDICAID) {
                                $updatedModel = new ClientInsuranceMedicaidFuture();
                            } else {
                                $updatedModel = new ClientInsuranceMedicareFuture();
                            }
                            $updatedModel->setSpanId($spanId);
                        }

                        $this->updatePlan(
                            $updatedModel,
                            $record,
                            $plan,
                            $client,
                            $isClientForMerge,
                            $isTemporaryClient,
                            $isBh,
                            $log
                        );

                        $this->removePresent($clientId, $spanId);
                        $this->removePast($clientId, $spanId);
                        break;
                    default:
                        break;
                }
            }
        }

        return $this;
    }

    /**
     * Updates plan with record's data
     *
     * @param ClientInsuranceMedicaid|ClientInsuranceMedicaidFuture|ClientInsuranceMedicare|ClientInsuranceMedicareFuture|EnrollmentHistory $updatedModel
     * @param Record $record
     * @param InsurancePlan $insurancePlan
     * @param Client $client
     * @param bool $isClientForMerge
     * @param bool $isTemporaryClient
     * @param bool $isBh
     * @param AdapterParseLog $log
     *
     * @throws \Exception
     */
    private function updatePlan(
        $updatedModel,
        Record $record,
        InsurancePlan $insurancePlan,
        Client $client,
        bool $isClientForMerge,
        bool $isTemporaryClient,
        bool $isBh,
        AdapterParseLog $log
    ) {
        if ($updatedModel->getId() === null || $isClientForMerge) {
            $updatedModel
                ->setSubscriberId($record->getSubscriberId())
                ->setAlternateMemberNumber($record->getAlternateMemberNumber())
                ->setClient($client);

            $sequentialMemberNumber = $record->getSequentialMemberNumber();
            if ($this->isMedicarePlan($updatedModel)) {
                $updatedModel->setMedicareSequentialMemberNumber($sequentialMemberNumber);
            } elseif ($this->isMedicaidPlan($updatedModel)) {
                $updatedModel->setMedicaidSequentialMemberNumber($sequentialMemberNumber);
            } else {
                $updatedModel->setSequentialMemberNumber($sequentialMemberNumber);
            }
        }

        $this->updateMainInformation($updatedModel, $record, $insurancePlan, $log);
        $this->updateEnrollmentDate($updatedModel, $insurancePlan, $isBh);

        if ($isTemporaryClient) {
            $updatedModel->setSubscriberId($record->getSubscriberId());
        }

        $this->markPlanForUpsert($updatedModel);
    }

    /**
     * Update enrollment date
     *
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $updatedModel
     * @param InsurancePlan $insurancePlan
     * @param bool $isBh
     *
     * @return void
     *
     * @throws \Exception
     */
    private function updateEnrollmentDate($updatedModel, InsurancePlan $insurancePlan, bool $isBh)
    {
        if ($this->isMedicaidPlan($insurancePlan) && $isBh) {
            if ($insurancePlan->getEnrollmentDate() !== null) {
                $updatedModel->setBhEnrollmentDate($insurancePlan->getEnrollmentDate());
            }

            if (!$insurancePlan->isPast() || $insurancePlan->getTerminationDate() !== null) {
                $updatedModel->setBhTerminationDate($insurancePlan->getTerminationDate());
            }
        } else {
            if ($insurancePlan->getEnrollmentDate() !== null) {
                $updatedModel->setEnrollmentDate($insurancePlan->getEnrollmentDate());
            }
            if (
                !$insurancePlan->isPast()
                || $insurancePlan->getTerminationDate() !== null
            ) {
                $updatedModel->setTerminationDate($insurancePlan->getTerminationDate());
            }
        }
    }

    /**
     * Update main client insurance plan data
     *
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $updatedModel
     * @param Record $record
     * @param InsurancePlan $insurancePlan
     * @param AdapterParseLog $log
     *
     * @throws DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return void
     */
    private function updateMainInformation(
        $updatedModel,
        Record $record,
        InsurancePlan $insurancePlan,
        AdapterParseLog $log
    ) {
        $subscriberNumber = $record->getMedicaidSubscriberNumber();
        if ($this->isMedicarePlan($updatedModel)
            || ($updatedModel instanceof EnrollmentHistory && $updatedModel->getPlanType() === EnrollmentHistory::PLAN_TYPE_MEDICARE)
        ) {
            $subscriberNumber = $record->getMedicareSubscriberNumber();
        }

        if ($updatedModel instanceof EnrollmentHistory) {
            $updatedModel->setIdNumber($subscriberNumber);
            if ($updatedModel->isMedicaid()
                && null !== ($waiver = $this->waiversMap->getIdByCodeOrName($insurancePlan->getWaiver()))
            ) {
                $updatedModel->setWaiver($waiver);
            }
        } elseif ($this->isMedicaidPlan($updatedModel)) {
            $updatedModel
                ->setNumber($subscriberNumber)
                ->setWaiver($this->waiversMap->getIdByCodeOrName($insurancePlan->getWaiver()))
                ->setRateCode($insurancePlan->getRateCode());
        } else {
            $updatedModel
                ->setNumber($subscriberNumber)
                ->setRateCode($insurancePlan->getRateCode());
        }

        if (!$updatedModel instanceof EnrollmentHistory && $updatedModel->getId() !== null) {
            $updatedModel->setUpdatedDate(new \DateTime());
        }

        if (!empty($record->getFrg())) {
            $updatedModel->setFrg($record->getFrg());
        }

        //fill careprovider's data
        if (null !== ($careprovider = $this->careprovidersMap->getByRecord($record))) {
            $lpoList = [];
            $ipaList = [];
            foreach ($careprovider->getNetworks() as $network) {
                if ($network->getIsPcp()) {
                    $lpoList[] = $network->getLpo();
                    foreach ($network->getIpas() as $ipa) {
                        $ipaList[] = $ipa->getIpaName();
                    }
                }
            }
            $updatedModel->setLpo(implode(', ', $lpoList));
            if (!empty($ipaList)) {
                $updatedModel->setIpa(implode(', ', $ipaList));
            }
        }

        if (null !== ($plan = $this->plansMap->getByPlanId($insurancePlan->getId()))) {
            $updatedModel
                ->setPlanId($plan->getPlanId())
                ->setPlanName($plan->getName())
                ->setServiceArea(
                    $this->serviceAreasMap->getServiceArea(
                        $this->regionsMap->getStateCodeByTitle($record->getHomeState()),
                        $record->getHomeCounty()
                    )
                );
            if ($plan->getProductType() !== null) {
                $updatedModel->setProductTypeId($plan->getProductType()->getId());
            }
            if ($plan->getLob() !== null) {
                $updatedModel->setLob($plan->getLob()->getName());
                $markets = [];
                foreach ($plan->getLob()->getRegions() as $region) {
                    $markets[] = $region->getCode();
                }
                $updatedModel->setMarket(\implode(', ', $markets));
            }
        } else {
            $baseValidator = new BaseValidator();
            $log->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Invalid Plan ID Value for %s: %s',
                    $baseValidator->buildSubscriberName($record, ' for "', '"'),
                    $insurancePlan->getId()
                )
            );
        }
    }

    /**
     * Transform plan to present period
     *
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $oldPlan
     * @param string $planType
     *
     * @return ClientInsuranceMedicaid|ClientInsuranceMedicare
     */
    private function planToPresent($oldPlan, $planType)
    {
        if ($planType === EnrollmentHistory::PLAN_TYPE_MEDICARE) {
            $newPlan = new ClientInsuranceMedicare();
        } else {
            $newPlan = new ClientInsuranceMedicaid();
        }

        $this->movePlanData($oldPlan, $newPlan);

        return $newPlan;
    }

    /**
     * Transform plan to future period
     *
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $oldPlan
     * @param string $planType
     *
     * @return ClientInsuranceMedicaidFuture|ClientInsuranceMedicareFuture
     */
    private function planToFuture($oldPlan, $planType)
    {
        if ($planType === EnrollmentHistory::PLAN_TYPE_MEDICARE) {
            $newPlan = new ClientInsuranceMedicareFuture();
        } else {
            $newPlan = new ClientInsuranceMedicaidFuture();
        }

        $this->movePlanData($oldPlan, $newPlan);

        return $newPlan;
    }

    /**
     * Move plan data
     *
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $oldPlan
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $newPlan
     *
     * @return void
     */
    private function movePlanData($oldPlan, $newPlan)
    {
        if ($oldPlan instanceof EnrollmentHistory) {
            $number = $oldPlan->getIdNumber();
            $sequentialMemberNumber = $oldPlan->getSequentialMemberNumber();
        } elseif ($this->isMedicaidPlan($oldPlan)) {
            $number = $oldPlan->getNumber();
            $sequentialMemberNumber = $oldPlan->getMedicaidSequentialMemberNumber();
        } else {
            $number = $oldPlan->getNumber();
            $sequentialMemberNumber = $oldPlan->getMedicareSequentialMemberNumber();
        }

        //fill common fields
        $newPlan
            ->setClient($oldPlan->getClient())
            ->setSpanId($oldPlan->getSpanId())
            ->setPlanId($oldPlan->getPlanId())
            ->setPlanName($oldPlan->getPlanName())
            ->setEnrollmentDate($oldPlan->getEnrollmentDate())
            ->setTerminationDate($oldPlan->getTerminationDate())
            ->setFrg($oldPlan->getFrg())
            ->setSubscriberId($oldPlan->getSubscriberId())
            ->setServiceArea($oldPlan->getServiceArea())
            ->setLob($oldPlan->getLob())
            ->setIpa($oldPlan->getIpa())
            ->setLpo($oldPlan->getLpo())
            ->setProductTypeId($oldPlan->getProductTypeId())
            ->setAlternateMemberNumber($oldPlan->getAlternateMemberNumber())
            ->setMarket($oldPlan->getMarket())
            ->setRateCode($oldPlan->getRateCode())
            ->setCreatedDate($oldPlan->getCreatedDate())
            ->setNumber($number);

        //fill sequential member number
        if ($this->isMedicaidPlan($newPlan)) {
            $newPlan->setMedicaidSequentialMemberNumber($sequentialMemberNumber);
        } elseif ($this->isMedicarePlan($newPlan)) {
            $newPlan->setMedicareSequentialMemberNumber($sequentialMemberNumber);
        }

        //fill medicaid specific fields if move from enrollment history (medicaid) or medicaid plan (present|future)
        if (($this->isMedicaidPlan($newPlan))
            && (
                $this->isMedicaidPlan($oldPlan)
                || ($oldPlan instanceof EnrollmentHistory && $oldPlan->getPlanType() === EnrollmentHistory::PLAN_TYPE_MEDICAID)
            )
        ) {
            $newPlan
                ->setBhSubscriberId($oldPlan->getBhSubscriberId())
                ->setBhEnrollmentDate($oldPlan->getBhEnrollmentDate())
                ->setBhTerminationDate($oldPlan->getBhTerminationDate())
                ->setWaiver($oldPlan->getWaiver());
        }
    }

    /**
     * Remove past client's plans by span_id
     *
     * @param $clientId
     * @param $spanId
     *
     * @return void
     */
    private function removePast($clientId, $spanId)
    {
        $plans = $this->pastList[$clientId][$spanId] ?? [];
        foreach ($plans as $plan) {
            $this->markPlanForRemove($plan);
        }
    }

    /**
     * Remove present client's plans by span_id
     *
     * @param $clientId
     * @param $spanId
     *
     * @return void
     */
    private function removePresent($clientId, $spanId)
    {
        $plans = $this->presentList[$clientId][$spanId] ?? [];
        foreach ($plans as $plan) {
            $this->markPlanForRemove($plan);
        }
    }

    /**
     * Remove future client's plans by span_id
     *
     * @param $clientId
     * @param $spanId
     *
     * @return void
     */
    private function removeFuture($clientId, $spanId)
    {
        $plans = $this->futureList[$clientId][$spanId] ?? [];
        foreach ($plans as $plan) {
            $this->markPlanForRemove($plan);
        }
    }

    /**
     * mark plan as removed
     *
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $entity
     *
     * @return void
     */
    private function markPlanForRemove($entity)
    {
        $clientId = $entity->getClient()->getUserId();
        if ($entity instanceof EnrollmentHistory) {
            unset($this->pastListForDisenrollment[$clientId][$entity->getId()]);
        } elseif ($entity instanceof ClientInsuranceMedicaid || $entity instanceof ClientInsuranceMedicare) {
            if (isset($this->presentListForDisenrollment[$clientId])) {
                $index = array_search(
                    $entity,
                    $this->presentListForDisenrollment[$clientId],
                    true
                );
                if ($index !== false && $index !== null) {
                    unset($this->presentListForDisenrollment[$clientId][$index]);
                }
            }
            unset($this->presentList[$clientId][$entity->getSpanId()]);
        } elseif ($entity instanceof ClientInsuranceMedicaidFuture) {
            unset($this->futureList[$clientId][$entity->getSpanId()][EnrollmentHistory::PLAN_TYPE_MEDICAID]);
        } elseif ($entity instanceof ClientInsuranceMedicareFuture) {
            unset($this->futureList[$clientId][$entity->getSpanId()][EnrollmentHistory::PLAN_TYPE_MEDICARE]);
        }
        $this->clientInsurancePersister->markForRemove($entity);
    }

    /**
     * Mark plan as upserted
     *
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $entity
     *
     * @return void
     */
    private function markPlanForUpsert($entity)
    {
        $clientId = $entity->getClient()->getUserId();
        if ($entity instanceof EnrollmentHistory) {
            $this->pastListForDisenrollment[$clientId][] = $entity;
        } elseif ($entity instanceof ClientInsuranceMedicaid) {
            $this->presentListForDisenrollment[$clientId][] = $entity;
            $this->presentList[$clientId][$entity->getSpanId()][EnrollmentHistory::PLAN_TYPE_MEDICAID] = $entity;
        } elseif ($entity instanceof ClientInsuranceMedicare) {
            $this->presentListForDisenrollment[$clientId][] = $entity;
            $this->presentList[$clientId][$entity->getSpanId()][EnrollmentHistory::PLAN_TYPE_MEDICARE] = $entity;
        } elseif ($entity instanceof ClientInsuranceMedicaidFuture) {
            $this->futureList[$clientId][$entity->getSpanId()][EnrollmentHistory::PLAN_TYPE_MEDICAID] = $entity;
        } elseif ($entity instanceof ClientInsuranceMedicareFuture) {
            $this->futureList[$clientId][$entity->getSpanId()][EnrollmentHistory::PLAN_TYPE_MEDICARE] = $entity;
        }
        $this->clientInsurancePersister->upsert($entity);
    }

    /**
     * Checks if plan is medicaid
     *
     * @param $plan
     *
     * @return bool
     */
    private function isMedicaidPlan($plan)
    {
        return $plan instanceof ClientInsuranceMedicaid || $plan instanceof ClientInsuranceMedicaidFuture;
    }

    /**
     * Checks if plan is medicare
     *
     * @param $plan
     *
     * @return bool
     */
    private function isMedicarePlan($plan)
    {
        return $plan instanceof ClientInsuranceMedicare || $plan instanceof ClientInsuranceMedicareFuture;
    }

    /**
     * @param array $presentPlans
     * @param int $clientId
     * @param string $spanId
     * @param string $planType
     * @param string $anotherPlanType
     * @return ClientInsuranceMedicaid|ClientInsuranceMedicare|null
     */
    private function processPresentPlan(
        array $presentPlans,
        $clientId,
        $spanId,
        string $planType,
        string $anotherPlanType
    ) {
        $updatedModel = $presentPlans[$planType] ?? null;
        if (!$updatedModel) {
            $presentPlan = $presentPlans[$anotherPlanType];
            $updatedModel = $this->planToPresent($presentPlan, $planType);
            // removing other plans with same type to prevent client_id duplication
            foreach ($this->presentList[$clientId] as $presentSpanId => $anotherPlans) {
                if ($presentSpanId === $spanId) {
                    continue;
                }
                if (isset($anotherPlans[$planType])) {
                    $this->markPlanForRemove($anotherPlans[$planType]);
                }
            }
        }
        return $updatedModel;
    }
}
