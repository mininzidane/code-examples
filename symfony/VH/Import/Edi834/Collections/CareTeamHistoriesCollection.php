<?php
/**
 * CareTeamHistoriesCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\OrmBundle\Entity\CareTeamHistory;
use VirtualHealth\OrmBundle\Entity\ClientGuardian;
use VirtualHealth\OrmBundle\Entity\ClientPoa;
use VirtualHealth\OrmBundle\Entity\User;
use VirtualHealth\OrmBundle\Repository\CareTeamHistoryRepository;
use VirtualHealth\OrmBundle\Saver\CareTeam\CareTeamHistorySaver;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class CareTeamHistoriesCollection
 * @package VirtualHealth\Import\Membership\Edi834\Collections
 */
class CareTeamHistoriesCollection
{
    /**
     * @var CareTeamHistoryRepository
     */
    private $repository;

    /**
     * @var CareTeamHistorySaver
     */
    private $saver;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /**
     * @var int
     */
    private $superAdminId;

    /**
     * @var string
     */
    private $superAdminName;

    /**
     * @var CareTeamHistory[]
     */
    private $list = [];

    /**
     * CareTeamHistoriesCollection constructor.
     * @param CareTeamHistoryRepository $repository
     * @param CareTeamHistorySaver $saver
     * @param CommonUsersGetter $usersGetter
     * @param CitiesMap $citiesMap
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __construct(
        CareTeamHistoryRepository $repository,
        CareTeamHistorySaver $saver,
        CommonUsersGetter $usersGetter,
        CitiesMap $citiesMap
    ) {
        $this->repository = $repository;
        $this->saver = $saver;
        $this->citiesMap = $citiesMap;
        $superAdmin = $usersGetter->getSystemSuperAdmin();
        if (!$superAdmin instanceof User) {
            throw new \RuntimeException('Could not get Super Administrator.');
        }
        $this->superAdminId = $superAdmin->getId();
        $this->superAdminName = $superAdmin->getFullName();
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param ClientGuardiansCollection $guardiansCollection
     * @param ClientPoasCollection $poasCollection
     */
    public function fill(
        ClientsCollection $clientsCollection,
        ClientGuardiansCollection $guardiansCollection,
        ClientPoasCollection $poasCollection
    ) {
        $this->list = [];
        $this->loadAndUpdateGuardians($guardiansCollection, $clientsCollection);
        $this->loadAndUpdatePoas($poasCollection, $clientsCollection);
    }

    /**
     * @throws \CDbException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function save()
    {
        $this->saver->saveAll($this->list);
    }

    /**
     * @param ClientGuardiansCollection $guardiansCollection
     * @param ClientsCollection $clientsCollection
     */
    private function loadAndUpdateGuardians(
        ClientGuardiansCollection $guardiansCollection,
        ClientsCollection $clientsCollection
    ) {
        $memberIds = array_map(
            function (ClientGuardian $model) {
                return $model->getId();
            },
            $guardiansCollection->getList()
        );
        $models = $this->repository->findBy(
            [
                'memberId' => $memberIds,
                'memberType' => 'guardian',
            ]
        );
        $map = [];
        foreach ($models as $model) {
            $map[$model->getMemberId()] = $model;
        }
        foreach ($guardiansCollection->getList() as $key => $guardian) {
            if ($clientsCollection->isPotentialMemberByRecordKey($key)) {
                continue;
            }
            $memberPhone = !empty($guardian->getPrimaryPhone())
                ? $guardian->getPrimaryPhone()
                : $guardian->getAlternatePhone();

            if (!isset($map[$guardian->getId()])) {
                $map[$guardian->getId()] = (new CareTeamHistory())
                    ->setClient($guardian->getClient())
                    ->setModelName('ClientGuardian')
                    ->setMemberId($guardian->getId())
                    ->setMemberType('guardian')
                    ->setMemberRole('Guardian')
                    ->setAddedById($this->superAdminId)
                    ->setAddedByName($this->superAdminName)
                    ->setMemberPhone($memberPhone)
                    ->setMemberAddedDate(new \DateTime())
                ;
            }
            $map[$guardian->getId()]
                ->setMemberName($this->buildName($guardian))
                ->setMemberAddress($this->buildAddress($guardian))
                ->setMemberPhone($memberPhone)
            ;
            $this->list[] = $map[$guardian->getId()];
        }
    }

    /**
     * @param ClientPoasCollection $poasCollection
     * @param ClientsCollection $clientsCollection
     * @throws \Exception
     */
    private function loadAndUpdatePoas(ClientPoasCollection $poasCollection, ClientsCollection $clientsCollection)
    {
        $memberIds = array_map(
            function (ClientPoa $model) {
                return $model->getId();
            },
            $poasCollection->getList()
        );
        $models = $this->repository->findBy(
            [
                'memberId' => $memberIds,
                'memberType' => 'poa',
            ]
        );
        $map = [];
        foreach ($models as $model) {
            $map[$model->getMemberId()] = $model;
        }
        foreach ($poasCollection->getList() as $key => $poa) {
            if ($clientsCollection->isPotentialMemberByRecordKey($key)) {
                continue;
            }
            $memberPhone = !empty($poa->getPrimaryPhone())
                ? $poa->getPrimaryPhone()
                : $poa->getAlternatePhone();

            if (!isset($map[$poa->getId()])) {
                $map[$poa->getId()] = (new CareTeamHistory())
                    ->setClient($poa->getClient())
                    ->setModelName('ClientPOA')
                    ->setMemberId($poa->getId())
                    ->setMemberType('poa')
                    ->setMemberRole('Power Of Attorney')
                    ->setAddedById($this->superAdminId)
                    ->setAddedByName($this->superAdminName)
                    ->setMemberPhone($memberPhone)
                    ->setMemberAddedDate(new \DateTime())
                ;
            }
            $map[$poa->getId()]
                ->setMemberName($this->buildName($poa))
                ->setMemberAddress($this->buildAddress($poa))
                ->setMemberPhone($memberPhone)
            ;
            $this->list[] = $map[$poa->getId()];
        }
    }

    /**
     * @param ClientGuardian|ClientPoa $model
     * @return string
     */
    private function buildName($model)
    {
        $result = [];
        if (!empty($model->getLastName())) {
            $result[] = $model->getLastName();
        }
        if (!empty($model->getFirstName())) {
            $result[] = $model->getFirstName();
        }
        return \implode(', ', $result);
    }

    /**
     * @param ClientGuardian|ClientPoa $model
     * @return string
     */
    private function buildAddress($model)
    {
        $result = [];
        if (!empty($model->getAddress1())) {
            $result[] = $model->getAddress1();
        }
        if (!empty($model->getAddress2())) {
            $result[] = $model->getAddress2();
        }
        if ($model instanceof ClientGuardian) {
            if (null !== ($city = $this->citiesMap->getCityById($model->getCityId()))) {
                $result[] = $city->getTitle();
            }
        } else {
            if ($model->getCity() !== null) {
                $result[] = $model->getCity()->getTitle();
            }
        }
        if (!empty($model->getPostalCode())) {
            $result[] = $model->getPostalCode();
        }
        return \implode(', ', $result);
    }
}
