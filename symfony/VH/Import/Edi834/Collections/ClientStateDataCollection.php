<?php
/**
 * ClientStateDataCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\CountiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\OrmBundle\Entity\ClientStateData;
use VirtualHealth\OrmBundle\Repository\ClientStateDataRepository;
use VirtualHealth\OrmBundle\Saver\Client\ClientStateDataSaver;

/**
 * Class ClientStateDataCollection
 * @package VirtualHealth\Import\Membership\Edi834\Collections
 */
class ClientStateDataCollection
{
    /**
     * @var ClientStateDataRepository
     */
    private $repository;

    /**
     * @var ClientStateDataSaver
     */
    private $saver;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /**
     * @var CountiesMap
     */
    private $countiesMap;

    /**
     * @var ClientStateData[]
     */
    private $list = [];

    /**
     * ClientStateDataCollection constructor.
     * @param ClientStateDataRepository $repository
     * @param ClientStateDataSaver $saver
     * @param CitiesMap $citiesMap
     * @param CountiesMap $countiesMap
     */
    public function __construct(
        ClientStateDataRepository $repository,
        ClientStateDataSaver $saver,
        CitiesMap $citiesMap,
        CountiesMap $countiesMap
    ) {
        $this->repository = $repository;
        $this->saver = $saver;
        $this->citiesMap = $citiesMap;
        $this->countiesMap = $countiesMap;
    }

    public function fill(RecordsCollection $recordsCollection, ClientsCollection $clientsCollection, RecordsMap $map)
    {
        $this->list = [];
        $this->loadModels($map);
        $this->updateCollection($recordsCollection, $clientsCollection);
    }

    /**
     * @throws \CDbException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function save()
    {
        $this->saver->saveAll($this->list);
    }

    /**
     * @param RecordsMap $map
     */
    private function loadModels(RecordsMap $map)
    {
        $models = $this->repository->findBy(['client' => $map->getClientIdsMap()]);
        foreach ($models as $model) {
            $this->list[$model->getClient()->getUserId()] = $model;
        }
    }

    /**
     * @param RecordsCollection $recordsCollection
     * @param ClientsCollection $clientsCollection
     */
    private function updateCollection(RecordsCollection $recordsCollection, ClientsCollection $clientsCollection)
    {
        foreach ($recordsCollection->getList() as $key => $record) {
            if ($clientsCollection->isPotentialMemberByRecordKey($key)) {
                continue;
            }
            $client = $clientsCollection->getList()[$key];
            $clientId = $client->getUserId();
            if (!isset($this->list[$clientId])) {
                $this->list[$clientId] = (new ClientStateData())
                    ->setClient($client);
            }
            $model = $this->list[$clientId];
            $model
                ->setFirstName($record->getSubscriberFirstName())
                ->setLastName($record->getSubscriberLastName())
                ->setAddress1($record->getHomeAddress1())
                ->setAddress2($record->getHomeAddress2())
                ->setPostalCode($record->getHomeZipCode())
                ->setMailingAddress1($record->getMailingAddress1())
                ->setMailingAddress2($record->getMailingAddress2())
                ->setMailingZipCode($record->getMailingPostalCode());
            if (!empty($record->getMobilePhone())) {
                $model->setPrimaryPhone($this->formatPhone($record->getMobilePhone()));
            } elseif (!empty($record->getHomePhone())) {
                $model->setPrimaryPhone($this->formatPhone($record->getHomePhone()));
            }
            if (null !== ($city = $this->citiesMap->getCity($record->getHomeCity(), $record->getHomeState()))) {
                $model
                    ->setCity($city)
                    ->setRegion($city->getRegion());
                if (null !== ($county = $this->countiesMap->getCounty($city->getRegionId(), $record->getHomeCounty()))) {
                    $model->setCounty($county->getTitle());
                }
            }
            if (null !== ($city = $this->citiesMap->getCity($record->getMailingCity(), $record->getMailingState()))) {
                $model
                    ->setMailingCity($city);
            }
            // if empty make blank (null)
            $model->setSettingOfCareId($record->getSettingOfCareId());
        }
    }

    /**
     * @param $phone
     * @return string
     */
    private function formatPhone($phone)
    {
        $phone = preg_replace('#[^\d]+#', '', $phone);
        $length = strlen($phone);
        return $length > 6 && $length < 11
            ? substr($phone, 0, 3) . '-' . substr($phone, 3, 3) . '-' . substr($phone, 6)
            : '000-000-0000';
    }
}
