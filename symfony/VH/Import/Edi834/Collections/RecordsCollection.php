<?php
/**
 * RecordsCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;
use VirtualHealth\Import\Membership\Edi834\Validators\Validator;

/**
 * Class RecordsCollection
 * @package VirtualHealth\Import\Membership
 */
class RecordsCollection
{
    /**
     * @var Record[]
     */
    private $list = [];

    /**
     * @var int
     */
    private $size = 0;

    /**
     * @var Validator[]
     */
    private $validators = [];

    /**
     * RecordsCollection constructor.
     * @param Validator[] $validators
     */
    public function __construct(array $validators)
    {
        $this->validators = $validators;
    }

    /**
     * Add a record to collection
     * @param Record $record
     */
    public function add(Record $record)
    {
        $this->list[] = $record;
        $this->size++;
    }

    /**
     * Delete the record from collection
     * @param string $key
     */
    public function delete($key)
    {
        unset($this->list[$key]);
        $this->size--;
    }

    /**
     * Get list of records
     * @return Record[]
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Get collection size
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Clean a collection
     */
    public function clear()
    {
        $this->list = [];
        $this->size = 0;
    }

    /**
     * @param AdapterParseLog $parseLog
     */
    public function validate(AdapterParseLog $parseLog)
    {
        foreach ($this->list as $key => $record) {
            foreach ($this->validators as $validator) {
                if (!$validator->validate($record, $parseLog)) {
                    $this->delete($key);
                    break;
                }
            }
        }
    }
}
