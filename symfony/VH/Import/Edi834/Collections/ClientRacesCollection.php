<?php
/**
 * ClientRacesCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\OrmBundle\Entity\ClientRace;
use VirtualHealth\OrmBundle\Repository\ClientRaceRepository;
use VirtualHealth\OrmBundle\Saver\Client\ClientRaceSaver;

/**
 * Class ClientRacesCollection
 * @package VirtualHealth\Import\Membership\Edi834\Collections
 */
class ClientRacesCollection
{
    /**
     * @var ClientRaceRepository
     */
    private $repository;

    /**
     * @var ClientRaceSaver
     */
    private $saver;

    /**
     * @var ClientRace[]
     */
    private $list = [];

    /**
     * MedicaresCollection constructor.
     * @param ClientRaceRepository $repository
     * @param ClientRaceSaver $saver
     */
    public function __construct(ClientRaceRepository $repository, ClientRaceSaver $saver)
    {
        $this->repository = $repository;
        $this->saver = $saver;
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $map
     */
    public function fill(ClientsCollection $clientsCollection, RecordsCollection $recordsCollection, RecordsMap $map)
    {
        $this->list = [];
        $this->loadModels($map);
        $this->updateCollection($clientsCollection, $recordsCollection, $map);
    }

    /**
     * Save all models
     * @throws \CDbException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function save()
    {
        $this->saver->saveAll($this->list);
    }

    /**
     * @return ClientRace[]
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param RecordsMap $map
     */
    private function loadModels(RecordsMap $map)
    {
        $models = $this->repository->findBy(['id' => $map->getClientRaceIdsMap()]);
        foreach ($models as $model) {
            $key = array_search($model->getId(), $map->getClientRaceIdsMap());
            $this->list[$key] = $model;
        }
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $map
     */
    private function updateCollection(ClientsCollection $clientsCollection, RecordsCollection $recordsCollection, RecordsMap $map)
    {
        foreach ($recordsCollection->getList() as $key => $record) {
            if (empty($record->getRace()) || $clientsCollection->isPotentialMemberByRecordKey($key)) {
                continue;
            }
            if (!isset($this->list[$key])) {
                $this->list[$key] = (new ClientRace())
                    ->setClient($clientsCollection->getList()[$key])
                    ->setRaceName($record->getRace());
            }
        }
    }

    /**
     * Clear Client's Race field before assigning the values from import file
     *
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @throws \Doctrine\ORM\ORMException
     */
    public function clearExisting(ClientsCollection $clientsCollection, RecordsCollection $recordsCollection) :void
    {
        $clientIds = [];
        foreach ($recordsCollection->getList() as $key => $record) {
            if (empty($record->getRace()) || $clientsCollection->isPotentialMemberByRecordKey($key)) {
                continue;
            }
            /** @var \VirtualHealth\OrmBundle\Entity\Client $client */
            $client = $clientsCollection->getList()[$key];
            $clientIds[] = $client->getUserId();
        }

        if ($clientIds) {
            $this->repository->clearRacesByClientIds($clientIds);
        }

    }

}
