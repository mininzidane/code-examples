<?php
/**
 * ClientMailingsCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RegionsMap;
use VirtualHealth\Import\Membership\Edi834\Record;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\ClientMailing;
use VirtualHealth\OrmBundle\Repository\ClientMailingRepository;
use VirtualHealth\OrmBundle\Saver\Client\ClientMailing\ClientMailingSaver;

/**
 * Class ClientMailingsCollection
 * @package VirtualHealth\Import\Membership\Edi834\Collections
 */
class ClientMailingsCollection
{
    /**
     * @var ClientMailingRepository
     */
    private $repository;

    /**
     * @var ClientMailingSaver
     */
    private $saver;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /** @var RegionsMap $regionsMap */
    private $regionsMap;

    /**
     * @var ClientMailing[]
     */
    private $list = [];

    /**
     * ClientMailingsCollection constructor.
     *
     * @param ClientMailingRepository $repository
     * @param ClientMailingSaver $saver
     * @param CitiesMap $citiesMap
     * @param RegionsMap $regionsMap
     */
    public function __construct(ClientMailingRepository $repository, ClientMailingSaver $saver, CitiesMap $citiesMap, RegionsMap $regionsMap)
    {
        $this->repository = $repository;
        $this->saver = $saver;
        $this->citiesMap = $citiesMap;
        $this->regionsMap = $regionsMap;
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $map
     */
    public function fill(ClientsCollection $clientsCollection, RecordsCollection $recordsCollection, RecordsMap $map)
    {
        $this->list = [];
        $this->loadModels($map);
        $this->updateCollection($clientsCollection, $recordsCollection, $map);
    }

    /**
     * Save all models
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function save()
    {
        $this->saver->saveAll($this->list);
    }

    /**
     * @return ClientMailing[]
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param RecordsMap $map
     */
    private function loadModels(RecordsMap $map)
    {
        $models = $this->repository->findBy(['client' => $map->getClientIdsMap()]);
        $modelsMap = [];
        foreach ($models as $model) {
            $modelsMap[$model->getClient()->getUserId()] = $model;
        }
        foreach ($map->getClientIdsMap() as $recordKey => $clientId) {
            if (!isset($modelsMap[$clientId])) {
                continue;
            }
            $this->list[$recordKey] = $modelsMap[$clientId];
        }
    }
    /**
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $map
     */
    private function updateCollection(ClientsCollection $clientsCollection, RecordsCollection $recordsCollection, RecordsMap $map)
    {
        foreach ($recordsCollection->getList() as $key => $record) {
            if ($clientsCollection->isPotentialMemberByRecordKey($key)) {
                continue;
            }

            if (!isset($this->list[$key])) {
                $this->list[$key] = $this->updateClientMailing(new ClientMailing(), $clientsCollection->getList()[$key], $record);
            }
            if ($map->isClientMerge($key)) {
                $this->updateClientMailing($this->list[$key], $clientsCollection->getList()[$key], $record);
            }
        }
    }

    /**
     * @param ClientMailing $clientMailing
     * @param Client $client
     * @param Record $record
     * @return ClientMailing
     */
    private function updateClientMailing(ClientMailing $clientMailing, Client $client, Record $record)
    {
        $clientMailing->setClient($client);

        $mailingState = $record->getMailingState();

        if ($record->getMailingAddress1() !== null) {
            $clientMailing->setAddress1($record->getMailingAddress1());
        }
        if ($record->getMailingAddress2() !== null) {
            $clientMailing->setAddress2($record->getMailingAddress2());
        }
        if ($record->getMailingPostalCode() !== null) {
            $clientMailing->setZipCode($record->getMailingPostalCode());
        }
        if (($city = $this->citiesMap->getCity($record->getMailingCity(), $mailingState)) !== null) {
            $clientMailing->setCity($city);
        }

        if ($mailingState && $region = $this->regionsMap->getStateByTitle($mailingState)) {
            $clientMailing->setRegion($region);
        }

        return $clientMailing;
    }
}
