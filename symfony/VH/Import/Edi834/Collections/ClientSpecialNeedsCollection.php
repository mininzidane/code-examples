<?php
/**
 * ClientSpecialNeedsCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\Import\Membership\Edi834\Maps\SpecialNeedsMap;
use VirtualHealth\Import\Membership\Edi834\Query\DeleteClientSpecialNeedsByIds;
use VirtualHealth\OrmBundle\Entity\SpecialNeeds;
use VirtualHealth\OrmBundle\Repository\ClientSpecialNeedRepository;
use VirtualHealth\OrmBundle\Entity\ClientSpecialNeeds;
use VirtualHealth\OrmBundle\Saver\Client\ClientSpecialNeedSaver;

/**
 * Class ClientSpecialNeedsCollection
 * @package VirtualHealth\Import\Membership\Edi834\Collections
 */
class ClientSpecialNeedsCollection
{
    private const INCARCERATED_CODE = '51';

    private const NY_CHILD_CODES = ['45', '46', 'K3', 'K4', 'K5', 'K6', 'K8', 'K9'];

    /**
     * @var ClientSpecialNeedRepository
     */
    private $repository;

    /**
     * @var ClientSpecialNeedSaver
     */
    private $saver;

    /**
     * @var SpecialNeedsMap
     */
    private $specialNeedsMap;

    /**
     * @var DeleteClientSpecialNeedsByIds
     */
    private $deleteClientSpecialNeedsByIdsQuery;

    /**
     * @var ClientSpecialNeeds[]
     */
    private $list;

    /**
     * ClientSpecialNeedsCollection constructor.
     * @param ClientSpecialNeedRepository $repository
     * @param ClientSpecialNeedSaver $saver
     * @param SpecialNeedsMap $specialNeedsMap
     * @param DeleteClientSpecialNeedsByIds $deleteClientSpecialNeedsByIdsQuery
     */
    public function __construct(
        ClientSpecialNeedRepository $repository,
        ClientSpecialNeedSaver $saver,
        SpecialNeedsMap $specialNeedsMap,
        DeleteClientSpecialNeedsByIds $deleteClientSpecialNeedsByIdsQuery
    ) {
        $this->repository = $repository;
        $this->saver = $saver;
        $this->specialNeedsMap = $specialNeedsMap;
        $this->deleteClientSpecialNeedsByIdsQuery = $deleteClientSpecialNeedsByIdsQuery;
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $map
     * @throws \Doctrine\DBAL\DBALException
     */
    public function fill(ClientsCollection $clientsCollection, RecordsCollection $recordsCollection, RecordsMap $map)
    {
        $this->list = [];
        $this->loadAndUpdate($clientsCollection, $recordsCollection, $map);
    }

    /**
     * Save a collection
     * @throws \CDbException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function save()
    {
        $this->saver->saveAll($this->list);
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $map
     * @throws \Doctrine\DBAL\DBALException
     */
    private function loadAndUpdate(
        ClientsCollection $clientsCollection,
        RecordsCollection $recordsCollection,
        RecordsMap $map
    ) {
        $idsToDelete = [];
        $incarceratedSpecialNeed = $this->specialNeedsMap->getByCode(self::INCARCERATED_CODE);
        $flBnetSpecialNeed = $this->specialNeedsMap->getByName(SpecialNeeds::NAME_FL_BNET);
        $stateWardSpecialNeed = $this->specialNeedsMap->getByName(SpecialNeeds::NAME_STATE_WARD);
        $nyChildIds = $this->getNYChildIds();
        $clientSpecialNeedsMap = $this->getClientSpecialNeedsMap($map);

        foreach ($clientsCollection->getList() as $recordKey => $client) {
            if ($map->isPotentialMergeType($recordKey)) {
                continue;
            }
            $clientId = $client->getUser()->getId();
            $record = $recordsCollection->getList()[$recordKey];
            if (isset($map->getNotFoundKeys()[$recordKey])) {
                $uniqueSpecialNeedCodes = [];
                foreach ($record->getPlans() as $plan) {
                    foreach ($plan->getSpecialNeeds() as $specialNeedCode) {
                        if (isset($uniqueSpecialNeedCodes[$specialNeedCode])) {
                            continue;
                        }
                        $uniqueSpecialNeedCodes[$specialNeedCode] = true;
                        if (null !== ($specialNeed = $this->specialNeedsMap->getByCodeOrName($specialNeedCode))) {
                            $model = (new ClientSpecialNeeds())
                                ->setUser($client)
                                ->setSpecialNeedsId($specialNeed->getId());
                            $this->list[] = $model;
                        }
                    }
                }
                if (
                    $record->isValidIncarcerated()
                    && !isset($uniqueSpecialNeedCodes[self::INCARCERATED_CODE])
                    && $incarceratedSpecialNeed !== null
                ) {
                    $model = (new ClientSpecialNeeds())
                        ->setUser($client)
                        ->setSpecialNeedsId($incarceratedSpecialNeed->getId());
                    $this->list[] = $model;
                }
            } else {
                $specialNeeds = [
                    [$record->isValidIncarcerated(), $incarceratedSpecialNeed],
                    [$record->isFlBnetSpecialNeed(), $flBnetSpecialNeed],
                    [$record->isStateWardSpecialNeed(), $stateWardSpecialNeed],
                ];
                /**
                 * @var bool $isTrue
                 * @var SpecialNeeds $specialNeed
                 */
                foreach ($specialNeeds as list($isTrue, $specialNeed)) {
                    if (null === $specialNeed) {
                        continue;
                    }
                    if ($isTrue) {
                        if (!in_array($specialNeed->getId(), $clientSpecialNeedsMap[$clientId] ?? [])) {
                            $model = (new ClientSpecialNeeds())
                                ->setUser($client)
                                ->setSpecialNeedsId($specialNeed->getId());
                            $this->list[] = $model;
                        }
                    } else {
                        foreach ($clientSpecialNeedsMap[$clientId] ?? [] as $id => $specialNeedsId) {
                            if ($specialNeedsId == $specialNeed->getId()) {
                                $idsToDelete[] = $id;
                            }
                        }
                    }
                }
            }
            if (in_array($record->getSpecialNeedsNYChildCode(), self::NY_CHILD_CODES)) {
                foreach ($nyChildIds as $specialNeedId) {
                    if (
                        false !== ($key = array_search($specialNeedId, $clientSpecialNeedsMap[$clientId] ?? [], false))
                    ) {
                        $idsToDelete[] = $key;
                        unset($clientSpecialNeedsMap[$clientId][$key]);
                    }
                }
                if (
                    null !== ($specialNeed = $this->specialNeedsMap->getByCode($record->getSpecialNeedsNYChildCode()))
                ) {
                    $model = (new ClientSpecialNeeds())
                        ->setUser($client)
                        ->setSpecialNeedsId($specialNeed->getId());
                    $this->list[] = $model;
                }
            }
        }
        $this->deleteClientSpecialNeedsByIdsQuery->write($idsToDelete);
    }

    /**
     * @return int[]
     */
    private function getNYChildIds()
    {
        $result = [];
        foreach (self::NY_CHILD_CODES as $code) {
            if (null !== ($specialNeed = $this->specialNeedsMap->getByCode($code))) {
                $result[] = $specialNeed->getId();
            }
        }
        return $result;
    }

    /**
     * @param RecordsMap $map
     * @return array
     */
    private function getClientSpecialNeedsMap(RecordsMap $map): array
    {
        $result = [];
        /** @var ClientSpecialNeeds[] $models */
        $models = $this->repository->findByUser($map->getClientIdsMap());
        foreach ($models as $model) {
            $clientId = $model->getUser()->getUser()->getId();
            if (!isset($result[$clientId])) {
                $result[$clientId] = [];
            }
            $result[$clientId][$model->getId()] = $model->getSpecialNeedsId();
        }
        return $result;
    }
}
