<?php
/**
 * VirtualHealth\Import\Membership\Edi834\Collections\ClientCohortCollection
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use VirtualHealth\Import\Membership\Edi834\Maps\PlanCohortsMap;
use VirtualHealth\Import\Membership\Edi834\Query\ClientCohortBulkUpdate;

/**
 * Class ClientCohortCollection
 * @package VirtualHealth\Import\Membership\Edi834\Collections
 */
class ClientCohortCollection
{
    /**
     * Map of Cohort ids for clients.
     * Index - client id, value - list if int with Cohort ids.
     * If client has't cohorts - value is empty array
     * @var int[][]
     */
    private $list = [];

    /**
     * @var PlanCohortsMap
     */
    private $planCohortsMap;

    /**
     * @var ClientCohortBulkUpdate
     */
    private $clientCohortBulkUpdate;

    /**
     * ClientCohortCollection constructor.
     * @param ClientCohortBulkUpdate $clientCohortBulkUpdate
     * @param PlanCohortsMap $planCohortsMap
     */
    public function __construct(ClientCohortBulkUpdate $clientCohortBulkUpdate, PlanCohortsMap $planCohortsMap)
    {
        $this->planCohortsMap = $planCohortsMap;
        $this->clientCohortBulkUpdate = $clientCohortBulkUpdate;
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param ClientInsuranceCollection $clientInsuranceCollection
     */
    public function fill(
        ClientsCollection $clientsCollection,
        ClientInsuranceCollection $clientInsuranceCollection
    ): void {

        foreach ($clientsCollection->getList() as $key => $client) {
            if ($clientsCollection->isPotentialMemberByRecordKey($key)) {
                continue;
            }

            $cohortIds = [];
            $clientId = (int) $client->getUserId();
            $planIds = $clientInsuranceCollection->getActivePlanIds($clientId);
            foreach ($planIds as $planId) {
                if ($planId === null) {
                    continue;
                }
                $cohortIds = array_merge($cohortIds, $this->planCohortsMap->getCohorts($planId));
            }
            $this->list[$clientId] = array_unique($cohortIds);
        }
    }

    /**
     * Save all client's cohorts.
     * @throws \Doctrine\DBAL\DBALException
     */
    public function save(): void
    {
        $this->clientCohortBulkUpdate->write($this->list);
    }
}
