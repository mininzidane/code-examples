<?php
/**
 * ClientPoasCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use VirtualHealth\Client\Formatter\ClientFormatter;
use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\Import\Membership\Edi834\ResponsibleMember;
use VirtualHealth\OrmBundle\Entity\ClientPoa;
use VirtualHealth\OrmBundle\Repository\ClientPoaRepository;
use VirtualHealth\OrmBundle\Saver\Client\ClientPoaSaver;

/**
 * Class ClientPoasCollection
 * @package VirtualHealth\Import\Membership\Edi834\Collections
 */
class ClientPoasCollection
{
    /**
     * @var ClientPoaRepository
     */
    private $repository;

    /**
     * @var ClientPoaSaver
     */
    private $saver;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /**
     * @var ClientFormatter
     */
    private $clientFormatter;

    /**
     * @var array
     */
    private $modelsByKey = [];

    /**
     * @var array
     */
    private $modelsByFullKey = [];

    /**
     * @var ClientPoa[]
     */
    private $list = [];

    /**
     * MedicaresCollection constructor.
     * @param ClientPoaRepository $repository
     * @param ClientPoaSaver $saver
     * @param CitiesMap $citiesMap
     * @param ClientFormatter $clientFormatter
     */
    public function __construct(
        ClientPoaRepository $repository,
        ClientPoaSaver $saver,
        CitiesMap $citiesMap,
        ClientFormatter $clientFormatter
    ) {
        $this->repository = $repository;
        $this->saver = $saver;
        $this->citiesMap = $citiesMap;
        $this->clientFormatter = $clientFormatter;
    }

    /**
     * @param string $dateFormat
     * @return $this
     */
    public function setDateFormat($dateFormat)
    {
        $this->saver->setDateFormat($dateFormat);
        return $this;
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $map
     */
    public function fill(ClientsCollection $clientsCollection, RecordsCollection $recordsCollection, RecordsMap $map)
    {
        $this->modelsByKey = [];
        $this->modelsByFullKey = [];
        $this->list = [];
        $this->loadModels($map);
        $this->updateCollection($clientsCollection, $recordsCollection, $map);
    }

    /**
     * Save all models
     * @throws \CDbException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function save()
    {
        $this->saver->saveAll($this->list);
    }

    /**
     * @return ClientPoa[]
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param RecordsMap $map
     */
    private function loadModels(RecordsMap $map)
    {
        $models = $this->repository->findBy(['client' => $map->getClientIdsMap()]);
        foreach ($models as $model) {
            $key = $this->buildKey(
                (string) $model->getClient()->getUserId(),
                (string) $model->getFirstName(),
                (string) $model->getLastName()
            );
            $this->modelsByKey[$key] = $model;
            $fullKey = $this->buildFullKey(
                (string) $model->getClient()->getUserId(),
                (string) $model->getFirstName(),
                (string) $model->getLastName(),
                (string) $model->getAddress1()
            );
            $this->modelsByFullKey[$fullKey] = $model;
        }
    }

    /**
     * @param ClientsCollection $clientsCollection
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $map
     */
    private function updateCollection(ClientsCollection $clientsCollection, RecordsCollection $recordsCollection, RecordsMap $map)
    {
        foreach ($recordsCollection->getList() as $key => $record) {
            if ($clientsCollection->isPotentialMemberByRecordKey($key)) {
                continue;
            }
            $client = $clientsCollection->getList()[$key];
            foreach ($record->getResponsibleMembers() as $member) {
                if (!$member->isPoa()) {
                    continue;
                }
                $poaKey = $this->buildKey(
                    (string) $client->getUserId(),
                    (string) $member->getFirstName(),
                    (string) $member->getLastName()
                );
                $poaFullKey = $this->buildFullKey(
                    (string) $client->getUserId(),
                    (string) $member->getFirstName(),
                    (string) $member->getLastName(),
                    (string) $member->getAddressLine1()
                );
                if (isset($this->modelsByFullKey[$poaFullKey])) {
                    $this->list[$poaKey] = $this->modelsByFullKey[$poaFullKey];
                } else {
                    if (isset($this->modelsByKey[$poaKey])) {
                        $this->list[$poaKey] = $this->modelsByKey[$poaKey];
                    }
                }
                if (!isset($this->list[$poaKey])) {
                    $this->list[$poaKey] = (new ClientPoa())
                        ->setClient($client);
                    $this->modelsByKey[$poaKey] = $this->list[$poaKey];
                    $this->modelsByFullKey[$poaFullKey] = $this->list[$poaKey];
                }
                $this->updateMainInformation($this->list[$poaKey], $member);
            }
        }
    }

    /**
     * @param ClientPoa $model
     * @param ResponsibleMember $member
     */
    private function updateMainInformation(ClientPoa $model, ResponsibleMember $member)
    {
        if (!empty($member->getFirstName())) {
            $model->setFirstName($member->getFirstName());
        }

        if (!empty($member->getLastName())) {
            $model->setLastName($member->getLastName());
        }

        $model->setAlternatePhone(
            $member->getAlternatePhone() !== null
                ? $this->formatPhone($member->getAlternatePhone())
                : null
        );
        $model->setPrimaryPhone(
            $member->getPrimaryPhone() !== null
                ? $this->formatPhone($member->getPrimaryPhone())
                : null
        );
        $model->setAddress1($member->getAddressLine1() ?: null);
        $model->setAddress2($member->getAddressLine2() ?: null);
        $model->setPostalCode($member->getPostalCode() ?: null);

        if (!empty($member->getCityTitle())) {
            $city = $this->citiesMap->getCity(
                $member->getCityTitle(),
                $member->getRegionTitle()
            );
            if ($city !== null) {
                $model
                    ->setCity($city)
                    ->setCountry($city->getCountry())
                    ->setRegion($city->getRegion());
            } else {
                $model
                    ->setCity(null)
                    ->setCountry(null)
                    ->setRegion(null);
            }
        } else {
            $model
                ->setCity(null)
                ->setCountry(null)
                ->setRegion(null);
        }
    }

    /**
     * @param string $clientId
     * @param string $firstName
     * @param string $lastName
     * @return string
     */
    private function buildKey(string $clientId, string $firstName, string $lastName): string
    {
        return $clientId . ':' . mb_strtolower($firstName . ':' . $lastName);
    }

    /**
     * @param string $clientId
     * @param string $firstName
     * @param string $lastName
     * @param string $address1
     * @return string
     */
    private function buildFullKey(string $clientId, string $firstName, string $lastName, string $address1): string
    {
        return $clientId . ':' . mb_strtolower($firstName . ':' . $lastName . ':' . $address1);
    }

    /**
     * @param string $phone
     * @return string
     */
    private function formatPhone(string $phone): string
    {
        $phone = preg_replace('#[^\d]#', '', $phone);
        $length = strlen($phone);
        return $length > 5 && $length < 13
            ? $this->clientFormatter->normalizePhone($phone)
            : '000-000-0000';
    }
}
