<?php
/**
 * UsersCollection class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Collections;

use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\OrmBundle\Entity\User;
use VirtualHealth\OrmBundle\Repository\UserRepository;
use VirtualHealth\OrmBundle\Saver\User\UserSaver;

/**
 * Class UsersCollection
 * @package VirtualHealth\Import\Membership\Edi834\Collections
 */
class UsersCollection
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var UserSaver
     */
    private $saver;

    /**
     * @var User[]
     */
    private $list = [];

    /**
     * UsersCollection constructor.
     * @param UserRepository $repository
     * @param UserSaver $saver
     */
    public function __construct(UserRepository $repository, UserSaver $saver)
    {
        $this->repository = $repository;
        $this->saver = $saver;
    }

    /**
     * @param RecordsCollection $recordsCollection
     * @param RecordsMap $map
     * @throws \CException
     */
    public function fill(RecordsCollection $recordsCollection, RecordsMap $map)
    {
        $this->list = [];
        foreach ($map->getNotFoundKeys() as $key) {
            $record = $recordsCollection->getList()[$key];
            $emailTemplate = \strtolower(
                !empty($record->getSubscriberFirstName())
                    ? $record->getSubscriberFirstName() . '.' . $record->getSubscriberLastName()
                    : $record->getSubscriberLastName()
            );
            $emailTemplate = \preg_replace('/(\.{2,})/im', '', \preg_replace('/[^a-z0-9.]/im', '', $emailTemplate));
            $emailTemplate = \trim($emailTemplate, '.');
            $email = 'c.' . $emailTemplate . \uniqid() . '@' . \hostName();
            $model = (new User())
                ->setEmail($email)
                ->setPassword(\uniqid())
                ->setType(User::TYPE_CLIENT)
                ->setStatus(\BaseModel::YES)
                ->setFirstName($record->getSubscriberFirstName())
                ->setLastName($record->getSubscriberLastName())
                ->setMiddleName($record->getSubscriberMiddleName())
            ;
            $this->list[$key] = $model;
        }
    }

    /**
     * Save a collection
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function save()
    {
        if (!empty($this->list)) {
            $this->saver->saveAll($this->list);
        }
    }

    /**
     * @return User[]
     */
    public function getList()
    {
        return $this->list;
    }
}
