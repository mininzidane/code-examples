<?php
/**
 * PlanNotFoundException class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Exception;

/**
 * Class PlanNotFoundException
 * @package VirtualHealth\Import\Membership\Edi834\EventListener
 */
class PlanNotFoundException extends \RuntimeException
{
}
