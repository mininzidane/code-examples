<?php
/**
 * InsurancePlan class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834;

/**
 * Class InsurancePlan
 * @package VirtualHealth\Import\Membership\Edi834
 */
class InsurancePlan
{
    const PERIOD_PAST = 'past';
    const PERIOD_PRESENT = 'present';
    const PERIOD_FUTURE = 'future';
    const PERIOD_UNKNOWN = 'unknown';

    /**
     * @var string
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $enrollmentDate;

    /**
     * @var \DateTime
     */
    private $terminationDate;

    /**
     * @var string
     */
    private $spanId;

    /**
     * @var string
     */
    private $waiver;

    /**
     * @var string
     */
    private $rateCode;

    /**
     * @var string[]
     */
    private $specialNeeds = [];

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return InsurancePlan
     */
    public function setId(string $id): InsurancePlan
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getEnrollmentDate(): ?\DateTime
    {
        return $this->enrollmentDate;
    }

    /**
     * @param \DateTime|null $enrollmentDate
     * @return InsurancePlan
     */
    public function setEnrollmentDate(?\DateTime $enrollmentDate): InsurancePlan
    {
        $this->enrollmentDate = $enrollmentDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getTerminationDate(): ?\DateTime
    {
        return $this->terminationDate;
    }

    /**
     * @param \DateTime|null $terminationDate
     * @return InsurancePlan
     */
    public function setTerminationDate(?\DateTime $terminationDate): InsurancePlan
    {
        $this->terminationDate = $terminationDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWaiver(): ?string
    {
        return $this->waiver;
    }

    /**
     * @param string|null $waiver
     * @return InsurancePlan
     */
    public function setWaiver($waiver): InsurancePlan
    {
        $this->waiver = $waiver;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRateCode(): ?string
    {
        return $this->rateCode;
    }

    /**
     * @param string $rateCode
     * @return InsurancePlan
     */
    public function setRateCode(string $rateCode): InsurancePlan
    {
        $this->rateCode = $rateCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpanId(): ?string
    {
        return $this->spanId;
    }

    /**
     * @param string $spanId
     * @return InsurancePlan
     */
    public function setSpanId(string $spanId): InsurancePlan
    {
        $this->spanId = $spanId;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getSpecialNeeds(): array
    {
        return $this->specialNeeds;
    }

    /**
     * @param string[] $specialNeeds
     * @return InsurancePlan
     */
    public function setSpecialNeeds(array $specialNeeds): InsurancePlan
    {
        $this->specialNeeds = $specialNeeds;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBh()
    {
        return $this->id === 'ZBH-BH';
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isPast(): bool
    {
        return $this->getPeriod() === self::PERIOD_PAST;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getPeriod(): string
    {
        $currentDate = new \DateTime('today');
        if ($this->terminationDate !== null) {
            $diff = $currentDate->diff($this->terminationDate);
            if ($diff->days > 0 && $diff->invert == 1) {
                return self::PERIOD_PAST;
            }
        }
        if ($this->enrollmentDate === null) {
            return self::PERIOD_UNKNOWN;
        }
        $enDiff = $currentDate->diff($this->enrollmentDate);
        if (
            ($enDiff->days == 0 || ($enDiff->days > 0 && $enDiff->invert == 1))
            && ($this->terminationDate === null || ($diff->days == 0 || ($diff->days > 0 && $diff->invert == 0)))
        ) {
            return self::PERIOD_PRESENT;
        }
        if ($enDiff->days > 0 && $enDiff->invert == 0) {
            return self::PERIOD_FUTURE;
        }
        return self::PERIOD_UNKNOWN;
    }
}
