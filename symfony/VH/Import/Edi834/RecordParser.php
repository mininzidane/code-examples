<?php
/**
 * RecordsParser class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834;

use VirtualHealth\Import\Membership\Edi834\Parsers\Address;
use VirtualHealth\Import\Membership\Edi834\Parsers\CaseNumber;
use VirtualHealth\Import\Membership\Edi834\Parsers\CommunicationNumber;
use VirtualHealth\Import\Membership\Edi834\Parsers\CurrentIndex;
use VirtualHealth\Import\Membership\Edi834\Parsers\DateDecease;
use VirtualHealth\Import\Membership\Edi834\Parsers\Demographic;
use VirtualHealth\Import\Membership\Edi834\Parsers\Element;
use VirtualHealth\Import\Membership\Edi834\Parsers\HealthCoverage;
use VirtualHealth\Import\Membership\Edi834\Parsers\HealthCoverageDate;
use VirtualHealth\Import\Membership\Edi834\Parsers\Language;
use VirtualHealth\Import\Membership\Edi834\Parsers\MailingAddress;
use VirtualHealth\Import\Membership\Edi834\Parsers\ProviderInformation;
use VirtualHealth\Import\Membership\Edi834\Parsers\ReportingCategoryIdentification;
use VirtualHealth\Import\Membership\Edi834\Parsers\ReportingCategoryName;
use VirtualHealth\Import\Membership\Edi834\Parsers\ReportingCategoryRange;
use VirtualHealth\Import\Membership\Edi834\Parsers\ResponsiblePartyAddress;
use VirtualHealth\Import\Membership\Edi834\Parsers\ResponsiblePartyName;
use VirtualHealth\Import\Membership\Edi834\Parsers\ResponsiblePartyPhone;
use VirtualHealth\Import\Membership\Edi834\Parsers\SequentialNumber;
use VirtualHealth\Import\Membership\Edi834\Parsers\SpecialNeedsIndicator;
use VirtualHealth\Import\Membership\Edi834\Parsers\SubscriberName;
use VirtualHealth\Import\Membership\Edi834\Parsers\SubscriberNumber;
use VirtualHealth\Import\Membership\Edi834\Parsers\UniqueSpanIdentifier;
use VirtualHealth\Import\Membership\Edi834\Parsers\TnChoiceWaiver;

/**
 * Class RecordParser
 * @package VirtualHealth\Import\Membership\Edi834
 */
class RecordParser
{
    public const SEGMENT_SEPARATOR = '~';
    public const ELEMENT_SEPARATOR = '*';
    public const SUB_ELEMENT_SEPARATOR = ':';
    public const REPETITION_SEPARATOR = '^';
    public const LOOP_INIT = 'HEADER';

    private $loop;
    private $parsers;

    public function __construct(
        DateDecease $dateDecease,
        SubscriberNumber $subscriberNumber,
        CaseNumber $caseNumber,
        SequentialNumber $sequentialNumber,
        SubscriberName $subscriberName,
        CommunicationNumber $communicationNumber,
        Address $address,
        Demographic $demographic,
        Language $language,
        MailingAddress $mailingAddress,
        ResponsiblePartyName $responsiblePartyName,
        ResponsiblePartyPhone $responsiblePartyPhone,
        ResponsiblePartyAddress $responsiblePartyAddress,
        HealthCoverage $healthCoverage,
        HealthCoverageDate $healthCoverageDate,
        UniqueSpanIdentifier $uniqueSpanIdentifier,
        SpecialNeedsIndicator $specialNeedsIndicator,
        CurrentIndex $currentIndex,
        ProviderInformation $providerInformation,
        ReportingCategoryName $reportingCategoryName,
        ReportingCategoryIdentification $reportingCategoryIdentification,
        ReportingCategoryRange $reportingCategoryRange,
        TnChoiceWaiver $tnChoiceWaiver
    ) {
        $this->parsers = [
            '2000' => [
                $dateDecease,
                $subscriberNumber,
                $caseNumber,
                $sequentialNumber,
            ],
            '2100A' => [
                $subscriberName,
                $communicationNumber,
                $address,
                $demographic,
                $language,
            ],
            '2100C' => [
                $mailingAddress,
            ],
            '2100G' => [
                $responsiblePartyName,
                $responsiblePartyPhone,
                $responsiblePartyAddress,
            ],
            '2300' => [
                $healthCoverage,
                $healthCoverageDate,
                $uniqueSpanIdentifier,
                $specialNeedsIndicator,
                $tnChoiceWaiver,
            ],
            '2310' => [
                $currentIndex,
                $providerInformation,
            ],
            '2750' => [
                $reportingCategoryName,
                $reportingCategoryIdentification,
                $reportingCategoryRange,
            ],
        ];
    }

    public function parseRecord(string $blob): Record
    {
        $this->loop = self::LOOP_INIT;
        $record = new Record();
        $segments = explode(self::SEGMENT_SEPARATOR, rtrim($blob, self::SEGMENT_SEPARATOR));
        foreach ($segments as $segment) {
            $elements = explode(self::ELEMENT_SEPARATOR, trim($segment));
            if (\count($elements) < 2) {
                continue;
            }
            $this->updateLoop($elements);
            if (isset($this->parsers[$this->loop])) {
                foreach ($this->parsers[$this->loop] as $parser) {
                    /** @var Element $parser */
                    $parser->parse($record, $elements);
                }
            }
        }
        return $record;
    }

    private function updateLoop(array $elements): void
    {
        // @todo: rewrite it clean
        $segmentId = $elements[0];
        $firstElement = $elements[1];
        if ($segmentId == 'N1') {
            switch ($firstElement) {
                case 'P5':
                    $this->loop = '1000A'; // SPONSOR NAME
                    break;
                case 'IN':
                    $this->loop = '1000B'; // PAYER
                    break;
                case '75':
                    $this->loop = '2750'; // Member Reporting
                    break;
            }
        } elseif ($segmentId == 'INS') {
            $this->loop = '2000'; // MEMBER LEVEL DETAIL
        } elseif ($segmentId == 'NM1') {
            switch ($firstElement) {
                case 'IL':
                    $this->loop = '2100A'; // Member Name
                    break;
                case '31':
                    $this->loop = '2100C'; // Postal Mailing Address
                    break;
                case 'E1':
                case 'QD':
                case 'J6':
                    $this->loop = '2100G'; // Responsible Person
                    break;
                case 'P3':
                    $this->loop = '2310'; // PROVIDER INFORMATION
                    break;
            }
        } elseif ($segmentId == 'HD') {
            $this->loop = '2300'; // HEALTH COVERAGE
        } elseif ($segmentId == 'ST' && $firstElement == '834') {
            $this->loop = 'TransactionSetHeader';
        }
    }
}
