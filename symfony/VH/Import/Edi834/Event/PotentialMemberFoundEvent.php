<?php

namespace VirtualHealth\Import\Membership\Edi834\Event;

use Symfony\Component\EventDispatcher\Event;
use VirtualHealth\OrmBundle\Entity\Client;

class PotentialMemberFoundEvent extends Event
{
    const NAME = 'import.potentialMemberFound';

    /** @var Client */
    private $client;
    /** @var array */
    private $data;
    /** @var int */
    private $membershipFileId;

    public function __construct(Client $client, array $data, $membershipFileId)
    {
        $this->client = $client;
        $this->data = $data;
        $this->membershipFileId = $membershipFileId;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getMembershipFileId()
    {
        return $this->membershipFileId;
    }
}