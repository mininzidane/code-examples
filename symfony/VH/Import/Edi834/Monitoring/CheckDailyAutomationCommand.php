<?php
/**
 * CheckDailyAutomationCommand class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Monitoring;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use VirtualHealth\Import\Membership\Edi834\CommandBus\DownloadAndProcessEdi834FileCommand;
use VirtualHealth\Import\Membership\Edi834\Services\AutomationRemoteFilesFinder;
use VirtualHealth\Import\Monitoring\Notifications\MonitoringNotifierInterface;
use VirtualHealth\OrmBundle\Entity\MembershipFile;
use VirtualHealth\OrmBundle\Repository\MembershipFileRepository;
use VirtualHealth\User\CommonUsersGetter;

/**
 * Class CheckDailyAutomationCommand
 * @package VirtualHealth\Import\Membership\Edi834\Monitoring
 */
class CheckDailyAutomationCommand extends Command
{
    private const MODE_ARGUMENT_NAME = 'mode';
    private const CHECK_STATUS_OPTION_NAME = 'check-progress';

    /**
     * @var AutomationRemoteFilesFinder
     */
    private $remoteFilesFinder;
    /**
     * @var CommonUsersGetter
     */
    private $usersGetter;
    /**
     * @var MembershipFileRepository
     */
    private $membershipFileRepository;
    /**
     * @var MonitoringNotifierInterface
     */
    private $monitoring;

    /**
     * CheckDailyAutomationCommand constructor.
     * @param CommonUsersGetter $usersGetter
     * @param MembershipFileRepository $membershipFileRepository
     * @param MonitoringNotifierInterface $monitoring
     * @param AutomationRemoteFilesFinder $remoteFilesFinder
     */
    public function __construct(
        CommonUsersGetter $usersGetter,
        MembershipFileRepository $membershipFileRepository,
        MonitoringNotifierInterface $monitoring,
        AutomationRemoteFilesFinder $remoteFilesFinder
    ) {
        parent::__construct();

        $this->remoteFilesFinder = $remoteFilesFinder;
        $this->usersGetter = $usersGetter;
        $this->membershipFileRepository = $membershipFileRepository;
        $this->monitoring = $monitoring;
    }

    /**
     * @inheritdoc
     */
    protected function configure(): void
    {
        $this
            ->setName('vh:import:monitoring:membership:check-automation')
            ->setDescription('Checks daily membership import automation')
            ->addArgument(self::MODE_ARGUMENT_NAME, InputArgument::REQUIRED, '\'change\' - check Change Automation OR \'audit\' - check Audit Automation')
            ->addOption(self::CHECK_STATUS_OPTION_NAME);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mode = $input->getArgument(self::MODE_ARGUMENT_NAME);
        $checkStatus = $input->getOption(self::CHECK_STATUS_OPTION_NAME);
        $author = $this->usersGetter->getSystemSuperAdmin();

        switch ($mode) {
            case DownloadAndProcessEdi834FileCommand::MODE_CHANGE:
                $date = new \DateTime('yesterday');
                break;
            case DownloadAndProcessEdi834FileCommand::MODE_AUDIT:
                $date = new \DateTime('previous sunday');
                break;
            default:
                throw new \InvalidArgumentException('Required attribute `mode` can be only - `change` OR `audit`');
                break;
        }

        $remotePaths = $this->remoteFilesFinder->getRemoteFilesPathsForDate($mode, $date, $author->getId(), false);
        $remoteFileNameList = array_keys($remotePaths);


        if (empty($remotePaths)) {
            $this->monitoring->error(
                'Import: membership files not found on SFTP',
                "Membership files not found on WC's SFTP.\nFile mode: {$mode}\nDate: {$date->format('y-m-d')}"
            );
        } else {
            $localFileList = $this->membershipFileRepository->findByFilenameOriginal($remoteFileNameList);

            $this->checkFilesPickedUp($localFileList, $remoteFileNameList, $mode);

            if ($checkStatus) {
                $this->checkFilesProcessedCompletely($localFileList);
            }
        }
    }

    /**
     * @param MembershipFile[] $localFileList
     * @param array $remoteFileNameList
     */
    private function checkFilesPickedUp($localFileList, $remoteFileNameList, $mode)
    {
        $remoteFileCount = count($remoteFileNameList);
        $localFileCount = count($localFileList);
        if (empty($localFileList)) {
            $this->monitoring->error(
                'Import: membership files not picked up',
                "Membership files not picked up.\nFile mode: {$mode}\nThere are {$remoteFileCount} file(s) ready to import."
            );
        } elseif ($localFileCount !== $remoteFileCount) {
            $this->monitoring->error(
                'Import: not all membership files automatically picked up',
                "Some membership files not picked up.\nFile mode: {$mode}\nFiles on SFTP: {$remoteFileCount}\nFiles fetched: {$localFileCount}."
            );
        } else {
            $this->monitoring->success(
                'Import: all membership files automatically picked up',
                "All membership files automatically picked up.\nFile mode: {$mode}\nFiles on SFTP: {$remoteFileCount}\nFiles fetched: {$localFileCount}."
            );
        }
    }

    /**
     * @param MembershipFile[] $localFileList
     */
    private function checkFilesProcessedCompletely($localFileList)
    {
        $filesStillInProgress = [];
        foreach ($localFileList as $file) {
            if ($file->getInProgress()) {
                $filesStillInProgress[] = $file;
            }
        }

        if (!empty($filesStillInProgress)) {
            $filesStillInProgressCount = count($filesStillInProgress);
            $this->monitoring->error(
                'Import: membership files still in progress',
                "Files in progress: {$filesStillInProgressCount}."
            );
        } elseif (!empty($localFileList)) {
            $this->monitoring->success(
                'Import: all automatically loaded membership files processed completely',
                'Looks good.'
            );
        }
    }
}
