<?php
/**
 * MembershipImportMonitor class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Monitoring;

use VirtualHealth\Import\Monitoring\Metrics\DTO\BatchProcessingMetrics;
use VirtualHealth\Import\Monitoring\Metrics\DTO\FileFinishMetrics;
use VirtualHealth\Import\Monitoring\Metrics\DTO\FileStartMetrics;
use VirtualHealth\Import\Monitoring\Metrics\DTO\FileTags;
use VirtualHealth\Import\Monitoring\Metrics\FileProcessingMonitorInterface;
use VirtualHealth\OrmBundle\Entity\MembershipFile;

/**
 * Class MembershipImportMonitor
 * @package VirtualHealth\Import\Membership\Edi834\Monitoring
 */
class MembershipImportMonitor
{
    /**
     * @var FileProcessingMonitorInterface
     */
    private $monitor;

    /**
     * MembershipImportMonitor constructor.
     * @param FileProcessingMonitorInterface $monitor
     */
    public function __construct(FileProcessingMonitorInterface $monitor)
    {
        $this->monitor = $monitor;
    }

    /**
     * @param MembershipFile $file
     */
    public function fileProcessingStarted(MembershipFile $file)
    {
        $metrics = new FileStartMetrics(
            $file->getId(),
            $file->getFilenameOriginal(),
            $file->getInProgress(),
            (int) $file->getFileSize(),
            $file->getFileOffset(),
            (int) $file->getProgressPercent(),
            $file->getLastProcessedLine(),
            (int) $file->getStartsCount(),
            0
        );

        $this->monitor->fileProcessingStarted($metrics, $this->prepareFileTags($file));
    }

    /**
     * @param MembershipFile $file
     */
    public function fileBatchProcessed(MembershipFile $file)
    {
        $metrics = new BatchProcessingMetrics(
            $file->getFileOffset(),
            (int) $file->getProgressPercent(),
            $file->getLastProcessedLine()
        );
        $this->monitor->fileBatchProcessed($metrics, $this->prepareFileTags($file));
    }

    /**
     * @param MembershipFile $file
     * @throws \Exception
     */
    public function fileProcessingFinished(MembershipFile $file)
    {
        $inProgressTime = 0;
        if ($file->getFirstStartDate() !== null) {
            $inProgressTime = (new \DateTimeImmutable())->getTimestamp() - $file->getFirstStartDate()->getTimestamp();
        }
        $metrics = new FileFinishMetrics(
            $file->getInProgress(),
            $file->getFileOffset(),
            (int) $file->getProgressPercent(),
            $file->getLastProcessedLine(),
            $inProgressTime
        );
        $this->monitor->fileProcessingFinished($metrics, $this->prepareFileTags($file));
    }

    /**
     * @param MembershipFile $file
     * @return FileTags
     */
    private function prepareFileTags(MembershipFile $file): FileTags
    {
        return new FileTags(
            $file->getId(),
            $file->getFilenameOriginal(),
            $file->getCreatedDate()
        );
    }
}
