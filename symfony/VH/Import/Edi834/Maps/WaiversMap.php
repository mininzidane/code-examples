<?php
/**
 * WaiversMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use VirtualHealth\Import\Membership\Edi834\Query\GetWaiverCodesAndIds;
use VirtualHealth\Import\Membership\Edi834\Query\GetWaiverNamesAndIds;

/**
 * Class WaiversMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class WaiversMap
{
    /**
     * @var GetWaiverCodesAndIds
     */
    private $waiverCodesAndIdsQuery;

    /**
     * @var GetWaiverNamesAndIds
     */
    private $waiverNamesAndIdsQuery;

    /**
     * @var int[]
     */
    private $codesMap;

    /**
     * @var int[]
     */
    private $namesMap;

    /**
     * WaiversMap constructor.
     * @param GetWaiverCodesAndIds $waiverCodesAndIdsQuery
     * @param GetWaiverNamesAndIds $waiverNamesAndIdsQuery
     */
    public function __construct(
        GetWaiverCodesAndIds $waiverCodesAndIdsQuery,
        GetWaiverNamesAndIds $waiverNamesAndIdsQuery
    ) {
        $this->waiverCodesAndIdsQuery = $waiverCodesAndIdsQuery;
        $this->waiverNamesAndIdsQuery = $waiverNamesAndIdsQuery;
    }

    /**
     * @param string|null $code
     * @return int|null
     */
    public function getIdByCode(?string $code): ?int
    {
        if (empty($code)) {
            return null;
        }
        if ($this->codesMap === null) {
            $this->codesMap = $this->waiverCodesAndIdsQuery->read();
        }
        return $this->codesMap[$code] ?? null;
    }

    /**
     * @param string|null $name
     * @return int|null
     */
    public function getIdByName(?string $name): ?int
    {
        if ($this->namesMap === null) {
            $this->namesMap = $this->waiverNamesAndIdsQuery->read();
        }
        return $this->namesMap[$name] ?? null;
    }

    /**
     * @param string|null $value
     * @return int|null
     */
    public function getIdByCodeOrName(?string $value): ?int
    {
        return $this->getIdByCode($value) ?: $this->getIdByName($value);
    }
}
