<?php
/**
 * RecordsMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Type;
use VirtualHealth\Import\Membership\Edi834\Collections\RecordsCollection;
use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Query\PotentialMergeClientsQuery;
use VirtualHealth\Import\Membership\Edi834\Validators\BaseValidator;

/**
 * Class ClientRecordMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class RecordsMap
{
    // @todo: Make constants private
    const CRITERIA_TYPE_SUBSCRIBER_NUMBER = 1;
    const CRITERIA_TYPE_ALTERNATE_ID = 2;
    const CRITERIA_TYPE_SUBSCRIBER_NUMBER_ADVANCED = 3;
    const CRITERIA_TYPE_TEMPORARY_CLIENT = 4;
    private const CRITERIA_TYPE_POTENTIAL_MERGE = 5;
    const CRITERIA_TYPE_MERGE_CLIENT = 6;
    // Criteria 2.1
    private const CRITERIA_TYPE_STATE_DATA_WITH_NUMBER = 7;
    // Criteria 2.2
    private const CRITERIA_TYPE_STATE_DATA_PERSON_INFO = 8;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /**
     * @var int[]
     */
    private $notFoundKeys;

    /**
     * @var string[]
     */
    private $subscriberIds;

    /**
     * @var string[]
     */
    private $sequentialNumbers;

    /**
     * @var string[]
     */
    private $medicaidIds;

    /**
     * @var array
     */
    private $medicaidSet;

    /**
     * @var string[]
     */
    private $medicareIds;

    /**
     * @var array
     */
    private $medicareSet;

    /**
     * @var string[]
     */
    private $alternateMemberNumbers;

    /**
     * @var int[]
     */
    private $clientIdsMap;

    /**
     * @var int[]
     */
    private $clientRaceIdsMap;

    /**
     * @var int[]
     */
    private $criteriaTypes;

    /**
     * @var PotentialMergeClientsQuery
     */
    private $potentialMergeClientsQuery;

    /**
     * ClientRecordMap constructor.
     * @param Connection $connection
     * @param CitiesMap $citiesMap
     * @param PotentialMergeClientsQuery $potentialMergeClientsQuery
     */
    public function __construct(
        Connection $connection,
        CitiesMap $citiesMap,
        PotentialMergeClientsQuery $potentialMergeClientsQuery
    ) {
        $this->connection = $connection;
        $this->citiesMap = $citiesMap;
        $this->potentialMergeClientsQuery = $potentialMergeClientsQuery;
    }

    /**
     * @param RecordsCollection $recordsCollection
     * @param AdapterParseLog $parseLog
     * @throws \Doctrine\DBAL\DBALException
     */
    public function fill(RecordsCollection $recordsCollection, AdapterParseLog $parseLog)
    {
        $this->initMaps();
        $this->extractValues($recordsCollection);
        $this->findClientIdsByCriteria1();
        $this->findClientIdsByCriteria2();
        $this->findClientIdsByCriteria3();
        $this->findClientIdsByCriteria4();
        $this->findClientIdsByCriteria5();
        $this->findRace($recordsCollection);
        $this->findPotentialMergeClientIds($recordsCollection);
        $this->validateSequentialNumbers($recordsCollection, $parseLog);
    }

    /**
     * Use for new clients only
     * @param RecordsCollection $recordsCollection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function fillWithoutSearch(RecordsCollection $recordsCollection)
    {
        $this->initMaps();
        $this->extractValues($recordsCollection);
        $this->findRace($recordsCollection);
    }

    /**
     * @param int $recordKey
     * @return int|null
     */
    public function getClientId($recordKey)
    {
        return $this->clientIdsMap[$recordKey] ?? null;
    }

    /**
     * @param int $key
     * @return int|null
     */
    public function getCriteriaTypeByRecordKey($key)
    {
        return $this->criteriaTypes[$key] ?? null;
    }

    /**
     * Whether import process is client merge
     * @param int $key
     * @return bool
     */
    public function isClientMerge($key)
    {
        return $this->getCriteriaTypeByRecordKey($key) == self::CRITERIA_TYPE_MERGE_CLIENT;
    }

    /**
     * @return int[]
     */
    public function getClientIdsMap()
    {
        return $this->clientIdsMap;
    }

    /**
     * @return int[]
     */
    public function getClientRaceIdsMap()
    {
        return $this->clientRaceIdsMap;
    }

    /**
     * @return int[]
     */
    public function getNotFoundKeys()
    {
        return $this->notFoundKeys;
    }

    /**
     * @param int $key
     * @param int $clientId
     */
    public function fillDataForMergedClient($key, $clientId)
    {
        $this->addRelationToClient($key, $clientId, self::CRITERIA_TYPE_MERGE_CLIENT);
    }

    /**
     * @param int $recordKey
     * @return bool
     */
    public function isPotentialMergeType(int $recordKey)
    {
        return $this->getCriteriaTypeByRecordKey($recordKey) === self::CRITERIA_TYPE_POTENTIAL_MERGE;
    }

    /**
     * Init maps
     */
    private function initMaps()
    {
        $this->notFoundKeys = [];
        $this->subscriberIds = [];
        $this->sequentialNumbers = [];
        $this->medicaidIds = [];
        $this->medicaidSet = [];
        $this->medicareIds = [];
        $this->medicareSet = [];
        $this->alternateMemberNumbers = [];
        $this->clientRaceIdsMap = [];
        $this->clientIdsMap = [];
        $this->criteriaTypes = [];
    }

    /**
     * @param RecordsCollection $recordsCollection
     */
    private function extractValues(RecordsCollection $recordsCollection)
    {
        foreach ($recordsCollection->getList() as $key => $record) {
            $this->notFoundKeys[$key] = $key;
            $this->subscriberIds[$key] = $record->getSubscriberId();
            if ($record->hasMedicaid()) {
                $this->medicaidIds[$key] = $record->getMedicaidSubscriberNumber();
                $this->medicaidSet[$key] = [
                    'isBh' => $record->isBh(),
                    'number' => $record->getMedicaidSubscriberNumber(),
                    'firstName' => $record->getSubscriberFirstName(),
                    'lastName' => $record->getSubscriberLastName(),
                    'birthday' => $record->getDateOfBirth(),
                    'address1' => $record->getHomeAddress1(),
                    'city' => $record->getHomeCity(),
                    'state' => $record->getHomeState(),
                    'sex' => $record->getSex(),
                ];
            }
            if ($record->hasMedicare()) {
                $this->medicareIds[$key] = $record->getMedicareSubscriberNumber();
                $this->medicareSet[$key] = [
                    'isBh' => $record->isBh(),
                    'number' => $record->getMedicareSubscriberNumber(),
                    'firstName' => $record->getSubscriberFirstName(),
                    'lastName' => $record->getSubscriberLastName(),
                    'birthday' => $record->getDateOfBirth(),
                    'address1' => $record->getHomeAddress1(),
                    'city' => $record->getHomeCity(),
                    'state' => $record->getHomeState(),
                    'sex' => $record->getSex(),
                ];
            }
            if (!empty($record->getAlternateMemberNumber())) {
                $this->alternateMemberNumbers[$key] = $record->getAlternateMemberNumber();
            }
            if (!empty($record->getSequentialMemberNumber())) {
                $this->sequentialNumbers[$key] = $record->getSequentialMemberNumber();
            }
        }
    }

    /**
     * Criteria 1
     * "Subscriber Number” in file == any one of the following 2 fields from section 2.2 above:
     *     Medicare block / Subscriber ID
     *     Medicaid block / Subscriber ID
     * @throws \Doctrine\DBAL\DBALException
     */
    private function findClientIdsByCriteria1()
    {
        // @todo: Move queries to separated Query-classes
        if (!empty($this->subscriberIds)) {
            $inConditionValuesString = $this->getInConditionValuesString($this->subscriberIds);
            $sql = <<<SQL
(
    SELECT `subscriber_id`, `client_id`
    FROM `client_insurance_medicaid` 
    WHERE `subscriber_id` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `subscriber_id`, `client_id`
    FROM `client_insurance_medicaid_future` 
    WHERE `subscriber_id` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `subscriber_id`, `client_id`
    FROM `client_insurance_medicare`
    WHERE `subscriber_id` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `subscriber_id`, `client_id`
    FROM `client_insurance_medicare_future`
    WHERE `subscriber_id` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `subscriber_id`, `client_id`
    FROM `enrollment_history`
    WHERE `subscriber_id` IN ({$inConditionValuesString})
)
SQL;
            $map = $this
                ->connection
                ->executeQuery($sql)
                ->fetchAll(\PDO::FETCH_KEY_PAIR);
            if ($map !== false) {
                foreach ($map as $subscriberId => $clientId) {
                    $key = array_search($subscriberId, $this->subscriberIds);
                    $this->addRelationToClient($key, $clientId, self::CRITERIA_TYPE_SUBSCRIBER_NUMBER);
                }
            }
        }

    }

    /**
     * Criteria 2
     *  (("Medicare ID" OR "Medicaid ID") AND "First Name" AND "Last Name" AND "Date of Birth")) in file
     *      == ("Medicare Number" OR "Medicaid Number") AND "State Data / First Name" AND "State Data / Last Name" AND "Date of Birth") in Helios
     *  OR
     * ("First Name" AND "Last Name" AND "Address Line 1" AND "City" AND "State" AND Date of Birth" AND "Sex") in file
     *      == ("State Data / First Name" AND "State Data / Last Name" AND  "State Data / Address Line 1" AND "State Data / City" AND "State Data / State" AND "Date of Birth" AND Sex) in Helios
     */
    private function findClientIdsByCriteria2(): void
    {
        $insurances = [
            'medicaid' => $this->medicaidSet,
            'medicare' => $this->medicareSet,
        ];

        // Criteria 2.1
        foreach ($insurances as $insurance => $sets) {
            if (!empty($sets)) {
                $numberToKey = [];
                $conditions = [];
                foreach ($sets as $key => $set) {
                    $numberToKey[$set['number']] = $key;
                    $number = $this->connection->quote($set['number'], Type::STRING);
                    $firstName = $this->connection->quote($set['firstName'], Type::STRING);
                    $lastName = $this->connection->quote($set['lastName'], Type::STRING);
                    $birthDate = $this->connection->quote($set['birthday']->format('m/d/Y'), Type::STRING);
                    $conditions[] = <<<SQL
(
    (
        `m`.`number` = vh_encrypt({$number})
        OR `mf`.`number` = vh_encrypt({$number})
        OR `eh`.`id_number`  = vh_encrypt({$number})
    )
    AND `csd`.`first_name` = vh_encrypt({$firstName})
    AND `csd`.`last_name` = vh_encrypt({$lastName})
    AND `c`.`birthday` = vh_encrypt({$birthDate})
)
SQL;
                }
                if (count($conditions) > 0) {
                    $conditionString = \implode(' OR ', $conditions);
                    $sql = <<<SQL
SELECT DISTINCT vh_decrypt(IFNULL(`m`.`number`, IFNULL(`mf`.`number`, `eh`.`id_number`))), `c`.`user_id`
FROM `client` `c`
LEFT JOIN `client_state_data` `csd` ON `c`.`user_id` = `csd`.`client_id`
LEFT JOIN `client_insurance_{$insurance}` `m` ON `c`.`user_id` = `m`.`client_id`
LEFT JOIN `client_insurance_{$insurance}_future` `mf` ON `c`.`user_id` = `mf`.`client_id`
LEFT JOIN `enrollment_history` `eh` ON `c`.`user_id` = `eh`.`client_id`
WHERE {$conditionString}
SQL;
                    $map = $this
                        ->connection
                        ->executeQuery($sql)
                        ->fetchAll(\PDO::FETCH_KEY_PAIR);
                    foreach ($map as $number => $clientId) {
                        $this->addRelationToClient(
                            $numberToKey[$number],
                            $clientId,
                            self::CRITERIA_TYPE_STATE_DATA_WITH_NUMBER
                        );
                    }
                }
            }
        }

        // Criteria 2.2
        foreach ($insurances as $insurance => $sets) {
            if (!empty($sets)) {
                $numberToKey = [];
                $conditions = [];
                $numbers = [];
                foreach ($sets as $key => $set) {
                    $city = $this->citiesMap->getCity($set['city'], $set['state']);
                    if (null === $city) {
                        continue;
                    }
                    $numberToKey[$set['number']] = $key;

                    $number = $this->connection->quote($set['number'], Type::STRING);
                    $numbers[] = $number;

                    $firstName = $this->connection->quote($set['firstName'], Type::STRING);
                    $lastName = $this->connection->quote($set['lastName'], Type::STRING);
                    $address1 = $this->connection->quote($set['address1'], Type::STRING);
                    $birthDate = $this->connection->quote($set['birthday']->format('m/d/Y'), Type::STRING);
                    $sex = $this->connection->quote($set['sex'], Type::STRING);
                    $conditions[] = <<<SQL
(
    `csd`.`first_name` = vh_encrypt({$firstName})
    AND `csd`.`last_name` = vh_encrypt({$lastName})
    AND `csd`.`address1` = vh_encrypt({$address1})
    AND `csd`.`city_id` = {$city->getId()}
    AND `csd`.`region_id` = {$city->getRegionId()}
    AND `c`.`gender` = vh_encrypt({$sex})
    AND `c`.`birthday` = vh_encrypt({$birthDate})
    AND `{$insurance}_numbers`.`number` = {$number}
)
SQL;
                }
                if (count($conditions) > 0) {
                    $numbersTable = 'SELECT NULL AS `number` UNION SELECT ' . implode(' UNION SELECT ', $numbers);
                    $conditionString = \implode(' OR ', $conditions);
                    $sql = <<<SQL
SELECT DISTINCT `{$insurance}_numbers`.`number`, `c`.`user_id`
FROM ({$numbersTable}) `{$insurance}_numbers`
JOIN `client` `c`
JOIN `client_state_data` `csd` ON `c`.`user_id` = `csd`.`client_id`
WHERE {$conditionString}
SQL;
                    $map = $this
                        ->connection
                        ->executeQuery($sql)
                        ->fetchAll(\PDO::FETCH_KEY_PAIR);
                    foreach ($map as $number => $clientId) {
                        $this->addRelationToClient(
                            $numberToKey[$number],
                            $clientId,
                            self::CRITERIA_TYPE_STATE_DATA_PERSON_INFO
                        );
                    }
                }
            }
        }
    }

    /**
     * Criteria 3
     *     "Alternate ID" in file == "Medicaid_Sequential_Member_Number" OR "Medicare_Sequential_Member_Number"
     * @throws \Doctrine\DBAL\DBALException
     */
    private function findClientIdsByCriteria3()
    {
        // @todo: Move queries to separated Query-classes
        if (!empty($this->alternateMemberNumbers)) {
            $inConditionValuesString = $this->getInConditionValuesString($this->alternateMemberNumbers);
            $sql = <<<SQL
(
    SELECT `medicaid_sequential_member_number`, `client_id`
    FROM `client_insurance_medicaid`
    WHERE `medicaid_sequential_member_number` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `medicaid_sequential_member_number`, `client_id`
    FROM `client_insurance_medicaid_future`
    WHERE `medicaid_sequential_member_number` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `medicare_sequential_member_number`, `client_id`
    FROM `client_insurance_medicare`
    WHERE `medicare_sequential_member_number` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `medicare_sequential_member_number`, `client_id`
    FROM `client_insurance_medicare_future`
    WHERE `medicare_sequential_member_number` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `sequential_member_number`, `client_id`
    FROM `enrollment_history`
    WHERE `sequential_member_number` IN ({$inConditionValuesString})
)
SQL;
            $map = $this
                ->connection
                ->executeQuery($sql)
                ->fetchAll(\PDO::FETCH_KEY_PAIR);
            if ($map !== false) {
                foreach ($map as $alternateMemberNumber => $clientId) {
                    $key = array_search(
                        $alternateMemberNumber,
                        $this->alternateMemberNumbers
                    );
                    $this->addRelationToClient($key, $clientId, self::CRITERIA_TYPE_ALTERNATE_ID);
                }
            }
        }
    }

    /**
     * Criteria 4
     * "Subscriber Number" in file == Medicaid block / BH Subscriber ID
     *     OR
     *     ( ("Medicare ID" OR "Medicaid ID") in file == ("Medicare Number" OR "Medicaid Number") in VH
     *     AND
     *     "First Name" in file == "First Name" in VH
     *     AND
     *     "Last Name" in file == "Last Name" in VH )
     *     AND
     *     "Plan Code" in file == "ZBH-BH"
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function findClientIdsByCriteria4()
    {
        // @todo: Move queries to separated Query-classes
        if (!empty($this->subscriberIds)) {
            $inConditionValuesString = $this->getInConditionValuesString($this->subscriberIds);
            $sql = <<<SQL
(
    SELECT `bh_subscriber_id`, `client_id`
    FROM `client_insurance_medicaid`
    WHERE `bh_subscriber_id` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `bh_subscriber_id`, `client_id`
    FROM `client_insurance_medicaid_future`
    WHERE `bh_subscriber_id` IN ({$inConditionValuesString})
) UNION DISTINCT (
    SELECT `bh_subscriber_id`, `client_id`
    FROM `enrollment_history`
    WHERE `bh_subscriber_id` IN ({$inConditionValuesString})
)
SQL;
            $map = $this
                ->connection
                ->executeQuery($sql)
                ->fetchAll(\PDO::FETCH_KEY_PAIR);
            if ($map !== false) {
                foreach ($map as $subscriberId => $clientId) {
                    $key = array_search($subscriberId, $this->subscriberIds);
                    $this->addRelationToClient(
                        $key,
                        $clientId,
                        self::CRITERIA_TYPE_SUBSCRIBER_NUMBER_ADVANCED
                    );
                }
            }
        }
        if (!empty($this->medicaidSet)) {
            $numberToKey = [];
            $conditions = [];
            foreach ($this->medicaidSet as $key => $set) {
                if (!$set['isBh']) {
                    continue;
                }
                $numberToKey[$set['number']] = $key;
                $number = $this->connection->quote($set['number'], Type::STRING);
                $firstName = $this->connection->quote($set['firstName'], Type::STRING);
                $lastName = $this->connection->quote($set['lastName'], Type::STRING);
                $conditions[] = <<<SQL
(
    (
        `m`.`number` = vh_encrypt({$number}) 
        OR `mf`.`number` = vh_encrypt({$number}) 
        OR `eh`.`id_number`  = vh_encrypt({$number})
    ) 
    AND `c`.`first_name` = vh_encrypt({$firstName}) 
    AND `c`.`last_name` = vh_encrypt({$lastName})
)
SQL;
            }
            if (count($conditions) > 0) {
                $conditionString = \implode(' OR ', $conditions);
                $sql = <<<SQL
SELECT DISTINCT vh_decrypt(IFNULL(`m`.`number`, IFNULL(`mf`.`number`, `eh`.`id_number`))), `c`.`user_id`
FROM `client` `c`
LEFT JOIN `client_insurance_medicaid` `m` ON `c`.`user_id` = `m`.`client_id`
LEFT JOIN `client_insurance_medicaid_future` `mf` ON `c`.`user_id` = `mf`.`client_id`
LEFT JOIN `enrollment_history` `eh` ON `c`.`user_id` = `eh`.`client_id`
WHERE {$conditionString}
SQL;
                $map = $this
                    ->connection
                    ->executeQuery($sql)
                    ->fetchAll(\PDO::FETCH_KEY_PAIR);
                foreach ($map as $number => $clientId) {
                    $this->addRelationToClient(
                        $numberToKey[$number],
                        $clientId,
                        self::CRITERIA_TYPE_SUBSCRIBER_NUMBER_ADVANCED
                    );
                }
            }
        }
        if (!empty($this->medicareSet)) {
            $numberToKey = [];
            $conditions = [];
            foreach ($this->medicareSet as $key => $set) {
                if (!$set['isBh']) {
                    continue;
                }
                $numberToKey[$set['number']] = $key;
                $number = $this->connection->quote($set['number'], Type::STRING);
                $firstName = $this->connection->quote($set['firstName'], Type::STRING);
                $lastName = $this->connection->quote($set['lastName'], Type::STRING);
                $conditions[] = <<<SQL
(
    (
        `m`.`number` = vh_encrypt({$number})
        OR `mf`.`number` = vh_encrypt({$number})
        OR `eh`.`id_number`  = vh_encrypt({$number})
    ) 
    AND `c`.`first_name` = vh_encrypt({$firstName}) 
    AND `c`.`last_name` = vh_encrypt({$lastName})
)
SQL;
            }
            if (count($conditions) > 0) {
                $conditionString = \implode(' OR ', $conditions);
                $sql = <<<SQL
SELECT DISTINCT vh_decrypt(IFNULL(`m`.`number`, IFNULL(`mf`.`number`, `eh`.`id_number`))), `c`.`user_id`
FROM `client` `c`
LEFT JOIN `client_insurance_medicare` `m` ON `c`.`user_id` = `m`.`client_id`
LEFT JOIN `client_insurance_medicare_future` `mf` ON `c`.`user_id` = `mf`.`client_id`
LEFT JOIN `enrollment_history` `eh` ON `c`.`user_id` = `eh`.`client_id`
WHERE {$conditionString}
SQL;
                $map = $this
                    ->connection
                    ->executeQuery($sql)
                    ->fetchAll(\PDO::FETCH_KEY_PAIR);
                foreach ($map as $number => $clientId) {
                    $this->addRelationToClient(
                        $numberToKey[$number],
                        $clientId,
                        self::CRITERIA_TYPE_SUBSCRIBER_NUMBER_ADVANCED
                    );
                }
            }
        }
    }

    /**
     * Criteria 5
     *     Temporary Client in VH ("Member Number" field value starts with a "T")
     *     AND
     *         ("Medicare ID" OR "Medicaid ID") in file == ("Medicare Number" OR "Medicaid Number") in VH
     *         OR
     *         ("First Name" AND "Last Name" AND "Date of Birth" AND "Address Line 1" in file ==
     *             "First Name" AND "Last Name" AND "Date of Birth" AND "Address Line 1" in VH)
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function findClientIdsByCriteria5()
    {
        // @todo: Move queries to separated Query-classes
        if (!empty($this->medicaidIds)) {
            $sql = <<<SQL
SELECT DISTINCT vh_decrypt(IFNULL(`m`.`number`, IFNULL(`mf`.`number`, `eh`.`id_number`))) AS `number`, `c`.`user_id`
FROM `client` `c`
LEFT JOIN `client_insurance_medicaid` `m` ON `c`.`user_id` = `m`.`client_id`
LEFT JOIN `client_insurance_medicaid_future` `mf` ON `c`.`user_id` = `mf`.`client_id`
LEFT JOIN `enrollment_history` `eh` ON `c`.`user_id` = `eh`.`client_id`
WHERE (
    vh_decrypt(`m`.`number`) IN (?)
    OR vh_decrypt(`mf`.`number`) IN (?)
    OR vh_decrypt(`eh`.`id_number`) IN (?)
) AND `c`.`id_medrec` LIKE 'T%'
SQL;
            $map = $this
                ->connection
                ->executeQuery(
                    $sql,
                    [$this->medicaidIds, $this->medicaidIds, $this->medicaidIds],
                    [Connection::PARAM_STR_ARRAY, Connection::PARAM_STR_ARRAY, Connection::PARAM_STR_ARRAY]
                )
                ->fetchAll(\PDO::FETCH_ASSOC);
            if ($map != false) {
                foreach ($map as $medicaid) {
                    $key = array_search($medicaid['number'], $this->medicaidIds);
                    $this->addRelationToClient($key, $medicaid['user_id'], self::CRITERIA_TYPE_TEMPORARY_CLIENT);
                }
            }
        }
        if (!empty($this->medicareIds)) {
            $sql = <<<SQL
SELECT DISTINCT vh_decrypt(IFNULL(`m`.`number`, IFNULL(`mf`.`number`, `eh`.`id_number`))) AS `number`, `c`.`user_id`
FROM `client` `c`
LEFT JOIN `client_insurance_medicare` `m` ON `c`.`user_id` = `m`.`client_id`
LEFT JOIN `client_insurance_medicare_future` `mf` ON `c`.`user_id` = `mf`.`client_id`
LEFT JOIN `enrollment_history` `eh` ON `c`.`user_id` = `eh`.`client_id`
WHERE (
    vh_decrypt(`m`.`number`) IN (?)
    OR vh_decrypt(`mf`.`number`) IN (?)
    OR vh_decrypt(`eh`.`id_number`) IN (?)
) AND `c`.`id_medrec` LIKE 'T%'
SQL;
            $map = $this
                ->connection
                ->executeQuery(
                    $sql,
                    [$this->medicareIds, $this->medicareIds, $this->medicareIds],
                    [Connection::PARAM_STR_ARRAY, Connection::PARAM_STR_ARRAY, Connection::PARAM_STR_ARRAY])
                ->fetchAll(\PDO::FETCH_ASSOC);
            if ($map != false) {
                foreach ($map as $medicare) {
                    $key = array_search($medicare['number'], $this->medicareIds);
                    $this->addRelationToClient($key, $medicare['user_id'], self::CRITERIA_TYPE_TEMPORARY_CLIENT);
                }
            }
        }
        // @todo: Implement code below via single query
        $sql = <<<SQL
SELECT DISTINCT vh_decrypt(IFNULL(`m`.`number`, IFNULL(`mf`.`number`, `eh`.`id_number`))) AS `number`, `c`.`user_id`
FROM `client` `c`
LEFT JOIN `client_insurance_medicaid` `m` ON `c`.`user_id` = `m`.`client_id`
LEFT JOIN `client_insurance_medicaid_future` `mf` ON `c`.`user_id` = `mf`.`client_id`
LEFT JOIN `enrollment_history` `eh` ON `c`.`user_id` = `eh`.`client_id`
WHERE `c`.`first_name` = vh_encrypt(?)
    AND `c`.`last_name` = vh_encrypt(?)
    AND `c`.`birthday` = vh_encrypt(?)
    AND `c`.`address1` = vh_encrypt(?)
    AND `c`.`id_medrec` LIKE 'T%'
SQL;
        foreach ($this->medicaidSet as $key => $item) {
            if (empty($item['birthday']) || empty($item['address1'])) {
                continue;
            }
            $result = $this
                ->connection
                ->executeQuery($sql, [$item['firstName'], $item['lastName'], $item['birthday']->format('m/d/Y'), $item['address1']])
                ->fetch(\PDO::FETCH_ASSOC);
            if ($result !== false) {
                $this->addRelationToClient($key, $result['user_id'], self::CRITERIA_TYPE_TEMPORARY_CLIENT);
            }
        }
        $sql = <<<SQL
SELECT DISTINCT vh_decrypt(IFNULL(`m`.`number`, IFNULL(`mf`.`number`, `eh`.`id_number`))) AS `number`, `c`.`user_id`
FROM `client` `c`
LEFT JOIN `client_insurance_medicare` `m` ON `c`.`user_id` = `m`.`client_id`
LEFT JOIN `client_insurance_medicare_future` `mf` ON `c`.`user_id` = `mf`.`client_id`
LEFT JOIN `enrollment_history` `eh` ON `c`.`user_id` = `eh`.`client_id`
WHERE `c`.`first_name` = vh_encrypt(?)
    AND `c`.`last_name` = vh_encrypt(?)
    AND `c`.`birthday` = vh_encrypt(?)
    AND `c`.`address1` = vh_encrypt(?)
    AND `c`.`id_medrec` LIKE 'T%'
SQL;
        foreach ($this->medicareSet as $key => $item) {
            if (empty($item['birthday']) || empty($item['address1'])) {
                continue;
            }
            $result = $this
                ->connection
                ->executeQuery($sql, [$item['firstName'], $item['lastName'], $item['birthday']->format('m/d/Y'), $item['address1']])
                ->fetch(\PDO::FETCH_ASSOC);
            if ($result !== false) {
                $this->addRelationToClient($key, $result['user_id'], self::CRITERIA_TYPE_TEMPORARY_CLIENT);
            }
        }
    }

    /**
     * @param RecordsCollection $recordsCollection
     */
    private function findPotentialMergeClientIds(RecordsCollection $recordsCollection)
    {
        foreach ($recordsCollection->getList() as $key => $record) {
            $criteriaType = $this->getCriteriaTypeByRecordKey($key);

            if (
                $criteriaType !== null
                || $record->getSubscriberFirstName() === null
                || $record->getSubscriberLastName() === null
                || $record->getHomeCity() === null
                || $record->getHomeState() === null
                || $record->getDateOfBirth() === null
                || $record->getHomeAddress1() === null
            ) {
                continue;
            }

            $clients = $this->potentialMergeClientsQuery->read(
                $record->getSubscriberFirstName(),
                $record->getHomeCity(),
                $record->getHomeState(),
                $record->getSubscriberLastName(),
                $record->getDateOfBirth()->format('m/d/Y'),
                $record->getHomeAddress1()
            );

            foreach ($clients as $client) {
                $this->addRelationToClient($key, $client->getUserId(), self::CRITERIA_TYPE_POTENTIAL_MERGE);
            }
        }
    }

    /**
     * @param int $recordKey
     * @param int $clientId
     * @param int $criteriaType
     */
    private function addRelationToClient($recordKey, $clientId, $criteriaType)
    {
        $this->clientIdsMap[$recordKey] = (int)$clientId;
        $this->criteriaTypes[$recordKey] = $criteriaType;
        unset(
            $this->notFoundKeys[$recordKey],
            $this->subscriberIds[$recordKey],
            $this->medicaidIds[$recordKey],
            $this->medicaidSet[$recordKey],
            $this->medicareIds[$recordKey],
            $this->medicareSet[$recordKey],
            $this->alternateMemberNumbers[$recordKey]
        );
    }

    /**
     * @param RecordsCollection $recordsCollection
     * @throws \Doctrine\DBAL\DBALException
     */
    private function findRace(RecordsCollection $recordsCollection)
    {
        foreach ($this->clientIdsMap as $key => $clientId) {
            $record = $recordsCollection->getList()[$key];
            if (empty($record->getRace())) {
                continue;
            }
            $sql = <<<SQL
SELECT `id`
FROM `client_race`
WHERE `client_id` = :clientId AND `race_name` = :raceName
SQL;
            $id = $this->connection
                ->executeQuery(
                    $sql,
                    [
                        ':clientId' => $clientId,
                        ':raceName' => $record->getRace(),
                    ]
                )
                ->fetch(\PDO::FETCH_COLUMN);
            if ($id !== false) {
                $this->clientRaceIdsMap[$key] = $id;
            }
        }
    }

    /**
     * @param RecordsCollection $recordsCollection
     * @param AdapterParseLog $parseLog
     * @throws \Doctrine\DBAL\DBALException
     */
    private function validateSequentialNumbers(RecordsCollection $recordsCollection, AdapterParseLog $parseLog)
    {
        $baseValidator = new BaseValidator();
        if (!empty($this->sequentialNumbers)) {
            $sql = <<<SQL
SELECT `medicare_sequential_member_number`, `client_id`
FROM `client_insurance_medicare`
WHERE `medicare_sequential_member_number` IN (?)
SQL;
            $medicareMap = $this
                ->connection
                ->executeQuery($sql, [$this->sequentialNumbers], [Connection::PARAM_STR_ARRAY])
                ->fetchAll(\PDO::FETCH_KEY_PAIR);
            $sql = <<<SQL
SELECT `medicare_sequential_member_number`, `client_id`
FROM `client_insurance_medicare_future`
WHERE `medicare_sequential_member_number` IN (?)
SQL;
            $medicareFutureMap = $this
                ->connection
                ->executeQuery($sql, [$this->sequentialNumbers], [Connection::PARAM_STR_ARRAY])
                ->fetchAll(\PDO::FETCH_KEY_PAIR);
            $sql = <<<SQL
SELECT `medicaid_sequential_member_number`, `client_id`
FROM `client_insurance_medicaid`
WHERE `medicaid_sequential_member_number` IN (?)
SQL;
            $medicaidMap = $this
                ->connection
                ->executeQuery($sql, [$this->sequentialNumbers], [Connection::PARAM_STR_ARRAY])
                ->fetchAll(\PDO::FETCH_KEY_PAIR);
            $sql = <<<SQL
SELECT `medicaid_sequential_member_number`, `client_id`
FROM `client_insurance_medicaid_future`
WHERE `medicaid_sequential_member_number` IN (?)
SQL;
            $medicaidFutureMap = $this
                ->connection
                ->executeQuery($sql, [$this->sequentialNumbers], [Connection::PARAM_STR_ARRAY])
                ->fetchAll(\PDO::FETCH_KEY_PAIR);
            $seqMemberNumbersCount = [];
            foreach ($this->sequentialNumbers as $key => $number) {
                $clientId = $this->getClientId($key);
                $hasError = false;
                if (isset($medicareMap[$number]) && (int) $medicareMap[$number] !== (int) $clientId) {
                    $hasError = true;
                }
                if (isset($medicareFutureMap[$number]) && (int) $medicareFutureMap[$number] !== (int) $clientId) {
                    $hasError = true;
                }
                if (isset($medicaidMap[$number]) && (int) $medicaidMap[$number] !== (int) $clientId) {
                    $hasError = true;
                }
                if (isset($medicaidFutureMap[$number]) && (int) $medicaidFutureMap[$number] !== (int) $clientId) {
                    $hasError = true;
                }
                if ($clientId === null) {
                    if (!isset($seqMemberNumbersCount[$number])) {
                        $seqMemberNumbersCount[$number] = 0;
                    }
                    $seqMemberNumbersCount[$number]++;
                    if ($seqMemberNumbersCount[$number] > 1) {
                        $hasError = true;
                    }
                }

                if ($hasError) {
                    $record = $recordsCollection->getList()[$key];
                    $parseLog->addError(
                        $record->getProcessedLine(),
                        $record->getSubscriberId(),
                        sprintf(
                            'Record %s could not be saved: Another client has same Sequential Member Number',
                            $baseValidator->buildSubscriberName($record, ' for "', '"')
                        ),
                        true
                    );
                    $recordsCollection->delete($key);
                    unset(
                        $this->notFoundKeys[$key],
                        $this->clientIdsMap[$key]
                    );
                }
            }
        }
    }

    /**
     * @param array $values
     * @param string $type
     * @return string
     */
    private function getInConditionValuesString(array $values, $type = Type::STRING)
    {
        $quotedValues = [];
        foreach ($values as $value) {
            $quotedValues[] = $this->connection->quote($value, $type);
        }
        return \implode(',', $quotedValues);
    }
}
