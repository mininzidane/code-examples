<?php
/**
 * LanguageMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use VirtualHealth\OrmBundle\Entity\Language;
use VirtualHealth\OrmBundle\Repository\LanguageRepository;

/**
 * Class LanguageMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class LanguagesMap
{
    /**
     * @var string[]
     */
    private $relations = [
        'cmn' => 'man',
        'zho' => 'can',
        'hat' => 'cre',
        'khm' => 'cam',
        'pan' => 'pun',
        'chi' => 'chi',
    ];

    /**
     * @var LanguageRepository
     */
    private $repository;

    /**
     * @var Language[]
     */
    private $map = [];

    /**
     * LanguagesMap constructor.
     * @param LanguageRepository $repository
     */
    public function __construct(LanguageRepository $repository)
    {
        $this->repository = $repository;
        $entities = $this->repository->findAll();
        foreach ($entities as $entity) {
            $isoCode = strtolower($entity->getIso6393());
            if (isset($this->relations[$isoCode])) {
                $this->map[$this->relations[$isoCode]] = $entity;
            } else {
                $code = $entity->getNisoZ39();
                if ($code === null) {
                    continue;
                }
                $this->map[strtolower($code)] = $entity;
            }
        }
    }

    /**
     * @param string $code
     * @return Language|null
     */
    public function getByCode($code)
    {
        if ($code === null) {
            return null;
        }
        $code = strtolower($code);
        return isset($this->map[$code]) ? $this->map[$code] : null;
    }
}
