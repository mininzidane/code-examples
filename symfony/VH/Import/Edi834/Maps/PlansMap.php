<?php
/**
 * PlansMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use VirtualHealth\OrmBundle\Entity\Plan;
use VirtualHealth\OrmBundle\Repository\PlanRepository;

/**
 * Class PlansMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class PlansMap
{
    /**
     * @var PlanRepository
     */
    private $repository;

    /**
     * @var Plan[]
     */
    private $planIdMap = [];

    /**
     * PlansMap constructor.
     * @param PlanRepository $repository
     */
    public function __construct(PlanRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $planId
     * @return Plan|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getByPlanId($planId)
    {
        if (!isset($this->planIdMap[$planId])) {
            $qb = $this->repository->createQueryBuilder('p');
            $qb
                ->select(['p', 'r', 'l', 'lr'])
                ->leftJoin('p.region', 'r')
                ->leftJoin('p.lob', 'l')
                ->leftJoin('l.regions', 'lr')
                ->orderBy('p.isDeleted', 'ASC')
                ->where('p.planId = :planId')
                ->setParameter(':planId', $planId)
                ->setMaxResults(1);
            $entity = $qb
                ->getQuery()
                ->getOneOrNullResult();
            $this->planIdMap[$planId] = $entity !== null ? $entity : false;
        }
        return $this->planIdMap[$planId] !== false ? $this->planIdMap[$planId] : null;
    }
}
