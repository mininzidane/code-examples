<?php
/**
 * PlanCohortsMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use VirtualHealth\Import\Membership\Edi834\Query\GetPlansCohortIds;

/**
 * Class PlanCohortsMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class PlanCohortsMap
{
    /**
     * @var GetPlansCohortIds
     */
    private $getPlansCohortIdsQuery;

    /**
     * @var int[][]
     */
    private $map = [];

    /**
     * PlanCohortsMap constructor.
     * @param GetPlansCohortIds $query
     */
    public function __construct(GetPlansCohortIds $query)
    {
        $this->getPlansCohortIdsQuery = $query;
    }

    /**
     * @param string $planId
     * @return int[]
     */
    public function getCohorts(string $planId): array
    {
        if (empty($this->map)) {
            $this->map = $this->getPlansCohortIdsQuery->read();
        }
        return $this->map[$planId] ?? [];
    }
}
