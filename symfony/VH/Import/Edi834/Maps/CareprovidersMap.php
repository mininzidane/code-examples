<?php
/**
 * CareproviderMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use VirtualHealth\Import\Membership\Edi834\Collections\RecordsCollection;
use VirtualHealth\Import\Membership\Edi834\Record;
use VirtualHealth\OrmBundle\Entity\Careprovider;
use VirtualHealth\OrmBundle\Query\Careprovider\FindCareprovidersByPfinIds;
use VirtualHealth\OrmBundle\Repository\CareproviderRepository;

/**
 * Class CareproviderMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class CareprovidersMap
{
    /**
     * @var CareproviderRepository
     */
    private $repository;

    /**
     * @var FindCareprovidersByPfinIds
     */
    private $findCareprovidersByPfinIdsQuery;

    /**
     * @var Careprovider[]
     */
    private $idMap = [];

    /**
     * @var Careprovider[]
     */
    private $npiMap = [];

    /**
     * CareprovidersMap constructor.
     * @param CareproviderRepository $repository
     * @param FindCareprovidersByPfinIds $findCareprovidersByPfinIdsQuery
     */
    public function __construct(
        CareproviderRepository $repository,
        FindCareprovidersByPfinIds $findCareprovidersByPfinIdsQuery
    ) {
        $this->repository = $repository;
        $this->findCareprovidersByPfinIdsQuery = $findCareprovidersByPfinIdsQuery;
    }

    /**
     * @param RecordsCollection $recordsCollection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function fillForRecordsCollection(RecordsCollection $recordsCollection)
    {
        $pfinIds = [];
        $npis = [];
        foreach ($recordsCollection->getList() as $key => $record) {
            if (
                $record->hasProviderId()
                && !isset($this->idMap[$record->getProviderId()])
                && !isset($pfinIds[$record->getProviderId()])
            ) {
                $pfinIds[$record->getProviderId()] = $record->getProviderId();
            }
            if (
                $record->hasProviderNpi()
                && !isset($this->npiMap[$record->getProviderNpi()])
                && !isset($npis[$record->getProviderNpi()])
            ) {
                $npis[$record->getProviderNpi()] = $record->getProviderNpi();
            }
        }
        $entities = $this->findCareprovidersByPfinIdsQuery
            ->read($pfinIds);
        foreach ($entities as $pfinId => $entity) {
            $this->idMap[$pfinId] = $entity;
            $this->npiMap[$entity->getNpi()] = $entity;
            unset($pfinIds[$pfinId], $npis[$entity->getNpi()]);
        }
        $entities = $this->repository->findBy(['npi' => $npis]);
        foreach ($entities as $entity) {
            $this->npiMap[$entity->getNpi()] = $entity;
            unset($npis[$entity->getNpi()]);
        }
        foreach ($pfinIds as $pfinId) {
            $this->idMap[$pfinId] = false;
        }
        foreach ($npis as $npi) {
            $this->npiMap[$npi] = false;
        }
    }

    /**
     * @param Record $record
     * @return Careprovider|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getByRecord(Record $record)
    {
        if ($record->hasProviderId()) {
            if (!isset($this->idMap[$record->getProviderId()])) {
                $entities = $this->findCareprovidersByPfinIdsQuery
                    ->read([$record->getProviderId()]);
                if (empty($entities)) {
                    $this->idMap[$record->getProviderId()] = false;
                } else {
                    foreach ($entities as $pfinId => $entity) {
                        $this->idMap[$pfinId] = $entity;
                        $this->npiMap[$entity->getNpi()] = $entity;
                    }
                }
            }
            return $this->idMap[$record->getProviderId()] !== false
                ? $this->idMap[$record->getProviderId()]
                : null;
        }
        if ($record->hasProviderNpi()) {
            if (!isset($this->npiMap[$record->getProviderNpi()])) {
                $entity = $this->repository->findOneBy(['npi' => $record->getProviderNpi()]);
                $this->npiMap[$record->getProviderNpi()] = $entity;
            }
            return $this->npiMap[$record->getProviderNpi()] !== false
                ? $this->npiMap[$record->getProviderNpi()]
                : null;
        }
        return null;
    }
}
