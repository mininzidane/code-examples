<?php
/**
 * CountiesMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use VirtualHealth\OrmBundle\Entity\County;
use VirtualHealth\OrmBundle\Repository\CountyRepository;

/**
 * Class CountiesMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class CountiesMap
{
    /**
     * @var CountyRepository
     */
    private $repository;

    /**
     * @var County[]
     */
    private $map = [];

    /**
     * CountiesMap constructor.
     * @param CountyRepository $repository
     */
    public function __construct(CountyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $regionId
     * @param string $title
     * @return County|null
     */
    public function getCounty($regionId, $title)
    {
        $key = $regionId . ':' . strtolower($title);
        if (!isset($this->map[$key])) {
            $entity = $this->repository->findOneBy(['region' => $regionId, 'title' => $title]);
            $this->map[$key] = $entity !== null ? $entity : false;
        }
        return $this->map[$key] !== false ? $this->map[$key] : null;
    }
}
