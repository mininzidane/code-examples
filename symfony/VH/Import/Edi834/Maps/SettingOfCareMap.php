<?php
/**
 * SettingOfCareMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use VirtualHealth\OrmBundle\Repository\SettingOfCareRepository;

/**
 * Class SettingOfCareMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class SettingOfCareMap
{
    /**
     * @var SettingOfCareRepository
     */
    private $settingOfCareRepository;

    /**
     * @var array
     */
    private $map;

    /**
     * SettingOfCareMap constructor.
     * @param SettingOfCareRepository $settingOfCareRepository
     */
    public function __construct(SettingOfCareRepository $settingOfCareRepository)
    {
        $this->settingOfCareRepository = $settingOfCareRepository;
    }

    /**
     * @param string|null $name
     * @return int|null
     */
    public function getIdByName(?string $name): ?int
    {
        if ($this->map === null) {
            foreach ($this->settingOfCareRepository->findAll() as $settingOfCare) {
                $this->map[$settingOfCare->getName()] = $settingOfCare->getId();
            }
        }

        return $this->map[$name] ?? null;
    }
}
