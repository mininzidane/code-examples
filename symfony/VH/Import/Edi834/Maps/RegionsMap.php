<?php
/**
 * RegionsMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use Doctrine\DBAL\Connection;
use VirtualHealth\OrmBundle\Entity\Region;
use VirtualHealth\OrmBundle\Repository\RegionRepository;

/**
 * Class RegionsMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class RegionsMap
{
    public const USA_COUNTRY_TITLE = 'United States';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var RegionRepository
     */
    private $regionRepository;

    /**
     * @var int
     */
    private $usaCountryId;

    /**
     * @var array
     */
    private $titlesMap = [];

    /**
     * @var array
     */
    private $idsMap = [];

    /**
     * @var Region[]
     */
    private $regionsByTitle = [];

    /**
     * RegionsMap constructor.
     * @param Connection $connection
     * @param RegionRepository $regionRepository
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct(Connection $connection, RegionRepository $regionRepository)
    {
        $this->connection = $connection;
        $this->regionRepository = $regionRepository;
        $this->usaCountryId = (int) $connection
            ->createQueryBuilder()
            ->select(['id'])
            ->from('country')
            ->where('title = :title')
            ->setParameter(':title', self::USA_COUNTRY_TITLE)
            ->execute()
            ->fetchColumn();

        $regions = $this
            ->regionRepository
            ->findBy(['country' => $this->usaCountryId]);

        foreach ($regions as $region) {
            $this->titlesMap[mb_strtolower($region->getCode())] = $region->getTitle();
            $this->idsMap[$region->getId()] = $region->getTitle();
            $this->regionsByTitle[mb_strtolower($region->getTitle())] = $region;
        }
    }

    /**
     * @param string $code
     * @return null|string
     */
    public function getStateTitleByCode(string $code): ?string
    {
        return $this->titlesMap[mb_strtolower($code)] ?? null;
    }

    /**
     * @param null|string $title
     * @return null|string
     */
    public function getStateCodeByTitle(?string $title): ?string
    {
        $code = array_search($title, $this->titlesMap);
        return $code !== false ? $code : null;
    }

    /**
     * @param string $title
     *
     * @return null|Region
     */
    public function getStateByTitle(string $title): ?Region
    {
        return $this->regionsByTitle[mb_strtolower($title)] ?? null;
    }

    /**
     * @return array
     */
    public function getIdsMap()
    {
        return $this->idsMap;
    }

    /**
     * @param string $regionTitle
     * @return int|null
     */
    public function getIdByTitle(string $regionTitle): ?int
    {
        $key = array_search($regionTitle, $this->idsMap);
        return $key !== false ? (int) $key : null;
    }

    /**
     * @return int
     */
    public function getUsaCountryId()
    {
        return $this->usaCountryId;
    }
}
