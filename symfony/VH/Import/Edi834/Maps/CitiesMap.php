<?php
/**
 * CitiesMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use Doctrine\ORM\Query\Expr\Join;
use VirtualHealth\OrmBundle\Entity\City;
use VirtualHealth\OrmBundle\Entity\PostalCode;
use VirtualHealth\OrmBundle\Repository\CityRepository;

/**
 * Class CitiesMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class CitiesMap
{
    /**
     * @var CityRepository
     */
    private $repository;

    /**
     * @var RegionsMap
     */
    private $regionsMap;

    /**
     * @var array
     */
    private $loadedRegions = [];

    /**
     * @var City[]
     */
    private $map = [];

    /**
     * @var City[]
     */
    private $idsMap = [];

    /**
     * @var array[]
     */
    private $byZipCodeMap = [];

    /**
     * @var array[]
     */
    private $byZipCodeStateMap = [];

    /**
     * @var City[]
     */
    private $byTitleMap = [];

    /**
     * CitiesMap constructor.
     *
     * @param CityRepository $repository
     * @param RegionsMap $regionsMap
     */
    public function __construct(CityRepository $repository, RegionsMap $regionsMap)
    {
        $this->repository = $repository;
        $this->regionsMap = $regionsMap;
    }

    /**
     * @param $id
     *
     * @return bool|City
     */
    public function getCityById($id)
    {
        if (!isset($this->idsMap[$id])) {
            $model = $id !== null ? $this->repository->find($id) : null;
            $this->idsMap[$id] = $model ?? false;
        }
        return $this->idsMap[$id] !== false ? $this->idsMap[$id] : null;
    }

    /**
     * @param string $cityName
     * @param string $stateTitle
     *
     * @return City|null
     */
    public function getCity($cityName, $stateTitle)
    {
        $this->fillForRegion($stateTitle);
        $key = mb_strtolower($cityName . ':' . $stateTitle);
        if (!isset($this->map[$key])) {
            /** @var City[] $results */
            $results = $this->repository
                ->createQueryBuilder('c')
                ->innerJoin('c.region', 'r')
                ->where('c.title = :title AND r.title = :regionTitle AND r.country = :usaCountryId')
                ->setParameters([
                    ':title' => $cityName,
                    ':regionTitle' => $stateTitle,
                    ':usaCountryId' => $this->regionsMap->getUsaCountryId()
                ])
                ->getQuery()
                ->getResult();
            if (\count($results) === 1) {
                $this->map[$key] = $results[0];
                $this->idsMap[$results[0]->getId()] = $results[0];
            } else {
                $this->map[$key] = false;
            }
        }
        return $this->map[$key] !== false ? $this->map[$key] : null;
    }

    /**
     * @param string $zipCode
     *
     * @return array
     */
    public function getCityIdsByZipCode(string $zipCode): array
    {
        if (!isset($this->byZipCodeMap[$zipCode])) {
            $this->byZipCodeMap[$zipCode] = [];
            /** @var City[] $cities */
            $cities = $this->repository
                ->createQueryBuilder('c')
                ->join('c.postalCodes', 'postalCodes', Join::INNER_JOIN)
                ->where('postalCodes.code = :code')
                ->setParameter(':code', $zipCode)
                ->getQuery()
                ->getResult();
            foreach ($cities as $city) {
                $this->fillForRegion($city->getRegion()->getTitle());
            }
        }
        return !empty($this->byZipCodeMap[$zipCode]) ? $this->byZipCodeMap[$zipCode] : [];
    }

    /**
     * @param string $zipCode
     * @param string $stateTitle
     *
     * @return array
     */
    public function getCityIdsByZipCodeAndStateTitle(string $zipCode, string $stateTitle): array
    {
        $this->fillForRegion($stateTitle);
        $key = mb_strtolower("$zipCode:$stateTitle");
        return !empty($this->byZipCodeStateMap[$key]) ? $this->byZipCodeStateMap[$key] : [];
    }

    /**
     * @param string $cityTitle
     * @param string $stateTitle
     *
     * @return bool
     */
    public function isCorrectCity(string $cityTitle, string $stateTitle): bool
    {
        // Without state we can't check city.
        if (empty($stateTitle)) {
            return false;
        }

        return null !== $this->getCity($cityTitle, $stateTitle);
    }

    /**
     * @param $regionTitle
     */
    private function fillForRegion($regionTitle): void
    {
        $regionTitleLower = mb_strtolower($regionTitle);
        if (
            !isset($this->loadedRegions[$regionTitleLower])
            && null !== ($regionId = $this->regionsMap->getIdByTitle($regionTitle ?? ''))
        ) {
            /** @var City[] $cities */
            $cities = $this->repository
                ->createQueryBuilder('c')
                ->addSelect('postalCodes')
                ->join('c.postalCodes', 'postalCodes', Join::WITH)
                ->where('c.region = :id')
                ->setParameter(':id', $regionId)
                ->getQuery()
                ->getResult();

            foreach ($cities as $city) {
                $cityId = $city->getId();
                $cityTitle = mb_strtolower($city->getTitle());
                $regionId = $city->getRegionId();
                $regionTitle = $this->regionsMap->getIdsMap()[$regionId];

                if (!isset($this->idsMap[$cityId])) {
                    $key = mb_strtolower($cityTitle . ':' . $regionTitle);
                    $this->map[$key] = $city;
                    $this->idsMap[$cityId] = $city;
                }

                if (!isset($this->byTitleMap[$cityTitle])) {
                    $this->byTitleMap[$cityTitle] = $city;
                }

                /** @var PostalCode[] $codes */
                $codes = $city->getPostalCodes();
                foreach ($codes as $code) {
                    $zipCode = $code->getCode();
                    $this->byZipCodeMap[$zipCode][$cityId] = $cityId;

                    $key = mb_strtolower("$zipCode:$regionTitle");
                    $this->byZipCodeStateMap[$key][$cityId] = $cityId;
                }
            }
            $this->loadedRegions[$regionTitleLower] = true;
        }
    }

    /**
     * @param string $subscriberId
     */
    public function dump(string $subscriberId)
    {
        try {
            $this->dumpMap($this->map, 'Map', "SubscriberId: " . $subscriberId . "\n");
            $this->dumpMap($this->byTitleMap, 'ByTitleMap');
            $this->dumpMap($this->byZipCodeMap, 'ByZipCodeMap');
            $this->dumpMap($this->byZipCodeStateMap, 'ByZipCodeStateMap', null, "==================================\n\n\n");
        } catch (\Throwable $e) {
            // do nothing
        }
    }

    /**
     * @param array $a
     * @param string $t
     * @param string|null $prepend
     * @param string|null $append
     */
    private function dumpMap(array $a, string $t, ?string $prepend = null, ?string $append = null): void
    {
        $filePath = VH_RUNTIME_DIR . DS . 'CitiesMap.log';
        if ($prepend !== null) {
            $dump = $prepend;
        } else {
            $dump = "";
        }
        $dump .= "{$t}:\n";
        foreach ($a as $key => $value) {
            if ($value instanceof City) {
                $val = $value->getId();
            } else {
                $val = is_array($value) ? implode(':', $value) : 'false';
            }
            $dump .= "\t{$key} => {$val}\n";
        }
        $dump .= "\n";
        if ($append !== null) {
            $dump .= $append;
        }
        file_put_contents($filePath, $dump, FILE_APPEND);
    }
}
