<?php
/**
 * TocCodesMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

/**
 * Class TocsMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class TocCodesMap
{
    /**
     * @var string[]
     */
    private $map = [
        28 => 'Blue Cross/Blue Shield',
        15 => 'Harmony Health Plan',
        24 => 'Illinicare Health Plan',
        19 => 'Meridian Health Plan',
        25 => 'Molina',
        56 => 'Cook County',
        59 => 'NextLevel',
        23 => 'Aetna Better Health',
        52 => 'Family Health Network',
        57 => 'Community Care Alliance of Illinois',
    ];

    /**
     * @param $code
     * @return string|null
     */
    public function getNameByCode($code)
    {
        return $this->map[$code] ?? null;
    }
}
