<?php
/**
 * SpecialNeedsMap class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Maps;

use VirtualHealth\OrmBundle\Entity\SpecialNeeds;
use VirtualHealth\OrmBundle\Repository\SpecialNeedsRepository;

/**
 * Class SpecialNeedsMap
 * @package VirtualHealth\Import\Membership\Edi834\Maps
 */
class SpecialNeedsMap
{
    /**
     * @var SpecialNeedsRepository
     */
    private $specialNeedsRepository;

    /**
     * @var SpecialNeeds[]
     */
    private $specialNeeds;

    /**
     * @var SpecialNeeds[]
     */
    private $codesMap;

    /**
     * @var SpecialNeeds[]
     */
    private $namesMap;

    /**
     * SpecialNeedsMap constructor.
     * @param SpecialNeedsRepository $specialNeedsRepository
     */
    public function __construct(SpecialNeedsRepository $specialNeedsRepository)
    {
        $this->specialNeedsRepository = $specialNeedsRepository;
    }

    /**
     * @return SpecialNeeds[]
     */
    private function getAll(): array
    {
        if ($this->specialNeeds === null) {
            $this->specialNeeds = $this->specialNeedsRepository->findAll();
        }

        return $this->specialNeeds;
    }

    /**
     * @param string $code
     * @return null|SpecialNeeds
     */
    public function getByCode(string $code): ?SpecialNeeds
    {
        if ($this->codesMap === null) {
            foreach ($this->getAll() as $model) {
                $this->codesMap[$model->getCode()] = $model;
            }
        }
        return $this->codesMap[$code] ?? null;
    }

    /**
     * @param string $name
     * @return SpecialNeeds|null
     */
    public function getByName(string $name): ?SpecialNeeds
    {
        if ($this->namesMap === null) {
            foreach ($this->getAll() as $model) {
                $this->namesMap[$model->getName()] = $model;
            }
        }

        return $this->namesMap[$name] ?? null;
    }

    /**
     * @param string $value
     * @return SpecialNeeds|null
     */
    public function getByCodeOrName(string $value): ?SpecialNeeds
    {
        return $this->getByCode($value) ?: $this->getByName($value);
    }
}
