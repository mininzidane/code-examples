<?php

namespace VirtualHealth\Import\Membership\Edi834\EventListener;

use Psr\Log\LoggerInterface;
use VirtualHealth\Import\Membership\Edi834\Event\PotentialMemberFoundEvent;
use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Entity\Department;
use VirtualHealth\OrmBundle\Entity\MembershipFileMergeLog;
use VirtualHealth\OrmBundle\Entity\Task;
use VirtualHealth\OrmBundle\Entity\TaskRelatedSource;
use VirtualHealth\OrmBundle\Entity\TaskRelation;
use VirtualHealth\OrmBundle\Entity\TaskTemplates;
use VirtualHealth\OrmBundle\Entity\User;
use VirtualHealth\OrmBundle\Repository\TaskTemplatesRepository;
use VirtualHealth\OrmBundle\Repository\DepartmentRepository;
use VirtualHealth\OrmBundle\Repository\MembershipFileMergeLogRepository;
use VirtualHealth\OrmBundle\Repository\MembershipFileRepository;
use VirtualHealth\OrmBundle\Repository\TaskRepository;
use VirtualHealth\OrmBundle\Repository\TaskRelatedSourceRepository;
use VirtualHealth\OrmBundle\Saver\Task\TaskSaver;
use VirtualHealth\User\CommonUsersGetter;

class PotentialMemberFoundEventListener
{
    const TEMPORARY_MEMBER_TASK_DESCRIPTION = 'Review potential member record merge.';

    /** @var LoggerInterface */
    private $logger;
    /** @var MembershipFileMergeLogRepository */
    private $membershipFileMergeLogRepository;
    /** @var TaskRepository */
    private $taskRepository;
    /** @var MembershipFileRepository */
    private $membershipFileRepository;
    /** @var CommonUsersGetter */
    private $commonUser;
    /** @var DepartmentRepository */
    private $departmentRepository;
    /**
     * @var \VirtualHealth\OrmBundle\Repository\TaskTemplatesRepository
     */
    private $taskTemplatesRepository;
    /** @var TaskSaver */
    private $taskSaver;
    /** @var TaskRelatedSourceRepository */
    private $taskRelatedSourceRepository;

    public function __construct(
        LoggerInterface $logger,
        MembershipFileMergeLogRepository $membershipFileMergeLogRepository,
        TaskRepository $taskRepository,
        MembershipFileRepository $membershipFileRepository,
        CommonUsersGetter $commonUsers,
        DepartmentRepository $departmentRepository,
        TaskTemplatesRepository $taskTemplatesRepository,
        TaskSaver $taskSaver,
        TaskRelatedSourceRepository $taskRelatedSourceRepository
    )
    {
        $this->logger = $logger;
        $this->membershipFileMergeLogRepository = $membershipFileMergeLogRepository;
        $this->taskRepository = $taskRepository;
        $this->membershipFileRepository = $membershipFileRepository;
        $this->commonUser = $commonUsers;
        $this->departmentRepository = $departmentRepository;
        $this->taskTemplatesRepository = $taskTemplatesRepository;
        $this->taskSaver = $taskSaver;
        $this->taskRelatedSourceRepository = $taskRelatedSourceRepository;
    }

    public function onMemberFound(PotentialMemberFoundEvent $event)
    {
        try {
            // save membership file
            $log = new MembershipFileMergeLog();
            $log->setType(MembershipFileMergeLog::TYPE_POTENTIAL_TEMPORARY_MEMBER);
            $log->setMembershipFile(null);
            $log->setClient($event->getClient());
            $log->setData(json_encode($event->getData()));
            $log->setMembershipFile($this->membershipFileRepository->getEntityReferenceById($event->getMembershipFileId()));
            $this->membershipFileMergeLogRepository->save($log);

            // save task
            $task = new Task();

            $taskType = $this->taskTemplatesRepository
                ->getActiveTaskTypeByName(TaskTemplates::LOCKED_TYPE_MEMBER_RECORD_MERGE);
            $task->setType($taskType);

            $task->setDuedate(new \DateTime());
            $task->setDescription(self::TEMPORARY_MEMBER_TASK_DESCRIPTION);

            $client = $this->taskRepository->getEntityReference(Client::class, $event->getClient()->getUserId());
            $task->setClient($client);

            $user = $this->taskRepository->getEntityReference(User::class, $this->commonUser->getSystemSuperAdmin()->getId());
            $task->setUser($user);

            $task->setPriority(Task::PRIORITY_HIGH);

            $department = $this->departmentRepository->findOneByNameNotDeleted(Department::NAME_MEMBERSHIP_REVIEW);
            $taskRelation = new TaskRelation();
            $taskRelation->setDepartment($department);
            $task->setRelation($taskRelation);

            if (!$this->taskSaver->saveOne($task)) {
                throw new \Exception(print_r($this->taskSaver->getErrors(), true));
            }

            // Create new Task Related Source to add "Merge Log" link
            $taskRelatedSource = new TaskRelatedSource();

            $taskId = $this->taskSaver->getSavedTaskId() !== 0
                ? $this->taskSaver->getSavedTaskId()
                : $this->taskSaver->getDuplicatedId();
            $taskEntity = $this->taskRepository->getEntityReference(Task::class, $taskId);
            $taskRelatedSource->setTask($taskEntity);
            $taskRelatedSource->setClient($client);
            $taskRelatedSource->setSourceId($event->getMembershipFileId());
            $taskRelatedSource->setSourceType('membershipfilemergelog');

            $this->taskRelatedSourceRepository->save($taskRelatedSource);

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}