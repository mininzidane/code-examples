<?php
/**
 * GetWaiverCodesAndIds class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Query;

use Doctrine\DBAL\Connection;

/**
 * Class GetWaiverCodesAndIds
 * @package VirtualHealth\Import\Membership\Edi834\Query
 */
class GetWaiverCodesAndIds
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * GetWaiverCodesAndIds constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return array
     */
    public function read(): array
    {
        return $this->connection
            ->createQueryBuilder()
            ->select(['code', 'id'])
            ->from('waiver')
            ->execute()
            ->fetchAll(\PDO::FETCH_KEY_PAIR);
    }
}
