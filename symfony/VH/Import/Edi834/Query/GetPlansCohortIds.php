<?php
/**
 * GetPlanCohortIds class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Query;

use Doctrine\DBAL\Connection;

/**
 * Class GetPlanCohortIds
 * @package VirtualHealth\Import\Membership\Edi834\Collections
 */
class GetPlansCohortIds
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * GetPlanCohortIds constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return int[][]
     */
    public function read(): array
    {
        $rows = $this
            ->connection
            ->createQueryBuilder()
            ->select(['p.plan_id', 'p.cohort_id'])
            ->from('plan', 'p')
            ->execute()
            ->fetchAll();
        $result = [];
        foreach ($rows as $row) {
            $planId = $row['plan_id'];
            if (!isset($result[$planId])) {
                $result[$planId] = [];
            }
            $result[$planId][] = (int) $row['cohort_id'];
        }
        return $result;
    }
}
