<?php
/**
 * DeleteClientSpecialNeedsByIds class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Query;

use Doctrine\DBAL\Connection;

/**
 * Class DeleteClientSpecialNeedsByIds
 * @package VirtualHealth\Import\Membership\Edi834\Query
 */
class DeleteClientSpecialNeedsByIds
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * DeleteClientSpecialNeedsByIds constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $ids
     * @throws \Doctrine\DBAL\DBALException
     */
    public function write(array $ids): void
    {
        if (empty($ids)) {
            return;
        }
        $sql = <<<SQL
DELETE 
FROM `client_special_needs`
WHERE `id` IN (?)
SQL;
        $this
            ->connection
            ->executeQuery($sql, [$ids], [Connection::PARAM_INT_ARRAY]);
    }
}
