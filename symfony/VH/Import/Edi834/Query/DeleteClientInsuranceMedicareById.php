<?php
/**
 * DeleteClientInsuranceMedicareById class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Query;

use Doctrine\DBAL\Connection;

/**
 * Class DeleteClientInsuranceMedicareById
 * @package VirtualHealth\Import\Membership\Edi834\Query
 */
class DeleteClientInsuranceMedicareById
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * DeleteClientInsuranceMedicareById constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $ids
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function write(array $ids): bool
    {
        if (empty($ids)) {
            return true;
        }
        $sql = <<<SQL
DELETE
FROM `client_insurance_medicare`
WHERE `id` IN (?)
SQL;
        return $this->connection
            ->executeQuery($sql, [$ids], [Connection::PARAM_STR_ARRAY])
            ->execute();
    }
}
