<?php
/**
 * GetWaiverNamesAndIds class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Query;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\PDOStatement;

/**
 * Class GetWaiverNamesAndIds
 * @package VirtualHealth\Import\Membership\Edi834\Query
 */
class GetWaiverNamesAndIds
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * GetWaiverNamesAndIds constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return array
     */
    public function read(): array
    {
        /** @var PDOStatement $statement */
        $statement = $this->connection
            ->createQueryBuilder()
            ->select(['name', 'id'])
            ->from('waiver')
            ->execute();

        return $statement->fetchAll(\PDO::FETCH_KEY_PAIR);
    }
}
