<?php
/**
 * VirtualHealth\Import\Membership\Edi834\Query\CohortBulkUpdate
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Query;


use Doctrine\DBAL\Connection;

/**
 * Class ClientCohortBulkUpdate
 * @package VirtualHealth\Import\Membership\Edi834\Query
 */
class ClientCohortBulkUpdate
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * ClientCohortBulkUpdate constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param int[][] $list Map of client to cohorts. See ClientCohortCollection::$list.
     * @throws \Doctrine\DBAL\DBALException
     */
    public function write(array $list): void
    {
        if (!$list) {
            return;
        }

        /** @var int[] $clientIds */
        $clientIds = array_keys($list);

        $qb = $this->connection->createQueryBuilder();

        // delete existed cohorts
        $qb->delete('client_cohort')
            ->where($qb->expr()->in('client_id', $clientIds))
            ->execute();

        $rows = '';

        // insert new cohorts
        foreach ($list as $clientId => $cohortIds) {

            if (!$cohortIds) {
                continue;
            }

            foreach ($cohortIds as $cohortId) {
                $rows .= ",({$clientId}, {$cohortId})";
            }
        }

        if (!$rows) {
            return;
        }

        $insertQuery = 'INSERT IGNORE INTO client_cohort (client_id, cohort_id) VALUES ' . ltrim($rows, ',');
        $this->connection->executeQuery($insertQuery);
    }
}
