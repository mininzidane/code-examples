<?php
/**
 * PotentialMergeClientsQuery class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Query;

use VirtualHealth\OrmBundle\Entity\Client;
use VirtualHealth\OrmBundle\Repository\ClientRepository;

class PotentialMergeClientsQuery
{
    /** @var ClientRepository */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Get clients those match potential merge logic conditions
     *
     * @param string $firstName
     * @param string $cityName
     * @param string $regionName
     * @param string $lastName
     * @param string $birthday
     * @param string $address1
     * @return Client[]
     */
    public function read(
        string $firstName,
        string $cityName,
        string $regionName,
        string $lastName,
        string $birthday,
        string $address1
    ): array
    {
        $qb = $this->clientRepository->createQueryBuilder('c');
        $qb->innerJoin('c.region', 'r');
        $qb->innerJoin('c.city', 'city');

        $firstNameCondition = $qb->expr()->eq('LOWER(vh_decrypt(c.firstName))', ':firstName');
        $lastNameCondition = $qb->expr()->eq('LOWER(vh_decrypt(c.lastName))', ':lastName');
        $cityCondition = $qb->expr()->eq('city.title', ':cityName');
        $birthDayCondition = $qb->expr()->eq('c.birthday', 'vh_encrypt(:birthday)');
        $addressCondition = $qb->expr()->eq('LOWER(vh_decrypt(c.address1))', ':address1');
        $stateCondition = $qb->expr()->eq('r.title', ':regionName');

        $firstCondition = $qb->expr()->andX(
            $firstNameCondition,
            $lastNameCondition,
            $cityCondition,
            $stateCondition,
            $birthDayCondition
        );
        $secondCondition = $qb->expr()->andX(
            $firstNameCondition,
            $addressCondition,
            $cityCondition,
            $stateCondition,
            $birthDayCondition
        );
        $thirdCondition = $qb->expr()->andX(
            $firstNameCondition,
            $lastNameCondition,
            $cityCondition,
            $stateCondition,
            $addressCondition
        );

        $qb->where($qb->expr()->like('c.idMedrec', ':idMedrec'));
        $qb->andWhere(
            $qb->expr()->orX(
                $firstCondition,
                $secondCondition,
                $thirdCondition
            )
        );
        $qb->setParameter('idMedrec', Client::TEMPORARY_MEMBER_NUMBER_PREFIX . '%');
        $qb->setParameter('firstName', \mb_strtolower($firstName));
        $qb->setParameter('cityName', $cityName);
        $qb->setParameter('regionName', $regionName);
        $qb->setParameter('lastName', \mb_strtolower($lastName));
        $qb->setParameter('birthday', $birthday);
        $qb->setParameter('address1', \mb_strtolower($address1));

        $clients = $qb
            ->getQuery()
            ->getResult();

        return $clients;
    }
}