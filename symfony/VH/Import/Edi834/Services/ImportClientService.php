<?php
/**
 * ImportClient class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Services;

use Doctrine\DBAL\Connection;
use Psr\Log\LoggerInterface;
use VirtualHealth\Import\Membership\Edi834\Collections\CareTeamHistoriesCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientCohortCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientGuardiansCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientInsuranceCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientMailingsCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientPoasCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientRacesCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientsCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientSpecialNeedsCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientStateDataCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\RecordsCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\UsersCollection;
use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;
use VirtualHealth\OrmBundle\Entity\Client;

/**
 * Class ImportClientService
 * @package VirtualHealth\Import\Membership\Edi834\Services
 */
class ImportClientService
{
    /**
     * @var ClientsCollection
     */
    private $clientsCollection;

    /**
     * @var ClientStateDataCollection
     */
    private $clientStateDataCollection;

    /**
     * @var UsersCollection
     */
    private $usersCollection;

    /**
     * @var RecordsMap
     */
    private $recordsMap;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /** @var
     * ClientMailingsCollection
     */
    private $clientMailingsCollection;

    /**
     * @var ClientGuardiansCollection
     */
    private $clientGuardiansCollection;

    /**
     * @var ClientPoasCollection
     */
    private $clientPoasCollection;

    /**
     * @var CareTeamHistoriesCollection
     */
    private $careTeamHistoriesCollection;

    /**
     * @var ClientRacesCollection
     */
    private $clientRacesCollection;

    /**
     * @var RecordsCollection
     */
    private $recordsCollection;

    /**
     * @var ClientSpecialNeedsCollection
     */
    private $clientSpecialNeedsCollection;

    /**
     * @var ClientCohortCollection
     */
    private $clientCohortCollection;

    /**
     * @var ClientInsuranceCollection
     */
    private $clientInsuranceCollection;

    /**
     * ImportClientService constructor.
     * @param Connection $connection
     * @param LoggerInterface $logger
     * @param RecordsMap $recordsMap
     * @param CitiesMap $citiesMap
     * @param RecordsCollection $recordsCollection
     * @param UsersCollection $usersCollection
     * @param ClientsCollection $clientsCollection
     * @param ClientStateDataCollection $clientStateDataCollection
     * @param ClientMailingsCollection $clientMailingsCollection
     * @param ClientInsuranceCollection $clientInsuranceCollection
     * @param ClientGuardiansCollection $clientGuardiansCollection
     * @param ClientPoasCollection $clientPoasCollection
     * @param CareTeamHistoriesCollection $careTeamHistoriesCollection
     * @param ClientRacesCollection $clientRacesCollection
     * @param ClientSpecialNeedsCollection $clientSpecialNeedsCollection
     * @param ClientCohortCollection $clientCohortCollection
     */
    public function __construct(
        Connection $connection,
        LoggerInterface $logger,
        RecordsMap $recordsMap,
        CitiesMap $citiesMap,
        RecordsCollection $recordsCollection,
        UsersCollection $usersCollection,
        ClientsCollection $clientsCollection,
        ClientStateDataCollection $clientStateDataCollection,
        ClientMailingsCollection $clientMailingsCollection,
        ClientInsuranceCollection $clientInsuranceCollection,
        ClientGuardiansCollection $clientGuardiansCollection,
        ClientPoasCollection $clientPoasCollection,
        CareTeamHistoriesCollection $careTeamHistoriesCollection,
        ClientRacesCollection $clientRacesCollection,
        ClientSpecialNeedsCollection $clientSpecialNeedsCollection,
        ClientCohortCollection $clientCohortCollection,
        string $dateFormat
    ) {
        $this->connection = $connection;
        $this->logger = $logger;
        $this->clientsCollection = $clientsCollection
            ->setDateFormat($dateFormat);
        $this->clientStateDataCollection = $clientStateDataCollection;
        $this->clientInsuranceCollection = $clientInsuranceCollection;
        $this->usersCollection = $usersCollection;
        $this->recordsMap = $recordsMap;
        $this->citiesMap = $citiesMap;
        $this->clientMailingsCollection = $clientMailingsCollection;
        $this->clientGuardiansCollection = $clientGuardiansCollection
            ->setDateFormat($dateFormat);
        $this->clientPoasCollection = $clientPoasCollection
            ->setDateFormat($dateFormat);
        $this->careTeamHistoriesCollection = $careTeamHistoriesCollection;
        $this->clientRacesCollection = $clientRacesCollection;
        $this->recordsCollection = $recordsCollection;
        $this->clientSpecialNeedsCollection = $clientSpecialNeedsCollection;
        $this->clientCohortCollection = $clientCohortCollection;
    }

    /**
     * @param Record $record
     * @return Client|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function createClient(Record $record)
    {
        $this->recordsCollection->add($record);
        $this->recordsMap->fillWithoutSearch($this->recordsCollection);
        return $this->saveData($this->recordsCollection);
    }

    /**
     * @param Record $record
     * @param int $clientId
     * @return Client|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function mergeClient(Record $record, $clientId)
    {
        $this->recordsCollection->add($record);
        $this->recordsMap->fillWithoutSearch($this->recordsCollection);
        $this->recordsMap->fillDataForMergedClient(0, $clientId);
        return $this->saveData($this->recordsCollection);
    }

    /**
     * @param RecordsCollection $recordsCollection
     * @return Client|null
     * @throws \Doctrine\DBAL\ConnectionException
     */
    private function saveData(RecordsCollection $recordsCollection)
    {
        // Create new empty log
        $insurancesLog = new AdapterParseLog();
        try {
            $this->connection->beginTransaction();
            $this->usersCollection->fill($recordsCollection, $this->recordsMap);
            $this->usersCollection->save();
            $this->clientsCollection->setInitiatorId(user()->id);
            $this->clientsCollection->fill($recordsCollection, $this->usersCollection, $this->recordsMap);
            $this->clientsCollection->save();
            $this->clientStateDataCollection->fill(
                 $this->recordsCollection,
                 $this->clientsCollection,
                 $this->recordsMap
            );
            $this->clientStateDataCollection->save();
            $this->clientMailingsCollection->fill($this->clientsCollection, $recordsCollection, $this->recordsMap);
            $this->clientMailingsCollection->save();
            $this->clientInsuranceCollection->handle(
                $this->clientsCollection,
                $recordsCollection,
                $this->recordsMap
            );
            $this->clientGuardiansCollection->fill($this->clientsCollection, $recordsCollection, $this->recordsMap);
            $this->clientGuardiansCollection->save();
            $this->clientPoasCollection->fill($this->clientsCollection, $recordsCollection, $this->recordsMap);
            $this->clientPoasCollection->save();
            $this->careTeamHistoriesCollection->fill(
                $this->clientsCollection,
                $this->clientGuardiansCollection,
                $this->clientPoasCollection
            );
            $this->careTeamHistoriesCollection->save();
            $this->clientRacesCollection->fill($this->clientsCollection, $recordsCollection, $this->recordsMap);
            $this->clientRacesCollection->clearExisting($this->clientsCollection, $recordsCollection);
            $this->clientRacesCollection->save();
            $this->clientSpecialNeedsCollection->fill($this->clientsCollection, $recordsCollection, $this->recordsMap);
            $this->clientSpecialNeedsCollection->save();
            $this->clientCohortCollection->fill(
                $this->clientsCollection,
                $this->recordsCollection
            );
            $this->clientCohortCollection->save();
            $this->connection->commit();

        } catch (\Exception $e) {
            $this->connection->rollBack();
            $this->logger->error($e->getMessage());
            return null;
        }

        if ($insurancesLog->hasErrors()) {
            $this->logger->error(json_encode($insurancesLog->getErrors()));
        }

        $recordsCollection->clear();
        return $this->clientsCollection->getList()[0];
    }
}
