<?php
/**
 * ClientData class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Services;

use VirtualHealth\Import\Membership\Edi834\InsurancePlan;
use VirtualHealth\Import\Membership\Edi834\Record;

class ClientDataService
{
    /**
     * Get formatted array of attributes and values grouped by models
     * @param Record $record
     * @return array
     */
    public function getData(Record $record)
    {
        $dateFormat = param('date');
        $result = [
            'client' => [
                'first_name' => $record->getSubscriberFirstName(),
                'middle_name' => $record->getSubscriberMiddleName(),
                'last_name' => $record->getSubscriberLastName(),
                'birthday' => $record->getDateOfBirth() ? $record->getDateOfBirth()->format($dateFormat) : null,
                'gender' => \Client::itemAlias('gender', $record->getSex() ?: \Client::GENDER_NONE),
                'language' => $record->getLanguage(),
                'racelist' => $record->getRace(),
                'ethnicity' => $record->getEthnicity(),
                'pcp' => $record->hasProviderNpi() ? $record->getProviderNpi() : (
                    $record->hasProviderId() ? $record->getProviderId() : null
                ),
                'email' => null,
                'phone' => $record->getHomePhone(),
                'address1' => $record->getHomeAddress1(),
                'address2' => $record->getHomeAddress2(),
                'region_id' => $record->getHomeState(),
                'city_id' => $record->getHomeCity(),
                'postal_code' => $record->getHomeZipCode(),
                'county_id' => $record->getHomeCounty(),
                'mobile' => $record->getMobilePhone(),
                'work_phone' => null,
                'temporary_address1' => null,
                'temporary_address2' => null,
                'temporary_city_id' => null,
                'temporary_state_id' => null,
                'temporary_zip_code' => null,
                'temporary_phone' => null,
            ],
            'mailing' => [
                'address1' => $record->getMailingAddress1(),
                'address2' => $record->getMailingAddress2(),
                'region_id' => $record->getMailingState(),
                'city_id' => $record->getMailingCity(),
                'zip_code' => $record->getMailingPostalCode(),
            ],
        ];

        $this->addInsurancePlans($result, $record);

        return $result;
    }

    /**
     * Looks for Insurance Plans in record and adds data to result
     *
     * @param array $result
     * @param Record $record
     */
    private function addInsurancePlans(&$result, $record)
    {
        $dateFormat = param('date');

        // One record can have Medicare OR Medicaid plans
        if ($record->hasMedicare()) {
            $planName = 'insuranceMedicare';
        } elseif ($record->hasMedicaid()) {
            $planName = 'insuranceMedicaid';
        } else {
            return;
        }
        
        $insurancePlans = $record->getPlans();
        foreach ($insurancePlans as $insurancePlan) {
            switch ($insurancePlan->getPeriod()) {
                case InsurancePlan::PERIOD_PRESENT:
                    // Do nothing, $planName stays the same
                    break;
                case InsurancePlan::PERIOD_FUTURE:
                    $planName .= 'Future';
                    break;
                case InsurancePlan::PERIOD_PAST:
                    $planName .= 'Past';
                    break;
                default:
                    // Could not define the period, skip this plan
                    continue 2;
            }

            $result[$planName] = [
                'enrollment_date' => $insurancePlan->getEnrollmentDate() ? $insurancePlan->getEnrollmentDate()->format($dateFormat) : null,
                'termination_date' => $insurancePlan->getTerminationDate()? $insurancePlan->getTerminationDate()->format($dateFormat) : null,
                'plan_id' => $insurancePlan->getId(),
            ];
        }

    }
}