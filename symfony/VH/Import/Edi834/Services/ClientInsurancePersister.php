<?php
/**
 * ClientInsuranceRemover class file
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Services;

use VirtualHealth\Import\Membership\Edi834\Query\DeleteClientInsuranceMedicaidById;
use VirtualHealth\Import\Membership\Edi834\Query\DeleteClientInsuranceMedicaidFutureById;
use VirtualHealth\Import\Membership\Edi834\Query\DeleteClientInsuranceMedicareById;
use VirtualHealth\Import\Membership\Edi834\Query\DeleteClientInsuranceMedicareFutureById;
use VirtualHealth\Import\Membership\Edi834\Query\DeleteEnrollmentHistoryById;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicaid;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicaidFuture;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicare;
use VirtualHealth\OrmBundle\Entity\ClientInsuranceMedicareFuture;
use VirtualHealth\OrmBundle\Entity\EnrollmentHistory;
use VirtualHealth\OrmBundle\Saver\Client\ClientInsuranceMedicaidFutureSaver;
use VirtualHealth\OrmBundle\Saver\Client\ClientInsuranceMedicaidSaver;
use VirtualHealth\OrmBundle\Saver\Client\ClientInsuranceMedicareFutureSaver;
use VirtualHealth\OrmBundle\Saver\Client\ClientInsuranceMedicareSaver;
use VirtualHealth\OrmBundle\Saver\Client\EnrollmentHistorySaver;
use VirtualHealth\OrmBundle\Saver\SaverException;

/**
 * Class ClientInsurancePersister
 * @package VirtualHealth\Import\Membership\Edi834\Services;
 */
class ClientInsurancePersister
{
    //models to delete
    /**
     * @var array
     */
    private $pastPlansToDelete = [];
    /**
     * @var array
     */
    private $presentMedicareToDelete = [];
    /**
     * @var array
     */
    private $presentMedicaidToDelete = [];
    /**
     * @var array
     */
    private $futureMedicareToDelete = [];

    /**
     * @var array
     */
    private $futureMedicaidToDelete = [];

    //models for upsert
    /**
     * @var array
     */
    private $pastPlansForUpsert = [];

    /**
     * @var array
     */
    private $presentMedicareForUpsert = [];

    /**
     * @var array
     */
    private $presentMedicaidForUpsert = [];

    /**
     * @var array
     */
    private $futureMedicareForUpsert = [];

    /**
     * @var array
     */
    private $futureMedicaidForUpsert = [];

    /**
     * @var DeleteEnrollmentHistoryById
     */
    private $deleteEnrollmentHistoryQuery;

    /**
     * @var DeleteClientInsuranceMedicareById
     */
    private $deleteMedicareQuery;

    /**
     * @var DeleteClientInsuranceMedicaidFutureById
     */
    private $deleteMedicareFutureQuery;

    /**
     * @var DeleteClientInsuranceMedicaidById
     */
    private $deleteMedicaidQuery;

    /**
     * @var DeleteClientInsuranceMedicaidFutureById
     */
    private $deleteMedicaidFutureQuery;

    /**
     * @var EnrollmentHistorySaver
     */
    private $enrollmentHistorySaver;

    /**
     * @var ClientInsuranceMedicaidSaver
     */
    private $medicaidSaver;

    /**
     * @var ClientInsuranceMedicareSaver
     */
    private $medicareSaver;

    /**
     * @var ClientInsuranceMedicaidFutureSaver
     */
    private $futureMedicaidSaver;

    /**
     * @var ClientInsuranceMedicareFutureSaver
     */
    private $futureMedicareSaver;

    /**
     * ClientInsurancePersister constructor.
     * @param DeleteEnrollmentHistoryById $deleteEnrollmentHistoryQuery
     * @param DeleteClientInsuranceMedicareById $deleteMedicareQuery
     * @param DeleteClientInsuranceMedicareFutureById $deleteMedicareFutureQuery
     * @param DeleteClientInsuranceMedicaidById $deleteMedicaidQuery
     * @param DeleteClientInsuranceMedicaidFutureById $deleteMedicaidFutureQuery
     * @param EnrollmentHistorySaver $enrollmentHistorySaver
     * @param ClientInsuranceMedicaidSaver $medicaidSaver
     * @param ClientInsuranceMedicaidFutureSaver $futureMedicaidSaver
     * @param ClientInsuranceMedicareSaver $medicareSaver
     * @param ClientInsuranceMedicareFutureSaver $futureMedicareSaver
     */
    public function __construct(
        DeleteEnrollmentHistoryById $deleteEnrollmentHistoryQuery,
        DeleteClientInsuranceMedicareById $deleteMedicareQuery,
        DeleteClientInsuranceMedicareFutureById $deleteMedicareFutureQuery,
        DeleteClientInsuranceMedicaidById $deleteMedicaidQuery,
        DeleteClientInsuranceMedicaidFutureById $deleteMedicaidFutureQuery,
        EnrollmentHistorySaver $enrollmentHistorySaver,
        ClientInsuranceMedicaidSaver $medicaidSaver,
        ClientInsuranceMedicaidFutureSaver $futureMedicaidSaver,
        ClientInsuranceMedicareSaver $medicareSaver,
        ClientInsuranceMedicareFutureSaver $futureMedicareSaver
    ) {
        $this->deleteEnrollmentHistoryQuery = $deleteEnrollmentHistoryQuery;
        $this->deleteMedicareQuery = $deleteMedicareQuery;
        $this->deleteMedicareFutureQuery = $deleteMedicareFutureQuery;
        $this->deleteMedicaidQuery = $deleteMedicaidQuery;
        $this->deleteMedicaidFutureQuery = $deleteMedicaidFutureQuery;

        $this->enrollmentHistorySaver = $enrollmentHistorySaver;
        $this->medicaidSaver = $medicaidSaver;
        $this->medicareSaver = $medicareSaver;
        $this->futureMedicaidSaver = $futureMedicaidSaver;
        $this->futureMedicareSaver = $futureMedicareSaver;
    }


    /**
     * add entity into pool for upsert
     *
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $entity
     *
     * @return void
     */
    public function upsert($entity)
    {
        if ($entity instanceof EnrollmentHistory) {
            $this->pastPlansForUpsert[] = $entity;
        } elseif ($entity instanceof ClientInsuranceMedicaid) {
            $this->presentMedicaidForUpsert[$entity->getClient()->getUserId()] = $entity;
        } elseif ($entity instanceof ClientInsuranceMedicare) {
            $this->presentMedicareForUpsert[$entity->getClient()->getUserId()] = $entity;
        } elseif ($entity instanceof ClientInsuranceMedicaidFuture) {
            $this->futureMedicaidForUpsert[$entity->getClient()->getUserId()] = $entity;
        } elseif ($entity instanceof ClientInsuranceMedicareFuture) {
            $this->futureMedicareForUpsert[$entity->getClient()->getUserId()] = $entity;
        } else {
            throw new \RuntimeException('Unknown type of client insurance');
        }
    }

    /**
     * add entity into pool for delete
     *
     * @param EnrollmentHistory|ClientInsuranceMedicare|ClientInsuranceMedicaid|ClientInsuranceMedicareFuture|ClientInsuranceMedicaidFuture $entity
     *
     * @return void
     */
    public function markForRemove($entity)
    {
        $id = $entity->getId();
        if (!$id) {
            return;
        }

        if ($entity instanceof EnrollmentHistory) {
            array_push($this->pastPlansToDelete, $id);
        } elseif ($entity instanceof ClientInsuranceMedicaid) {
            array_push($this->presentMedicaidToDelete, $id);
        } elseif ($entity instanceof ClientInsuranceMedicare) {
            array_push($this->presentMedicareToDelete, $id);
        } elseif ($entity instanceof ClientInsuranceMedicaidFuture) {
            array_push($this->futureMedicaidToDelete, $id);
        } elseif ($entity instanceof ClientInsuranceMedicareFuture) {
            array_push($this->futureMedicareToDelete, $id);
        } else {
            throw new \RuntimeException('Unknown type of client\'s insurance plan');
        }
    }

    /**
     * Save changes
     *
     * @return void
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \CDbException
     * @throws SaverException
     */
    public function save()
    {
        //delete
        $this->deleteEnrollmentHistoryQuery->write($this->pastPlansToDelete);
        $this->deleteMedicaidQuery->write($this->presentMedicaidToDelete);
        $this->deleteMedicareQuery->write($this->presentMedicareToDelete);
        $this->deleteMedicaidFutureQuery->write($this->futureMedicaidToDelete);
        $this->deleteMedicareFutureQuery->write($this->futureMedicareToDelete);

        //upsert
        $this->enrollmentHistorySaver->saveAll($this->pastPlansForUpsert);
        $this->medicaidSaver->saveAll($this->presentMedicaidForUpsert);
        $this->medicareSaver->saveAll($this->presentMedicareForUpsert);
        $this->futureMedicaidSaver->saveAll($this->futureMedicaidForUpsert);
        $this->futureMedicareSaver->saveAll($this->futureMedicareForUpsert);

        $this->cleanState();
    }

    /**
     * Cleans state
     *
     * @return void
     */
    private function cleanState()
    {
        $this->pastPlansToDelete = [];
        $this->pastPlansForUpsert = [];

        $this->presentMedicaidToDelete = [];
        $this->presentMedicaidForUpsert = [];

        $this->presentMedicareToDelete = [];
        $this->presentMedicareForUpsert = [];

        $this->futureMedicaidToDelete = [];
        $this->futureMedicaidForUpsert = [];

        $this->futureMedicareToDelete = [];
        $this->futureMedicareForUpsert = [];
    }
}