<?php
/**
 * AutomationRemoteFilesFinder class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

declare(strict_types=1);

namespace VirtualHealth\Import\Membership\Edi834\Services;

use League\Flysystem\FilesystemInterface;
use VirtualHealth\Import\Membership\Edi834\CommandBus\DownloadAndProcessEdi834FileCommand;
use VirtualHealth\OrmBundle\Repository\LobRepository;
use VirtualHealth\OrmBundle\Repository\MembershipFileRepository;
use VirtualHealth\OrmBundle\Entity\Lob;

/**
 * Class AutomationRemoteFilesFinder
 * @package VirtualHealth\Import\Membership\Edi834\Services
 */
class AutomationRemoteFilesFinder
{
    /**
     * @var string
     */
    private $lobListPattern;

    /**
     * @var FilesystemInterface
     */
    private $remoteFilesystem;
    /**
     * @var MembershipFileRepository
     */
    private $membershipFileRepository;
    /**
     * @var LobRepository
     */
    private $lobRepository;

    public function __construct(
        MembershipFileRepository $membershipFileRepository,
        LobRepository $lobRepository,
        FilesystemInterface $remoteFilesystem
    )
    {

        $this->remoteFilesystem = $remoteFilesystem;
        $this->membershipFileRepository = $membershipFileRepository;
        $this->lobRepository = $lobRepository;
    }

    /**
     * @param string $mode
     * @param \DateTime $date
     * @param int $ownerId
     * @param bool $excludeExistingFiles
     * @return array
     */
    public function getRemoteFilesPathsForDate(string $mode, \DateTime $date, int $ownerId, $excludeExistingFiles = true)
    {
        $fileNameList = $filePathList = [];

        foreach ($this->remoteFilesystem->listContents() as $file) {
            if ($this->isRightFileName($mode, $date, $file['basename'])) {
                $fileNameList[] = $file['basename'];
                $filePathList[$file['basename']] = $file['path'];
            }
        }

        if ($excludeExistingFiles && !empty($fileNameList)) {
            $fileList = $this->membershipFileRepository->findBy(['filename' => $fileNameList, 'author' => $ownerId]);

            foreach ($fileList as $file) {
                unset($filePathList[$file->getFilename()]);
            }
        }

        return $filePathList;
    }

    /**
     * @param string $mode
     * @param \DateTime $date
     * @param string $fileName
     * @return bool
     */
    private function isRightFileName(string $mode, \DateTime $date, string $fileName): bool
    {
        $lobPattern = $this->getLobListPattern();

        if ($mode == DownloadAndProcessEdi834FileCommand::MODE_CHANGE) {
            $fileChangePattern = "/^Enroll834_VIRTUAL_?HEALTH_{$lobPattern}_CHANGE_{$date->format('Ymd')}\d+\.txt$/";

            return (bool)preg_match($fileChangePattern, $fileName);
        } elseif ($mode == DownloadAndProcessEdi834FileCommand::MODE_AUDIT) {
            $fileAuditPattern = "/^Enroll834_VIRTUAL_?HEALTH_{$lobPattern}_AUDIT_(\d{12})\.txt$/";

            if (preg_match($fileAuditPattern, $fileName, $matches)) {
                $fileDate = \DateTime::createFromFormat('YmdHi', $matches[2]);
                if (!$fileDate) {
                    return false;
                }

                return $fileDate->getTimestamp() - $date->getTimestamp() >= 0;
            }

        }
        return false;
    }

    /**
     * @return string
     */
    private function getLobListPattern(): string
    {
        if ($this->lobListPattern === null) {
            $lobList = $this->lobRepository->findAll();

            if (empty($lobList)) {
                $this->lobListPattern = '';
                return $this->lobListPattern;
            }

            $lobNameList = array_map(function (Lob $lob) {
                return preg_quote($lob->getName(), '/');
            }, $lobList);

            $this->lobListPattern = '(' . implode('|', $lobNameList) . ')';
        }

        return $this->lobListPattern;
    }
}
