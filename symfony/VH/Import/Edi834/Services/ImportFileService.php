<?php
/**
 * ImportFileService class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Services;

use VirtualHealth\Import\Membership\Edi834\Collections\CareTeamHistoriesCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientCohortCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientGuardiansCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientInsuranceCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientMailingsCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientPoasCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientRacesCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientsCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientSpecialNeedsCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\ClientStateDataCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\RecordsCollection;
use VirtualHealth\Import\Membership\Edi834\Collections\UsersCollection;
use VirtualHealth\Import\Membership\Edi834\Exception\ImportFileException;
use VirtualHealth\Import\Membership\Edi834\File;
use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\Import\Membership\Edi834\Maps\RecordsMap;
use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLogFile;
use VirtualHealth\Import\Membership\Edi834\ParseLog\ParseLogPartsJsonCsvConsolidator;
use VirtualHealth\Import\Membership\Edi834\RecordParser;
use VirtualHealth\OrmBundle\Entity\MembershipFile;

/**
 * Class ImportFileService
 * @package VirtualHealth\Import\Membership\Edi834\Services
 */
class ImportFileService
{
    /**
     * @var RecordsMap
     */
    private $recordsMap;

    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /**
     * @var RecordsCollection
     */
    private $recordsCollection;

    /**
     * @var UsersCollection
     */
    private $usersCollection;

    /**
     * @var ClientsCollection
     */
    private $clientsCollection;

    /**
     * @var ClientStateDataCollection
     */
    private $clientStateDataCollection;

    /**
     * @var ClientMailingsCollection
     */
    private $clientMailingsCollection;

    /**
     * @var ClientInsuranceCollection
     */
    private $clientInsuranceCollection;

    /**
     * @var ClientGuardiansCollection
     */
    private $clientGuardiansCollection;

    /**
     * @var ClientPoasCollection
     */
    private $clientPoasCollection;

    /**
     * @var CareTeamHistoriesCollection
     */
    private $careTeamHistoriesCollection;

    /**
     * @var ClientRacesCollection
     */
    private $clientRacesCollection;

    /**
     * @var ClientSpecialNeedsCollection
     */
    private $clientSpecialNeedsCollection;

    /**
     * @var ClientCohortCollection
     */
    private $clientCohortCollection;

    /**
     * @var RecordParser
     */
    private $recordsParser;

    /**
     * ImportFilePartCommandHandler constructor.
     * @param RecordsMap $recordsMap
     * @param CitiesMap $citiesMap
     * @param RecordsCollection $recordsCollection
     * @param UsersCollection $usersCollection
     * @param ClientsCollection $clientsCollection
     * @param ClientStateDataCollection $clientStateDataCollection
     * @param ClientMailingsCollection $clientMailingsCollection
     * @param ClientInsuranceCollection $clientInsuranceCollection
     * @param ClientGuardiansCollection $clientGuardiansCollection
     * @param ClientPoasCollection $clientPoasCollection
     * @param CareTeamHistoriesCollection $careTeamHistoriesCollection
     * @param ClientRacesCollection $clientRacesCollection
     * @param ClientSpecialNeedsCollection $clientSpecialNeedsCollection
     * @param ClientCohortCollection $clientCohortCollection
     * @param RecordParser $recordsParser
     */
    public function __construct(
        RecordsMap $recordsMap,
        CitiesMap $citiesMap,
        RecordsCollection $recordsCollection,
        UsersCollection $usersCollection,
        ClientsCollection $clientsCollection,
        ClientStateDataCollection $clientStateDataCollection,
        ClientMailingsCollection $clientMailingsCollection,
        ClientInsuranceCollection $clientInsuranceCollection,
        ClientGuardiansCollection $clientGuardiansCollection,
        ClientPoasCollection $clientPoasCollection,
        CareTeamHistoriesCollection $careTeamHistoriesCollection,
        ClientRacesCollection $clientRacesCollection,
        ClientSpecialNeedsCollection $clientSpecialNeedsCollection,
        ClientCohortCollection $clientCohortCollection,
        RecordParser $recordsParser
    ) {
        $this->recordsMap = $recordsMap;
        $this->citiesMap = $citiesMap;
        $this->recordsCollection = $recordsCollection;
        $this->usersCollection = $usersCollection;
        $this->clientsCollection = $clientsCollection
            ->setDateFormat(param('date'));
        $this->clientStateDataCollection = $clientStateDataCollection;
        $this->clientMailingsCollection = $clientMailingsCollection;
        $this->clientInsuranceCollection = $clientInsuranceCollection;
        $this->clientGuardiansCollection = $clientGuardiansCollection
            ->setDateFormat(param('date'));
        $this->clientPoasCollection = $clientPoasCollection
            ->setDateFormat(param('date'));
        $this->careTeamHistoriesCollection = $careTeamHistoriesCollection;
        $this->clientRacesCollection = $clientRacesCollection;
        $this->clientSpecialNeedsCollection = $clientSpecialNeedsCollection;
        $this->clientCohortCollection = $clientCohortCollection;
        $this->recordsParser = $recordsParser;
    }

    /**
     * Processes file batch. Performs all DB writes but without COMMIT.
     *
     * @param File $file
     * @param MembershipFile $fileEntity
     * @param int $batchSize
     * @param string|null $idMedrec
     * @param ParseLogPartsJsonCsvConsolidator $parseLogConsolidator
     * @return string|null
     * @throws \CDbException
     * @throws \CException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     * @throws \VirtualHealth\OrmBundle\Saver\SaverException
     */
    public function processBatch(
        File $file,
        MembershipFile $fileEntity,
        int $batchSize,
        ?string $idMedrec,
        ParseLogPartsJsonCsvConsolidator $parseLogConsolidator
    ): ?string
    {
        if (!$file->valid()) {
            throw new ImportFileException(sprintf('Invalid file: %s', $file->getFilePath()));
        }

        if ($batchSize < 1) {
            throw new ImportFileException(sprintf('Invalid batch size: %s', $batchSize));
        }

        $this->recordsCollection->clear();
        $parseLogFile = new AdapterParseLogFile($file->getFilePath());
        $processedLine = $fileEntity->getLastProcessedLine();
        // read the batch
        while ($file->valid() && $this->recordsCollection->getSize() !== $batchSize) {
            $blobRecord = $file->current();
            $parseLogFile->getParseLog()->incrementTotal();
            $file->next();
            // parsing
            $record = $this->recordsParser->parseRecord($blobRecord);
            $record->setProcessedLine(++$processedLine);
            $this->recordsCollection->add($record);
        }
        $this->recordsCollection->validate($parseLogFile->getParseLog());
        $this->recordsMap->fill($this->recordsCollection, $parseLogFile->getParseLog());
        $parseLogFile->getParseLog()
            ->incrementTotalCreated(\count($this->recordsMap->getNotFoundKeys()));
        $parseLogFile->getParseLog()
            ->incrementTotalUpdated(
                $this->recordsCollection->getSize() - count($this->recordsMap->getNotFoundKeys())
            );
        // save data
        $this->usersCollection->fill($this->recordsCollection, $this->recordsMap);
        $this->usersCollection->save();
        $this->clientsCollection
            ->setMembershipFileId($fileEntity->getId())
            ->setLastIdMedrec($idMedrec);
        $this->clientsCollection->fill($this->recordsCollection, $this->usersCollection, $this->recordsMap);
        $this->clientsCollection->save();
        $this->clientStateDataCollection->fill(
            $this->recordsCollection,
            $this->clientsCollection,
            $this->recordsMap
        );
        $this->clientStateDataCollection->save();
        $this->clientMailingsCollection->fill(
            $this->clientsCollection,
            $this->recordsCollection,
            $this->recordsMap
        );
        $this->clientMailingsCollection->save();
        $this->clientInsuranceCollection->handle(
            $this->clientsCollection,
            $this->recordsCollection,
            $this->recordsMap,
            $parseLogFile->getParseLog()
        );
        $this->clientGuardiansCollection->fill(
            $this->clientsCollection,
            $this->recordsCollection,
            $this->recordsMap
        );
        $this->clientGuardiansCollection->save();
        $this->clientPoasCollection->fill(
            $this->clientsCollection,
            $this->recordsCollection,
            $this->recordsMap
        );
        $this->clientPoasCollection->save();
        $this->careTeamHistoriesCollection->fill(
            $this->clientsCollection,
            $this->clientGuardiansCollection,
            $this->clientPoasCollection
        );
        $this->careTeamHistoriesCollection->save();
        $this->clientRacesCollection->fill(
            $this->clientsCollection,
            $this->recordsCollection,
            $this->recordsMap
        );
        $this->clientRacesCollection->clearExisting(
            $this->clientsCollection,
            $this->recordsCollection
        );
        $this->clientRacesCollection->save();
        $this->clientSpecialNeedsCollection->fill(
            $this->clientsCollection,
            $this->recordsCollection,
            $this->recordsMap
        );
        $this->clientSpecialNeedsCollection->save();
        $this->clientCohortCollection->fill(
            $this->clientsCollection,
            $this->clientInsuranceCollection
        );
        $this->clientCohortCollection->save();
        // update file meta
        $fileEntity
            ->setFileOffset($file->getFilePosition())
            ->setLastProcessedLine($processedLine)
            ->setInProgress($file->valid());

        $partNumber = ceil($processedLine / $batchSize) - 1;

        $parseLogFile->save($partNumber);
        \Yii::getLogger()->flush(true);

        $parseLogConsolidator->addPart($partNumber);

        return $this->clientsCollection->getLastIdMedrec();
    }
}
