<?php
/**
 * RaceEthnicityGender class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class RaceEthnicityGender
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
class RaceEthnicityGender extends BaseValidator implements Validator
{
    /**
     * @inheritDoc
     */
    public function validate(Record $record, AdapterParseLog $parseLog)
    {
        if (empty($record->getRace()) && empty($record->getEthnicity())) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Invalid Race or Ethnicity Code value%s.',
                    $this->buildSubscriberName($record)
                )
            );
        }
        if (empty($record->getSex())) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Invalid Gender Code value%s.',
                    $this->buildSubscriberName($record)
                )
            );
        }
        return true;
    }
}
