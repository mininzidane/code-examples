<?php
/**
 * SubscriberId class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class SubscriberId
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
class SubscriberId extends BaseValidator implements Validator
{
    /**
     * @inheritDoc
     */
    public function validate(Record $record, AdapterParseLog $parseLog)
    {
        if (mb_strpos($record->getSubscriberId(), '`') !== false) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Invalid Subscriber Id value %s.',
                    $record->getSubscriberId()
                ),
                true
            );
            return false;
        }
        return true;
    }
}
