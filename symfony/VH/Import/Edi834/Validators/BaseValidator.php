<?php
/**
 * BaseValidator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class BaseValidator
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
class BaseValidator
{
    /**
     * @param Record $record
     * @param string $prefix
     * @param string $suffix
     * @return string
     */
    public function buildSubscriberName(Record $record, $prefix = ' for ', $suffix = '')
    {
        $pieces = [];
        if (!empty($record->getSubscriberLastName())) {
            $pieces[] = $record->getSubscriberLastName();
        }
        if (!empty($record->getSubscriberFirstName())) {
            $pieces[] = $record->getSubscriberFirstName();
        }
        if (count($pieces) == 0) {
            return '';
        }
        return $prefix . implode(', ', $pieces) . $suffix;
    }
}
