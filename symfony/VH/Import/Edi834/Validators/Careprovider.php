<?php
/**
 * Careprovider class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\Maps\CareprovidersMap;
use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class Careprovider
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
class Careprovider extends BaseValidator implements Validator
{
    /**
     * @var CareprovidersMap
     */
    private $careprovidersMap;

    /**
     * Careprovider constructor.
     * @param CareprovidersMap $careprovidersMap
     */
    public function __construct(CareprovidersMap $careprovidersMap)
    {
        $this->careprovidersMap = $careprovidersMap;
    }

    /**
     * @inheritDoc
     */
    public function validate(Record $record, AdapterParseLog $parseLog)
    {
        if ($this->careprovidersMap->getByRecord($record) === null) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'PCP identifier could not be matched%s.',
                    $this->buildSubscriberName($record)
                )
            );
        }
        return true;
    }
}
