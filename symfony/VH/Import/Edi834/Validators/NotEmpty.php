<?php
/**
 * NotEmpty class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class NotEmpty
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
class NotEmpty extends BaseValidator implements Validator
{
    /**
     * @inheritDoc
     */
    public function validate(Record $record, AdapterParseLog $parseLog)
    {
        if (empty($record->getSequentialMemberNumber())) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Missing %s%s.',
                    'Sequential Member ID',
                    $this->buildSubscriberName($record)
                )
            );
        }
        $isSpanIdValid = !empty($record->getPlans());
        foreach ($record->getPlans() as $plan) {
            $isSpanIdValid = $isSpanIdValid && !empty($plan->getSpanId());
        }
        if (!$isSpanIdValid) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Missing %s%s.',
                    'Unique Span Identifier',
                    $this->buildSubscriberName($record)
                )
            );
        }
        return true;
    }
}
