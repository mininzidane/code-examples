<?php
/**
 * Language class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\Maps\LanguagesMap;
use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

class Language extends BaseValidator implements Validator
{
    /**
     * @var LanguagesMap
     */
    private $languagesMap;

    /**
     * Language constructor.
     * @param LanguagesMap $languagesMap
     */
    public function __construct(LanguagesMap $languagesMap)
    {
        $this->languagesMap = $languagesMap;
    }

    /**
     * @inheritDoc
     */
    public function validate(Record $record, AdapterParseLog $parseLog)
    {
        if ($this->languagesMap->getByCode($record->getLanguage()) === null) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Invalid Member Language Code value%s.',
                    $this->buildSubscriberName($record)
                )
            );
        }
        return true;
    }
}
