<?php
/**
 * Waiver class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2019 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\Maps\WaiversMap;
use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class Waiver
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
class Waiver extends BaseValidator implements Validator
{
    /**
     * @var WaiversMap
     */
    private $waiversMap;

    /**
     * Waiver constructor.
     * @param WaiversMap $waiversMap
     */
    public function __construct(WaiversMap $waiversMap)
    {
        $this->waiversMap = $waiversMap;
    }

    /**
     * @inheritDoc
     */
    public function validate(Record $record, AdapterParseLog $parseLog)
    {
        foreach ($record->getPlans() as $plan) {
            if (
                !empty($plan->getWaiver())
                && $this->waiversMap->getIdByCodeOrName($plan->getWaiver()) === null
            ) {
                $parseLog->addError(
                    $record->getProcessedLine(),
                    $record->getSubscriberId(),
                    sprintf(
                        'Invalid Waiver Code or Name for %s: %s',
                        $this->buildSubscriberName($record, ' for "', '"'),
                        $plan->getWaiver()
                    )
                );
            }
        }

        return true;
    }
}
