<?php
/**
 * City class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\Maps\CitiesMap;
use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class City
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
class City extends BaseValidator implements Validator
{
    /**
     * @var CitiesMap
     */
    private $citiesMap;

    /** @var bool $attemptAddressAnyway */
    private $attemptAddressAnyway;

    /**
     * City constructor.
     * @param CitiesMap $citiesMap
     * @param bool $attemptAddressAnyway
     */
    public function __construct(CitiesMap $citiesMap, bool $attemptAddressAnyway)
    {
        $this->citiesMap = $citiesMap;
        $this->attemptAddressAnyway = $attemptAddressAnyway;
    }

    /**
     * @inheritDoc
     */
    public function validate(Record $record, AdapterParseLog $parseLog)
    {
        if (
            $record->getHomeCity() === 'Unknown'
            || $record->getHomeState() === 'Unknown'
            || (!empty($record->getHomeCity())
                && $this->citiesMap->getCity($record->getHomeCity(), $record->getHomeState()) === null
            )
        ) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Invalid Member Primary City value%s.',
                    $this->buildSubscriberName($record)
                )
            );
        }
        if (
            !empty($record->getMailingCity())
            && $this->citiesMap->getCity($record->getMailingCity(), $record->getMailingState()) === null
        ) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Invalid Member Mailing City value%s.',
                    $this->buildSubscriberName($record)
                )
            );
        }

        if ($this->attemptAddressAnyway) {
            if (empty($record->getHomeZipCode())) {
                $this->citiesMap->dump((string) $record->getSubscriberId());
                $parseLog->addError(
                    $record->getProcessedLine(),
                    $record->getSubscriberId(),
                    sprintf(
                        'Blank or Invalid Home Zip Code value%s.',
                        $this->buildSubscriberName($record)
                    )
                );
            }

            if (empty($record->getHomeState())) {
                $parseLog->addError(
                    $record->getProcessedLine(),
                    $record->getSubscriberId(),
                    sprintf(
                        'Blank or invalid Home State value%s.',
                        $this->buildSubscriberName($record)
                    )
                );
            }

            if (empty($record->getMailingPostalCode())) {
                $parseLog->addError(
                    $record->getProcessedLine(),
                    $record->getSubscriberId(),
                    sprintf(
                        'Blank or Invalid Mailing Zip Code value%s.',
                        $this->buildSubscriberName($record)
                    )
                );
            }

            if (empty($record->getMailingState())) {
                $parseLog->addError(
                    $record->getProcessedLine(),
                    $record->getSubscriberId(),
                    sprintf(
                        'Blank or invalid Mailing State value%s.',
                        $this->buildSubscriberName($record)
                    )
                );
            }
        }

        return true;
    }
}
