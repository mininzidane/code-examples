<?php
/**
 * Required class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class Required
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
class Required extends BaseValidator implements Validator
{
    private $requiredFieldMethods = [
        'Subscriber ID' => 'getSubscriberId',
        'Subscriber First Name' => 'getSubscriberFirstName',
        'Subscriber Last Name' => 'getSubscriberLastName',
        'Member Date of Birth' => 'getDateOfBirth',
        'Member Address Line 1' => 'getHomeAddress1',
    ];

    /**
     * @inheritDoc
     */
    public function validate(Record $record, AdapterParseLog $parseLog)
    {
        foreach ($this->requiredFieldMethods as $requiredFieldName => $requiredFieldMethod) {
            if (empty($record->{$requiredFieldMethod}())) {
                $parseLog->addError(
                    $record->getProcessedLine(),
                    $record->getSubscriberId(),
                    sprintf(
                        'Record%s could not be parsed: Blank %s field',
                        $this->buildSubscriberName($record, ' for "', '"'),
                        $requiredFieldName
                    ),
                    true
                );
                return false;
            }
        }
        return true;
    }
}
