<?php
/**
 * Validator class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Interface Validator
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
interface Validator
{
    /**
     * Validate a record.
     * @param Record $record
     * @param AdapterParseLog $parseLog
     * @return bool whether the record hasn't critical errors
     */
    public function validate(Record $record, AdapterParseLog $parseLog);
}
