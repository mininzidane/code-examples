<?php
/**
 * InsuranceNumber class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2018 Virtual Frameworks LLC
 */

namespace VirtualHealth\Import\Membership\Edi834\Validators;

use VirtualHealth\Import\Membership\Edi834\ParseLog\AdapterParseLog;
use VirtualHealth\Import\Membership\Edi834\Record;

/**
 * Class InsuranceNumber
 * @package VirtualHealth\Import\Membership\Edi834\Validators
 */
class InsuranceNumber extends BaseValidator implements Validator
{
    /**
     * @inheritDoc
     */
    public function validate(Record $record, AdapterParseLog $parseLog)
    {
        if (
            empty($record->getMedicaidSubscriberNumber())
            && empty($record->getMedicareSubscriberNumber())
        ) {
            $parseLog->addError(
                $record->getProcessedLine(),
                $record->getSubscriberId(),
                sprintf(
                    'Record%s could not be parsed: Blank Medicaid Number and Medicare Number fields',
                    $this->buildSubscriberName($record, ' for "', '"')
                ),
                true
            );
            return false;
        }
        return true;
    }
}
